import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from '@app/core';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { FlyoutModule } from 'ngx-flyout';
import { WindowWidthService } from '@app/shared/services/windowWidth.service';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { RegistrationService } from './services/registration.service';
import { ContentService } from './services/content.service';
import { AuthGuardService } from './services/auth-guard.service';
import { JoinModule } from './features/join/join.module';
import { ToastrModule } from 'ngx-toastr';
import { FAQModule } from './features/faq/faq.module';
import { NgxJsonLdModule } from '@ngx-lite/json-ld';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { DirectAccessGuardService } from './services/directaccess-guard.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';


@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule.withServerTransition({appId: 'universal-cli'}),
		BrowserAnimationsModule,
		FlyoutModule,
		FormsModule,
		AppRoutingModule,
		CoreModule,
		AngularFontAwesomeModule,
		BsDropdownModule.forRoot(),
		HttpModule,
		HttpClientModule,
		JoinModule,
		ToastrModule.forRoot({
		    timeOut: 10000,
		    positionClass: 'toast-top-right',
		    preventDuplicates: true,
		}),
		FAQModule,
		ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
		FontAwesomeModule
		
		
	],
	exports: [ToastrModule],
	providers: [
		WindowWidthService,RegistrationService,ContentService,AuthGuardService,DirectAccessGuardService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
