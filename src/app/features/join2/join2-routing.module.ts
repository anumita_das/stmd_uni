import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Join2Component } from './join2.component';
import { PaymentComponent } from './payment/payment.component';

const routes: Routes = [
	{ path: '', component: Join2Component },
	{ path: 'banner/:id',  component: Join2Component },
        { path: 'payment', component: PaymentComponent },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class Join2RoutingModule {

}
