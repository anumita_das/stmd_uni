import { NgModule } from '@angular/core';
//import { NumberOnlyDirective } from '../../number.directive';
import { Join2RoutingModule } from './join2-routing.module';
import { Join2Component } from './join2.component';
import { SharedModule } from '@app/shared';
import { HttpModule }    from '@angular/http';
import { FormsModule }   from '@angular/forms';
import { PasswordStrengthBarModule } from 'ng2-password-strength-bar';
import { PaymentComponent } from './payment/payment.component';

@NgModule({
	imports: [
		Join2RoutingModule,
		SharedModule,
		HttpModule,
		FormsModule,
		PasswordStrengthBarModule
	],
	declarations: [
            Join2Component,
            PaymentComponent,
	]
})
export class Join2Module {	

}
