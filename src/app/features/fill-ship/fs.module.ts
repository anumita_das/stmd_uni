import { NgModule } from '@angular/core';
import { FSRoutingModule } from './fs-routing.module';
import { FSComponent } from './fs.component';
import { SharedModule } from '@app/shared';
import { FSOrderFormComponent } from './fs-order-form/fs-order-form.component';
import { OverviewComponent } from './overview/overview.component';
import { ArchivedComponent } from './overview/archived.component';
import { ShowShipoutModalComponent } from './overview/showshipout-modal.component';
import { DeliverydetailsModalComponent } from './overview/deliverydetails-modal.component';
import { ExtracostComponent } from './overview/extracharge.component';
import { CartComponent } from './checkout/cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { PersonalDetailComponent } from './checkout/personal-detail/personal-detail.component';
import { ShippingAndPaymentComponent } from './checkout/shipping-and-payment/shipping-and-payment.component';
import { PlaceOrderComponent } from './checkout/place-order/place-order.component';

@NgModule({
	imports: [
		FSRoutingModule,
		SharedModule,
	],
	exports: [
		ShowShipoutModalComponent,
		DeliverydetailsModalComponent
	],
	declarations: [
		FSComponent,
		FSOrderFormComponent,
		CartComponent,
		CheckoutComponent,
		PersonalDetailComponent,
		ShippingAndPaymentComponent,
		PlaceOrderComponent,
		OverviewComponent,
		ArchivedComponent,
		ExtracostComponent,
		ShowShipoutModalComponent,
		DeliverydetailsModalComponent
	],
	entryComponents: [
		ShowShipoutModalComponent,
		DeliverydetailsModalComponent
	]
})
export class FSModule {

}
