import { Component, OnInit, HostListener } from '@angular/core';
import { RegistrationService } from '../../../services/registration.service';
import { Router  , ActivatedRoute } from '@angular/router';
import { NgForm }   from '@angular/forms';
import { AppGlobals } from '../../../app.globals';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { PaystackModalComponent } from '../../../shared/components/paystack-modal/paystack-modal.component';
import { PayeezyModalComponent } from '../../../shared/components/payeezy-modal/payeezy-modal.component';
import { ContentService } from '../../../services/content.service';
import { PayPalConfig, PayPalEnvironment, PayPalIntegrationType } from 'ngx-paypal';
import { FillshipService } from '../../../services/fillship.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-fillship-extracharge',
	templateUrl: './extracharge.component.html',
	styleUrls: ['./extracharge.component.scss'],
	providers: [AppGlobals]
})
export class ExtracostComponent implements OnInit {
	isMediumScreen: boolean;
	sidebar: boolean;
	isCollapsed = true;
	paymentMethodList: Array<any>;
	userId: number;
	paymentMethodId  : any;
	paymentMethodName : any;
	paymentMethodKey : any;
	shipmentMethodId : any;
	paymentTax : any = "0.00";
	paymentTaxType : any;
	paidAmount : any;
	ewalletTransferAmount : any;
    formSubmit:boolean = false;
    ewallet : any = '';
    ewalletMsg: string = '';
    ewalletReadonly:boolean = false; 
    ewalletId: any;
	payCardType: any = '';
	invoice: any;
	defaultCurrencySymbol : any;
	bsModalRef: BsModalRef;
	paypalipnConfig : any;
	payPalConfig: any;
	paypalTransaction : any = [];
	fillshipShipmentid : any;
	defaultCurrencyCode : any;
	totalCost: number;

	constructor(
		private  regService : RegistrationService,
		private router : ActivatedRoute,
		private route : Router,
		private _global: AppGlobals,
		private toastr: ToastrService,
		private modalService: BsModalService,
		private content : ContentService,
		public fillshipService : FillshipService,
		public spinner: NgxSpinnerService,
	) { 
		this.userId = this.regService.item.user.id;
		this.defaultCurrencySymbol = this._global.defaultCurrencySymbol;
	}

	ngOnInit() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		this.checkWindowSize(width);


		this.getPaymentMethodList();
		this.getDefaultCurrencyCode();
		
		this.fillshipService.currentExtracost.subscribe(data=>{
			this.paidAmount = data;
			localStorage.setItem('extraCostPaidAmount', JSON.stringify(this.paidAmount));
		});

		this.fillshipService.currentEwalletTransferAmount.subscribe(data=>{
			this.ewalletTransferAmount = data;
			localStorage.setItem('ewalletTransferAmount', JSON.stringify(this.ewalletTransferAmount));
		});
		
		this.fillshipShipmentid = this.router.snapshot.params['id'];
		//console.log(this.paidAmount);

		this.totalCost = parseFloat(this.paidAmount);
	}


	getPaymentMethodList() {
		const data = {userId : this.userId};
		this.regService.getPaymentMethodList(data).subscribe((data:any) => {
			this.paymentMethodList = data;
		});
	}

	getDefaultCurrencyCode() {
		
		this.regService.defaultcurrencycode().subscribe((data:any) => {
			this.defaultCurrencyCode = data.defaultcurrencyCode;
		});
	}

	radioClopsed = function (indx) {
		this.paymentMethodList.forEach(item => {
			item.collapsed = false;
		});
		this.paymentMethodList[indx].collapsed = !this.paymentMethodList[indx].collapsed;

	};

	changePaymentMethod($event, paymentMethodName, paymentMethodKey, paymentTax, paymentTaxType,formData) {
		this.paymentMethodName = paymentMethodName;
		this.paymentMethodKey = paymentMethodKey;
		//this.paymentTax = paymentTax;
		this.paymentTaxType = paymentTaxType;
		
		this.paidAmount = parseFloat(this.paidAmount)-parseFloat(this.paymentTax);
		this.paidAmount = parseFloat(this.paidAmount).toFixed(2);
		if(paymentTaxType == '0') // flat charge
		{
			this.paymentTax  = paymentTax;
		}
		else // percent charge
		{
			this.paymentTax = parseFloat(this.paidAmount)*(paymentTax/100);
		}
		this.paidAmount = parseFloat(this.paidAmount)+parseFloat(this.paymentTax);
		this.paidAmount = parseFloat(this.paidAmount).toFixed(2);

		if(paymentMethodKey == 'paypalstandard') {
			formData.value.paymentMethodName = this.paymentMethodName;
			formData.value.paymentMethodKey = this.paymentMethodKey;
			this.paypalConfig(formData);
		}
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	createRangeYear(number){
		var d = new Date();
	    var n = d.getFullYear();

		var expiryYear: number[] = [];
		for(var i = n; i <= n+20; i++){
		 	expiryYear.push(i);
		}
		return expiryYear;
	}

	onSearchChange(searchValue : string ) {  
	
		let regexMap = [
	      {regEx: /^4[0-9]{5}/ig,cardType: "VISA"},
	      {regEx: /^5[1-5][0-9]{4}/ig,cardType: "MASTERCARD"},
	      {regEx: /^3[47][0-9]{3}/ig,cardType: "AMEX"},
	      {regEx: /^(5[06-8]\d{4}|6\d{5})/ig,cardType: "MAESTRO"},
	      {regEx: /^(6(011|5[0-9][0-9])[0-9]{12})/ig,cardType: "DISCOVER"}
	    ];

	    this.payCardType = '';
		for (let j = 0; j < regexMap.length; j++) {
		  if (searchValue.match(regexMap[j].regEx)) {
		    this.payCardType = regexMap[j].cardType;
		    break;
		  }
		}
	}

	validateEwallet() {
		if(this.ewallet !== ''){
			const formData = {"ewalletId" : this.ewallet, "amountToBePaid" : this.invoice['totalBillingAmount'], "userId" : this.userId};
			 this.regService.validateEwallet(formData).subscribe((response:any) => {
		        if(response.status == 1){
		        	this.ewalletReadonly = true;
		        	this.ewalletId = response.results;
		        	this.ewalletMsg = '';
		        	this.toastr.success("E-Wallet ID veirfied successfully.");
		        } else {
		        	this.ewalletMsg = response.results;
		        }
	         });
		}
	}



	onSubmit(form : NgForm) {

		this.spinner.show();
		const formData = {"data" : form.value,"fillshipShipmentId":this.fillshipShipmentid,"ewalletTransferAmount":this.ewalletTransferAmount, "userId" : this.userId,"paypalTransaction" : this.paypalTransaction};
		if(this.paymentMethodKey == 'paystack_checkout')
		{
			this.content.getAmountForPaystack(this.paidAmount,this.defaultCurrencyCode).subscribe((data:any) => {
				this.spinner.hide();
				localStorage.setItem('fillandshipExtracost', JSON.stringify(formData));
				this.bsModalRef = this.modalService.show(PaystackModalComponent, {});
				this.bsModalRef.content.invoiceData = formData;
				this.bsModalRef.content.payAmount = data.amountForPaystack;
			});
			
		} else if(this.paymentMethodKey == 'payeezy'){
			//this.showPaystackOverlay = true;

			//var storedData =  JSON.parse(localStorage.getItem('userWarehouseCart'));

			//Clear Storage Data
    		//delete storedData.paymentdetails ;

			this.content.getAmountForPayeezy(this.paidAmount,this.defaultCurrencyCode).subscribe((data:any) => {
				this.bsModalRef = this.modalService.show(PayeezyModalComponent, {backdrop: 'static', keyboard: false});
				this.bsModalRef.content.payAmount = data.amountForPayeezy;
				this.bsModalRef.content.invoiceData = formData;
				this.bsModalRef.content.amountDisplay = data.amountDisplay;
				this.bsModalRef.content.paySymbol = this.defaultCurrencySymbol;
			});

			this.modalService.onHide.subscribe(() => {
				setTimeout(() => {
					//this.showPaystackOverlay = false;
			    }, 3000);
				
			});
			
		} else {
			this.spinner.show();
			this.fillshipService.payextracharge(formData).subscribe((response:any) => {
				this.spinner.hide();
				if(response.status == 1){
					localStorage.removeItem('extraCostPaidAmount');
					localStorage.removeItem('ewalletTransferAmount');
					this.route.navigate(['fill-and-ship','overview']);
					this.toastr.success("Extracharge payment proccessed successfully.");
				}else if(response.status == 2)
             	{
             		localStorage.removeItem('extraCostPaidAmount');
             		localStorage.removeItem('ewalletTransferAmount');
             		this.toastr.success('', response.msg,{
						disableTimeOut : true,
						positionClass: 'toast-center-center',
						closeButton : true
					});

             	}else if(response.status == '-1') {

					this.toastr.error(response.results);
				} 
				else {
					//window.location.reload();
					this.toastr.error("Unable to proccess request. Please try again.");
				}
			});
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	paypalConfig(formValues) {
		this.spinner.show();
		let total = parseFloat(this.paidAmount).toFixed(2);
		console.log(this.defaultCurrencySymbol);

		this.content.getpaypalipnsettings('', total).subscribe((response:any) => {
			this.paypalipnConfig = response.results;

		if(this.paypalipnConfig.mode == "Sandbox")
		{

	      	this.payPalConfig = new PayPalConfig(PayPalIntegrationType.ClientSideREST, PayPalEnvironment.Sandbox, {
	        commit: true,
	        client: {
	          //sandbox: 'AR3rs-NnrWJC6iL3ja_g7FHO0m3P3n3RM0x6mZHfuXKY1tjt9XHGfG23gYolJ3Kr_e0KXUMlE-y7Ce7c',
	          sandbox: this.paypalipnConfig.key,
	        },
	        button: {
	          label: 'paypal',
	        },
	        onPaymentComplete: (data, actions) => {
	        	this.paypalTransaction = data;
	        	this.onSubmit(formValues);
	        },
	        onCancel: (data, actions) => {
	          this.toastr.error('The Payment has not been proccessed. Please try again');
	        },
	        onError: (err) => {
	          this.toastr.error('The Payment has not been proccessed. Please try again');
	        },
	        transactions: [{
	          amount: {
	            currency: this.paypalipnConfig.currencyCode,
	            total: this.paypalipnConfig.total
	          }
	        }]
	      });
	    }
	    else if(this.paypalipnConfig.mode == "Production")
	    {
	    	this.payPalConfig = new PayPalConfig(PayPalIntegrationType.ClientSideREST, PayPalEnvironment.Production, {
	        commit: true,
	        client: {
	          //sandbox: 'AR3rs-NnrWJC6iL3ja_g7FHO0m3P3n3RM0x6mZHfuXKY1tjt9XHGfG23gYolJ3Kr_e0KXUMlE-y7Ce7c',
	          production: this.paypalipnConfig.key,
	        },
	        button: {
	          label: 'paypal',
	        },
	        onPaymentComplete: (data, actions) => {
	        	this.paypalTransaction = data;
	        	this.onSubmit(formValues);
	        },
	        onCancel: (data, actions) => {
	          this.toastr.error('The Payment has not been proccessed. Please try again');
	        },
	        onError: (err) => {
	          this.toastr.error('The Payment has not been proccessed. Please try again');
	        },
	        transactions: [{
	          amount: {
	            currency: this.paypalipnConfig.currencyCode,
	            total: this.paypalipnConfig.total
	          }
	        }]
	      });
	    }

	    setTimeout(() => {
	    	/** spinner ends after 5 seconds */
	   		this.spinner.hide();
		}, 2000);

      }); // paypal ipn config bracket end
    }
}
