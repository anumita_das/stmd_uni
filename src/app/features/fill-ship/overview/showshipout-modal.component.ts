import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { RegistrationService } from '../../../services/registration.service';
import {Router} from "@angular/router";
import { AppGlobals } from '../../../app.globals';
import { FillshipService } from '../../../services/fillship.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-shipoutitem-modal',
	templateUrl: './showshipout-modal.component.html',
	styleUrls: ['./showshipout-modal.component.scss'],
	providers: [AppGlobals]
})
export class ShowShipoutModalComponent implements OnInit {

	title: string;
	closeBtnName: string;
	shipoutOptions: any = "";
	fillshipShipmentId : any;
	constructor(
		public bsModalRef: BsModalRef,
		public userInfo : RegistrationService,
		private router: Router,
		private _global: AppGlobals,
		private fillshipService : FillshipService,
		private spinner: NgxSpinnerService,
		private toastr: ToastrService,
	) {
	}

	ngOnInit() {

	}

	proceedCheckout(ewalletTransferAmount)
	{
		this.bsModalRef.hide();
		this.spinner.show();
		console.log(this.fillshipShipmentId)
		console.log(ewalletTransferAmount);
		this.fillshipService.shipout(this.fillshipShipmentId,ewalletTransferAmount).subscribe((data:any)=>{
			this.spinner.hide();
			if(data.status == 1)
			{
				this.toastr.success('Shipout request received successfull. Kindly check your email for tracking details');
				window.location.reload();
			}
			else
			{
				this.toastr.error('Something Went wrong. Please proceed once again.')
			}

		});
	}

	payExtraCost(extracost,ewalletTransferAmount) {
		//console.log(extracost);
		this.bsModalRef.hide();
		this.fillshipService.setExtracharge(extracost);
		this.fillshipService.setEwallettransferAmount(ewalletTransferAmount);
		this.router.navigate(['fill-and-ship','extracost',this.fillshipShipmentId]);
	}
}
