import { Component, OnInit, NgZone, HostListener } from '@angular/core';
import { NgForm }   from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { RegistrationService } from '../../../services/registration.service';
import { ContentService } from '../../../services/content.service';
import { AppGlobals } from '../../../app.globals';
import { ToastrService } from 'ngx-toastr';
import { Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { WarehouseModalComponent } from '../../../shared/components/warehouse-modal/warehouse-modal.component';
import { FillshipService } from '../../../services/fillship.service';

@Component({
	selector: 'app-fs-order-form',
	templateUrl: './fs-order-form.component.html',
	styleUrls: ['./fs-order-form.component.scss']
})
export class FSOrderFormComponent implements OnInit {
	isMediumScreen: boolean;
	showShipping: boolean;
	sidebar: boolean;
	isCollapsed: boolean;
	bsModalRef: BsModalRef;
	rows: Array<any>;
	columns: Array<any>;
	dtOptions: DataTables.Settings = {};

	shippingTypes: Array<any>;

    bagList : Array<any> = [];
	userId: number;
	paymentDetails : Array<any>;	
	defaultCurrencySymbol: any;
	shippingMethodList: Array<any>;
	allWarehouse : Array<object>;
	selectedWarehouse : number;
	selectedWarehouseDetails: any;
	defaultProfile : string;
	userShippingAddress : Array<object>;
	earnedPoints: any;
	rewardName: any;
	expString: any;
 	categoryList : any;
 	subcategoryList : any;
	productList : any;
	showFindBtn : boolean = false;
	boxDetails : any = {};	
	allAddedBox : any = [];
	selectboxShipping : boolean = false;
	selectedBoxshippingTypes : any;
	selectedBoxDetails : any;
	shipmentExistInwarehouse : any;


	fillship =  {
		warehouseId : "",
		siteCategoryId : "",
		siteSubCategoryId : "",
		siteProductId : "",
		itemPrice : "",
		itemQuantity : "",
		siteProductImage : "",
	};

	constructor(
		private ngZone: NgZone,
		private fillshipService : FillshipService,
		public  regService : RegistrationService,
		private content: ContentService,
		private _global: AppGlobals,
		private toastr: ToastrService,
		private modalService: BsModalService,
		private spinner: NgxSpinnerService,
		private router: Router,
	) {
		this.userId = this.regService.item.user.id;
		this.defaultCurrencySymbol = this._global.defaultCurrencySymbol;

		localStorage.removeItem('userFillShipCart');
	 }


	ngOnInit() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		this.checkWindowSize(width);

		this.getUserAddressInfo();
		this.getRewardDetails();
		this.getAllWarehouse();
		this.getCategoryList();
		this.getAllAddedBox();
		this.nonShipedoutdataExist();

		this.allWarehouse = [];
		this.defaultProfile = JSON.parse(localStorage.getItem('profileImage'));
	}

	getUserAddressInfo() {
		this.regService.userShippingAddress('shipping').subscribe((data:any) =>{
			if(data.status == 1)
			{
				this.userShippingAddress = data.results;
			}
		});
	}

	getRewardDetails() {
		this.content.getRewardDetails(this.userId).subscribe((data:any) => { 
			this.earnedPoints = data.results.earnedPoints;
			this.rewardName = data.results.rewardName;
			this.expString = data.results.expStringShort;
		});
	}

	getAllWarehouse(){
		this.content.allWarehouse().subscribe((data:any) => {
			let index = 0;
			if(data.status == 1)
			{
				for(let eachWarehouse of data.results) {
					this.allWarehouse.push(eachWarehouse);
					if(index == JSON.parse(localStorage.getItem('selectedWarehouse')))
					{
						this.selectedWarehouseDetails = eachWarehouse;
						this.fillship.warehouseId = this.selectedWarehouseDetails.id;
					}
					index++;
				}
			}
			this.selectedWarehouse = JSON.parse(localStorage.getItem('selectedWarehouse'));

			this.getUserCartList();
		});
	}

	selectWarehose(event){
		let index = 0;
		var selectedDrop = event.target.value;
		localStorage.setItem('selectedWarehouse',JSON.stringify(selectedDrop));
		this.selectedWarehouse = selectedDrop;
		for(let eachWarehouse of this.allWarehouse)
		{
			if(index == selectedDrop)
			{
				this.selectedWarehouseDetails = eachWarehouse;
				this.fillship.warehouseId = this.selectedWarehouseDetails.id;
			}
			index++;
		}

		this.getUserCartList();

		
		let el = document.getElementById('warehouseAddress');
	  	el.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'end'});	  	
	}

	 showWarehouseAddress() {
    	this.bsModalRef = this.modalService.show(WarehouseModalComponent, {});
    	this.bsModalRef.content.warehouseAddress = this.selectedWarehouseDetails;
		this.bsModalRef.content.closeBtnName = 'Close';		
    }

    //Fetch Category List
	getCategoryList(){
		const formdata = {type : 'shopforme', storeId : ''};
		this.content.categorylist(formdata).subscribe((data:any) => {
			this.categoryList = data;
		});
	}

	//Fetch Sub Category List
	categoryChanged(categoryId){
		this.subcategoryList = [];
		this.productList = [];

		if(categoryId != ''){
			this.content.subcategorylist(categoryId).subscribe((data:any) => {
				if(data.length > 0){
					this.subcategoryList = data;
					this.fillship.siteSubCategoryId  = "";
					this.fillship.siteProductId = "";
				} else {
					this.fillship.siteSubCategoryId  = "";
					this.fillship.siteProductId = "";
				}
			});
		}
	}


	//Fetch Product List
	subCategoryChanged(subCategoryId){
		this.productList = [];

		if(subCategoryId != '') {
			this.content.productlist(subCategoryId).subscribe((data:any) => {
				if(data.length > 0){
					this.productList = data;
					this.fillship.siteProductId  = "";
				} else {
					this.fillship.siteProductId  = "";
				}
			});
		}
		
	}

	//Set Product Image
	productChanged($event){
		this.fillship.siteProductImage = $event.target.getAttribute('data-image');
	}

	//Fetch Cart List 
	getUserCartList() {
		this.showShipping = false;

		const formData = {
	    	id : this.userId,
	        warehouseId : this.fillship.warehouseId,
	        type : 'fillship'
		}
		this.fillshipService.getUserCartList(formData).subscribe((data:any) => {
			this.bagList = data.items;
			this.paymentDetails = data.details;

			if(data.items && data.items.length > 0)
				this.showFindBtn = true;
			else
				this.showFindBtn = false;	
		});


	}

	//Remove Item From Cart List 
	deleteItem(index, id){
	  this.fillshipService.removeCartItem(id).subscribe((data:any) => {
	    if(data.results == 'success'){
	 		this.getUserCartList();
	 		this.showShipping = false;
	 	}
	  });
	}

	getAllAddedBox() {
		this.fillshipService.getAllAddedBox().subscribe((data:any) => {
			if(data.status == "1")
				this.allAddedBox = data.results
		});
	}

	nonShipedoutdataExist() {
		this.spinner.show();
		this.fillshipService.getnonShipedoutdata().subscribe((data:any) => {
			this.spinner.hide();
			if(data.status == 1)
				this.shipmentExistInwarehouse = data.results;
		});
	}

	findMyBox() {
		 const formData =  {
				items : this.bagList, 
				warehouseId : this.fillship.warehouseId, 
				paymentDetails :  this.paymentDetails
			};
		this.fillshipService.findmybox(formData).subscribe((data:any) => {
			if(data.results == 'success'){
				this.boxDetails = data.boxDetails;
				this.shippingTypes = data.shippingCharges;
				this.showShipping = true;
			} else {
				this.toastr.error('Suitable box not found. Please contact site admin.');	
			}
		 });	
	}

	selectBox(boxId) {
		const data = {
			warehouseId : this.fillship.warehouseId,
			boxId : boxId
		}

		this.fillshipService.selectBox(data).subscribe((data:any) => {
			if(data.status == '1')
			{
				this.selectboxShipping = true;
				this.selectedBoxDetails = data.boxDetails;
				this.selectedBoxshippingTypes = data.shippingCharges;
				let el = document.getElementById('selectedFromBoxList');
	  			el.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'end'});
				
			}
		})
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	//Save Cart Items For Procurement
	submitFillShipData($event, shippingId) {

		const formData =  {
			items : this.bagList, 
			shippingId : shippingId,
			warehouseId : this.fillship.warehouseId, 
			paymentDetails :  this.paymentDetails,
			boxDetails :  this.boxDetails
		};
		this.fillshipService.submitfillshipdata(formData).subscribe((data:any) => {
			localStorage.removeItem('userFillShipCart');
	 		localStorage.setItem('userFillShipCart', JSON.stringify(data.fillship));
	 		this.router.navigate(['fill-and-ship','checkout']);
		});
	}

	submitSelectedBox(shippingId) {
		const formData =  {
			items : [], 
			shippingId : shippingId,
			warehouseId : this.fillship.warehouseId, 
			paymentDetails :  [],
			boxDetails :  this.selectedBoxDetails
		};

		this.fillshipService.submitfillshipdata(formData).subscribe((data:any) => {
			localStorage.removeItem('userFillShipCart');
	 		localStorage.setItem('userFillShipCart', JSON.stringify(data.fillship));
	 		this.router.navigate(['fill-and-ship','checkout']);
		});
	}

	//Add Items to Bag On Submit
	onSubmit(form: NgForm){
		this.spinner.show();

		this.showShipping = false;

		this.fillshipService.updateUserCart(form.value, 'fillship').subscribe((data:any) => {
			if(data.status == '1'){

			this.getUserCartList();

			this.fillship =  {
				warehouseId : this.fillship.warehouseId,
				siteCategoryId : "",
				siteSubCategoryId : "",
				siteProductId : "",
				siteProductImage : "",
				itemPrice : "",
				itemQuantity : "",
			};

			form.resetForm(this.fillship);

			setTimeout(() => {
		        /** spinner ends after 3 seconds */
		        this.spinner.hide();
	    	}, 3000);
		} else {
			setTimeout(() => {
		        /** spinner ends after 3 seconds */
		        this.spinner.hide();
		    }, 3000);
		}
		});	
	}

	setTwoNumberDecimal($event) {
		if(!isNaN(parseFloat($event.target.value))){
		 	$event.target.value = Math.abs(parseFloat($event.target.value)).toFixed(2);
		}
	 	else {
	 		$event.target.value = parseFloat("0").toFixed(2);
	 	}
	}

	setInteger($event)
	{
		$event.target.value = Math.abs(Math.floor($event.target.value));
	}


	@HostListener('window: resize', ['$event'])
	onResize(event) {
		console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}
}
