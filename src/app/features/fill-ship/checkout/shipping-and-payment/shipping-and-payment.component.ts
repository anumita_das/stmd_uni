import { Component, OnInit } from '@angular/core';
import { FillshipService } from '../../../../services/fillship.service';
import {Router} from "@angular/router";
import { RegistrationService } from '../../../../services/registration.service';

@Component({
	selector: 'app-shipping-and-payment',
	templateUrl: './shipping-and-payment.component.html',
	styleUrls: ['./shipping-and-payment.component.scss']
})
export class ShippingAndPaymentComponent implements OnInit {
	isReadOnly = false;
	personalDetails : any;
	paymentMethodList : Array<any>;
	billingCountryId : any;
	paymentMethodId  : any;
	paymentMethodName : any;
	paymentMethodKey : any;
	shipmentMethodId : any;
	paymentTax : any;
	paymentTaxType : any;
	totalCost: number;

	constructor(
		private router: Router,
		public fillshipService : FillshipService,
                private regService : RegistrationService,
	) {
		if("userFillShipCart" in localStorage){
		  	var usercart = localStorage.getItem('userFillShipCart');
			if(usercart == null || usercart == ""){
			 	this.router.navigate(['fill-and-ship','order-form']);   
			 } else {
			 	if("personaldetails" in JSON.parse(localStorage.getItem('userFillShipCart'))){
			 		//do nothing
			 	} else {
			 		 this.router.navigate(['fill-and-ship','checkout','personal-detail']);   
			 	}
			}
		} else {
		   this.router.navigate(['fill-and-ship','order-form']);   
		}
	 }

	ngOnInit() {
		const usercart =  JSON.parse(localStorage.getItem('userFillShipCart'));
		if(usercart.data.shippingMethodId != '')
			this.isReadOnly = true;

		if(typeof(usercart.paymentMethod) != 'undefined'){
			this.paymentMethodId = usercart.paymentMethod.paymentMethodId;
			this.paymentMethodName = usercart.paymentMethod.paymentMethodName;
			this.paymentMethodKey = usercart.paymentMethod.paymentMethodKey;
			this.paymentTaxType = usercart.paymentMethod.paymentTaxType;
			this.paymentTax = usercart.paymentMethod.paymentTax;
		}

		this.personalDetails = usercart.personaldetails;
		this.billingCountryId  = usercart.personaldetails.billingCountryId;
		this.shipmentMethodId = usercart.data.shipmentMethodId;
		this.totalCost = parseFloat(usercart.data.totalCost);

		this.getPaymentMethodList();
	}

	getPaymentMethodList() {
		const data = {billingCountryId : this.billingCountryId};
		this.fillshipService.getPaymentMethodList(data).subscribe((data:any) => {
			this.paymentMethodList = data;
		});
	}

	changePaymentMethod($event, paymentMethodName, paymentMethodKey, paymentTax, paymentTaxType) {
		this.paymentMethodName = paymentMethodName;
		this.paymentMethodKey = paymentMethodKey;
		this.paymentTax = paymentTax;
		this.paymentTaxType = paymentTaxType;
	}


	proceedToPlaceorder() {
		if(typeof(this.paymentMethodId) != 'undefined'){
			var storedData =  JSON.parse(localStorage.getItem('userFillShipCart'));
	    	
	    	//Clear Storage Data
	    	delete storedData.paymentMethod;

	    	var totalTax = 0;
	    	var defaultTotalTax = 0;

	    	var taxApplicableAmount =   parseFloat(storedData.data.shippingCost);
	    	
	    	if(this.paymentTaxType == 0)
	    			totalTax = parseFloat(this.paymentTax);
	    		else
	    			totalTax = (taxApplicableAmount / 100)*parseFloat(this.paymentTax);

	    	
	    	//Reassign Storage Data
	    	storedData['data']['totalTax'] = totalTax.toFixed(2);
		    localStorage.setItem('userFillShipCart', JSON.stringify(storedData));

	    	//Reassign Storage Data
	    	const newStoredData = Object.assign({"paymentMethod":{"paymentMethodId" : this.paymentMethodId, "paymentMethodName" : this.paymentMethodName, "paymentMethodKey" : this.paymentMethodKey, "paymentTax" : this.paymentTax, "paymentTaxType" : this.paymentTaxType}},storedData);
	    	
	    	localStorage.setItem('userFillShipCart', JSON.stringify(newStoredData));

                if(this.regService.item.user.subscription.couponCode){
                  if("coupondetails" in JSON.parse(localStorage.getItem('userFillShipCart'))){  
                    this.router.navigate(['fill-and-ship','checkout', 'place-order']);
                  } else {
                    this.validateCoupon(this.regService.item.user.subscription.couponCode);
                  }
                } else {
                    this.router.navigate(['fill-and-ship','checkout', 'place-order']);
                }

	    	
	    } else {
	    	alert("Please select a payment method to continue");
	    	return false;
	    }
	}

        validateCoupon(couponcode){
            this.fillshipService.validatecouponcode(couponcode).subscribe((response:any) => {
                var storedData =  JSON.parse(localStorage.getItem('userFillShipCart'));
                if(response.results == 'valid'){
                    if(storedData.isCurrencyChanged == 'Y'){
                        var defaultTotalDiscount = parseFloat(response.amount_to_be_discounted);
                        var discount = defaultTotalDiscount * storedData.exchangeRate;
                        var totalCost = parseFloat(storedData.data.totalCost);
                        var totalDiscountCost = totalCost-discount;

                        storedData['data'].totalDiscount = discount;
                        storedData['data'].totalBDiscountCost = totalCost;
                        storedData['data'].defaultTotalBDiscountCost = storedData.data.defaultTotalCost; 
                        storedData['data'].totalCost = totalDiscountCost;
                        storedData['data'].defaultTotalCost = parseFloat(storedData.data.defaultTotalCost) - defaultTotalDiscount;
                    } else {
                        var discount = parseFloat(response.amount_to_be_discounted);
                        var totalCost = parseFloat(storedData.data.totalCost);
                        var totalDiscountCost = totalCost-discount;

                        storedData['data'].totalDiscount = discount;
                        storedData['data'].totalCost = totalDiscountCost;
                        storedData['data'].totalBDiscountCost = totalCost;
                    }
                    localStorage.setItem('userFillShipCart', JSON.stringify(storedData));  

                    delete storedData.coupondetails;

                    const couponStore = Object.assign({"coupondetails":{"couponCode": couponcode, "discountAmount":discount, "discountPoint":'', "discountType" : "amount"}},storedData);
                    localStorage.setItem('userFillShipCart', JSON.stringify(couponStore));

                    this.router.navigate(['fill-and-ship','checkout', 'place-order']);
                } else {
                    this.router.navigate(['fill-and-ship','checkout', 'place-order']);
                }

            });
	}
        
}
