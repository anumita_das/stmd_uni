import { Component, OnInit } from '@angular/core';
import { FillshipService } from '../../../../services/fillship.service';
import {Router} from "@angular/router";
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { CurrencyModalComponent } from '../../../../shared/components/currency-modal/currency-modal.component';
import { RegistrationService } from '../../../../services/registration.service';

@Component({
	selector: 'app-cart',
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
	dtOptions: DataTables.Settings = {};
	bagList : Array<any> = [];
	cartDetails : any;
	showDiscount = false;
	bsModalRef: BsModalRef;
    exchangeRate: any = 1;
    currencySymbol: any = '';
    defaultCurrencySymbol: any = '';
    isCurrencyChanged: any = 'N';
    couponcode: string;
    couponInValidMsg: string;
    couponInValid:boolean = false;
    blankCouponCode:boolean = true;
	personaldetails : Array<any> = [];
	user : Array<any>;

	constructor(
		private fillshipService :  FillshipService,
		private auth: RegistrationService,
		private router: Router,
		private modalService: BsModalService,
	) { 
	   	this.user = this.auth.item.user; 
	}

	ngOnInit() {
		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

		this.getUserCartList();
	}

	//Fetch Cart List from Session
  	getUserCartList() {
  		const usercart = JSON.parse(localStorage.getItem('userFillShipCart'));
  		this.bagList = usercart.items;
  		this.cartDetails = usercart.data;

  		this.currencySymbol = usercart.currencySymbol;
  		this.defaultCurrencySymbol = usercart.defaultCurrencySymbol;
  		this.isCurrencyChanged = usercart.isCurrencyChanged;
  		this.exchangeRate = usercart.exchangeRate;

	}

	clearCart() {
		this.fillshipService.clearUserCart('fillship').subscribe((response:any) => {
		if(response.results == 'success')
			{
				localStorage.removeItem('userFillShipCart');
				this.router.navigate(['/fill-and-ship','order-form']);
			}
			
		});
	}

	showCurrency() {
		
		const usercart = JSON.parse(localStorage.getItem('userFillShipCart'));
		const currencyCode = usercart.currencyCode;

		this.fillshipService.getCurrencyList(currencyCode).subscribe((response:any) => {
			this.bsModalRef = this.modalService.show(CurrencyModalComponent, {});
			this.bsModalRef.content.currencyList = response;
		});

	}

	checkout() {
		this.router.navigate(['/fill-and-ship','checkout','personal-detail']);
	}

	hack(value) {
	   return Object.values(value)
	}

	validateCoupon(){
		this.fillshipService.validatecouponcode(this.couponcode).subscribe((response:any) => {
			this.couponInValid = false;
			this.couponInValidMsg = '';
			if(response.results == 'invalid'){
				this.couponInValidMsg = response.message;
				this.couponInValid = true;
			}
		});
	}

	checkEmptyCouponInput(){
		if(this.couponcode == ''){
			this.blankCouponCode = true;
		} else {
			this.blankCouponCode = false;
		}
	}
}
