import { Component, OnInit, NgZone, HostListener } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { RecipientModalComponent } from '../../shared/components/recipient-modal/recipient-modal.component';
import { RegistrationService } from '../../services/registration.service';
import { ContentService } from '../../services/content.service';
import { User } from '../../shared/models/user.model';
import { NgForm,FormControl }   from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-modify-account',
	templateUrl: './modify-account.component.html',
	styleUrls: ['./modify-account.component.scss']
})
export class ModifyAccountComponent implements OnInit {
	isMediumScreen: boolean;
	sidebar: boolean;
	isCollapsed: boolean;
	bsModalRef: BsModalRef;
	profileImg : boolean;
	defaultProfile : string;
	user = new User();
	formSuccess : boolean;
	passwordError : boolean;
	passwordErrorMessage : string;
	datepickerModel: Date;
	formSubmit : boolean;
	userShippingAddress : Array<object>;
	selectedWarehouseDetails: any;
	public barLabel: string = "Password strength:";
    public myColors = ['#DD2C00', '#FF6D00', '#FFD600', '#AEEA00', '#00C853'];
    //wcodePattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&^()_?+=])[A-Za-z\\d@$_!%*?&-^]{8,20}$"; 
    wcodePattern = "^.{8,}$";
    newPassword : any;
   	earnedPoints: any;
	rewardName: any;
	expString: any;

    hidePwdStrength:any;
    deactiveBtn : any = true;
    newRepeatPassword:any;

    showPassText : boolean = false;
    showRepeatPassText : boolean = false;
    
	constructor(
		private ngZone: NgZone,
		private modalService: BsModalService,
		public regService : RegistrationService,
		public content : ContentService,
		public toastr: ToastrService,
	) { }

	ngOnInit() {
		this.getUserAddressInfo();
		//this.getAllWarehouse();
		this.getUserWarehose();
		this.getRewardDetails();

		if(this.regService.item.user.dateOfBirth!=null)
			this.datepickerModel = new Date(this.regService.item.user.dateOfBirth);
		else
			this.datepickerModel = new Date();

		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		//console.log('Width: ' + width);
		this.checkWindowSize(width);

		// account isCollapsed
		this.isCollapsed = false;
		this.profileImg = false;
		this.formSuccess = false;
		this.passwordError = false; 
		this.formSubmit = false;
		this.defaultProfile = JSON.parse(localStorage.getItem('profileImage'));
		this.user = this.regService.item.user;
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	// modal
	openRecipientModal() {
		this.bsModalRef = this.modalService.show(RecipientModalComponent, {
			class: 'modal-xs modal-dialog-centered'

		});
		this.bsModalRef.content.closeBtnName = 'Close';
	}


	fileUpload(event) {
  	
  		let fileItem = event.target.files[0];
  		let fileType = fileItem.type;
  		
        //console.log("file input has changed. The file is", fileItem);
        if(fileType == 'image/png' || fileType == 'image/jpeg')
		{
			this.defaultProfile = '';
	        var reader = new FileReader();
	        reader.onload = (event: any) => {
	            this.profileImg = event.target.result;
	            //console.log(this.profileImg);
	        }
	        reader.readAsDataURL(event.target.files[0]);
	        const fileSelected: File = fileItem;
	        this.regService.uploadFile(fileSelected).subscribe((data:any) => {
	        	if(data.status == 1)
	        	{
	        		localStorage.setItem('profileImage',JSON.stringify(data.results));
	        	}
	        });
    	} else {
    		this.toastr.error('Please upload JPEG/JPG/PNG for Profile Picture');
			event.srcElement.value = null;
    	}
    }

    onSubmit(form : NgForm) {
    	this.formSubmit = true;
    	this.regService.updateUser(form.value).subscribe((data:any) => {
    		this.formSubmit = false;
    		if(data.status == 1)
    		{
    			this.formSuccess = true;
    			this.passwordError = false;
    			var userInfo = JSON.parse(localStorage.getItem('tokens'));
    			userInfo.user = data.results;
    			localStorage.setItem('tokens', JSON.stringify(userInfo));
    			//location.reload();

    		}
    		else if(data.status == '-2' || data.status == '-3')
    		{
    			this.formSuccess = false
    			this.passwordError = true;
    			this.passwordErrorMessage = data.results;
    		}
    	});
    }

    getUserAddressInfo() {
		this.regService.userShippingAddress('shipping').subscribe((data:any) =>{
			if(data.status == 1)
			{
				this.userShippingAddress = data.results;
			}
		});
	}

	getRewardDetails() {
		this.content.getRewardDetails(this.regService.userId).subscribe((data:any) => { 
			this.earnedPoints = data.results.earnedPoints;
			this.rewardName = data.results.rewardName;
			this.expString = data.results.expStringShort;
		});
	}

	getUserWarehose() {
		this.content.getUserWarehose(this.regService.item.user.id).subscribe((data:any) => {
			let index = 0;
			//console.log(JSON.parse(localStorage.getItem('selectedWarehouse')));
			if(data.status == 1)
			{
				for(let eachWarehouse of data.results) {
					if(index == JSON.parse(localStorage.getItem('selectedWarehouse')))
					{
						this.selectedWarehouseDetails = eachWarehouse;
					}
					index++;
				}
				//console.log(this.selectedWarehouseDetails);
			}
		});
	}

	getAllWarehouse(){
		this.content.allWarehouse().subscribe((data:any) => {
			let index = 0;
			if(data.status == 1)
			{
				for(let eachWarehouse of data.results) {
					if(index == JSON.parse(localStorage.getItem('selectedWarehouse')))
					{
						this.selectedWarehouseDetails = eachWarehouse;
					}
					index++;
				}
			}
		});
	}

	checkEmptyPassword(event){
		if(event == ''){
			this.hidePwdStrength = true;
		} else {
			this.hidePwdStrength = false;
		}

		if(this.newRepeatPassword !== event){
			this.deactiveBtn = false;
		} else {
			this.deactiveBtn = true;
		}

	}

	checkMatchPasswords(event){ 

		if(this.newPassword !== event){
			this.deactiveBtn = false;
		} else {
			this.deactiveBtn = true;
		}

	}

	changePasswordView(field) {
		if(field == 'pass')
			this.showPassText = !this.showPassText;
		else
			this.showRepeatPassText = !this.showRepeatPassText
	}

}
