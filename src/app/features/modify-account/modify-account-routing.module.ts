import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModifyAccountComponent } from './modify-account.component';

const routes: Routes = [
	{ path: '', component: ModifyAccountComponent }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ModifyAccountRoutingModule {

}
