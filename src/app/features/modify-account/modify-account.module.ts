import { NgModule } from '@angular/core';
import { ModifyAccountRoutingModule } from './modify-account-routing.module';
import { ModifyAccountComponent } from './modify-account.component';
import { SharedModule } from '@app/shared';
// import { BsDatepickerModule } from 'ngx-bootstrap';
import { PasswordStrengthBarModule } from 'ng2-password-strength-bar';

@NgModule({
	imports: [
		ModifyAccountRoutingModule,
		SharedModule,
		PasswordStrengthBarModule,
		// BsDatepickerModule.forRoot()
	],
	declarations: [ModifyAccountComponent]
})
export class ModifyAccountModule {

}
