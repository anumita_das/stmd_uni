import { NgModule } from '@angular/core';
import { SendUsMessageRoutingModule } from './send-us-message-routing.module';
import { SendUsMessageComponent } from './send-us-message.component';
import { SharedModule } from '@app/shared';
import { ThankyouComponent } from './thankyou/thankyou.component';


@NgModule({
	imports: [
		SendUsMessageRoutingModule,
		SharedModule
	],
	declarations: [
		SendUsMessageComponent,
		ThankyouComponent
	]
})
export class SendUsMessageModule {

}
