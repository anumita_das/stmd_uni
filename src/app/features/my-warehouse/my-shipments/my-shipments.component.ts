import { Component, OnInit, NgZone, HostListener } from '@angular/core';
import { NgForm, FormControl }   from '@angular/forms';
import { MywarehouseService } from '../../../services/mywarehouse.service';
import { RegistrationService } from '../../../services/registration.service';
import { ContentService } from '../../../services/content.service';
import { AppGlobals } from '../../../app.globals';
import { ToastrService } from 'ngx-toastr';
import { Router } from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import { Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import { WarehouseModalComponent } from '../../../shared/components/warehouse-modal/warehouse-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { config } from '../../../app.config';
import { ContactFormModalComponent } from '../../../shared/components/contactform-modal/contactform-modal.component';

@Component({
	selector: 'app-my-shipment',
	templateUrl: './my-shipments.component.html',
	styleUrls: ['./my-shipments.component.scss'],
	providers: [DatePipe]
})
export class MyshipmentsComponent implements OnInit {
	isFloating: boolean = false;
	isFloating2: boolean = false;
	isMediumScreen: boolean;
	sidebar: boolean;
	isCollapsed: boolean;
	dtOptions: DataTables.Settings = {};
	showShippingCost: boolean;
	shippingTypes: Array<any>;
	warehouseShipmentList: Array<any> = [];
	showShipping: boolean = false;
	userId: number;
	defaultCurrencySymbol: any;
	shipment: any;
	shippingMethodList: Array<any>;
	allWarehouse: Array<object>;
	selectedWarehouse: number;
	selectedWarehouseSearch : any = '';
	selectedStatusSearch : any = '';
	selectedWarehouseDetails: any;
	defaultProfile: string;
	userShippingAddress: Array<object>;
	earnedPoints: any;
	rewardName: any;
	expString: any;
	showCalculateBtn: boolean = false;
	today: any;
	bannerImage: any;
	bannerShoptomydoor: Array<object>;
	enableCheckout: boolean = false;
	totalShipingValue: any;
	showInventorymsg: boolean = false;
	deliverySection: Array<any> = [false];
	deliveryData: Array<any> = [];
	showShippingMethod: Array<any> = [false];
	closeDelivery: Array<any> = [false];
	showSection: Array<any> = [];
	showRepresentative: boolean = false;
	representativeDetail: any;
	representativeImage: any;
	userSubscription : any = "";
	userSubscriptionData : any = "";
	userSubscriptionStatus : any = "";
	locationStatus : any = "1";
	perPage:any = 10;
	currentPage: number = 1;
	pageSize: number;
	totalItems: number;
	dtTrigger: Subject<any> = new Subject();
	orderDate:any = 'AD';
	isPeriodDisabled: boolean;
	bsModalRef: BsModalRef;
	serverUrl : any = '';
	referralCode : string = '';
	chips: Array<any>;
	message: any = '';
	formSubmit : boolean;
	formError : boolean;
	submitSuccess : boolean;
	users : any;
	submitError : boolean;
	rangeDate : any;
	socialIcons: Array<object> = [];
	referralLink = '';


	constructor(
		private ngZone: NgZone,
		public mywarehouseService: MywarehouseService,
		public regService: RegistrationService,
		private content: ContentService,
		private _global: AppGlobals,
		private toastr: ToastrService,
		private spinner: NgxSpinnerService,
		private router: Router,
		private datePipe: DatePipe,
		private modalService: BsModalService,
	) {
		this.userId = this.regService.item.user.id;
		this.defaultCurrencySymbol = this._global.defaultCurrencySymbol;
		this.totalShipingValue = "$0.00";

		localStorage.removeItem('userWarehouseCart');
	}

	ngOnInit() {
		this.formSubmit = false; 
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		this.checkWindowSize(width);

		// account isCollapsed
		this.isCollapsed = false;

		this.defaultProfile = JSON.parse(localStorage.getItem('profileImage'));

		this.getUserAddressInfo();
		this.getRewardDetails();
		//this.getAllWarehouse();
		this.getUserWarehose();
		this.getAccountManagerDetails();
		this.allWarehouse = [];
		this.bannerShoptomydoor = [];

		this.today = this.datePipe.transform(new Date(), 'yyyy-MM-dd');

		this.serverUrl = config.serverUrl;
		if(this.regService.item.user.referralCode)
		{
			this.referralCode = this.regService.item.user.referralCode;
			this.referralLink = this.serverUrl+'join/refer/'+this.referralCode;
		}


		setTimeout(() => {
			this.getUserWarehouseList();

		}, 1000);

		// datatable
		this.dtOptions[0] = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

		this.dtOptions[1] = {
			paging: false,
			ordering: false,
			searching: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

		localStorage.setItem('selectedWarehouse', JSON.stringify(0));
		this.userSubscription = this.regService.item.user.isSubscribed;
		this.userSubscriptionStatus = this.regService.item.user.subscription.subcscritionStatus;
		this.getSubscriptionDetails();
		this.chips = ['Javascript', 'Typescript'];

		//this.message = this.serverUrl+'join/refer/'+ this.referralCode;
		this.message = "Hello,\nI use Shoptomydoor to shop and ship from USA/UK to my door step. I can buy from Amazon, Ebay and hundreds of other stores and have items shipped at amazing prices in a few business days. \nClick on the link below to get started.\nBest Regards."
	}

	processPayeezy() {
		this.mywarehouseService.preocessPayeezy().subscribe((data: any) => {
			console.log(data);
		})
	}

	getSubscriptionDetails () {
		if(this.regService.item.user.isSubscribed == 'Y'){
			this.regService.getusersubscriptiondetail(this.regService.item.user.id).subscribe((response:any)=>{
	            if(response.status == 1)
	             this.userSubscriptionData = response.results;
	        });
		}
	}

	getAccountManagerDetails() {
            if(this.regService.item.user.subscription.representativeId){
		this.mywarehouseService.getAccountManager(this.regService.item.user.subscription.representativeId).subscribe((data: any) => {
			if (data.status == 1) {
				this.representativeDetail = data.results.userdetails;
				this.representativeImage = data.results.userimage;
				this.showRepresentative = true;
			}
			else {
				this.showRepresentative = false;
			}
		});
           } else {
				this.showRepresentative = false;
           }
	}

	openContactForm(event, firstName, lastName, email, contactno) {
		this.bsModalRef = this.modalService.show(ContactFormModalComponent, {
			class: 'modal-lg modal-dialog-centered'

		});

		this.bsModalRef.content.firstName = firstName;
		this.bsModalRef.content.lastName = lastName;
		this.bsModalRef.content.email = email;
		this.bsModalRef.content.contactno = contactno;
		this.bsModalRef.content.userId = this.userId;
		this.bsModalRef.content.closeBtnName = 'Close';

		this.modalService.onHide.subscribe(() => {
			this.spinner.show();

			if (this.bsModalRef.content.isPhotoShotDisabled == 'Y') {

			} else {

			}

			window.location.reload();
		});
	}

	getUserAddressInfo() {
		this.regService.userShippingAddress('shipping').subscribe((data: any) => {
			if (data.status == 1) {
				this.userShippingAddress = data.results;
				this.locationStatus = data.locationStatus;
			}
		});
	}

	getRewardDetails() {
		this.content.getRewardDetails(this.userId).subscribe((data: any) => {
			this.earnedPoints = data.results.earnedPoints;
			this.rewardName = data.results.rewardName;
			this.expString = data.results.expStringShort;
		});
	}

	getUserWarehose() {
		this.content.getUserWarehose(this.regService.item.user.id).subscribe((data:any) => {
			let index = 0;
			//console.log(JSON.parse(localStorage.getItem('selectedWarehouse')));
			if(data.status == 1)
			{
				for(let eachWarehouse of data.results) {
					this.allWarehouse.push(eachWarehouse);
					if(index == JSON.parse(localStorage.getItem('selectedWarehouse')))
					{
						this.selectedWarehouseDetails = eachWarehouse;
					}
					index++;
				}
				//console.log(this.selectedWarehouseDetails);
			}

			for (let bannerImage of data.banner) {
				this.bannerShoptomydoor.push({ 'imagePath': bannerImage.imagePath, 'link': bannerImage.pageLink });
			}
			this.selectedWarehouse = JSON.parse(localStorage.getItem('selectedWarehouse'));
		});
	}

	getAllWarehouse() {
		this.content.allWarehouse().subscribe((data: any) => {
			let index = 0;
			if (data.status == 1) {
				for (let eachWarehouse of data.results) {
					this.allWarehouse.push(eachWarehouse);
					if (index == JSON.parse(localStorage.getItem('selectedWarehouse'))) {
						this.selectedWarehouseDetails = eachWarehouse;
					}
					index++;
				}

				for (let bannerImage of data.banner) {
					this.bannerShoptomydoor.push({ 'imagePath': bannerImage.imagePath, 'link': bannerImage.pageLink });
				}
				/*if(data.results.warehouseBannerImage!='')
				{
					this.bannerImage = data.banner;
				}
				else{
					this.bannerImage = "";
				}*/
			}
			this.selectedWarehouse = JSON.parse(localStorage.getItem('selectedWarehouse'));
		});
	}

	selectWarehose(event) {
		let index = 0;
		var selectedDrop = event.target.value;
		localStorage.setItem('selectedWarehouse', JSON.stringify(selectedDrop));
		this.selectedWarehouse = selectedDrop;
		for (let eachWarehouse of this.allWarehouse) {
			if (index == selectedDrop) {
				this.selectedWarehouseDetails = eachWarehouse;
			}
			index++;
		}

		//this.getUserWarehouseList();


		let el = document.getElementById('warehouseAddress');
		el.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'end' });
	}

	showWarehouseAddress() {
		this.bsModalRef = this.modalService.show(WarehouseModalComponent, {});
		this.bsModalRef.content.warehouseAddress = this.selectedWarehouseDetails;
		this.bsModalRef.content.closeBtnName = 'Close';
	}


	getUserWarehouseList() {
		this.spinner.show();
		this.mywarehouseService.getUserMyShipmentList(this.selectedWarehouseSearch,this.selectedStatusSearch,this.currentPage,this.pageSize,'').subscribe((data: any) => {

			if (data.shipments) {
				this.warehouseShipmentList = data.shipments;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;
				this.spinner.hide();
				this.dtTrigger.next();

			} else {
				this.warehouseShipmentList = [];
			}
		});

	}

	pageChanged($event)  {

		this.spinner.show();
		this.mywarehouseService.getUserMyShipmentList(this.selectedWarehouseSearch,this.selectedStatusSearch,$event,this.pageSize,'').subscribe((data: any) => {
			if (data.shipments) {
				this.warehouseShipmentList = data.shipments;
				this.currentPage = $event;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;
				this.spinner.hide();

			} else {
				this.warehouseShipmentList = [];
			}
		});
	}

	perPageChanged($event)  {

		this.spinner.show();
		this.mywarehouseService.getUserMyShipmentList(this.selectedWarehouseSearch,this.selectedStatusSearch,1,this.pageSize,'').subscribe((data: any) => {
			if (data.shipments) {
				this.warehouseShipmentList = data.shipments;
				this.currentPage = $event;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;
				this.spinner.hide();

			} else {
				this.warehouseShipmentList = [];
			}
		});
	}

	enableRange(event) {
		if (event === 'FR') {
			this.isPeriodDisabled = true;
		} else {
			this.isPeriodDisabled = false;
		}
	}

	onSubmit(form: NgForm) {
		console.log(form.value);
		var shipmentDateSearch = {'orderDate':form.value.orderDate,'rangeDate':form.value.rangeDate};
		this.spinner.show();
		this.mywarehouseService.getUserMyShipmentList(this.selectedWarehouseSearch,this.selectedStatusSearch,this.currentPage,this.pageSize,shipmentDateSearch).subscribe((data: any) => {
			this.spinner.hide();
			if (data.shipments) {
				this.warehouseShipmentList = data.shipments;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;

			} else {
				this.warehouseShipmentList = [];
			}
		});
	}
	
	onRefer(form : NgForm) {		
		//console.log(form.value);
		let error = 0;
		let submittedUser = form.value.users;
		for(let eachuser of submittedUser)
		{
			if(!this.checkEmailValid(eachuser.value))
				error = 1;
		}
		if(error == 0)
		{
			this.spinner.show()
			this.formSubmit = true; 
			this.regService.sendInivitation(form.value).subscribe((data:any) => {
				this.formSubmit = false;
				this.spinner.hide();
				if(data.status == 1)
				{
					
					form.resetForm();
					this.toastr.success("Invitations sent successfully");
				}
				else
				{
					this.toastr.error("Something went wrong")
					
				}
			});
		}
		else
		{
			this.toastr.error('Please enter valid email')
		}
	}
	checkEmailValid(email) {
		let reg = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
		return reg.test(email);
	}

	private validateAsync(control: FormControl): Promise<any> {
        return new Promise(resolve => {
            const value = control.value;
            let reg = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
            const result: any = !reg.test(value) ? {
                email: true
            } : null;

            setTimeout(() => {
                resolve(result);
            }, 400);
        });
    }

    public asyncErrorMessages = {
        email: 'Please add valid email id'
    };

    public asyncValidators = [this.validateAsync];
	

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		this.checkWindowSize(event.target.innerWidth);
	}



	hack(value) {

		if (value != undefined && value != null) {
			return Object.values(value);
		}

	}

	clickTofloat(){
		this.isFloating = !this.isFloating;       
	}

	clickTofloat2(){
		this.isFloating2 = !this.isFloating2; 
	}

	fb(e) {
    let url = 'www.facebook.com';
    e.preventDefault();
    var facebookWindow = window.open(
      'https://www.facebook.com/sharer/sharer.php?url='+ 'Use Referral code ->' + this.referralCode + '   to join the newest fantasy sports platform' +  url + '. Earn extra points along with lots of exciting prizes. Don’t delay, join the match today! url',
      'facebook-popup',
      'height=350,width=600'
    );
    if (facebookWindow.focus) {
      facebookWindow.focus();
    }
    return false;
  }
}
