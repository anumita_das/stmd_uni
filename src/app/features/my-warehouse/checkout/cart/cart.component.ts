import { Component, OnInit } from '@angular/core';
import { MywarehouseService } from '../../../../services/mywarehouse.service';
import { Router} from "@angular/router";
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { CurrencyModalComponent } from '../../../../shared/components/currency-modal/currency-modal.component';
import { RegistrationService } from '../../../../services/registration.service';
import { ToastrService } from 'ngx-toastr';
import { CartSubscriptionModalComponent } from '../../../../shared/components/cart-subscription-modal/cart-subscription-modal.component';
import { SubscribeService } from '@app/shared/services/subscribe.service';

@Component({
	selector: 'app-cart',
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
	dtOptions: DataTables.Settings = {};
	bagList : Array<any> = [];
	
	cartDetails : any;
	showDiscount = false;
	bsModalRef: BsModalRef;
    exchangeRate: any;
    currencySymbol: any = '';
    defaultCurrencySymbol: any = '';
    isCurrencyChanged: any = 'N';
    couponcode: string;
    couponInValidMsg: string;
    couponInValid:boolean = false;
    blankCouponCode:boolean = true;
	personaldetails : Array<any> = [];
	user : Array<any>;
	isSubscribtionOpened: boolean = false;	
	subplan:any;
	selectedSubplan: boolean = false;
	subplanvalue: any;
	savedAmount: any;
	discountAmount:any;
	isSubscribed:any;
	salesRepresentativeId:any;

	constructor(
		private  mywarehouseService : MywarehouseService,
		private auth: RegistrationService,
		private router: Router,
		private modalService: BsModalService,
		private toastr: ToastrService,
		private subscribeService: SubscribeService,
	) {
		this.user = this.auth.item.user;
	 }

	ngOnInit() {
		// datatable
		this.dtOptions = {
			paging: false,
			ordering: false,
			searching: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

		this.getUserCartList();
		this.subplan = this.subscribeService.getsubscriptionplan();
		this.isSubscribed = this.auth.item.user.isSubscribed;
		this.salesRepresentativeId = this.auth.item.user.salesRepresentativeId;
	}

	//Fetch Cart List from Session
  	getUserCartList() {
  		const usercart = JSON.parse(localStorage.getItem('userWarehouseCart'));
  		this.cartDetails = usercart;
  		this.isCurrencyChanged = usercart.isCurrencyChanged;
  		this.currencySymbol = usercart.currencySymbol;
  		this.defaultCurrencySymbol = usercart.defaultCurrencySymbol;
  		this.exchangeRate = usercart.exchangeRate;
	}


	showCurrency() {
		
		const usercart = JSON.parse(localStorage.getItem('userWarehouseCart'));
		const currencyCode = usercart.currencyCode;

		this.mywarehouseService.getCurrencyList(currencyCode).subscribe((response:any) => {
			this.bsModalRef = this.modalService.show(CurrencyModalComponent, {});
			this.bsModalRef.content.currencyList = response;
		});

	}

	addInsurance($event) {
		const usercart = JSON.parse(localStorage.getItem('userWarehouseCart'));

		if($event.target.checked == true){
			this.cartDetails['isInsuranceCharged']  = 'Y';
			this.cartDetails['totalCost'] = parseFloat(this.cartDetails['totalInsurance'])+parseFloat(this.cartDetails['totalCost']);	
		} else {
			this.cartDetails['isInsuranceCharged'] = 'N';	
			this.cartDetails['totalCost'] = parseFloat(this.cartDetails['totalCost'])-parseFloat(this.cartDetails['totalInsurance']);
		}

		//Reassign Storage Data
	    localStorage.setItem('userWarehouseCart', JSON.stringify(this.cartDetails));
	}

	checkout() {
		this.router.navigate(['/my-warehouse','checkout','personal-detail']);
	}

	clearcart() {
		localStorage.removeItem('userWarehouseCart');
		this.router.navigate(['/my-warehouse','overview']);
	}

	hack(value) {
	   return Object.values(value)
	}


	checkEmptyCouponInput(){
		if(this.couponcode == ''){
			this.blankCouponCode = true;
		} else {
			this.blankCouponCode = false;
		}
	}

	showSubscribtion(){
		this.bsModalRef = this.modalService.show(CartSubscriptionModalComponent, {class: 'modal-lg'});
		this.isSubscribtionOpened = true;

		this.modalService.onHide.subscribe(() => {
			const usercart = JSON.parse(localStorage.getItem('userWarehouseCart'));
			this.cartDetails = usercart;
			this.subplan = this.subscribeService.getsubscriptionplan();
			
			if(this.subplan)
			{
				this.selectedSubplan = true;
				this.subplanvalue = this.subplan.selectedPlan.amount;
				const baseAmount = this.subplan.baseAmount;

				//this.discountAmount = Math.ceil((((baseAmount*this.subplan.selectedPlan.duration)-this.subplan.selectedPlan.amount) / (baseAmount*this.subplan.selectedPlan.duration))*100);
				this.discountAmount = Math.ceil(10.00);

				if(this.discountAmount>0)
				{
					this.savedAmount = ((this.cartDetails.shippingCost*this.exchangeRate)*(this.discountAmount/100));
				}else{
					this.savedAmount = 0;
				}
				this.cartDetails['subscriptionAmount'] = this.subplan.selectedPlan.amount;
				this.cartDetails['savedAmount'] = this.savedAmount;
				this.cartDetails['subscriptionPlan'] = this.subplan.selectedPlan;
				this.cartDetails['totalDiscount'] = this.savedAmount;
				

				localStorage.setItem('userWarehouseCart', JSON.stringify(this.cartDetails));

			}else{
				this.selectedSubplan = false;
			}
		
		});
	}

	hideSubscribtion(){
		this.isSubscribtionOpened = false;
		
	}
}
