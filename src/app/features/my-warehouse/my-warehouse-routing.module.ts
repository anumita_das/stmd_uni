import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyWarehouseComponent } from './my-warehouse.component';
import { OverviewComponent } from './overview/overview.component';
import { MyshipmentsComponent } from './my-shipments/my-shipments.component';
import { CartComponent } from './checkout/cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { PersonalDetailComponent } from './checkout/personal-detail/personal-detail.component';
import { ShippingAndPaymentComponent } from './checkout/shipping-and-payment/shipping-and-payment.component';
import { PlaceOrderComponent } from './checkout/place-order/place-order.component';
import { PaymentComponent } from './payment/payment.component';

// const routes: Routes = [
// 	{ path: '', component: MyWarehouseComponent }
// ];

const routes: Routes = [
	{
		path: '', redirectTo: 'overview', pathMatch: 'full'
	},
	{
		path: '',
		component: MyWarehouseComponent,
		children: [
			{ path: 'overview', component: OverviewComponent },
			{ path: 'overview/:shipmentId', component: OverviewComponent },
			{ path: 'my-shipments', component: MyshipmentsComponent },
			{
				path: 'checkout',
				component: CheckoutComponent,
				children: [
					{ path: '', redirectTo: 'cart', pathMatch: 'full' },
					{ path: 'cart', component: CartComponent },
					{ path: 'personal-detail', component: PersonalDetailComponent },
					{ path: 'shipping-and-payment', component: ShippingAndPaymentComponent },
					{ path: 'place-order', component: PlaceOrderComponent }
				]
			},
			{ path: 'payment', component: PaymentComponent },
		]
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class MyWarehouseRoutingModule {

}
