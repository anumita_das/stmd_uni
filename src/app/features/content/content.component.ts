import { Component, OnInit, Renderer2 } from '@angular/core';
import { ServiceModalComponent } from '../../shared/components/service-modal/service-modal.component';
import { ContentService } from '../../services/content.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { ActivatedRoute,Router } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
import { NgxJsonLdModule } from '@ngx-lite/json-ld';

@Component({
	selector: 'app-content',
	templateUrl: './content.component.html',
	styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
	pageContent: any;
	sub: any;
	href: any;
	schema: any;
	contentFound : boolean = true;

	constructor(
		private renderer: Renderer2,
		private contentService: ContentService,
		private sanitizer: DomSanitizer,
		private route: ActivatedRoute,
		private meta: Meta,
		private title: Title,
		private router: Router,
	) { }


	

	ngOnInit() {
		this.sub = this.route.params.subscribe(params => {
			this.href = params['page'];
			this.getPageContent();
		});
		
	}

	getSafeHTML(value: {}) {
    const json = JSON.stringify(value, null, 2);
    const html = `${json}`;
    // Inject to inner html without Angular stripping out content
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }

	getPageContent() {
		this.contentService.pageContent(this.href).subscribe((data: any) => {

			if (data.status == 1) {
				this.contentFound = true;
				let content = data.pageContent[0];

				this.title.setTitle(content.pageTitle);
				this.meta.updateTag({
					name: 'keywords', content: content.metaKeyword
				});
				this.meta.updateTag({
					name: 'description', content: content.metaDescription
				});
				const schema = {
				    '@context': 'http://schema.org',	    
					"@id": "https://www.shoptomydoor.com/#organization",
					"@type": "Organization",
					"name": "Shop To My Door ",
					"url": "https://www.shoptomydoor.com/",
					"logo": "https://www.shoptomydoor.com/assets/imgs/logo.png",
					"contactPoint": [
						{
							"@type": "ContactPoint",
							"telephone": "(+234) 0700 800 8000", 
							"contactType": " Sales & Logistics ",
							"areaServed": "Nigeria"
						},
						{
							"@type": "ContactPoint",
							"telephone": "(+234) 0809 092 0297", 
							"contactType": "Sales & Logistics",
							"areaServed": "Nigeria"
						},
						{
							"@type": "ContactPoint",
							"telephone": "(+234) 0808 074 1207", 
							"contactType": " Sales & Logistics ",
							"areaServed": "Nigeria"
						}			
					],
					"sameAs": [
						"https://www.facebook.com/shoptomydoor/",
						"https://www.linkedin.com/company/american-airsea-cargo",
						"https://twitter.com/shop2mydoor/",
						"https://www.instagram.com/shop2mydoor/"
					]
			  	};


				this.pageContent = this.sanitizer.bypassSecurityTrustHtml(content.pageContent);
				this.schema = this.getSafeHTML(schema);
			} else {
				this.pageContent = "";
				this.contentFound = false;
			}

                        },error => {
                      if(error == '404')
                      {
                        this.pageContent = "";
                                this.contentFound = false;
                      }
                      //this.spinnerProgress = false;
		});
	}


}
