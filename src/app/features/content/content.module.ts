import { NgModule } from '@angular/core';
import { ContentRoutingModule } from './content-routing.module';
import { ContentComponent } from './content.component';
import { SharedModule } from '@app/shared';
import { BrowserModule } from '@angular/platform-browser';
import { NgxJsonLdModule } from '@ngx-lite/json-ld';

@NgModule({
  imports: [
    ContentRoutingModule,
    SharedModule,
    NgxJsonLdModule
  ],
  declarations: [
  	ContentComponent,
  ]
})
export class ContentModule { }
