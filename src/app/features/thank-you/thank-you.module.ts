import { NgModule } from '@angular/core';
import { ThankYouRoutingModule } from './thank-you-routing.module';
import { ThankYouComponent } from './thank-you.component';
import { SharedModule } from '@app/shared';


@NgModule({
	imports: [
		ThankYouRoutingModule,
		SharedModule
	],
	declarations: [
		ThankYouComponent
	]
})
export class ThankYouModule {

}
