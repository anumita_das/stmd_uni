import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, ParamMap } from '@angular/router';

@Component({
	selector: 'app-thank-you',
	templateUrl: './thank-you.component.html',
	styleUrls: ['./thank-you.component.scss']
})
export class ThankYouComponent implements OnInit {

	showDiv : boolean;
	showMsg : boolean;
	flag : any;

	constructor(
	private route: ActivatedRoute,
	) { }
	socialIcons: Array<object>;

	

	ngOnInit() {
		this.showDiv = false;
		this.showMsg = false;

		if(this.route.snapshot.paramMap.get('flag'))
		{
			this.flag = this.route.snapshot.paramMap.get('flag');
			if(this.flag == '1')
			{
				this.showDiv = true;
				this.showMsg = false;
			}else{
				this.showDiv = false;
				this.showMsg = true;
			}
		}
		this.socialIcons = [
			{
				name: 'facebook',
				bg: '#3d5a98',
				link: 'https://www.facebook.com/ShipToNaija/'
			},
			{
				name: 'instagram',
				bg: '#ff8832',
				link: 'https://www.instagram.com/shiptonaija/'
			},
			{
				name: 'twitter',
				bg: '#4ec8f5',
				link: 'https://twitter.com/shiptonaija/'
			}
		];
	}



}
