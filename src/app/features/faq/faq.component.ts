import { Component, OnInit } from '@angular/core';
import { ContentService } from '../../services/content.service';
import { Router, ActivatedRoute, Params, ParamMap } from '@angular/router';
import { AppGlobals } from '../../app.globals';
import { Meta, Title } from '@angular/platform-browser';

@Component({
	selector: 'app-faq',
	templateUrl: './faq.component.html',
	styleUrls: ['./faq.component.scss'],
	providers: [AppGlobals]
})
export class FAQComponent implements OnInit {
	oneAtATime: boolean;
	isFirstOpen: boolean;
	activationMsg : string;
	getFaqs = [];
	constructor(
		private contentService:ContentService,
		private route: ActivatedRoute,
		private _global: AppGlobals,
		private meta: Meta, 
		private title: Title
		) {
			title.setTitle(this._global.siteTitle);
		    meta.addTags([ 
		      {
		        name: 'keywords', content: this._global.siteMetaKeywords
		      },
		      {
		        name: 'description', content: this._global.siteMetaDescription
		      },
		    ]);
		 }

	ngOnInit() {
		// accordion
		this.isFirstOpen = true;
		this.oneAtATime = true;
		this.activationMsg = 'Checking Data. Please wait';
		this.route.params.subscribe(params => {
      			this.getFaq();
    	});
	}

	getFaq(){
		this.contentService.getFaq().subscribe((data:any) => { 
			this.getFaqs = data.results.Faq;
		});
	}

}
