import { NgModule } from '@angular/core';
import { SubscriptionPlanRoutingModule } from './subscription-plan-routing.module';
import { SubscriptionPlanComponent } from './subscription-plan.component';
import { SharedModule } from '@app/shared';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
	imports: [
		SubscriptionPlanRoutingModule,
		SharedModule,
		LazyLoadImageModule,
	],
	declarations: [
		SubscriptionPlanComponent
	]
})
export class SubscriptionPlanModule {

}
