import { Component, OnInit, HostListener } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ServiceModalComponent } from '../../shared/components/service-modal/service-modal.component';
import { AppGlobals } from '../../app.globals';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { SubscriptionPlanService } from '@app/shared/services/subscription-plan.service';


@Component({
	selector: 'app-subscription-plan',
	templateUrl: './subscription-plan.component.html',
	styleUrls: ['./subscription-plan.component.scss'],
	providers: [AppGlobals]
})
export class SubscriptionPlanComponent implements OnInit {

	isFirstOpen: boolean;
	oneAtATime: boolean;
	bsModalRef: BsModalRef;
	dtOptions: DataTables.Settings = {};
	featureList: any;
	show: boolean;
	isMediumScreen: boolean;

	constructor(
		private modalService: BsModalService,
		public spinner: NgxSpinnerService,
		public router: Router,
		public route: ActivatedRoute,
		private featureListData: SubscriptionPlanService
	) { }

	ngOnInit() {
		this.spinner.hide();

		// accordion
		this.isFirstOpen = true;
		this.oneAtATime = true;



		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

		this.featureList = this.featureListData.getData(5);
		// const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		// if (width < 992) {
		// 	this.featureList = this.featureListData.getData();
		// }
	}

	openServiceModal(content) {
		this.bsModalRef = this.modalService.show(ServiceModalComponent, {});
		this.bsModalRef.content.serviceContent = content;
		this.bsModalRef.content.closeBtnName = 'Close';
	}
	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	isLargeScreen() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		if (width > 992) {
			return true;
		} else {
			return false;
		}
	}

	showHideFeature() {
		console.log('aaaa');
		this.featureList = this.featureListData.getData();
	}
}
