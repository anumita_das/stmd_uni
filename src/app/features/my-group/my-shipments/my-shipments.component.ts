import { Component, OnInit, NgZone, HostListener } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { MywarehouseService } from '../../../services/mywarehouse.service';
import { RegistrationService } from '../../../services/registration.service';
import { ContentService } from '../../../services/content.service';
import { AppGlobals } from '../../../app.globals';
import { ToastrService } from 'ngx-toastr';
import { Router } from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { WarehouseModalComponent } from '../../../shared/components/warehouse-modal/warehouse-modal.component';
import { DatePipe } from '@angular/common';
import { ContactFormModalComponent } from '../../../shared/components/contactform-modal/contactform-modal.component';
import { Subject } from 'rxjs';
import 'rxjs/add/operator/map';

@Component({
	selector: 'app-my-group-shipment-overview',
	templateUrl: './my-shipments.component.html',
	styleUrls: ['./my-shipments.component.scss'],
	providers: [DatePipe]
})
export class MyshipmentsComponent implements OnInit {
	isMediumScreen: boolean;
	sidebar: boolean;
	isCollapsed: boolean;
	bsModalRef: BsModalRef;
	dtOptions: DataTables.Settings = {};
	showShippingCost: boolean;
	shippingTypes: Array<any>;
	warehouseShipmentList: Array<any> = [];
	showShipping: boolean = false;
	userId: number;
	defaultCurrencySymbol: any;
	shipment: any;
	shippingMethodList: Array<any>;
	allWarehouse: Array<object>;
	selectedWarehouse: number;
	selectedWarehouseSearch : any = '';
	selectedWarehouseDetails: any;
	defaultProfile: string;
	userShippingAddress: Array<object>;
	earnedPoints: any;
	rewardName: any;
	expString: any;
	showCalculateBtn: boolean = false;
	today: any;
	bannerImage: any;
	bannerShoptomydoor: Array<object>;
	enableCheckout: boolean = false;
	totalShipingValue: any;
	showInventorymsg: boolean = false;
	deliverySection: Array<any> = [false];
	deliveryData: Array<any> = [];
	showShippingMethod: Array<any> = [false];
	closeDelivery: Array<any> = [false];
	showSection: Array<any> = [];
	showRepresentative: boolean = false;
	representativeDetail: any;
	representativeImage: any;
	userSubscription : any = "";
	userSubscriptionData : any = "";
	userSubscriptionStatus : any = "";
	groupMemberRole : any = "";
	groupId : any = "";
	perPage : any = 10;
	currentPage : number = 1;
	pageSize : number;
	totalItems : number;
	orderDate : any = 'AD';
	rangeDate : any = '';
	isPeriodDisabled : boolean = false;
	dtTrigger: Subject<any> = new Subject();

	constructor(
		private ngZone: NgZone,
		private modalService: BsModalService,
		public mywarehouseService: MywarehouseService,
		public regService: RegistrationService,
		private content: ContentService,
		private _global: AppGlobals,
		private toastr: ToastrService,
		private spinner: NgxSpinnerService,
		private router: Router,
		private datePipe: DatePipe
	) {
		this.userId = this.regService.item.user.id;
		this.defaultCurrencySymbol = this._global.defaultCurrencySymbol;
		this.totalShipingValue = "$0.00";

		localStorage.removeItem('userWarehouseCart');
	}

	ngOnInit() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		this.checkWindowSize(width);

		// account isCollapsed
		this.isCollapsed = false;

		this.defaultProfile = JSON.parse(localStorage.getItem('profileImage'));
		this.getAccountManagerDetails();
		this.getUserAddressInfo();
		this.getRewardDetails();
		//this.getAllWarehouse();
		this.getUserWarehose();
		this.allWarehouse = [];
		this.bannerShoptomydoor = [];

		this.today = this.datePipe.transform(new Date(), 'yyyy-MM-dd');

		setTimeout(() => {
			this.getUserWarehouseList();

		}, 1000);


		this.dtOptions = {
			paging: false,
			ordering: false,
			searching: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};
	}

	processPayeezy() {
		this.mywarehouseService.preocessPayeezy().subscribe((data: any) => {
			console.log(data);
		})
	}


	openContactForm(event, firstName, lastName, email, contactno) {
		this.bsModalRef = this.modalService.show(ContactFormModalComponent, {
			class: 'modal-lg modal-dialog-centered'

		});

		this.bsModalRef.content.firstName = firstName;
		this.bsModalRef.content.lastName = lastName;
		this.bsModalRef.content.email = email;
		this.bsModalRef.content.contactno = contactno;
		this.bsModalRef.content.userId = this.userId;
		this.bsModalRef.content.closeBtnName = 'Close';

		this.modalService.onHide.subscribe(() => {
			this.spinner.show();

			if (this.bsModalRef.content.isPhotoShotDisabled == 'Y') {

			} else {

			}

			window.location.reload();
		});
	}

	getUserAddressInfo() {
		this.regService.userGroupShippingAddress('shipping').subscribe((data: any) => {
			if (data.status == 1) {
				this.userShippingAddress = data.results;
			}
		});
	}

	getAccountManagerDetails() {
        //if(this.regService.item.user.subscription.representativeId) {
			this.mywarehouseService.getGroupManager().subscribe((data: any) => {
				if (data.status == 1) {
					this.representativeDetail = data.results.userdetails;
					this.representativeImage = data.results.userimage;
					this.showRepresentative = true;
				}
				else {
					this.showRepresentative = false;
				}
			});
        /*} else {
				this.showRepresentative = false;
        }*/
	}

	getRewardDetails() {
		this.content.getRewardDetails(this.userId).subscribe((data: any) => {
			this.earnedPoints = data.results.earnedPoints;
			this.rewardName = data.results.rewardName;
			this.expString = data.results.expStringShort;
		});
	}

	getUserWarehose() {
		this.content.getUserWarehose(this.userId).subscribe((data:any) => {
			let index = 0;
			//console.log(JSON.parse(localStorage.getItem('selectedWarehouse')));
			if(data.status == 1)
			{
				for(let eachWarehouse of data.results) {
					this.allWarehouse.push(eachWarehouse);
					if(index == JSON.parse(localStorage.getItem('selectedWarehouse')))
					{
						this.selectedWarehouseDetails = eachWarehouse;
					}
					index++;
				}
				//console.log(this.selectedWarehouseDetails);
			}

			for (let bannerImage of data.banner) {
				this.bannerShoptomydoor.push({ 'imagePath': bannerImage.imagePath, 'link': bannerImage.pageLink });
			}
			this.selectedWarehouse = JSON.parse(localStorage.getItem('selectedWarehouse'));
		});
	}

	getAllWarehouse() {
		this.content.allWarehouse().subscribe((data: any) => {
			let index = 0;
			if (data.status == 1) {
				for (let eachWarehouse of data.results) {
					this.allWarehouse.push(eachWarehouse);
					if (index == JSON.parse(localStorage.getItem('selectedWarehouse'))) {
						this.selectedWarehouseDetails = eachWarehouse;
					}
					index++;
				}

				for (let bannerImage of data.banner) {
					this.bannerShoptomydoor.push({ 'imagePath': bannerImage.imagePath, 'link': bannerImage.pageLink });
				}
				/*if(data.results.warehouseBannerImage!='')
				{
					this.bannerImage = data.banner;
				}
				else{
					this.bannerImage = "";
				}*/
			}
			this.selectedWarehouse = JSON.parse(localStorage.getItem('selectedWarehouse'));
		});
	}

	selectWarehose(event) {
		let index = 0;
		var selectedDrop = event.target.value;
		localStorage.setItem('selectedWarehouse', JSON.stringify(selectedDrop));
		this.selectedWarehouse = selectedDrop;
		for (let eachWarehouse of this.allWarehouse) {
			if (index == selectedDrop) {
				this.selectedWarehouseDetails = eachWarehouse;
			}
			index++;
		}

		//this.getUserWarehouseList();


		//let el = document.getElementById('warehouseAddress');
		//el.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'end' });
	}

	showWarehouseAddress() {
		this.bsModalRef = this.modalService.show(WarehouseModalComponent, {});
		this.bsModalRef.content.warehouseAddress = this.selectedWarehouseDetails;
		this.bsModalRef.content.closeBtnName = 'Close';
	}

	getUserWarehouseList() {
		this.spinner.show();

		this.regService.getGroupInfo().subscribe((groupData:any) => {

			if(groupData.status == 1) {
				if(groupData.results.groupInfo.coordinatorId == this.userId)
					this.groupMemberRole = 'coordinator';
				else
					this.groupMemberRole = 'member';

				this.groupId = groupData.results.groupInfo.id;
			}

			this.mywarehouseService.getUserGroupAllShipmentList(this.selectedWarehouseSearch,this.currentPage,this.pageSize,'').subscribe((data: any) => {
				this.spinner.hide();
				if (data.shipments) {
					this.warehouseShipmentList = data.shipments;

					var array = this.hack(this.warehouseShipmentList);
					this.totalItems = data.totalItems;
					this.pageSize= data.itemsPerPage;
					this.spinner.hide();
					this.dtTrigger.next();

				} else {
					this.warehouseShipmentList = [];
				}
			});

		});

	}

	pageChanged($event)  {

		this.spinner.show();
		this.mywarehouseService.getUserGroupAllShipmentList(this.selectedWarehouseSearch,$event,this.pageSize,'').subscribe((data: any) => {
			if (data.shipments) {
				this.warehouseShipmentList = data.shipments;
				this.currentPage = $event;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;
				this.spinner.hide();

			} else {
				this.warehouseShipmentList = [];
			}
		});
	}

	perPageChanged($event)  {

		this.spinner.show();
		this.mywarehouseService.getUserGroupAllShipmentList(this.selectedWarehouseSearch,1,this.pageSize,'').subscribe((data: any) => {
			if (data.shipments) {
				this.warehouseShipmentList = data.shipments;
				this.currentPage = $event;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;
				this.spinner.hide();

			} else {
				this.warehouseShipmentList = [];
			}
		});
	}

	onSubmit(form: NgForm) {
		console.log(form.value);
		var shipmentDateSearch = {'orderDate':form.value.orderDate,'rangeDate':form.value.rangeDate};
		this.spinner.show();
		this.mywarehouseService.getUserGroupAllShipmentList(this.selectedWarehouseSearch,this.currentPage,this.pageSize,shipmentDateSearch).subscribe((data: any) => {
			this.spinner.hide();
			if (data.shipments) {
				this.warehouseShipmentList = data.shipments;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;

			} else {
				this.warehouseShipmentList = [];
			}
		});
	}

	enableRange(event) {
		if (event === 'FR') {
			this.isPeriodDisabled = true;
		} else {
			this.isPeriodDisabled = false;
		}
	}
	

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		this.checkWindowSize(event.target.innerWidth);
	}

	
	hack(value) {

		if (value != undefined && value != null) {
			return Object.values(value);
		}
	}

}
