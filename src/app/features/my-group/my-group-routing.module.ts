import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GroupInfoComponent } from './group-info/group-info.component';
import { GroupshipmentOverviewComponent } from './overview/groupshipment-overview.component';
import { CommissiondetailsComponent } from './commission-details/commission-details.component';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { MyshipmentsComponent } from './my-shipments/my-shipments.component';

const routes: Routes = [
	{ path: 'group-info', component: GroupInfoComponent },
	{ path: 'group-shipment', component: GroupshipmentOverviewComponent },
	{ path: 'group-shipment/:shipmentId', component: GroupshipmentOverviewComponent },
	{ path: 'commission-details', component: CommissiondetailsComponent },
	{ path: 'order-history', component: OrderHistoryComponent },
	{ path: 'my-shipments', component: MyshipmentsComponent },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class MyGroupRoutingModule {

}