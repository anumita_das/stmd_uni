import { Component, OnInit } from '@angular/core';
import { MywarehouseService} from '../../../services/mywarehouse.service';
import { RegistrationService } from '../../../services/registration.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.scss']
})
export class OrderHistoryComponent implements OnInit {

	userId : any = '';
	warehouseShipmentList: Array<any> = [];
  dtOptions: DataTables.Settings = {};
  isMediumScreen: boolean;
  	constructor(
	  	private mywarehouseService : MywarehouseService,
	  	private regService : RegistrationService,
      private spinner: NgxSpinnerService,
  	) { }

  	ngOnInit() {
      const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
      this.checkWindowSize(width);
  		this.userId = this.regService.item.user.id;
  		this.groupShipmentHistory();
      // datatable
      this.dtOptions = {
        paging: false,
        // bInfo: false,
        ordering: false,
        searching: false,
        // bSort: false,
        autoWidth: false,
        dom: '<"table-responsive"t>'
      };
  	}

    checkWindowSize(width) {
      if (width >= 992) {
        this.isMediumScreen = false;
      } else {
        this.isMediumScreen = true;
      }
    }

  	groupShipmentHistory() {
      this.spinner.show();
  		this.mywarehouseService.getgroupshipmenthistory().subscribe((data:any) => {
        this.spinner.hide();
  			if(data.status == '1') {
  				this.warehouseShipmentList = data.results;
  			} else {
          this.warehouseShipmentList = []
        }
  		});
  	}

    hack(value) {

      if (value != undefined && value != null) {
        return Object.values(value);
      }

    }

}
