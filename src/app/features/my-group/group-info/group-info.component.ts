import { Component, OnInit, HostListener } from '@angular/core';
import { NgForm }   from '@angular/forms';
import { Router, ActivatedRoute, Params, ParamMap } from '@angular/router';
import { AppGlobals } from '../../../app.globals';
import { Meta, Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response } from '@angular/http';
import { Observable,BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

import { ContentService } from '../../../services/content.service';
import { RegistrationService } from '../../../services/registration.service';
import { MywarehouseService } from '../../../services/mywarehouse.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ChangeCoordinatorComponent } from '../../../shared/components/change-coordinator/change-coordinator.component';
import { ContactFormModalComponent } from '../../../shared/components/contactform-modal/contactform-modal.component';

@Component({
  selector: 'app-group-info',
  templateUrl: './group-info.component.html',
  styleUrls: ['./group-info.component.scss']
})
export class GroupInfoComponent implements OnInit {

	isMediumScreen: boolean;
	showGrouprequestForm : boolean = false;
	formSubmitted : boolean = false;
	countryList : any;
	stateList : any;
	cityList : any;
	groupDetails : any;
	userEmails : any = [];
	groupId : any = '';
	userId : any = '';
	groupMembers : any;
	nonRegisteredMembers : any;
	dtOptions : any;
	isAccepted : any = '';
	showRepresentative: boolean = false;
	representativeDetail: any;
	representativeImage: any;
	bsModalRef: BsModalRef;
	users : any;
	message : any;
	formSubmit : boolean = false;

  	constructor(

  		private route: ActivatedRoute,
		private _global: AppGlobals,
		private meta: Meta, 
		private title: Title,
		private toastr: ToastrService,
		public spinner : NgxSpinnerService,
		public router: Router,
		private contentService:ContentService,
		private regService : RegistrationService,
		private http: HttpClient,
		public mywarehouseService: MywarehouseService,
		private modalService: BsModalService,
  	) { }

  	ngOnInit() {

  		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	  	this.checkWindowSize(width);
	  	//this.getUserEmailsForInvitation();
	  	this.getAccountManagerDetails();
	  	this.getGroupDetails();
	  	this.userId = this.regService.item.user.id;

	  	// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

	}

  	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		// console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	openGrouprequestForm() {
		this.showGrouprequestForm = true;
	}

	getAllCountry(){
		this.contentService.allCountry().subscribe((data:any)=>{
			this.countryList = data;
		});
	}

	getStateCity(event){
		let selectedCountry = event.target.value;
		this.contentService.stateCityOfCountry(selectedCountry).subscribe((data:any) => {
			if(data.stateList)
			{
				this.stateList = data.stateList;
				this.cityList = [];
				this.cityList = data.cityList;
			}
		});
	}

	getCity(event) {
		let selectedState = event.target.value;
		this.contentService.cityOfStates(selectedState).subscribe((data:any) => {
			if(data.cityList)
			{
				this.cityList = data.cityList;
			}
		});
	}

	onSubmit(form: NgForm){
		this.spinner.show();

		this.regService.saveGroupRequest(form.value).subscribe((data:any) => {
			this.spinner.hide();
			if(data.status == 0) {
				this.toastr.error(data.results);
			}
			else if(data.status == 1) {
				this.toastr.success(data.results);
				this.getGroupDetails();
				form.resetForm();
			}
		});

	}

	getGroupDetails() {
		this.spinner.show();
		this.regService.getGroupInfo().subscribe((data:any) => {
			this.spinner.hide();

			if(data.status == 1) {
				this.groupDetails = data.results.groupInfo;
				this.groupId = this.groupDetails.id;
				this.groupMembers = data.results.groupMembers;
				this.isAccepted = data.results.isAccepted;
				this.nonRegisteredMembers = data.results.notRegisteredMembers;
			}
			else {
				this.getAllCountry();
			}
		});
	}

	getAccountManagerDetails() {
		this.mywarehouseService.getGroupManager().subscribe((data: any) => {
			if (data.status == 1) {
				this.representativeDetail = data.results.userdetails;
				this.representativeImage = data.results.userimage;
				this.showRepresentative = true;
			}
			else {
				this.showRepresentative = false;
			}
		});
	}

	/*getUserEmailsForInvitation(event) {
		this.regService.getAllUserEmail().subscribe((data:any) => {
			if(data.status == 1) {
				this.userEmails = data.results;
			}
		});
	}*/

	onTextChange(event) {

		this.regService.getAllUserEmail(event).subscribe((data:any) => {
			if(data.status == 1) {
				this.userEmails = data.results;
			}
			else
				this.userEmails = [];
		});
	}

	sendInvitation(form : NgForm) {
		this.spinner.show();
		this.formSubmit = true;
		this.regService.sendInvitation(form.value,this.groupId).subscribe((data:any) => {
			this.formSubmit = false;
			this.spinner.hide();
			if(data.status == 1) {
				this.toastr.success(data.results);
			}
			else {
				this.toastr.error(data.results);
			}
		});
	}

	updateInvitationStatus(status) {
		this.spinner.show();
		this.regService.updateInvitationStatus(status,this.groupId).subscribe((data:any) => {
			this.spinner.hide();
			if(data.status == 1) {
				this.toastr.success("Status updated successfully");
				window.location.reload();
			} else if(data.status == 0) {
				this.toastr.error(data.results);
			}

		});
	}

	changeCoordinator(groupId) {

		this.regService.allCoordinator(groupId).subscribe((data:any) => {
			if(data.status == '1') {
				this.bsModalRef = this.modalService.show(ChangeCoordinatorComponent, {});
				this.bsModalRef.content.groupId = groupId;
				this.bsModalRef.content.coordinatorList = data.results;
			} else {
				this.toastr.error(data.results);
			}
		});
	}

	openContactForm(event, firstName, lastName, email, contactno) {
		this.bsModalRef = this.modalService.show(ContactFormModalComponent, {
			class: 'modal-lg modal-dialog-centered'

		});

		this.bsModalRef.content.firstName = firstName;
		this.bsModalRef.content.lastName = lastName;
		this.bsModalRef.content.email = email;
		this.bsModalRef.content.contactno = contactno;
		this.bsModalRef.content.userId = this.userId;
		this.bsModalRef.content.closeBtnName = 'Close';

		this.modalService.onHide.subscribe(() => {
			this.spinner.show();

			if (this.bsModalRef.content.isPhotoShotDisabled == 'Y') {

			} else {

			}

			window.location.reload();
		});
	}

}
