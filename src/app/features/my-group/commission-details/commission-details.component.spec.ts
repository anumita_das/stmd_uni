import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissiondetailsComponent } from './commission-details.component';

describe('CommissiondetailsComponent', () => {
  let component: CommissiondetailsComponent;
  let fixture: ComponentFixture<CommissiondetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissiondetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissiondetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
