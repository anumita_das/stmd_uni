import { Component, OnInit, HostListener } from '@angular/core';
import { NgForm }   from '@angular/forms';
import { Router, ActivatedRoute, Params, ParamMap } from '@angular/router';
import { AppGlobals } from '../../../app.globals';
import { Meta, Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response } from '@angular/http';
import { Observable,BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

import { ContentService } from '../../../services/content.service';
import { RegistrationService } from '../../../services/registration.service';

@Component({
  selector: 'app-commission-details-info',
  templateUrl: './commission-details.component.html',
  styleUrls: ['./commission-details.component.scss']
})
export class CommissiondetailsComponent implements OnInit {

	isMediumScreen: boolean;
	showGrouprequestForm : boolean = false;
	formSubmitted : boolean = false;
	countryList : any;
	stateList : any;
	cityList : any;
	groupDetails : any;
	userEmails : any = [];
	groupId : any = '';
	userId : any = '';
	groupMembers : any;
	dtOptions : any;
	isAccepted : any = '';
	commissionInfo : any = '';
	accountInfo : any = '';

  	constructor(

  		private route: ActivatedRoute,
		private _global: AppGlobals,
		private meta: Meta, 
		private title: Title,
		private toastr: ToastrService,
		public spinner : NgxSpinnerService,
		public router: Router,
		private contentService:ContentService,
		private regService : RegistrationService,
		private http: HttpClient
  	) { }

  	ngOnInit() {

  		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	  	this.checkWindowSize(width);

	  	this.userId = this.regService.item.user.id;
	  	this.getCommissionInfo();

	  	// datatable
		this.dtOptions = {
			paging: false,
			ordering: false,
			searching: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

	}

  	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		// console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	getCommissionInfo() {
		this.spinner.show();
		this.regService.getCoordinatorCommissionInfo().subscribe((data:any) => {
			this.spinner.hide();
			this.commissionInfo = data.results.commissionInfo;
			this.accountInfo = data.results.accountInfo;
		});
	}

	onSubmit(form : NgForm) {
		this.spinner.show();
		this.regService.saveCoordinatorAccountInfo(form.value,this.userId).subscribe((data:any) => {
			this.spinner.hide();
			if(data.status == 1)
				this.toastr.success("Account Information Saved Successfully.");
			else
				this.toastr.error("Something went wrong! Please try again later.")
		});
	}

	requestForPayment(id) {

		this.spinner.show();
		this.regService.requestForCommission(id).subscribe((data:any) => {
			if(data.status == 1) {
				this.toastr.success("Payment Request Saved Successfully.");
				this.getCommissionInfo();
			}
			else
				this.toastr.error("Something went wrong! Please try again later.")
		});
	}

}
