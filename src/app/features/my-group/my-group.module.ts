import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { MyGroupRoutingModule } from './my-group-routing.module';
import { SharedModule } from '@app/shared';
import { GroupInfoComponent } from './group-info/group-info.component';
import { GroupshipmentOverviewComponent } from './overview/groupshipment-overview.component';
import { CommissiondetailsComponent } from './commission-details/commission-details.component';
import { PhotoshotModalComponent } from './overview/photoshot-modal.component';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { MyshipmentsComponent } from './my-shipments/my-shipments.component';

@NgModule({
  declarations: [
  	GroupInfoComponent,
  	GroupshipmentOverviewComponent,
  	CommissiondetailsComponent,
    PhotoshotModalComponent,
    OrderHistoryComponent,
    MyshipmentsComponent,
  ],
  imports: [
    CommonModule,
    MyGroupRoutingModule,
	  SharedModule,
    NgxPaginationModule,
  ]
})
export class MyGroupModule { }
