import { Component, OnInit, NgZone, HostListener } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute, Params, ParamMap } from '@angular/router';
import { ContentService } from '../../services/content.service';
import { DomSanitizer } from '@angular/platform-browser';
import { SubscribeService } from '@app/shared/services/subscribe.service';

@Component({
	selector: 'app-getquote',
	templateUrl: './getquote.component.html',
	styleUrls: ['./getquote.component.scss']
})
export class GetquoteComponent implements OnInit {
	isMediumScreen: boolean;
	sidebar: boolean;
	isCollapsed: boolean;
	valueAddedImg: any;
	blockValueAddedService: Array<object>;
	bannerImage : string;
	isFirstOpen: boolean;
	oneAtATime: boolean;
	isShowhide : boolean;
	dtOptions: DataTables.Settings = {};
	featureList: any;
    accountFeatureList : any;
    featuresdata : any;
    baseAmount: any;
    settings: any;
    selectedDuration: any;
	selectedAmount: any;
	customers : any;
	max: number = 5;
	isReadonly: boolean = true;
	show: boolean;
	getquoteform : any = 'getquotecomp';

	constructor(
		private ngZone: NgZone,
		public toastr : ToastrService,
		public spinner : NgxSpinnerService,
		public router: Router,
		private sanitizer:DomSanitizer,
		private contentService : ContentService,
		public route: ActivatedRoute,
		private subscribeService: SubscribeService
	) { }

	ngOnInit() {
		//this.spinner.show();
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		//console.log('Width: ' + width);
		this.checkWindowSize(width);

		// account isCollapsed
		this.isCollapsed = false;
		this.blockValueAddedService=[];
		this.customers = [];
		this.valueAddedImg = [	'free-warehouse.svg',
								'free-storage.svg',
								'free-repackaging.svg',
								'customer-care.svg',
								'online-tracking.svg',
								'split-shipping.svg',
								'free-export.svg',
								'discount-on-shipping.svg',
							];
		// accordion
		this.isFirstOpen = true;
		this.oneAtATime = true;
		this.isShowhide = true;

		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};
		this.getBanner();
        this.getSubscriptionDetails();					
		this.getHomepageBlocks();					
	}

	ngAfterViewInit() {
		this.route.fragment.subscribe((fragment: string) => {
			if(fragment){
	       	  this.scroll(fragment);
			}
	    });
	}

	scroll(id) {
	  let el = document.getElementById(id);
	  el.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		//console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	getBanner() {

		this.contentService.getquoteBanner().subscribe((data:any) => {
			if(data.status == 1)
			{
				this.bannerImage = data.results.bannerImage;
			}

			//console.log(this.bannerImage);
		});
	}

	/* Subscription Related functions */

	getSubscriptionDetails() {
		this.subscribeService.getsubscriptiondetails().subscribe((response:any) => {
                    if(response.features){
                        this.featuresdata = response.features.account;				
                        this.featureList = response.features.general;		
                    }
                    if(response.settings) {
                        this.settings = response.settings;
                    }  
                    this.baseAmount = response.baseSubscriptionAmount;
		});
	}

	isLargeScreen() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		if (width > 992) {
			return true;
		} else {
			return false;
		}
	}

	showHideFeature() {
            this.accountFeatureList = this.featuresdata;
            this.isShowhide = false;
	}

    signup(selectedPlan) {
        localStorage.removeItem('subscribedetail');
        localStorage.setItem('subscribedetail', JSON.stringify(selectedPlan));
        this.router.navigate(['/join']);
    }

    freesignup() {
        localStorage.removeItem('subscribedetail');
        this.router.navigate(['/join']);
    }
	
	selectDuration(duration, amount)
    {
    	this.selectedDuration = duration;
    	this.selectedAmount = amount;

    }
    signupmobile(settings)
    {
    	let selectedamt = [];
    	for(let item in settings)
    	{
    		if(this.selectedDuration == settings[item].duration)
    		{
    			selectedamt.push(settings[item]);
    		}
    	}
    	localStorage.removeItem('subscribedetail');
        localStorage.setItem('subscribedetail', JSON.stringify(selectedamt[0]));
        this.router.navigate(['/join']);	
    }

    /* Subscription functions end */

	getHomepageBlocks() {

		this.contentService.homepageBlockContent().subscribe((data:any) => {

			var valueAddedBlockIndex = 0;
			//console.log(data.results);
			for(let eachBlockContent of data.results) {

				eachBlockContent.content = this.sanitizer.bypassSecurityTrustHtml(eachBlockContent.content);
				if(eachBlockContent.slug == 'value_added_service')
				{
					eachBlockContent.image = this.valueAddedImg[valueAddedBlockIndex];
					this.blockValueAddedService.push(eachBlockContent);
					valueAddedBlockIndex++;
				}
				else if(eachBlockContent.slug == 'testimonial') {
					this.customers.push(eachBlockContent);
				}
			}

		});
	}

	changeFormView(event) {
		console.log(event.target.value);
		this.getquoteform = event.target.value;
	}
}
