import { NgModule } from '@angular/core';
import { GetquoteRoutingModule } from './getquote-routing.module';
import { GetquoteComponent } from './getquote.component';
import { SharedModule } from '@app/shared';

@NgModule({
	imports: [
		GetquoteRoutingModule,
		SharedModule
	],
	declarations: [GetquoteComponent]
})
export class GetquoteModule {

}
