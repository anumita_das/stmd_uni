import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GetquoteComponent } from './getquote.component';

const routes: Routes = [
	{ path: '', component: GetquoteComponent }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class GetquoteRoutingModule {

}
