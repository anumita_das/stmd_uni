import { NgModule } from '@angular/core';
import { AddressbookRoutingModule } from './addressbook-routing.module';
import { AddressbookComponent } from './addressbook.component';
import { SharedModule } from '@app/shared';

@NgModule({
	imports: [
		AddressbookRoutingModule,
		SharedModule
	],
	declarations: [AddressbookComponent]
})
export class AddressbookModule {

}
