import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShipmentSettingComponent } from './shipment-setting.component';

const routes: Routes = [
	{ path: '', component: ShipmentSettingComponent }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ShipmentSettingRoutingModule {

}
