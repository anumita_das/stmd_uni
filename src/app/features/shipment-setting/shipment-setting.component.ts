import { Component, OnInit, NgZone, HostListener } from '@angular/core';
import { NgForm }   from '@angular/forms';
import { UserShipmentSettings } from '../../shared/models/usershipmentsettings.model';
import { RegistrationService } from '../../services/registration.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-shipment-setting',
	templateUrl: './shipment-setting.component.html',
	styleUrls: ['./shipment-setting.component.scss']
})
export class ShipmentSettingComponent implements OnInit {
	isMediumScreen: boolean;
	sidebar: boolean;
	updates: Array<any>;
	ships: Array<any>;
	others: Array<any>;
	formSubmit : boolean;
	submitError : boolean;
	submitSuccess : boolean;
	shipmentSettings = new UserShipmentSettings();
	shipmentHistory : any;
	constructor(
		private ngZone: NgZone,
		public regService : RegistrationService,
		public toastr : ToastrService,
		public spinner : NgxSpinnerService,
	) { }

	ngOnInit() {
		this.spinner.show();
		this.formSubmit = false;
		this.getSettingsData();
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		this.checkWindowSize(width);

		// updates
		this.updates = [
			{
				title: 'Always remove shoe boxes. (Select Yes to save money)',
				// tslint:disable-next-line:max-line-length
				description: 'Set this to yes to save on shipping cost. If set to yes, we will remove the bulky boxes and pack your shoes neatly to reduce the dimensional weights. Only set to NO if you intend to resell the shoes and need the boxes.',
				position: 'left',
				name : 'remove_shoe_box'
			},
			{
				title: 'Ship all my items in their original boxes (Select No to save money)',
				// tslint:disable-next-line:max-line-length
				description: 'We recommend you leave this at No. Our packagers will repackage to reduce the dimensional weights and hence save you on shipping cost. They will also decide those items due to their nature that must be shipped in the original boxes.',
				position: 'left',
				name : 'original_box'
			},
			{
				title: 'Load shipment in my group',
				// tslint:disable-next-line:max-line-length
				description: '',
				position: 'left',
				name : 'shipment_ingroup'
			},
			/*{
				title: 'Ship all electronics in the original box (If Yes, we will ship such items in the box they came in)',
				// tslint:disable-next-line:max-line-length
				description: 'We recommend leaving this at Yes. Most electronics are fragile and its best to ship in the boxes they came in for maximum protection. Irrespective of your settings, we will still ship most electronics using the best method that ensures safety of items been shipped.',
				position: 'left',
				name : 'original_box_all'
			},*/
			/*{
				title: 'Quick ship my items. (For subscribed customers shipping via DHL Priority)',
				// tslint:disable-next-line:max-line-length
				description: 'A solution designed for our subscribed customers who always want the faster shipping option always. Once items are received, they are packed and ship alone via our DHL priority option. The is no consolidation option when this is selected as items are shipped individually as they are received to ensure you receive your items as fast as possible.',
				position: 'left',
				name : 'quick_shipout'
			},*/
			/*{
				title: 'Include Magazines, Letters and other unsolicited mails in my package',
				// tslint:disable-next-line:max-line-length
				description: 'A lot of unwanted magazines from stores usually arrive for you. Let us know by selecting this, if you always want these included in your shipments.',
				position: 'left',
				name : 'magazine_letter'
			}*/
		];

		// ships
		/*this.others = [
			{
				title: 'Take pictures of my items before shipping (If Yes, items are inventoried in the US/UK and a $9.99 charge applies)',
				// tslint:disable-next-line:max-line-length
				description: 'We recommend this is set to No. Set this to Yes if you want to see images of your items before we ship them out. As most items under 10 pounds are shipped out as soon as they arrive, requesting for pictures will mean that we do a 100% inventory, store the item in the warehouse and only ship it out after payment is made.',
				position: 'left',
				name : 'take_photo'
			},
			{
				title: 'Scan invoice/receipt for me (If Yes, a $4.99 charge per delivery invoice applies)',
				// tslint:disable-next-line:max-line-length
				description: 'We recommend this is set to No. If set to Yes, we will scan all receipts to you for each delivery. ',
				position: 'left',
				name : 'scan_invoice'
			},
			{
				title: 'Inventory my items in the US/UK (If Yes, a $7.99 charge per delivery is added)',
				// tslint:disable-next-line:max-line-length
				description: 'We recommend this is set to No. If set to yes, all items will be stored physically in the US/UK and wont be shipped out until payment is made for them. A $7.99 inventory and storage charge per delivery applies. ',
				position: 'left',
				name : 'item_inventory'
			}
			
			
		]; */

		setTimeout(() => {
	        	/** spinner ends after 1 seconds */
	        	this.spinner.hide();
	    	}, 2000);
		
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	onSubmit(form: NgForm){
		console.log(form.value);
		this.spinner.show();
		this.formSubmit = true;
		this.regService.saveShipmentSettings(form.value, '').subscribe((data:any) => {
			this.formSubmit = false;
			if(data.status == 1)
			{
				this.getSettingsData();
				this.toastr.success("Settings saved successfully");
				//this.submitError = false;
				//this.submitSuccess = true;
			}
			else
			{
				this.toastr.error("Something went wrong")
				//this.submitError = true;
				//this.submitSuccess = false;

			}
			this.spinner.hide();
		});
	}

	getSettingsData() {
		this.regService.getShipmentSettings().subscribe((data:any) => {
			if(data.status == 1)
			{
				this.shipmentSettings = data.userData; 
				this.shipmentHistory = data.historyData;
			}
		});
	}


}
