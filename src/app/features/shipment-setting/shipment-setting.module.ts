import { NgModule } from '@angular/core';
import { ShipmentSettingRoutingModule } from './shipment-setting-routing.module';
import { ShipmentSettingComponent } from './shipment-setting.component';
import { SharedModule } from '@app/shared';

@NgModule({
	imports: [
		ShipmentSettingRoutingModule,
		SharedModule
	],
	declarations: [ShipmentSettingComponent]
})
export class ShipmentSettingModule {

}
