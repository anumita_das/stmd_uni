import { Component, OnInit, HostListener } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ServiceModalComponent } from '../../shared/components/service-modal/service-modal.component';
import { AppGlobals } from '../../app.globals';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { SubscribeService } from '@app/shared/services/subscribe.service';
import { SignUpModalComponent } from '../../shared/components/signup-modal/signup-modal.component';

@Component({
	selector: 'app-subscribe',
	templateUrl: './subscribe.component.html',
	styleUrls: ['./subscribe.component.scss'],
	providers: [AppGlobals]
})
export class SubscribeComponent implements OnInit {
	isFloating: boolean = false;
	isFirstOpen: boolean;
	oneAtATime: boolean;
	bsModalRef: BsModalRef;
	dtOptions: DataTables.Settings = {};
	featureList: any;
        accountFeatureList : any;
        featuresdata : any;
        baseAmount: any;
        settings: any;
	isMediumScreen: boolean;
	show: boolean;
	isShowhide : boolean;
	selectedDuration: any;
	selectedAmount: any;

	constructor(
		private modalService: BsModalService,
		public spinner: NgxSpinnerService,
		public router: Router,
		public route: ActivatedRoute,
		private subscribeService: SubscribeService
	) { }

	ngOnInit() {
		this.spinner.hide();

		// accordion
		this.isFirstOpen = true;
		this.oneAtATime = true;
		this.isShowhide = true;

		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

        this.getDetails();
        this.openDialogmodal();
        
	}

	openDialogmodal() {
		this.bsModalRef = this.modalService.show(SignUpModalComponent, {});
		this.bsModalRef.content.closeBtnName = 'Close';
	}

	getDetails() {
		this.subscribeService.getsubscriptiondetails().subscribe((response:any) => {
                    if(response.features){
                        this.featuresdata = response.features.account;				
                        this.featureList = response.features.general;		
                    }
                    if(response.settings) {
                        this.settings = response.settings;
                    }  
                    this.baseAmount = response.baseSubscriptionAmount;
		});
	}

	openServiceModal(content) {
		this.bsModalRef = this.modalService.show(ServiceModalComponent, {});
		this.bsModalRef.content.serviceContent = content;
		this.bsModalRef.content.closeBtnName = 'Close';
	}
	
	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	isLargeScreen() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		if (width > 992) {
			return true;
		} else {
			return false;
		}
	}

	showHideFeature() {
            this.accountFeatureList = this.featuresdata;
            this.isShowhide = false;
	}

    signup(selectedPlan) {
        localStorage.removeItem('subscribedetail');
        localStorage.setItem('subscribedetail', JSON.stringify(selectedPlan));
        this.router.navigate(['/join']);
    }

    freesignup() {
        localStorage.removeItem('subscribedetail');
        this.router.navigate(['/join']);
    }
	
	selectDuration(duration, amount)
    {
    	this.selectedDuration = duration;
    	this.selectedAmount = amount;

    }
    signupmobile(settings)
    {
    	let selectedamt = [];
    	for(let item in settings)
    	{
    		if(this.selectedDuration == settings[item].duration)
    		{
    			selectedamt.push(settings[item]);
    		}
    	}
    	localStorage.removeItem('subscribedetail');
        localStorage.setItem('subscribedetail', JSON.stringify(selectedamt[0]));
        this.router.navigate(['/join']);

    	
    }


}
