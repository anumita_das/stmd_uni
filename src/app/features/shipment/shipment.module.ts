import { NgModule } from '@angular/core';
import { ShipmentRoutingModule } from './shipment-routing.module';
import { ShipmentComponent } from './shipment.component';
import { SharedModule } from '@app/shared';


@NgModule({
	imports: [
		ShipmentRoutingModule,
		SharedModule
	],
	declarations: [
		ShipmentComponent
	]
})
export class ShipmentModule {

}
