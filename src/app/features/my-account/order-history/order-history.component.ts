import { Component, OnInit } from '@angular/core';
import { NgForm }   from '@angular/forms';
import { AppGlobals } from '../../../app.globals';
import { Meta, Title } from '@angular/platform-browser';
import { ContentService } from '../../../services/content.service'
import { RegistrationService } from '../../../services/registration.service';
import { ShopformeService } from '../../../services/shopforme.service';
import { MywarehouseService} from '../../../services/mywarehouse.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ShowItemDetailModalComponent } from './showitem-modal.component';
import { SnapshotModalComponent } from './snapshot-modal.component';
import { Router,ActivatedRoute} from "@angular/router";
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-order-history',
	templateUrl: './order-history.component.html',
	styleUrls: ['./order-history.component.scss']
})
export class OrderHistoryComponent implements OnInit {
	allShipmentStatus : any;
	details:any;
	totalCost:any;
	grossCost:any;
	orderId:any;
	shipmentId : any;
	orderDate:any = 'AD';
	status:any = '';
	groupShipmentType : any = '';
	rangeDate:any;
	perPage:any = 10;
	shipmentRecords:any;
	dtOptions: DataTables.Settings = {};
	showMoreOptions = false;
	selectedAll: any;
	orders: Array<any>;
	names: any;
	isPeriodDisabled: boolean;
	isRequestPeriodDisabled: boolean;
	check_all;
	showHistory: boolean;
	currentPage: number = 1;
	pageSize: number;
	totalItems: number;
	checkMore:boolean = false;
	bsModalRef: BsModalRef;
	modalRef: BsModalRef;
	totalProcurementCost: any;
	totalItemCost: any;
	totalProcessingFee :any;
	urgentPurchaseCost :any;
	requestForCostShipment : Array<any>;
	requestforcostDate:any = 'AD';
	requestrangeDate:any;
	totaRequestForCostItems : any;
	reqCurrentPage: number = 1;
	reqPageSize:any;
	reqPerPage:any = 10;
	urlShipmentId : number;

	config = {
		ignoreBackdropClick: false
	};

	constructor(
		private shopformeService : ShopformeService,
		private router: Router,
		private route: ActivatedRoute,
		private modalService: BsModalService,
		private toastr: ToastrService,
		private mywarehouseService : MywarehouseService,
	) {
		this.check_all = {
			type: false
		};
	}

	ngOnInit() { 
		this.route.params.subscribe(params => {
			this.urlShipmentId = params['shipmentId'];
		});
		console.log(this.urlShipmentId);
		this.getAllShipmentStatus();
	if (localStorage.getItem("shipmentOrderHistory") === null) {
		console.log('here');
		var formdata = {};
		if(this.urlShipmentId != undefined) {
			formdata = {currentPage:this.currentPage, perPage:this.perPage, shipmentId : this.urlShipmentId};
			this.shipmentId = this.urlShipmentId;
			this.showMoreOptions = true;
			this.checkMore = true;
		}
		else	
			formdata = {currentPage:this.currentPage, perPage:this.perPage};

		this.mywarehouseService.shipmentOrderHistory(formdata).subscribe((data:any) => {
			if(data.results == 'success'){
				this.details = data.shipmentRecords;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;	
			}
		});
	} 	else {
		 	console.log('here');
			var shipmentOrderHistory =  JSON.parse(localStorage.getItem('shipmentOrderHistory'));
			this.orderId = shipmentOrderHistory.orderId;
			if(this.urlShipmentId != undefined) {
				this.shipmentId = this.urlShipmentId;
				shipmentOrderHistory.shipmentId = this.urlShipmentId;
			}
			else
				this.shipmentId = shipmentOrderHistory.shipmentId;
				
			this.orderDate = shipmentOrderHistory.orderDate;
			this.rangeDate = shipmentOrderHistory.rangeDate;
			this.currentPage = shipmentOrderHistory.currentPage;
			this.status = shipmentOrderHistory.status;
			this.groupShipmentType = shipmentOrderHistory.groupShipmentType;
			this.perPage = shipmentOrderHistory.perPage;
			if(this.orderDate == 'FR'){
				this.isPeriodDisabled = true;
			} else {
				this.isPeriodDisabled = false;
			}
			if(shipmentOrderHistory.orderId != '' || shipmentOrderHistory.shipmentId != '' || shipmentOrderHistory.status != '' || shipmentOrderHistory.groupShipmentType != ''){ 
				this.showMoreOptions = !this.showMoreOptions;
				this.checkMore = true;
			}

			this.mywarehouseService.shipmentOrderHistory(shipmentOrderHistory).subscribe((data:any) => {
				if(data.results == 'success'){
					this.details = data.shipmentRecords;
					this.totalItems = data.totalItems;
					this.pageSize= data.itemsPerPage;
				}
			});
		}

		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};
	}

	getAllShipmentStatus() {
		this.mywarehouseService.getallshipmentstatus().subscribe((data:any)=>{
			this.allShipmentStatus = data;
		});
	}

	showMore(event) {
		this.showMoreOptions = !this.showMoreOptions;
	}

	// openHistory
	openHistory() {
		this.showHistory = true;
	}

	onSubmit(form: NgForm){
		if(!form.value.orderId){
			form.value.orderId = "";
		}

		if(!form.value.shipmentId){
			form.value.shipmentId = "";
		}
		if(!form.value.status){
			form.value.status = "";
		}
		if(!form.value.groupShipmentType){
			form.value.groupShipmentType = "";
		}

		var shipmentOrderHistory = {'orderDate':form.value.orderDate,'rangeDate':form.value.rangeDate, 'orderId':form.value.orderId,'shipmentId':form.value.shipmentId, 'status':form.value.status,'groupShipmentType':form.value.groupShipmentType, 'currentPage':this.currentPage, perPage:this.perPage};
		localStorage.setItem("shipmentOrderHistory", JSON.stringify(shipmentOrderHistory));

    	this.mywarehouseService.shipmentOrderHistory(shipmentOrderHistory).subscribe((data:any) => {
			if(data.results == 'success'){
				this.details = data.shipmentRecords;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;	
			}
		});
    }


    pageChanged($event)  {

    	let param = {orderDate:this.orderDate, rangeDate:this.rangeDate, orderId:this.orderId,'shipmentId':this.shipmentId, status:this.status,groupShipmentType:this.groupShipmentType, currentPage:$event, perPage:this.perPage};

    	localStorage.removeItem("shipmentOrderHistory");
    	localStorage.setItem("shipmentOrderHistory", JSON.stringify(param));

		this.mywarehouseService.shipmentOrderHistory(param).subscribe((data:any) => {
			if(data.results == 'success'){
				this.details = data.shipmentRecords;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;
				this.currentPage = $event;
			}
		});
	}

	perPageChanged($event, type)  {

		if(type == 'pagination')
			this.reqCurrentPage = $event;

    	let param = {orderDate:this.orderDate, rangeDate:this.rangeDate, orderId:this.orderId,'shipmentId':this.shipmentId, status:this.status, currentPage:this.reqCurrentPage, perPage:this.reqPerPage};

    	localStorage.removeItem("shipmentOrderHistory");
    	localStorage.setItem("shipmentOrderHistory", JSON.stringify(param));


		this.mywarehouseService.shipmentOrderHistory(param).subscribe((data:any) => {
			if(data.results == 'success'){
				this.details = data.shipmentRecords;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;
			}
		});
	}




	selectAll(space) {
		if (space === 'type') {
			if (this.check_all.type === true) {
				this.orders.forEach(type => {
					type.selected = false;
				});
			} else {
				this.orders.forEach(type => {
					type.selected = true;
				});
			}
		}
		this.filterSearch('');
	}

	filterSearch(selected) {
		for (const type of this.orders) {
			if (!type.selected) {
				this.check_all.type = false;
				break;
			} else {
				this.check_all.type = true;
			}
		}
	}

	// modal photo shot
	enableRange(event) {
		if (event === 'FR') {
			this.isPeriodDisabled = true;
		} else {
			this.isPeriodDisabled = false;
		}
	}

	enableRequestRange(event) {
		if (event === 'FR') {
			this.isRequestPeriodDisabled = true;
		} else {
			this.isRequestPeriodDisabled = false;
		}
	}

	getText(status, invoiceSent) {
	   if (status === "submitted") return "Submitted";
	   if (status === "cancelled") return "Cancelled";
	   if (status === "requestforcost") {
		if(invoiceSent == 'N')
	   		return "Requested For Shipping Cost";
	   	else
	   		return "Shipping Cost Sent";
	   }

	   if (status === "rejected") return "Rejected";
	}


	resetFilter(){
		this.orderId = '';
		this.requestforcostDate ='AD';
		this.orderDate = 'AD';
		this.status = '';
		this.groupShipmentType = '';
		this.rangeDate = '';
		this.checkMore = false;
		this.perPage = 10;
		this.showMoreOptions = false;

		localStorage.removeItem("placeOrderOrderHistory");
		this.shopformeService.getprocurementshipmentdetails('', this.perPage, this.currentPage).subscribe((data:any) => {
			if(data.results == 'success'){
				this.details = data.shipmentRecords;
				this.requestForCostShipment = data.requestForCostShipment;
				this.totaRequestForCostItems = data.totaRequestForCostItems;
				this.totalCost = data.totalCost;
				this.grossCost = data.grossCost;	
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;
			}
		});

	}


	showItemDetail(shipmentid)
	{
		this.mywarehouseService.getShipmentdetail(shipmentid).subscribe((response:any) => {
			//console.log(response.shipmentdetails.deliveries);
			this.bsModalRef = this.modalService.show(ShowItemDetailModalComponent, {
				class: 'modal-md modal-dialog-centered'
			});

			this.bsModalRef.content.showShipping = true;
			this.bsModalRef.content.shipmentId = shipmentid;
			this.bsModalRef.content.bagList = response.shipmentdetails.deliveries;
			this.bsModalRef.content.isGroupShipment = response.isGroupShipment;
			
		});

	}

	showSnapshotimages(shipmentid)
	{
		this.mywarehouseService.getShipmentsnapshots(shipmentid).subscribe((response:any) => {
			console.log(response.results);
			this.bsModalRef = this.modalService.show(SnapshotModalComponent, {
				class: 'modal-md modal-dialog-centered'
			});

			this.bsModalRef.content.showShipping = true;
			this.bsModalRef.content.shipmentId = shipmentid;
			if(response.results.length>0)
				this.bsModalRef.content.snapimages = response.results;
			
		});

	}

	print(shipmentId,shipmentType,paymentStatus) {
		let printContents, popupWin;
		this.mywarehouseService.printInvoice(shipmentId,shipmentType,paymentStatus).subscribe((response:any) => {
		    printContents = response.data;
		    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
		    popupWin.document.open();
		    popupWin.document.write(`
		      <html>
		        <head>
		          <title>Print tab</title>
		          <style>
		          //........Customized style.......
		          </style>
		        </head>
		    <body onload="window.print();window.close()">${printContents}</body>
		      </html>`
		    );
		    popupWin.document.close();
		});
	}

}
