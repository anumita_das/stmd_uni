import { Component, OnInit } from '@angular/core';
import { ContentService } from '../../../services/content.service';
import { RegistrationService } from '../../../services/registration.service';
import { Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { NgForm }   from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import {Router} from "@angular/router";

@Component({
	selector: 'app-document',
	templateUrl: './document.component.html',
	styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit {
	dtOptions: DataTables.Settings = {};
	documents: Array<any>;
	mydocuments:any = [];
	userId : number;
	msg:any;
	dtTrigger: Subject<any> = new Subject();
	formSubmit : boolean = false;
	docImage : any;
	constructor(

		public regService : RegistrationService,
		private content: ContentService,
		private toastr: ToastrService,
		private router: Router,
		public spinner : NgxSpinnerService,
	) { 
			this.userId = this.regService.item.user.id;
	}

	ngOnInit() {
		// datatable
		this.dtOptions = {
			paging: false,
			ordering: false,
			searching: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

		this.getDocuments();
	}

	// deleteDocumentRow
	deleteDocumentRow(docId) {
		this.spinner.show();

		this.content.deleteDocument(this.userId, docId).subscribe((data:any) => { 
			this.msg = data.results;
			this.getDocuments();
			this.spinner.hide();
			this.toastr.success(data.results);
		});
	}

	getDocuments(){
		this.spinner.show();

		this.content.getDocuments(this.userId).subscribe((data:any) => { 
			setTimeout(() => {
	        	/** spinner ends after 1 seconds */
	        	this.spinner.hide();
	        	this.documents = data.results;
				this.dtTrigger.next();   	
	    	}, 1000);
		});
	}

	img(event) {

		let fileItem = event.target.files[0];
  		const file: File = fileItem;
  		this.docImage = file;
	}



	onSubmit(form : NgForm) {
		this.formSubmit = true;	
		const formData = new FormData();
		this.spinner.show();

		if(this.docImage !== undefined){
			console.log('img= '+this.docImage);
			formData.append('itemImage', this.docImage, this.docImage.name);

			for(let eachField in form.value)
	        {
	          formData.append(eachField,form.value[eachField]);
	        }

			this.content.uploadDocument(formData,this.userId).subscribe((data:any) => {
				this.msg = data.results;
				form.resetForm(); 
				this.getDocuments();
				setTimeout(() => {
		        	/** spinner ends after 2 seconds */
		        	this.spinner.hide();
		        	this.toastr.success(data.results);
		        	window.location.reload();		        	
	    		}, 2000);
				
			});
		} else {
			this.spinner.hide();
			this.toastr.error('Choose a file');
		}

		
	}
}
