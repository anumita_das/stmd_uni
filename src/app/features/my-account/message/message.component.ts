import { Component, OnInit } from '@angular/core';
import { ContentService } from '../../../services/content.service';
import { RegistrationService } from '../../../services/registration.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import { MsgModalComponent } from '../../../shared/components/msg-modal/msg-modal.component';

@Component({
	selector: 'app-message',
	templateUrl: './message.component.html',
	styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {
	dtOptions: DataTables.Settings = {};
	activities: Array<any>;
	getRewardDetail:any;
	userId : number;
	getUserName: any;
	earnedPoints:any;
	rewardName: any;
	rewardImage:any;
	balancePoint:any;
	remaining:any;
	totalOrders: any;
	totalCost:any;
	expString:string;
	activity:any = 'all';
	msgtype:any = '';
	btnGo:any;
	isBtnDisabled:any = true;
	isBtnDisabledWalletCode:any = true;
	isActDisabled:any = false;
	transctions:any;
	myId:any;
	walletBalance:any;
	bsModalRef: BsModalRef;
	dtTrigger: Subject<any> = new Subject();
	perPage:any = 10;
	currentPage: number = 1;
	pageSize: number;
	totalItems: number;
	msgId:number;
	useEmail:any;
	totalClaim:number;
	page:number;
	resultSet:any;

	constructor(
		public regService : RegistrationService,
		private content: ContentService,
		private modalService: BsModalService,
	) {

	
		this.userId = this.regService.item.user.id;
		this.useEmail = this.regService.item.user.email;

		this.getallclaims();

	}

	ngOnInit() {
		// datatable
		this.dtOptions = {
			paging: false,
			ordering: false,
			searching: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};
		this.selectTransaction();

	}

	selectTransaction(){ 
		this.isActDisabled = true;
		this.content.getMessages(this.activity, this.msgtype, this.userId, this.currentPage, this.perPage).subscribe((data:any) => { 
			this.transctions = data.results.transctions;
			this.dtTrigger.next();
			this.totalItems = data.totalItems;
			this.pageSize= data.itemsPerPage;
		});
	}

	pageChanged($event)  {
		this.content.getMessages(this.activity, this.msgtype, this.userId, $event, this.perPage).subscribe((data:any) => {
				this.currentPage = $event;
				this.transctions = data.results.transctions;
				this.totalItems = data.totalItems;
				this.pageSize = data.itemsPerPage;
		});
	}

	perPageChanged($event)  {
		this.content.getMessages(this.activity, this.msgtype, this.userId, 1, this.perPage).subscribe((data:any) => {
				this.currentPage = 1;
				this.transctions = data.results.transctions;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;
		});
	}


	enableDisabledBtn(event){
		if(event == ''){
			this.isBtnDisabled = false;
		} else {
			this.isBtnDisabled = true;
		}
	}

	msgPopup(msgId){
		if(msgId)
		localStorage.setItem('msgId', msgId);
		this.bsModalRef = this.modalService.show(MsgModalComponent, {});
	}


	msgClaim(msgText){		
		localStorage.setItem('msgReply', msgText);
		this.bsModalRef = this.modalService.show(MsgModalComponent, {});
	}

	getallclaims()
	{
		this.content.getClaims(this.useEmail, this.currentPage, this.perPage).subscribe((data:any) => { 
			this.resultSet = data.results.resultSet;
			this.dtTrigger.next();
			this.totalClaim = data.totalItems;
			this.page= data.itemsPerPage;
		});

	}
	

	
}
