import { Component, OnInit } from '@angular/core';
import { AppGlobals } from '../../../app.globals';
import { Meta, Title } from '@angular/platform-browser';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { GalleryModalComponent } from '@app/shared/components/gallery-modal/gallery-modal.component';
import { ContentService } from '../../../services/content.service'
import { RegistrationService } from '../../../services/registration.service';
import { ShopformeService } from '../../../services/shopforme.service';


@Component({
	selector: 'app-gallery',
	templateUrl: './gallery.component.html',
	styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
	galleryImages: Array<any>;
	bsModalRef: BsModalRef;
	constructor(
		private shopformeService : ShopformeService,
		private modalService: BsModalService
	) { }

	ngOnInit() {

		this.shopformeService.getShipmentImagesList().subscribe((data:any) => {
			if(data.results=='success'){
				this.galleryImages = data.item;
			}
		});

	}

	openGalleryModal(itemId, shipmentId) {		
		this.shopformeService.getShipmentImages(itemId, shipmentId).subscribe((response:any) => {
			this.bsModalRef = this.modalService.show(GalleryModalComponent, {
				class: 'modal-sm modal-dialog-centered'

			});
			//if(reponse.item.type == 'shipacar')
			//alert(response.item.type);
			this.bsModalRef.content.imagepath = response.item.imagepath;
			this.bsModalRef.content.type = response.item.type;
			
			this.bsModalRef.content.closeBtnName = 'Close';
		});
	}
}
