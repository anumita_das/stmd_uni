import { Component, OnInit, NgZone, HostListener } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { RegistrationService } from '../../../services/registration.service';
import { ContentService } from '../../../services/content.service';
import { AddressbookModalComponent } from '../../../shared/components/addressbook-modal/addressbook-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { UserAddress } from '../../../shared/models/useraddress.model';
import { WarehouseModalComponent } from '../../../shared/components/warehouse-modal/warehouse-modal.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { SubscribeService } from '@app/shared/services/subscribe.service';

import { AppGlobals } from '../../../app.globals';

@Component({
	selector: 'app-how-it-work',
	templateUrl: './account-detail.component.html',
	styleUrls: ['./account-detail.component.scss']
})
export class AccountDetailComponent implements OnInit {
	isMediumScreen: boolean;
	sidebar: boolean;
	userShippingAddress : Array<object>;
	allWarehouse : Array<object>;
	selectedWarehouseDetails: any;
	bsModalRef: BsModalRef;
	selectedWarehouse : number;
	userAddress = new UserAddress();
	profileImg : any;
	defaultProfile : string;
	earnedPoints:any;
	rewardName:any;
	expString:any;
	featureList:any;
	settings:any;
	baseAmount:number;
	showsubscribe:boolean = false;
    userSubscription :any;
    userSubscriptionStatus : any ;
    userSubscriptionData : any;
    defaultCurrencySymbol : string;
    subscribedFor: number;
    selectedAmount: any;
    selectedPlan: any;
    locationStatus : any = "1";
    show : boolean = false;
    featuresdata : any;


	constructor(
		private ngZone: NgZone,
		public auth: RegistrationService,
		private modalService: BsModalService,
		private content: ContentService,
		public spinner : NgxSpinnerService,
		private featureListData: SubscribeService,
		private router: Router,
        private _global: AppGlobals,
        private activeRoute: ActivatedRoute
	) { }

	ngOnInit() {
	
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		this.checkWindowSize(width);
		this.spinner.show();
		this.getUserAddressInfo();
		//this.getAllWarehouse();
		this.getUserWarehose();
		this.allWarehouse = [];
		this.defaultProfile = JSON.parse(localStorage.getItem('profileImage'));
                this.defaultCurrencySymbol = this._global.defaultCurrencySymbol;

		let warehouse = JSON.parse(localStorage.getItem('selectedWarehouse'));
		if(warehouse == null)
			localStorage.setItem('selectedWarehouse',JSON.stringify(0));

		this.content.getRewardDetails(this.auth.item.user.id).subscribe((data:any) => { 
			this.earnedPoints = data.results.earnedPoints;
			this.rewardName = data.results.rewardName;
			this.expString = data.results.expStringShort;
			this.spinner.hide();
		});

		if("subscriptionDetails" in localStorage){
			let subscriptionDetails = JSON.parse(localStorage.getItem('subscriptionDetails'));
			if(subscriptionDetails.isSubscribed == 'N')
			{
				this.featureListData.getsubscriptiondetails().subscribe((results:any)=>{
	                if(results.features){				
	                    this.featureList = results.features.general;
	                    this.featuresdata = results.features.account;		
	                }
	                if(results.settings) {
	                    this.settings = results.settings;
	                }  
	                this.baseAmount = results.baseSubscriptionAmount;
	            });
            	this.showsubscribe = true;
            	this.subscribedFor = 0;

			}else{
				this.auth.getusersubscriptiondetail(this.auth.item.user.id).subscribe((response:any)=>{
                if(response.status == 1)
                 	this.userSubscriptionData = response.results;
                 	this.subscribedFor = this.userSubscriptionData.subscribedFor;
                 	this.userSubscriptionStatus = this.userSubscriptionData.status;
                 	if(this.userSubscriptionStatus == 'active')
                 	{
                 		this.userSubscription = 'Y';
                 	}else{
                 		this.userSubscription = 'N';
                 	}
            	});
			}			
            this.userSubscription = subscriptionDetails.isSubscribed; 
		}else if("paymentsuccess" in localStorage)
		{
			this.auth.getusersubscriptiondetail(this.auth.item.user.id).subscribe((response:any)=>{
                if(response.status == 1)
                 	this.userSubscriptionData = response.results;
                 	this.userSubscriptionStatus = this.userSubscriptionData.status;                 	
                 	this.subscribedFor = this.userSubscriptionData.subscribedFor;
                 	if(this.userSubscriptionStatus == 'active')
                 	{
                 		this.userSubscription = 'Y';
                 	}else{
                 		this.userSubscription = 'N';
                 	}
                 	
            	});

            
		}
		else{
			if(this.auth.item.user.isSubscribed == 'N'){
	            this.featureListData.getsubscriptiondetails().subscribe((results:any)=>{
	                if(results.features){				
	                    this.featureList = results.features.general;
	                    this.featuresdata = results.features.account;		
	                }
	                if(results.settings) {
	                    this.settings = results.settings;
	                }  
	                this.baseAmount = results.baseSubscriptionAmount;
	                this.subscribedFor = 0;
	            });
	        } else {
	            this.auth.getusersubscriptiondetail(this.auth.item.user.id).subscribe((response:any)=>{
	                if(response.status == 1)
	                {
	                	this.userSubscriptionData = response.results;
		                 if(this.userSubscriptionData.subscribedFor)
		                 {
		                 	this.subscribedFor = this.userSubscriptionData.subscribedFor;
		                 }else{
		                 	this.subscribedFor = 0;
		                 }

	                }else{
	                	this.userSubscription = 'N';
	                	
	                }
	                 
	                 

	            });
	        }

	        this.userSubscription = this.auth.item.user.isSubscribed;

	        if(this.auth.item) {
	        	this.userSubscriptionStatus = this.auth.item.user.subscription.subcscritionStatus;
	        }

	        

		}




	}

	ngAfterViewInit() {
	  // Scroll (Anchor) Function to Order History
	  	this.activeRoute.fragment.subscribe((fragment: string) => {
		  	if(fragment == 'show-subscription')
		  	{ 
				this.showData();
			}
		});
		
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		this.checkWindowSize(event.target.innerWidth);
	}

	getUserAddressInfo() {
		this.auth.userShippingAddress('shipping').subscribe((data:any) =>{
			if(data.status == 1)
			{
				this.userShippingAddress = data.results;
				this.locationStatus = data.locationStatus;
			}
			/*else if(data.status == '-1')
			{
				this.bsModalRef = this.modalService.show(AddressbookModalComponent, 
					{
						backdrop  : 'static',
   						keyboard  : false
   					}
   				);
			}*/
		});
	}

	getUserWarehose() {
		this.content.getUserWarehose(this.auth.item.user.id).subscribe((data:any) => {
			let index = 0;
			//console.log(JSON.parse(localStorage.getItem('selectedWarehouse')));
			if(data.status == 1)
			{
				for(let eachWarehouse of data.results) {
					this.allWarehouse.push(eachWarehouse);
					if(index == JSON.parse(localStorage.getItem('selectedWarehouse')))
					{
						this.selectedWarehouseDetails = eachWarehouse;
					}
					index++;
				}
				//console.log(this.selectedWarehouseDetails);
			}
			this.selectedWarehouse = JSON.parse(localStorage.getItem('selectedWarehouse'));
		});
	}

	getAllWarehouse(){
		this.content.allWarehouse().subscribe((data:any) => {
			let index = 0;
			//console.log(JSON.parse(localStorage.getItem('selectedWarehouse')));
			if(data.status == 1)
			{
				for(let eachWarehouse of data.results) {
					this.allWarehouse.push(eachWarehouse);
					if(index == JSON.parse(localStorage.getItem('selectedWarehouse')))
					{
						this.selectedWarehouseDetails = eachWarehouse;
					}
					index++;
				}
				//console.log(this.selectedWarehouseDetails);
			}
			this.selectedWarehouse = JSON.parse(localStorage.getItem('selectedWarehouse'));
		});
	}

	selectWarehose(event){
		let index = 0;
		var selectedDrop = event.target.value;
		localStorage.setItem('selectedWarehouse',JSON.stringify(selectedDrop));
		this.selectedWarehouse = selectedDrop;
		
		for(let eachWarehouse of this.allWarehouse)
		{
			if(index == selectedDrop)
			{
				this.selectedWarehouseDetails = eachWarehouse;
			}
			index++;
			//console.log(eachWarehouse);
		}
		
		let el = document.getElementById('warehouseAddress');
	  	el.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'end'});
	}

    showWarehouseAddress() {
    	this.bsModalRef = this.modalService.show(WarehouseModalComponent, {});
    	this.bsModalRef.content.warehouseAddress = this.selectedWarehouseDetails;
		this.bsModalRef.content.closeBtnName = 'Close';		
    }

    fileUpload(event) {
  	
  		let fileItem = event.target.files[0];
  		this.defaultProfile = '';
        var reader = new FileReader();
        reader.onload = (event: any) => {
            this.profileImg = event.target.result;
        }
        reader.readAsDataURL(event.target.files[0]);
        const fileSelected: File = fileItem;
        this.auth.uploadFile(fileSelected).subscribe((data:any) => {
        	if(data.status == 1)
        	{
        		localStorage.setItem('profileImage',JSON.stringify(data.results));
        	}
        });
    }
  
	

	isLargeScreen() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		if (width > 992) {
			return true;
		} else {
			return false;
		}
	}

	showData()
	{

        this.showsubscribe = true;
        
        let el = document.getElementById('warehouseAddress');
	    el.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'end'});
	}
	showUpgradeData()
	{
		this.showsubscribe = true;

		this.featureListData.getsubscriptiondetails().subscribe((results:any)=>{
            if(results.features){				
                this.featureList = results.features.general;
                this.featuresdata = results.features.account;		
            }
            if(results.settings) {
                this.settings = results.settings;
            }  
            this.baseAmount = results.baseSubscriptionAmount;
        });
        this.auth.getusersubscriptiondetail(this.auth.item.user.id).subscribe((response:any)=>{
		        if(response.status == 1)
		         this.userSubscriptionData = response.results;
		         this.selectedPlan = this.userSubscriptionData.subscribedFor;
		         this.selectedAmount = this.userSubscriptionData.subscribedAmount;
		  });
        
        let el = document.getElementById('warehouseAddress');
	    el.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'end'});

	}

	payforSubscription(amount, duration)
	{
		
		if((duration <= this.subscribedFor) || duration == '' || duration =="undefined")
		{
			this.router.navigate(['my-account','account-detail']);

		}else{
			const data= {userId:this.auth.userId, paidAmount:amount, duration:duration, isSubscribed:this.auth.item.user.isSubscribed};

			localStorage.setItem('subscriptionDetails',  JSON.stringify(data));

			this.router.navigate(['my-account','payment']);

		}
		
	}

	selectPlan(amount, duration)
	{
		this.selectedAmount = amount;

		this.selectedPlan = duration;

	}
}
