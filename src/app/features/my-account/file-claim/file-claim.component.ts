import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgForm }   from '@angular/forms';
import { AppGlobals } from '../../../app.globals';
import { Meta, Title } from '@angular/platform-browser';
import { ContentService } from '../../../services/content.service';
//import { Router } from "@angular/router";
import { RegistrationService } from '../../../services/registration.service';
import { Router, ActivatedRoute, Params, ParamMap } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-file-claim',
	templateUrl: './file-claim.component.html',
	styleUrls: ['./file-claim.component.scss']
})
export class FileClaimComponent implements OnInit {
	selectedFile : any;
	selectedFile2 : any;
	userId : number;
	formSubmit : boolean = false;
	form:any;
	orderNumber : any = '';

	constructor(
		private _global: AppGlobals,
		public route: ActivatedRoute,
		private meta: Meta, 
		private title: Title,
		public content : ContentService,
		private toastr: ToastrService,
		public router: Router,
		public regService : RegistrationService,
		public spinner : NgxSpinnerService,

	) { 
		this.userId = this.regService.item.user.id;
	}

	ngOnInit() {

	if(this.route.snapshot.paramMap.get('ordernumber'))
	{
		this.orderNumber = this.route.snapshot.paramMap.get('ordernumber');
		if(this.orderNumber){
			console.log(this.orderNumber);
		}
		//this.getTrackingDetails();
	}

	}

	fileUpload(event) {
  		let fileItem = event.target.files[0];
  		const file: File = fileItem;
  		this.selectedFile = file;
  		//console.log(this.selectedFile);
  	}

  	fileUpload2(event) {
  		let fileItem2 = event.target.files[0];
  		const file2: File = fileItem2;
  		this.selectedFile2 = file2;
  		//console.log(this.selectedFile2);
  	}

  	onSubmit(form : NgForm){

		console.log(form.value);
		this.spinner.show();
		
		this.formSubmit = true;
		const formData = new FormData();
  		if(this.selectedFile != null)
      		formData.append('itemImage', this.selectedFile, this.selectedFile.name);

  		if(this.selectedFile2 != null)
  			formData.append('damagedItemImage', this.selectedFile2, this.selectedFile2.name);


      	for(let eachField in form.value)
      	{
      		formData.append(eachField,form.value[eachField]);
      	}
		this.content.postclaim(formData,this.userId).subscribe((data:any) => {
			if(data.results.status == '1')
			{
				this.spinner.hide();
				this.toastr.success(data.results.res);
				form.resetForm(); 

			} else {
				this.spinner.hide();
				this.toastr.error(data.results.res);
			}

			setTimeout(() => {
			        /** spinner ends after 3 seconds */
			        this.spinner.hide();
			    }, 2000);
		});
	}

	onChange(e){
		
	}
}
