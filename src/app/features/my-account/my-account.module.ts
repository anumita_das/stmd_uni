import { NgModule } from '@angular/core';
import { MyAccountRoutingModule } from './my-account-routing.module';
import { MyAccountComponent } from './my-account.component';
import { SharedModule } from '@app/shared';
import { AccountDetailComponent } from './account-detail/account-detail.component';
import { RewardComponent } from './reward/reward.component';
import { DocumentComponent } from './document/document.component';
import { FileClaimComponent } from './file-claim/file-claim.component';
//import { NumberOnlyDirective } from '../../number.directive';
import {NgxPaginationModule} from 'ngx-pagination';
import { MessageComponent } from './message/message.component';
import { GalleryComponent } from './gallery/gallery.component';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { ShowItemDetailModalComponent } from './order-history/showitem-modal.component';
import { SnapshotModalComponent } from './order-history/snapshot-modal.component';
import { PaymentComponent } from './payment/payment.component';


@NgModule({
	imports: [
		MyAccountRoutingModule,
		SharedModule,
		NgxPaginationModule,
	],
	exports: [
		ShowItemDetailModalComponent,
		SnapshotModalComponent
	],
	declarations: [
		MyAccountComponent,
		AccountDetailComponent,
		RewardComponent,
		DocumentComponent,
		FileClaimComponent,
		//NumberOnlyDirective,
		MessageComponent,
		GalleryComponent,
		OrderHistoryComponent,
		ShowItemDetailModalComponent,
		SnapshotModalComponent,
		PaymentComponent
	],
	entryComponents: [
		ShowItemDetailModalComponent,
		SnapshotModalComponent
	]
})
export class MyAccountModule {

}
