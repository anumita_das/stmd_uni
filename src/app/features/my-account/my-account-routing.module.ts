import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyAccountComponent } from './my-account.component';
import { AccountDetailComponent } from './account-detail/account-detail.component';
import { RewardComponent } from './reward/reward.component';
import { DocumentComponent } from './document/document.component';
import { FileClaimComponent } from './file-claim/file-claim.component';
import { AuthGuardService } from '../../services/auth-guard.service';
import { MessageComponent } from './message/message.component';
import { GalleryComponent } from './gallery/gallery.component';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { PaymentComponent } from './payment/payment.component';

const routes: Routes = [
	{
		path: '', redirectTo: 'account-detail', pathMatch: 'full'
	},
	{
		path: '',
		component: MyAccountComponent,
		children: [
			{ path: 'account-detail',canActivateChild: [AuthGuardService], component: AccountDetailComponent },
			{ path: 'reward',canActivateChild: [AuthGuardService], component: RewardComponent },
			{ path: 'my-documents',canActivateChild: [AuthGuardService], component: DocumentComponent },
			{ path: 'file-claim/:ordernumber',canActivateChild: [AuthGuardService], component: FileClaimComponent },
			{ path: 'file-claim',canActivateChild: [AuthGuardService], component: FileClaimComponent },
			{ path: 'message',canActivateChild: [AuthGuardService], component: MessageComponent },
			{ path: 'gallery', component: GalleryComponent },
			{ path: 'order-history',canActivateChild: [AuthGuardService], component: OrderHistoryComponent },
			{ path: 'order-history/:shipmentId',canActivateChild: [AuthGuardService], component: OrderHistoryComponent },	
			{ path: 'payment', component: PaymentComponent },
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class MyAccountRoutingModule {

}
