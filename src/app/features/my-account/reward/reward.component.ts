import { Component, OnInit } from '@angular/core';
import { ContentService } from '../../../services/content.service';
import { RegistrationService } from '../../../services/registration.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import { WalletModalComponent } from '../../../shared/components/wallet-modal/wallet-modal.component';
import { PointtowalletModalComponent } from '../../../shared/components/pointtowallet-modal/pointtowallet-modal.component';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
	selector: 'app-reward',
	templateUrl: './reward.component.html',
	styleUrls: ['./reward.component.scss']
})
export class RewardComponent implements OnInit {
	dtOptions: DataTables.Settings = {};
	activities: Array<any>;
	getRewardDetail:any;
	userId : number;
	getUserName: any;
	earnedPoints:any;
	rewardName: any;
	rewardImage:any;
	rewardDescription:any;
	balancePoint:any;
	remaining:any;
	totalOrders: any;
	totalCost:any;
	expString:string;
	activity:any = 'all';
	btnGo:any;
	isBtnDisabled:any = true;
	isBtnDisabledWalletCode:any = true;
	isActDisabled:any = false;
	transctions:any;
	myId:any;
	walletBalance:any;
	bsModalRef: BsModalRef;
	dtTrigger: Subject<any> = new Subject();
	perPage:any = 10;
	currentPage: number = 1;
	pageSize: number;
	totalItems: number;

	constructor(
		public regService : RegistrationService,
		private content: ContentService,
		private modalService: BsModalService,
		private sanitizer:DomSanitizer,
	) {

	this.userId = this.regService.item.user.id;

	}

	ngOnInit() {
		// datatable
		this.dtOptions = {
			paging: false,
			ordering: false,
			searching: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

		this.content.getRewardDetails(this.userId).subscribe((data:any) => { 
			
			this.getUserName = data.results.userName;
			this.balancePoint = data.results.balancePoint;
			this.earnedPoints = data.results.earnedPoints;
			this.rewardName = data.results.rewardName;
			this.rewardImage = data.results.rewardImage;
			this.remaining = data.results.remaining;

			this.totalCost = data.results.totalCost;
			this.totalOrders = data.results.totalOrders;

			this.expString = data.results.expString;
			this.rewardDescription = this.sanitizer.bypassSecurityTrustHtml(data.results.rewardDescription);
			this.walletBalance = data.results.walletBalance;
		});

		this.selectTransaction();

		// activities
		/*this.activities = [
			{
				date: '06-01-2018 11:42',
				reason: 'Registration',
				point: '0'
			},
			{
				date: '05-31-2018 10:47',
				reason: 'Registration',
				point: '0'
			},
			{
				date: '05-31-2018 10:47',
				reason: 'Registration',
				point: '0'
			},
			{
				date: '05-31-2018 10:46',
				reason: 'Registration',
				point: '0'
			},
			{
				date: '05-28-2018 08:58',
				reason: 'Registration',
				point: '0'
			},
			{
				date: '05-28-2018 08:58',
				reason: 'Registration',
				point: '0'
			},
			{
				date: '04-13-2018 13:33',
				reason: 'Converted to E-Wallet',
				point: '-167'
			}
		];*/
	}

	selectTransaction(){ 
		this.isActDisabled = true;
		this.content.getRewardTransaction(this.activity, this.userId, this.currentPage, this.perPage).subscribe((data:any) => { 
			this.transctions = data.results.transctions;
			this.dtTrigger.next();
			this.totalItems = data.totalItems;
			this.pageSize= data.itemsPerPage;
		});
	}

	pageChanged($event)  {
		this.content.getRewardTransaction(this.activity, this.userId, $event, this.perPage).subscribe((data:any) => {
				this.currentPage = $event;
				this.transctions = data.results.transctions;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;
		});
	}

	perPageChanged($event)  {
		this.content.getRewardTransaction(this.activity, this.userId, 1, this.perPage).subscribe((data:any) => {
				this.currentPage = 1;
				this.transctions = data.results.transctions;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;
		});
	}


	enableDisabledBtn(event){
		if(event == ''){
			this.isBtnDisabled = false;
		} else {
			this.isBtnDisabled = true;
		}
	}

	walletPopup(){
		this.bsModalRef = this.modalService.show(WalletModalComponent, {});
	}

	pointsToWallet() {
		this.bsModalRef = this.modalService.show(PointtowalletModalComponent, {});
		this.bsModalRef.content.userPoints = this.balancePoint;
		this.bsModalRef.content.userTotalPoints = this.balancePoint;
	}


	

	
}
