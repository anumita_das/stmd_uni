import { NgModule } from '@angular/core';
import { ShopFromWebComponent } from './shop-from-web.component';
import { SharedModule } from '@app/shared';
import { ShopFromWebRoutingModule } from './shop-from-web-routing.module';

@NgModule({
	imports: [
		ShopFromWebRoutingModule,
		SharedModule
	],
	declarations: [
		ShopFromWebComponent
	]
})
export class ShopFromWebModule {

}
