import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShopFromWebComponent } from './shop-from-web.component';

const routes: Routes = [
	{ path: '', component: ShopFromWebComponent },
	{
		path: '',
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ShopFromWebRoutingModule {

}
