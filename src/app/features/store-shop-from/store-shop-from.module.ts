import { NgModule } from '@angular/core';
import { StoreShopFromComponent } from './store-shop-from.component';
import { SharedModule } from '@app/shared';
import { StoreShopFromRoutingModule } from './store-shop-from-routing.module';
//import { ShopDirectComponent } from './shop-direct/shop-direct.component';

@NgModule({
	imports: [
		StoreShopFromRoutingModule,
		SharedModule
	],
	declarations: [
		StoreShopFromComponent
	]
})
export class StoreShopFromModule {

}
