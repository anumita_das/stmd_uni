import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoreShopFromComponent } from './store-shop-from.component';
//import { ShopDirectComponent } from './shop-direct/shop-direct.component';

const routes: Routes = [
	{ path: '', component: StoreShopFromComponent },
	
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class StoreShopFromRoutingModule {

}
