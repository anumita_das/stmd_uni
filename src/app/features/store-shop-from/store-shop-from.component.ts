import { Component, OnInit, HostListener } from '@angular/core';
import { ContentService } from '../../services/content.service';
import { RegistrationService } from '../../services/registration.service';
import { AppGlobals } from '../../app.globals';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router} from "@angular/router";
import { Meta, Title } from '@angular/platform-browser';


@Component({
	selector: 'app-shop-from',
	templateUrl: './store-shop-from.component.html',
	styleUrls: ['./store-shop-from.component.scss']
})
export class StoreShopFromComponent implements OnInit {
	userId : number;
	isLoggedIn : boolean;
	isMediumScreen: boolean;
	isFloating: boolean = false;
	isFloating2: boolean = false;
	sidebar: boolean;
	isCollapsed: boolean;

	public warehouseList : any;
	public categoryList : any;


	constructor(
		private content: ContentService,
		private regService : RegistrationService,
		private spinner: NgxSpinnerService,
		private router: Router,
		private meta: Meta,
		private title: Title,
		private _global: AppGlobals
		) {
			this.title.setTitle("Find Stores to Shop From Shoptomydoor – Your Personal Shopper");
			this.meta.updateTag({
				name: 'keywords', content: "Find Stores and Get Personal Shopper Solution | Shoptomydoor"
			});
			this.meta.updateTag({
				name: 'description', content: "To get personal shopper solution, use our shop-for-me service today. Our stores to shop include macy’s, Zappos, amazon & ebay. Write us at contact@shoptomydoor.com "
			});


			if(this.regService.item != null)
			{
				this.userId = this.regService.item.user.id;
				this.isLoggedIn = true;
			}else
			{
				this.isLoggedIn = false;
			}
			
		}

	ngOnInit() {

		this.getWarehouseList();
		this.getCategoryList();
	 }

	isLargeScreen() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		if (width > 992) {
			return true;
		} else {
			return false;
		}
	}

	//Fetch Category List
	getCategoryList(){
		this.content.categorylist('shopforme').subscribe((data:any) => {
			this.categoryList = data;
		});
	}

	//Fetch Warehouse List
	getWarehouseList(){
		if(this.userId)
		{
			this.content.warehouselistshopfrom(this.userId).subscribe((data:any) => {
				if(data.length > 0){
					this.warehouseList = data;
				}
			});
		}else{

			this.content.allwarehouseshopfrom().subscribe((data:any) => {
				if(data.length > 0){
					this.warehouseList = data;
				}
			});
		}

		
	}

	getWarehouseId(warehouseId, warehouseCode)
	{
		//alert(warehouseId);
		$('#warehouseId').val(warehouseId);
		$('#warehouseCode').val(warehouseCode);
	}
	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		this.checkWindowSize(event.target.innerWidth);
	}
}
