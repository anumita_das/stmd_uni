import { NgModule } from '@angular/core';
//import { NumberOnlyDirective } from '../../number.directive';
import { JoinRoutingModule } from './join-routing.module';
import { JoinComponent } from './join.component';
import { SharedModule } from '@app/shared';
import { HttpModule }    from '@angular/http';
import { FormsModule }   from '@angular/forms';
import { PasswordStrengthBarModule } from 'ng2-password-strength-bar';
import { PaymentComponent } from './payment/payment.component';

@NgModule({
	imports: [
		JoinRoutingModule,
		SharedModule,
		HttpModule,
		FormsModule,
		PasswordStrengthBarModule
	],
	declarations: [
            JoinComponent,
            PaymentComponent,
	]
})
export class JoinModule {	

}
