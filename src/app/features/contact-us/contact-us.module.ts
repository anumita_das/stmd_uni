import { NgModule } from '@angular/core';
import { ContactUsRoutingModule } from './contact-us-routing.module';
import { ContactUsComponent } from './contact-us.component';
import { SharedModule } from '@app/shared';
import { ThankyouComponent } from './thankyou/thankyou.component';


@NgModule({
	imports: [
		ContactUsRoutingModule,
		SharedModule
	],
	declarations: [
		ContactUsComponent,
		ThankyouComponent
	]
})
export class ContactUsModule {

}
