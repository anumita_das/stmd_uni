
import { NgModule } from '@angular/core';
//import { NumberOnlyDirective } from '../../number.directive';
import { ConfirmRoutingModule } from './confirm-routing.module';
import { ConfirmComponent } from './confirm.component';
import { SharedModule } from '@app/shared';
import { HttpModule }    from '@angular/http';
import { FormsModule }   from '@angular/forms';
import { PasswordStrengthBarModule } from 'ng2-password-strength-bar';


@NgModule({
	imports: [
		ConfirmRoutingModule,
		SharedModule,
		HttpModule,
		FormsModule,
		PasswordStrengthBarModule
	],
	declarations: [
	ConfirmComponent,
	//NumberOnlyDirective
	]
})
export class ConfirmModule {

	

}
