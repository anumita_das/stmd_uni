import { Component, OnInit, ElementRef } from '@angular/core';
import { NgForm }   from '@angular/forms';
import { User } from '../../shared/models/user.model';
import { ContentService } from '../../services/content.service';
import { Router, ActivatedRoute, Params, ParamMap } from '@angular/router';
import { AppGlobals } from '../../app.globals';
import { Meta, Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
//import { NumberOnlyDirective } from '../../number.directive';

@Component({
	selector: 'app-confirm',
	templateUrl: './confirm.component.html',
	styleUrls: ['./confirm.component.scss'],
	providers: [AppGlobals]
})
export class ConfirmComponent implements OnInit {

	showText : boolean = false;
	shipmentId : any;
	deliveryId : any;
	confirmationFlag: any;
	bannerImage: any;	


	public barLabel: string = "Password strength:";
   

	constructor(
		private content:ContentService,
		private route: ActivatedRoute,
		private toastr: ToastrService,
		public spinner : NgxSpinnerService,
		public router: Router,
	)  { 
		
	}

	ngOnInit() {
	
		this.getBanner();

		if(this.route.snapshot.paramMap.get('shipmentId') && this.route.snapshot.paramMap.get('deliveryId'))
		{
			this.shipmentId = this.route.snapshot.paramMap.get('shipmentId');
			this.deliveryId = this.route.snapshot.paramMap.get('deliveryId');
			this.confirmationFlag = this.route.snapshot.paramMap.get('flag');


			if(this.confirmationFlag == "Yes")
			{	this.spinner.show();
				this.content.rateourservice(this.shipmentId, this.deliveryId).subscribe((data:any) => {
				this.spinner.hide();
				if(data.status == 1)
				{
					this.toastr.success("Thank you for choosing us");
					setTimeout(function(){
					        this.router.navigate(['home']);
					    },1000);
				}
				else
				{
					this.toastr.error("Something went wrong");
				}
			});

			}
			if(this.confirmationFlag == "No"){
				this.spinner.show();
				this.content.delaymailtoadmin(this.shipmentId, this.deliveryId).subscribe((data:any) => {
				this.spinner.hide();
				if(data.status == 1)
				{							
					this.showText = true;
				}
				else
				{
					this.toastr.error("Something went wrong");
				}
			});


		  }
		}
		
		
	}

	getBanner(){

		this.content.registrationBanner().subscribe((data:any) => {
			if(data.status == 1)
			{
				this.bannerImage = data.results.bannerImage;
			}

			
		});
	}



	

}
