import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../../services/auth-guard.service';

import { AutoPartsComponent } from './autoparts.component';
import { HowItWorkComponent } from './how-it-work/how-it-work.component';
import { OrderFormComponent } from './order-form/order-form.component';
import { MyWishlistComponent } from './my-wishlist/my-wishlist.component';
import { CartComponent } from './checkout/cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { PersonalDetailComponent } from './checkout/personal-detail/personal-detail.component';
import { ShippingAndPaymentComponent } from './checkout/shipping-and-payment/shipping-and-payment.component';
import { PlaceOrderComponent } from './checkout/place-order/place-order.component';
import { OrderHistoryComponent } from './order-history/order-history.component';


const routes: Routes = [
	{
		path: '', redirectTo: 'how-it-work', pathMatch: 'full'
	},
	{
		path: '',
		component: AutoPartsComponent,
		canActivateChild: [ AuthGuardService ],
		children: [
			{ path: 'how-it-work', canActivateChild: [AuthGuardService], component: HowItWorkComponent },
			{ path: 'order-form', canActivateChild: [AuthGuardService], component: OrderFormComponent },
			{ path: 'my-wishlist', canActivateChild: [AuthGuardService], component: MyWishlistComponent },
			{ path: 'checkout',
				component: CheckoutComponent,
				children: [
					{ path: '', redirectTo: 'cart', pathMatch: 'full' },
					{ path: 'cart', canActivateChild: [AuthGuardService], component: CartComponent },
					{ path: 'personal-detail', canActivateChild: [AuthGuardService], component: PersonalDetailComponent },
					{ path: 'shipping-and-payment', canActivateChild: [AuthGuardService], component: ShippingAndPaymentComponent },
					{ path: 'place-order', canActivateChild: [AuthGuardService], component: PlaceOrderComponent }
				]
			},
			{ path: 'order-history', canActivateChild: [AuthGuardService], component: OrderHistoryComponent }
		]
	}

];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class AutoPartsRoutingModule {

}
