import { Component, OnInit } from '@angular/core';
import { NgForm }   from '@angular/forms';
import { AppGlobals } from '../../../app.globals';
import { Meta, Title } from '@angular/platform-browser';
import { ContentService } from '../../../services/content.service'
import { RegistrationService } from '../../../services/registration.service';
import { ShopformeService } from '../../../services/shopforme.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ShippingcostModalComponent } from './shippingcost-modal.component';
import { ShowItemDetailModalComponent } from './showitem-modal.component';
import { Router, ActivatedRoute, Params, ParamMap} from "@angular/router";
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-order-history',
	templateUrl: './order-history.component.html',
	styleUrls: ['./order-history.component.scss']
})
export class OrderHistoryComponent implements OnInit {
	isMediumScreen: boolean;
	details:any;
	totalCost:any;
	grossCost:any;
	orderId:any;
	orderDate:any = 'AD';
	status:any = '';
	rangeDate:any;
	perPage:any = 10;
	procurementShipment:any;
	dtOptions: DataTables.Settings = {};
	showMoreOptions = false;
	selectedAll: any;
	orders: Array<any>;
	names: any;
	isPeriodDisabled: boolean;	
	isRequestPeriodDisabled: boolean;
	check_all;
	showHistory: boolean;
	currentPage: number = 1;
	pageSize: number;
	totalItems: number;
	checkMore:boolean = false;
	bsModalRef: BsModalRef;
	modalRef: BsModalRef;
	totalProcurementCost: any;
	totalItemCost: any;
	totalProcessingFee :any;
	urgentPurchaseCost :any;	
	requestForCostShipment : Array<any>;
	totaRequestForCostItems : any;
	requestforcostDate:any = 'AD';
	requestrangeDate:any;
	reqCurrentPage: number = 1;
	reqPageSize:any;
	reqPerPage:any = 10;
	private fragment: string;


	config = {
		ignoreBackdropClick: false
	};

	constructor(

		public shopformeService : ShopformeService,
		private router: Router,
		private modalService: BsModalService,
		private toastr: ToastrService,
		private activeRoute: ActivatedRoute
	) {
		this.check_all = {
			type: false
		};
	}

	ngOnInit() {

	if (localStorage.getItem("autoPartsOrderHistory") === null) {		
		var formdata = {currentPage:this.currentPage, perPage:this.perPage, searchtype: 'allshipment'};
		this.shopformeService.getprocurementshipmentforautopartsdetailsPaging(formdata).subscribe((data:any) => {
			if(data.results == 'success'){
				this.details = data.procurementShipment;
				this.totalCost = data.totalCost;
				this.grossCost = data.grossCost;	
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;	
			}
		});
	} else {
		var autoPartsOrderHistory =  JSON.parse(localStorage.getItem('autoPartsOrderHistory'));
		this.orderId = autoPartsOrderHistory.orderId;
		this.currentPage = autoPartsOrderHistory.currentPage;
		this.status = autoPartsOrderHistory.status;
		this.orderDate = autoPartsOrderHistory.orderDate;
		this.rangeDate = autoPartsOrderHistory.rangeDate;
		this.perPage = autoPartsOrderHistory.perPage;
		if(this.orderDate == 'FR'){
			this.isPeriodDisabled = true;
		} else {
			this.isPeriodDisabled = false;
		}
		if(autoPartsOrderHistory.orderId != '' || autoPartsOrderHistory.status != ''){ 
			this.showMoreOptions = !this.showMoreOptions;
			this.checkMore = true;
		}

		this.shopformeService.getprocurementshipmentforautopartsdetailsPaging(autoPartsOrderHistory).subscribe((data:any) => {
			if(data.results == 'success'){
				this.details = data.procurementShipment;
				this.totalCost = data.totalCost;
				this.grossCost = data.grossCost;	
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;
			}
		});
	}

	if (localStorage.getItem("autoPartsReqOrderHistory") === null) {
			var formdata2 = {currentPage:this.reqCurrentPage, perPage:this.reqPerPage, searchtype: 'requestforshipment'};
			this.shopformeService.getprocurementshipmentforautopartsdetailsPaging(formdata2).subscribe((data:any) => {
				if(data.results == 'success'){
					this.requestForCostShipment = data.requestForCostShipment;
					this.totaRequestForCostItems = data.totaRequestForCostItems;
					this.reqPageSize= data.itemsPerPage;	
				}
			});
		} 	else {
			var autoPartsReqOrderHistory =  JSON.parse(localStorage.getItem('autoPartsReqOrderHistory'));
			this.requestforcostDate = autoPartsReqOrderHistory.requestforcostDate;
			this.requestrangeDate = autoPartsReqOrderHistory.requestrangeDate;
			this.reqCurrentPage = autoPartsReqOrderHistory.currentPage;
			this.reqPerPage = autoPartsReqOrderHistory.perPage;

			if(this.requestforcostDate == 'FR'){
				this.isRequestPeriodDisabled = true;
			} else {
				this.isRequestPeriodDisabled = false;
			}
			
			this.shopformeService.getprocurementshipmentforautopartsdetailsPaging(autoPartsReqOrderHistory).subscribe((data:any) => {
				if(data.results == 'success'){				
					this.requestForCostShipment = data.requestForCostShipment;
					this.totaRequestForCostItems = data.totaRequestForCostItems;
					this.reqPageSize= data.itemsPerPage;
				}
			});
		}

		
		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};
	}

	ngAfterViewInit() {
	  // Scroll (Anchor) Function to Order History
	  	

	  	this.activeRoute.fragment.subscribe((fragment: string) => {
		  	if(fragment == 'orderhistory')
		  	{ 
				setInterval(()=>{
				this.scroll('orderhistory')
				},2000);
			}
		});
		
	}

	scroll(id) {
	  let el = document.getElementById(id);
	  el.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});
	}

	showMore(event) {
		this.showMoreOptions = !this.showMoreOptions;
	}

	// openHistory
	openHistory() {
		this.showHistory = true;
	}

	onSubmit(form: NgForm){

	if(!form.value.orderId){
		form.value.orderId = "";
	}
	if(!form.value.status){
		form.value.status = "";
	}



	var autoPartsOrderHistory = {'searchtype': 'allshipment', 'orderDate':form.value.orderDate,  'rangeDate':form.value.rangeDate, 'orderId':form.value.orderId, 'status':form.value.status, 'currentPage':this.currentPage, perPage:this.perPage};

	localStorage.removeItem("autoPartsOrderHistory");
	localStorage.setItem("autoPartsOrderHistory", JSON.stringify(autoPartsOrderHistory));

    	this.shopformeService.getprocurementshipmentforautopartsdetailsPaging(autoPartsOrderHistory).subscribe((data:any) => {
			if(data.results == 'success'){
				this.details = data.procurementShipment;
				this.totalCost = data.totalCost;
				this.grossCost = data.grossCost;	
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;
			}
		});
    }

    filterRequestCostShipment(form: NgForm) {


	if(!form.value.requestforcostDate)
		{
			form.value.requestforcostDate ="";
		}


    	var requestCostShipmentHistory = {'requestforcostDate':form.value.requestforcostDate,'requestrangeDate':form.value.requestrangeDate, 'currentPage':this.reqCurrentPage, perPage:this.reqPerPage, 'searchtype': 'requestforshipment'};

    	localStorage.removeItem("autoPartsReqOrderHistory");
    	localStorage.setItem("autoPartsReqOrderHistory", JSON.stringify(requestCostShipmentHistory));

    	this.shopformeService.getprocurementshipmentforautopartsdetailsPaging(requestCostShipmentHistory).subscribe((data:any) => {
			if(data.results == 'success'){
				this.requestForCostShipment = data.requestForCostShipment;
				this.totaRequestForCostItems = data.totaRequestForCostItems;
				this.reqPageSize= data.itemsPerPage;	
			}
		});
    }

    pageChanged($event)  {

    	let param = {'searchtype': 'allshipment', orderDate:this.orderDate, rangeDate:this.rangeDate, orderId:this.orderId, status:this.status, currentPage:$event, perPage:this.perPage};
    	localStorage.removeItem("autoPartsOrderHistory");
    	localStorage.setItem("autoPartsOrderHistory", JSON.stringify(param));

		this.shopformeService.getprocurementshipmentforautopartsdetailsPaging(param).subscribe((data:any) => {
			if(data.results == 'success'){
				this.currentPage = $event;
				this.details = data.procurementShipment;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;
				this.totalCost = data.totalCost;
				this.grossCost = data.grossCost;
				this.currentPage = 	$event;
			}
		});
	}

	perPageChanged($event, type)  {

		if(type == 'reqpagination')
			this.reqCurrentPage = $event;

    	let param = {requestforcostDate:this.requestforcostDate, requestrangeDate:this.requestrangeDate, currentPage:this.reqCurrentPage, perPage:this.reqPerPage, 'searchtype': 'requestforshipment'};

    	localStorage.removeItem("autoPartsReqOrderHistory");
    	localStorage.setItem("autoPartsReqOrderHistory", JSON.stringify(param));


		this.shopformeService.getprocurementshipmentforautopartsdetailsPaging(param).subscribe((data:any) => {
			if(data.results == 'success'){
				this.requestForCostShipment = data.requestForCostShipment;
				this.totaRequestForCostItems = data.totaRequestForCostItems;
			}
		});
	}


	selectAll(space) {
		if (space === 'type') {
			if (this.check_all.type === true) {
				this.orders.forEach(type => {
					type.selected = false;
				});
			} else {
				this.orders.forEach(type => {
					type.selected = true;
				});
			}
		}
		this.filterSearch('');
	}

	filterSearch(selected) {
		for (const type of this.orders) {
			if (!type.selected) {
				this.check_all.type = false;
				break;
			} else {
				this.check_all.type = true;
			}
		}
	}



	// modal photo shot
	enableRange(event) {
		if (event === 'FR') {
			this.isPeriodDisabled = true;
		} else {
			this.isPeriodDisabled = false;
		}
	}

	enableRequestRange(event) {
		if (event === 'FR') {
			this.isRequestPeriodDisabled = true;
		} else {
			this.isRequestPeriodDisabled = false;
		}
	}

	getText(status, invoiceSent) {
	   if (status === "submitted") return "Submitted";	   
	   if (status === "orderplaced") return "Order Placed";
	   if (status === "cancelled") return "Cancelled";
	   if (status === "requestforcost") {
		if(invoiceSent == 'N')
	   		return "Requested For Shipping Cost";
	   	else
	   		return "Shipping Cost Sent";
	   }
	   if (status === "rejected") return "Rejected";
	}

	getTextUrgent(val) {
	   if (val === "Y") return "Yes";
	   if (val === "N") return "No";
	}

	resetFilter(type){
		this.orderId = '';
		this.orderDate = 'AD';		
		this.requestforcostDate ='AD';
		this.status = '';
		this.rangeDate = '';
		this.checkMore = false;
		this.perPage = 10;
		this.showMoreOptions = false;


		if(type == 'allshipment')
		{
			localStorage.removeItem("autoPartsOrderHistory");
			this.shopformeService.getprocurementshipmentforautopartsdetails('', this.perPage, this.currentPage).subscribe((data:any) => {
				if(data.results == 'success'){
					this.details = data.procurementShipment;
					this.totalCost = data.totalCost;
					this.grossCost = data.grossCost;	
					this.totalItems = data.totalItems;
					this.pageSize= data.itemsPerPage;
				}
			});

		}

		if(type == 'requestforcost')
		{
			localStorage.removeItem("autoPartsReqOrderHistory");
			this.shopformeService.getprocurementshipmentforautopartsdetails('', this.perPage, this.currentPage).subscribe((data:any) => {
				if(data.results == 'success'){
					this.requestForCostShipment = data.requestForCostShipment;
					this.totaRequestForCostItems = data.totaRequestForCostItems;
					this.totalCost = data.totalCost;
					this.grossCost = data.grossCost;
					this.pageSize= data.itemsPerPage;
				}
			});

		}
		
	}

	
  viewShippingCost($event, procurementId) {

		this.shopformeService.getprocurementshippingcost(procurementId).subscribe((response:any) => {
			this.bsModalRef = this.modalService.show(ShippingcostModalComponent, {
				class: 'modal-md modal-dialog-centered'
			});
			this.bsModalRef.content.showShipping = true;
			this.bsModalRef.content.procurementId = procurementId;
			this.bsModalRef.content.shippingTypes = response.data.shippingcharges;
			this.bsModalRef.content.paymentDetails = response.data.shipment;
			this.bsModalRef.content.bagList = response.data.packages;
			this.bsModalRef.content.totalItemCost = response.data.shipment.totalItemCost;
			this.bsModalRef.content.totalProcessingFee = response.data.shipment.totalProcessingFee;
			this.bsModalRef.content.urgentPurchaseCost = response.data.shipment.urgentPurchaseCost;
			this.bsModalRef.content.totalProcurementCost = response.data.shipment.totalProcurementCost;
		});
	}

	showItemDetail($event, procurementId)
	{
		this.shopformeService.getautopartsdetail(procurementId).subscribe((response:any) => {

			this.bsModalRef = this.modalService.show(ShowItemDetailModalComponent, {
				class: 'modal-md modal-dialog-centered'
			});

			this.bsModalRef.content.showShipping = true;
			this.bsModalRef.content.procurementId = procurementId;
			this.bsModalRef.content.bagList = response.procurement.items;
			
		});

	}

	print(procurementId) {
		let printContents, popupWin;

		this.shopformeService.getAutopartsInvoice(procurementId).subscribe((response:any) => {
			if(response.status == 1) {
			    printContents = response.data;
			    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
			    popupWin.document.open();
			    popupWin.document.write(`
			      <html>
			        <head>
			          <title>Print tab</title>
			          <style>
			          //........Customized style.......
			          </style>
			        </head>
			    <body onload="window.print();window.close()">${printContents}</body>
			      </html>`
			    );
			    popupWin.document.close();
		    } else {
				this.toastr.error('Invoice not available. Please contact site admin!!');
			  }
		});

	    
	}


}
