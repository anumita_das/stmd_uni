import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm }   from '@angular/forms';
import { RegistrationService } from '../../../services/registration.service';
import { ShopformeService } from '../../../services/shopforme.service';
import {Router} from "@angular/router";
import { AppGlobals } from '../../../app.globals';


@Component({
	selector: 'app-shippingcost-modal',
	templateUrl: './shippingcost-modal.component.html',
	styleUrls: ['./shippingcost-modal.component.scss'],
	providers: [AppGlobals]
})
export class ShippingcostModalComponent implements OnInit {
	isMediumScreen: boolean;
	title: string;
	closeBtnName: string;
	shippingTypes: any;
	paymentDetails: any;
	defaultCurrencySymbol: string;
	procurementId: any;
	dtOptions: DataTables.Settings = {};
	bagList: any;
	totalProcurementCost: any;
	totalItemCost: any;
	totalProcessingFee :any;
	urgentPurchaseCost :any;
	totaRequestForCostItems : any;
	showShipping: any;

	constructor(
		public bsModalRef: BsModalRef,
		public userInfo : RegistrationService,
		private shopformeService :  ShopformeService,
		private router: Router,
		private _global: AppGlobals,
	) {
			this.defaultCurrencySymbol = this._global.defaultCurrencySymbol;
	}

	ngOnInit() {
		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};
	}

	//Save Cart Items For Procurement
	submitProcurement($event, shippingId) {
		const formData =  {
			procurementId : this.procurementId,
			shippingId : shippingId,
			paymentDetails :  this.paymentDetails
		};
		this.shopformeService.submitrequestforcostautopartsdata(formData).subscribe((data:any) => {
			if(data.results == 'success'){
				localStorage.setItem('cartData', '');
				localStorage.setItem('cartData', JSON.stringify(data.procurement));
				this.bsModalRef.hide();
				this.router.navigate(['auto-parts','checkout']);
			}
		});
	}


	hack(value) {
	   return Object.values(value)
	}
  
}
