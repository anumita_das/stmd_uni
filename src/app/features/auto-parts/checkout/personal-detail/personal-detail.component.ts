import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { NgForm }   from '@angular/forms';
import { AppGlobals } from '../../../../app.globals';
import { Meta, Title } from '@angular/platform-browser';
import { AutoService } from '../../../../shared/services/auto.service';
import { ContentService } from '../../../../services/content.service';
import { RegistrationService } from '../../../../services/registration.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
	selector: 'app-personal-detail',
	templateUrl: './personal-detail.component.html',
	styleUrls: ['./personal-detail.component.scss']
})
export class PersonalDetailComponent implements OnInit {
	billForm = true;
	formSubmit : boolean;	
	bsModalRef: BsModalRef;
	datepickerModel: Date;
	userDetails:Array<any> = [];
	bagList : Array<any> = [];
	cartDetails : Array<any> = [];
	showedit = false;
	isShippingReadOnly = false;
	user:any;
	userDefaultShippingAddress : any = [];
	userDefaultBillingAddress : any = [];
	shippingCountryName  : any;
	shippingStateName  : any;
	shippingCityName : any;
	billingCountryName : any;
	billingStateName : any;
	billingCityName : any;
	defaultBillingCountryId : any;
	phcodeList: any;
	shippingisdCode : any;

	public countryList : any;
	public stateList : any;
	public cityList : any;
	public billingstateList : any;
	public billingcityList : any;
	

	constructor(
	private router: Router,
	private modalService: BsModalService,
	public regService: RegistrationService,
	public spinner: NgxSpinnerService,
	public content: ContentService) { 

		if("cartData" in localStorage){
		  	 var usercart = localStorage.getItem('cartData');
				 if(usercart == null || usercart == ""){
				 	this.router.navigate(['/auto-parts','order-form']);   
				 } 
				 else {
				 	if("data" in JSON.parse(localStorage.getItem('cartData'))){
				 		//do nothing
				 	} else {
				 		 this.router.navigate(['/auto-parts','checkout','cart']);   
				 	}
			}
			
		} else {
		   this.router.navigate(['/auto-parts','order-form']);   
		}

		this.user = this.regService.item.user;

		this.spinner.show();
	}

	ngOnInit() {
		const usercart = JSON.parse(localStorage.getItem('cartData'));	
	
		if(usercart.data.shippingMethodId != '')
			{this.showedit = true;}

		this.formSubmit = false;

		this.getAllCode();
		this.getUserDetails();	
		this.getCountryList();
		this.getUserShippingAddress();	
		this.getUserBillingAddress();

		setTimeout(() => {
			this.spinner.hide();
		}, 3000);

		this.user['phcode'] = "234";
			
	}

	showForm(event) {
		this.billForm = !this.billForm;
	}

	//Fetch country list

	getCountryList()
	{
		this.content.allCountry().subscribe((data:any) => {
				this.countryList = data;
			});
	}

	getAllCode()
	{
		this.content.allCode().subscribe((data:any)=>{
			this.phcodeList = data;
		});
	}

	getStateCity(event,type){
		let selectedCountry = event.target.value;
		this.content.stateCityOfCountry(selectedCountry).subscribe((data:any) => {
			if(type == 'billing'){
				this.userDefaultBillingAddress['stateId'] = '';
				this.userDefaultBillingAddress['cityId'] = '';
				this.billingstateList = data.stateList;
			}
			else {
				this.userDefaultShippingAddress['stateId'] = '';
				this.userDefaultShippingAddress['cityId'] = '';
				this.stateList = data.stateList;
			}
		});
	}

	getCity(event,type) {
		let selectedState = event.target.value;
		this.content.cityOfStates(selectedState).subscribe((data:any) => {
			if(type == 'billing'){
				this.userDefaultBillingAddress['cityId'] = '';
				this.billingcityList = data.cityList;
			}
			else {
				this.userDefaultShippingAddress['cityId'] = '';
				this.cityList = data.cityList;
			}
		});
	}


	getUserDetails() {
		var usercart =  JSON.parse(localStorage.getItem('cartData'));


		if(typeof(usercart.personalDetails) != 'undefined'){
			this.user['title'] = usercart.personalDetails['title'];
			this.user['email'] = usercart.personalDetails['userEmail'];
			this.user['firstName'] = usercart.personalDetails['firstName'];
			this.user['lastName'] = usercart.personalDetails['lastName'];
			this.user['company'] = usercart.personalDetails['company'];
			if(usercart.personalDetails['phcode']!='' || usercart.personalDetails['phcode'] != 'undefined')
			{
				this.user['phcode'] = usercart.personalDetails['phcode'];
			}
			else{
				this.user['phcode'] = "234";
			}
			this.user['contactNumber'] = usercart.personalDetails['phone'];
		} else {
			this.user = this.regService.item.user;
			this.user['phcode'] = "234";
			this.user['title'] = "Mr.";
		}
		
	}


	getUserShippingAddress() {
		var usercart =  JSON.parse(localStorage.getItem('cartData'));
		
		if(typeof(usercart.personalDetails) !== 'undefined'){

			if(usercart.personalDetails['locationId'] != '')
				this.isShippingReadOnly = true;

			//Set Data From Local Storage
			this.userDefaultShippingAddress['firstName'] = usercart.personalDetails['shippingFirstName'];
			this.userDefaultShippingAddress['lastName'] = usercart.personalDetails['shippingLastName'];
			this.userDefaultShippingAddress['email'] = usercart.personalDetails['shippingEmail'];
			this.userDefaultShippingAddress['address'] = usercart.personalDetails['shippingAddress'];
			this.userDefaultShippingAddress['alternateAddress'] = usercart.personalDetails['shippingAddress2'];
			this.userDefaultShippingAddress['countryId'] = usercart.personalDetails['shippingCountry'];
			this.userDefaultShippingAddress['stateId'] = usercart.personalDetails['shippingState'];
			this.userDefaultShippingAddress['cityId'] = usercart.personalDetails['shippingCity'];
			this.userDefaultShippingAddress['zipcode'] = usercart.personalDetails['shippingZipCode'];
			this.userDefaultShippingAddress['phone'] = usercart.personalDetails['shippingContact'];				
			
			if(usercart.personalDetails['shippingIsdcode']!='' || usercart.personalDetails['shippingIsdcode'] != 'undefined')
			{
				this.userDefaultShippingAddress['isdCode'] = usercart.personalDetails['shippingIsdcode'];

			}
			else{
				this.userDefaultShippingAddress['isdCode'] = "234";
			}
			
			this.userDefaultShippingAddress['locationId'] = usercart.personalDetails['locationId'];

			this.shippingCountryName = usercart.personalDetails['shippingCountryName'];
			this.shippingStateName  = usercart.personalDetails['shippingStateName'];
			this.shippingCityName  = usercart.personalDetails['shippingCityName'];
			this.shippingisdCode = usercart.personalDetails['shippingIsdcode'];

		}
		else{

			this.regService.userShippingAddress('shipping').subscribe((data:any) =>{
				if(data.status == 1)
				{
					if(data.results[0].locationId != '')
						this.isShippingReadOnly = true;
						
					this.userDefaultShippingAddress = data.results[0];
					this.shippingCountryName  = data.results[0].country.name;
					this.shippingStateName  = data.results[0].state.name;
					this.shippingCityName  = data.results[0].city.name;
					this.userDefaultShippingAddress['isdCode'] = "234";
					this.shippingisdCode = "234";
				}
				
			});

		}
		
	}

	getUserBillingAddress() {

		var usercart =  JSON.parse(localStorage.getItem('cartData'));


		if(typeof(usercart.personalDetails) !== 'undefined'){

			//Set Data From Local Storage
			this.userDefaultBillingAddress['firstName'] = usercart.personalDetails['billingFirstName'];
			this.userDefaultBillingAddress['lastName'] = usercart.personalDetails['billingLastName'];
			this.userDefaultBillingAddress['address'] = usercart.personalDetails['billingAddress'];
			this.userDefaultBillingAddress['email'] = usercart.personalDetails['billingEmail'];
			this.userDefaultBillingAddress['alternateAddress'] = usercart.personalDetails['billingAddress2'];
			this.userDefaultBillingAddress['countryId'] = usercart.personalDetails['billingCountry'];
			this.userDefaultBillingAddress['stateId'] = usercart.personalDetails['billingState'];
			this.userDefaultBillingAddress['cityId'] = usercart.personalDetails['billingCity'];
			this.userDefaultBillingAddress['zipcode'] = usercart.personalDetails['billingZipCode'];
			this.userDefaultBillingAddress['phone'] = usercart.personalDetails['billingContact'];
			
			if(usercart.personalDetails['billingIsdcode']!='' || usercart.personalDetails['billingIsdcode'] != 'undefined')
			{
				this.userDefaultBillingAddress['isdCode'] = usercart.personalDetails['billingIsdcode'];
			}
			else{
				this.userDefaultBillingAddress['isdCode'] = "234";
			}
			


			this.defaultBillingCountryId = usercart.personalDetails['billingCountry'];
			this.billingCountryName = usercart.personalDetails['billingCountryName'];
			this.billingStateName  = usercart.personalDetails['billingStateName'];
			this.billingCityName  = usercart.personalDetails['billingCityName'];

			this.getStateCityList();
		}
		else{

			this.regService.userShippingAddress('billing').subscribe((data:any) =>{
				if(data.status == 1)
				{ 
					this.userDefaultBillingAddress = data.results[0];
					this.defaultBillingCountryId = data.results[0].countryId;
					this.billingCountryName  = data.results[0].country.name;
					this.billingStateName  = data.results[0].state.name;
					this.userDefaultBillingAddress.cityId = data.results[0].city.id;

					if(data.results[0].city.name != null || data.results[0].city.name != '')
					{
						this.billingCityName  = data.results[0].city.name;
					}
					this.userDefaultBillingAddress['isdCode'] = "234";
					this.getStateCityList();
				}
				
			});
		}
	}

	getStateCityList() {
		this.content.stateCityOfCountry(this.defaultBillingCountryId).subscribe((data:any) => {
				if(data.stateList)
				{
					this.stateList = data.stateList;
					this.billingstateList = data.stateList;
				}
				if(data.cityList)
				{
					this.cityList = data.cityList;
					this.billingcityList = data.cityList;
				}
			});
	}


	setBillingCountryName(event) {
		this.billingCountryName = event.target.text;
	}

	setBillingStateName(event) {
		this.billingStateName = event.target.text;
	}

	setBillingCityName(event) {
		this.billingCityName = event.target.text;
	}

	setShippingCountryName(event){
		this.shippingCountryName = event.target.text;
	}

	setShippingStateName(event){
		this.shippingStateName = event.target.text;
	}

	setShippingCityName(event){
		this.shippingCityName = event.target.text;
	}

	setshippingpingIsdCode(event)
	{
		this.shippingisdCode = event.target.text;
	}

  	onSubmit(form: NgForm){  		
  		
  		const storageData = JSON.parse(localStorage.getItem('cartData'));

  		//Clear Storage Data
  		delete storageData.personalDetails;

  		//Reassign Storage Data
    	const newStorageData = Object.assign({"personalDetails":form.value},storageData);
    	localStorage.setItem('cartData', JSON.stringify(newStorageData));


    	this.router.navigate(['/auto-parts','checkout', 'shipping-and-payment']);
		
	}



	
}
