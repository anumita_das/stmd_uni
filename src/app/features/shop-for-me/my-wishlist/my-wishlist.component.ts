import { Component, OnInit, HostListener } from '@angular/core';
import { ShopformeService } from '../../../services/shopforme.service';
import {Router} from "@angular/router";
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-my-wishlist',
	templateUrl: './my-wishlist.component.html',
	styleUrls: ['./my-wishlist.component.scss']
})
export class MyWishlistComponent implements OnInit {
	currentPage: number = 1;
	pageSize: number;
	totalItems: number;
	isActive: boolean;
	selectedAll: any;
	saveForLaterList: Array<any>;
	selectedWishlistItems: Array<any> = [];
	check_all;
	showItems : boolean;
	noItems : boolean;
	public isMediumScreen: boolean;

	constructor(
		public shopformeService : ShopformeService,
		private router: Router,
		private toastr: ToastrService,
	) {
      this.check_all = {
			type: false
		};
	}

	ngOnInit() {
		
		this.getUserWishlistItems();
		this.showItems = false;
		this.noItems = true;
	}

	getUserWishlistItems() {
		this.shopformeService.getSaveForLaterList(this.currentPage).subscribe((response:any) => {
			if(response.results == 'success'){
				this.saveForLaterList = response.data.items;				
				this.totalItems = response.totalItems;
				this.pageSize= response.itemsPerPage;
				if(this.totalItems>0)
				{
					this.showItems = true;
					this.noItems = false;
				}
				else{
					this.showItems = false;
					this.noItems = true;
				}
			}
			
		});
	}

	pageChanged($event)  {
		this.shopformeService.getSaveForLaterList($event).subscribe((response:any) => {
			if(response.results == 'success'){
				this.currentPage = $event;
				this.saveForLaterList = response.data.items;
				this.totalItems = response.totalItems;
				this.pageSize= response.itemsPerPage;
			}
		});
	}

	//Remove Item From Cart List 
	deleteItem(id){
	  this.shopformeService.removeCartItem(id).subscribe((data:any) => {
	    if(data.results == 'success'){
	 		this.getUserWishlistItems();
	 	}
	  });
	}



	selectAll(space) {
		if (space === 'type') {
			this.selectedWishlistItems = [];

			if (this.check_all.type === true) {
				this.saveForLaterList.forEach(type => {
					type.selected = false;
				});
			} else {
				this.saveForLaterList.forEach(type => {
					this.selectedWishlistItems.push(String(type.id));
					type.selected = true;
				});
			}
		}
		this.filterSearch('','');	

	}

	filterSearch($event,selected) {
		if($event.target.checked) {
			this.selectedWishlistItems.push($event.target.value);
		}else {
			var index = this.selectedWishlistItems.indexOf($event.target.value);
			if (index > -1) 
				this.selectedWishlistItems.splice(index, 1);
		}

		for (const type of this.saveForLaterList) {
			if (!type.selected) {
				this.check_all.type = false;
				break;
			} else {
				this.check_all.type = true;
			}
		}
	}

		moveToCart() {
			if(this.selectedWishlistItems.length > 0){
				this.shopformeService.moveToUserCart(this.selectedWishlistItems).subscribe((data:any) => {
				    if(data.results == 'success'){
				 		this.router.navigate(['/shop-for-me','order-form']);
				 	}
				  });
			}else {
				this.toastr.error('Please select an item to proceed!');
			}
		}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}
	
	@HostListener('window: resize', ['$event'])
		onResize(event) {
		this.checkWindowSize(event.target.innerWidth);
	}
	

}
