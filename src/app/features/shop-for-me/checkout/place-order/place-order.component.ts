import { Component, OnInit } from '@angular/core';
import { ShopformeService } from '../../../../services/shopforme.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { CurrencyModalComponent } from '../../../../shared/components/currency-modal/currency-modal.component';
import { Router} from "@angular/router";
import { RegistrationService } from '../../../../services/registration.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ContentService } from '../../../../services/content.service';
import { PaystackModalComponent } from '../../../../shared/components/paystack-modal/paystack-modal.component';
import { PayeezyModalComponent } from '../../../../shared/components/payeezy-modal/payeezy-modal.component';
import { PayPalConfig, PayPalEnvironment, PayPalIntegrationType } from 'ngx-paypal';


@Component({
	selector: 'app-place-order',
	templateUrl: './place-order.component.html',
	styleUrls: ['./place-order.component.scss']
})
export class PlaceOrderComponent implements OnInit {
	dtOptions: DataTables.Settings = {};
	usercart: any;
	bsModalRef: BsModalRef;
	acceptTermsConditions:any;
	couponcode: string;
        subscriptionCouponCode: string;
        couponInValidMsg: string;
        couponInValid:boolean = false;
        blankCouponCode:boolean = true;
        pointEarned:string = '';
        amountEarned:string = '';
        isInvalid:  {
                    poNumber : boolean,
                    companyName : boolean,
                    buyerName : boolean,
                    position : boolean,
            }; 
        poNumber : any = '';
        companyName : any = '';
        buyerName : any = '';
        position : any = '';
        formSubmit:boolean = false;
        ewallet : any = '';
        ewalletInvalid:boolean = false;
        userId: number;
        ewalletMsg: string = '';
        ewalletReadonly:boolean = false; 
        ewalletId: any;
	ccardNumber: any = '';
	expiryMonth: any = '';
	expiryYear: any = '';
	cvvCode: any = '';
	payCardType: any = '';
	couponApplied:boolean = false;
	paypalPayment : boolean = false;
	payPalConfig: any;
	totalPayableCost : number;
	paypalipnConfig:any;
	showPaystackOverlay : boolean = false;
	tempDataId : any = '';

	cardInvalid:  {
    	ccardNumber : boolean,
    	expiryMonth : boolean,
    	expiryYear : boolean,
    	cvvCode : boolean,
    	payCardType : boolean,
	}; 


	constructor(
		private modalService: BsModalService,
		private shopformeService :  ShopformeService,
		private router: Router,
		private regService : RegistrationService,
		private toastr: ToastrService,
		public spinner: NgxSpinnerService,
		private content : ContentService
	) {

		if("userShopForMeCart" in localStorage){
		  	 var usercart = localStorage.getItem('userShopForMeCart');
			 if(usercart == null || usercart == ""){
			 	this.router.navigate(['shop-for-me','order-form']);   
			 } else {
			 	if("paymentMethod" in JSON.parse(localStorage.getItem('userShopForMeCart'))){
			 		//do nothing
			 	} else {
			 		 this.router.navigate(['shop-for-me','checkout','shipping-and-payment']);   
			 	}

			 	if("coupondetails" in JSON.parse(localStorage.getItem('userShopForMeCart'))){
				} else {
					var storedData = JSON.parse(localStorage.getItem('userShopForMeCart'));
					const couponStore = Object.assign({"coupondetails":{"couponCode": '',  "discountAmount":'', "discountPoint":'',"discountType":''}},storedData);
                                        localStorage.setItem('userShopForMeCart', JSON.stringify(couponStore));
				}
			 }
		} else {
		   this.router.navigate(['shop-for-me','order-form']);   
		}

		this.acceptTermsConditions = 'N';
		this.userId = this.regService.item.user.id;
	}

	ngOnInit() {
		// datatable
		this.dtOptions = {
			paging: false,
			ordering: false,
			searching: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

		this.isInvalid = {
	    	poNumber : false,
	    	companyName : false,
	    	buyerName : false,
	    	position : false,
    	}; 

    	this.cardInvalid =  {
	    	ccardNumber : false,
	    	expiryMonth : false,
	    	expiryYear : false,
	    	cvvCode : false,
	    	payCardType : false,
		}; 

		const usercart = JSON.parse(localStorage.getItem('userShopForMeCart'));
		this.usercart = usercart;
		this.couponcode  = usercart.coupondetails.couponCode;

                if(this.regService.item.user.subscription.couponCode)
                  this.subscriptionCouponCode = this.regService.item.user.subscription.couponCode;

		if(this.couponcode != '')
			this.couponApplied = true;

		if(this.usercart.paymentMethod.paymentMethodKey == 'paypalstandard'){
			this.paypalConfig();
		}
	}

	showCurrency() {
		const currencyCode = this.usercart['currencyCode'];

		this.shopformeService.getCurrencyList(currencyCode).subscribe((response:any) => {
			this.bsModalRef = this.modalService.show(CurrencyModalComponent, {});
			this.bsModalRef.content.currencyList = response;
		});

	}

	checkTermsConditions($event) {
		if($event.target.checked == true)
			this.acceptTermsConditions = 'Y';
		else 
			this.acceptTermsConditions = 'N';	

	}


	submitOrder($event) {
		var storedData =  JSON.parse(localStorage.getItem('userShopForMeCart'));
				console.log(storedData);
		if(this.acceptTermsConditions == 'Y'){

			if(this.usercart['paymentMethod'].paymentMethodKey == 'paystack_checkout')
			{
				this.showPaystackOverlay = true;
				if(this.usercart['defaultCurrencyCode'] == 'NGN' || this.usercart['currencyCode'] == 'NGN')
				{
					this.totalPayableCost = parseFloat(this.usercart['data']['defaultTotalCost']) + parseFloat(this.usercart['data']['defaultTotalTax']);					
				}else{
					this.totalPayableCost = parseFloat(this.usercart['data']['totalCost']) + parseFloat(this.usercart['data']['totalTax']);
				}
				
				this.content.getAmountForPaystack(this.totalPayableCost,this.usercart['defaultCurrencyCode']).subscribe((data:any) => {
					this.bsModalRef = this.modalService.show(PaystackModalComponent, {});
					this.bsModalRef.content.warehouseId = this.usercart['warehouseId'];
					this.bsModalRef.content.payAmount = data.amountForPaystack;
				});

				this.modalService.onHide.subscribe(() => {
					setTimeout(() => {
						this.showPaystackOverlay = false;
				    }, 3000);
					
				});
				
			} else if(this.usercart['paymentMethod'].paymentMethodKey == 'wire_transfer'){
				if(this.poNumber == ''){
					this.isInvalid.poNumber = true;
				} else {
					this.isInvalid.poNumber = false;
				}

				if(this.companyName == ''){
					this.isInvalid.companyName = true;
				} else {
					this.isInvalid.companyName = false;
				}

				if(this.buyerName == ''){
					this.isInvalid.buyerName = true;
				} else {
					this.isInvalid.buyerName = false;
				}

				if(this.position == ''){
					this.isInvalid.position = true;	
				} else {
					this.isInvalid.position = false;
				}

				if(this.poNumber == '' || this.companyName== '' || this.buyerName == '' || this.position  == ''){
			        this.formSubmit = false;
				} else {
					
					var storedData =  JSON.parse(localStorage.getItem('userShopForMeCart'));

					//Clear Storage Data
	    			delete storedData.paymentdetails ;

					const paymentData = Object.assign({"paymentdetails":{"poNumber": this.poNumber, "companyName": this.companyName, "buyerName":this.buyerName,  "position" : this.position}},storedData);
		    		localStorage.setItem('userShopForMeCart', JSON.stringify(paymentData));

					this.formSubmit = true;
				}
			}
			else if(this.usercart['paymentMethod'].paymentMethodKey == 'ewallet'){
				if(this.ewallet){
					this.formSubmit = true;

					var storedData =  JSON.parse(localStorage.getItem('userShopForMeCart'));

					//Clear Storage Data
	    			delete storedData.paymentdetails ;

					const paymentData = Object.assign({"paymentdetails":{"id": this.ewalletId,"ewalletId": this.ewalletId}},storedData);
		    		localStorage.setItem('userShopForMeCart', JSON.stringify(paymentData));
				} else{
					this.ewalletInvalid = true;
					this.formSubmit = false;
				}
			} 
			else if(this.usercart['paymentMethod'].paymentMethodKey == 'credit_debit_card'){
				if(this.ccardNumber == '')
					this.cardInvalid.ccardNumber = true;
				else 
					this.cardInvalid.ccardNumber = false;

				if(this.expiryMonth == '')
					this.cardInvalid.expiryMonth = true;
				else 
					this.cardInvalid.expiryMonth = false;
				
				if(this.expiryYear == '')
					this.cardInvalid.expiryYear = true;
				else 
					this.cardInvalid.expiryYear = false;	
				
				if(this.cvvCode == '')
					this.cardInvalid.cvvCode = true;
				else 
					this.cardInvalid.cvvCode = false;	

				if(this.ccardNumber == '' || this.expiryMonth== '' || this.expiryYear == '' || this.cvvCode  == ''){
			        this.formSubmit = false;
				} else {
					
					var storedData =  JSON.parse(localStorage.getItem('userShopForMeCart'));

					//Clear Storage Data
	    			delete storedData.paymentdetails ;

					const paymentData = Object.assign({"paymentdetails":{"cardNumber": this.ccardNumber, "expiryMonth": this.expiryMonth, "expiryYear":this.expiryYear, "cvvCode":this.cvvCode}},storedData);
		    		localStorage.setItem('userShopForMeCart', JSON.stringify(paymentData));

					this.formSubmit = true;
				}	
			} else if(this.usercart['paymentMethod'].paymentMethodKey == 'payeezy'){
				this.showPaystackOverlay = true;

				var storedData =  JSON.parse(localStorage.getItem('userShopForMeCart'));
				//console.log(storedData);
				//Clear Storage Data
	    		delete storedData.paymentdetails ;

	    		var totalCostPayable = parseFloat(storedData.data.totalCost)+parseFloat(storedData.data.totalTax);

	    		this.regService.storeTempData(JSON.parse(localStorage.getItem('userShopForMeCart')),'shopforme','payeezy').subscribe((res:any) => {
	    			this.content.getAmountForPayeezy(totalCostPayable,storedData.currencyCode).subscribe((data:any) => {
						this.bsModalRef = this.modalService.show(PayeezyModalComponent, {backdrop: 'static', keyboard: false});
						this.bsModalRef.content.warehouseId = this.usercart['warehouseId'];
						this.bsModalRef.content.payAmount = data.amountForPayeezy;
						this.bsModalRef.content.amountDisplay = data.amountDisplay;
						this.bsModalRef.content.paySymbol = storedData.defaultCurrencySymbol;
						this.bsModalRef.content.tempDataId = res.tempDataId;
					});

					this.modalService.onHide.subscribe(() => {
						setTimeout(() => {
							this.showPaystackOverlay = false;
					    }, 3000);
						
					});
	    		});
				
				
			}
			  else
				this.formSubmit = true;


			if(this.formSubmit == true){
						this.spinner.show();

						const formData = JSON.parse(localStorage.getItem('userShopForMeCart'));

			            this.shopformeService.submitOrder(formData).subscribe((response:any) => {
				             if(response.status == 1){
				             	localStorage.removeItem('userShopForMeCart');  
				             	setTimeout(() => {
							        /** spinner ends after 3 seconds */
							        this.spinner.hide();
							    }, 3000);
							    this.toastr.success('Your order has been placed successfully!!');
				              	this.router.navigate(['shop-for-me','order-history']);   
				             } else {
				             	localStorage.removeItem('userShopForMeCart');  
				             	if(response.results == 'payment_failed')
				             	{

				             		this.toastr.error(response.msg);
				             		setTimeout(() => {
								        /** spinner ends after 3 seconds */
								        this.spinner.hide();
								    }, 3000);
								    this.router.navigate(['shop-for-me','order-form']);

				             	}else if(response.results == 'payment_under_review')
				             	{
				             		this.toastr.success('', response.msg,{
										disableTimeOut : true,
										positionClass: 'toast-center-center',
										closeButton : true
									});
				             		this.router.navigate(['shop-for-me','order-form']);
				             	}
				             	else
				             	{

				             		this.toastr.error('There seems to be error some while processing the order. Please contact site admin.');	
				             		setTimeout(() => {
								        /** spinner ends after 3 seconds */
								        this.spinner.hide();
								    }, 3000);
								    this.router.navigate(['shop-for-me','order-form']);
				             	}
				             }
			            });
			       
			}
		 } else {
		        	this.toastr.error('Please read and accept Terms and Conditions to continue!!');
		            return false;
		  }

	}

	validateCoupon(){
		this.shopformeService.validatecouponcode(this.couponcode).subscribe((response:any) => {

			var storedData =  JSON.parse(localStorage.getItem('userShopForMeCart'));
			delete storedData.coupondetails;

			this.couponInValid = false;
			this.couponInValidMsg = '';
			if(response.results == 'invalid'){
				this.couponInValidMsg = response.message;
				this.couponInValid = true;
				this.couponApplied = false;;
			}

			if(response.results == 'valid'){
				this.couponInValidMsg = response.message;
				this.couponInValid = true;
				if(response.amount_or_point == 'Point'){							
					const couponStore = Object.assign({"coupondetails":{"couponCode": this.couponcode, "discountAmount":'', "discountPoint":response.point_to_be_discounted, "discountType" : "points"}},storedData);
			    	localStorage.setItem('userShopForMeCart', JSON.stringify(couponStore));
				} else {
					if(storedData.isCurrencyChanged == 'Y'){
						var defaultTotalDiscount = parseFloat(response.amount_to_be_discounted);
						var discount = defaultTotalDiscount * storedData.exchangeRate;
						var totalCost = parseFloat(storedData.data.totalCost);
						var totalDiscountCost = totalCost-discount;

						storedData['data'].totalDiscount = discount;
						storedData['data'].defaultTotalDiscount = defaultTotalDiscount;
						storedData['data'].totalBDiscountCost = totalCost;
						storedData['data'].defaultTotalBDiscountCost = storedData.data.defaultTotalCost; 
						storedData['data'].totalCost = totalDiscountCost;
						storedData['data'].defaultTotalCost = parseFloat(storedData.data.defaultTotalCost) - defaultTotalDiscount;

					} else {
						var discount = parseFloat(response.amount_to_be_discounted);
						var totalCost = parseFloat(storedData.data.totalCost);
						var totalDiscountCost = totalCost-discount;

						storedData['data'].totalDiscount = discount;
						storedData['data'].totalCost = totalDiscountCost;
						storedData['data'].totalBDiscountCost = totalCost;
					}
					localStorage.setItem('userShopForMeCart', JSON.stringify(storedData));

					const couponStore = Object.assign({"coupondetails":{"couponCode": this.couponcode, "subscriptionCouponCode": this.subscriptionCouponCode, "type" : response.type, "discountAmount":discount, "discountPoint":'', "discountType" : "amount"}},storedData);
			    	localStorage.setItem('userShopForMeCart', JSON.stringify(couponStore));
				}

				this.couponApplied = true;
				if(this.usercart.paymentMethod.paymentMethodKey == 'paypalstandard'){
					this.paypalConfig();
				}	
			}

			this.usercart =  JSON.parse(localStorage.getItem('userShopForMeCart'));
		});
	}

	clearCouponCode() {
		var storedData =  JSON.parse(localStorage.getItem('userShopForMeCart'));

		storedData['coupondetails'].couponCode = "";
		storedData['coupondetails'].discountAmount = "";
		storedData['coupondetails'].discountPoint = "";
		storedData['coupondetails'].discountType = "";
		storedData['data'].totalCost = storedData.data.totalBDiscountCost;

		if(storedData.isCurrencyChanged == 'Y')
			storedData['data'].defaultTotalCost = storedData.data.defaultTotalBDiscountCost;

		localStorage.setItem('userShopForMeCart', JSON.stringify(storedData));

		this.usercart =  JSON.parse(localStorage.getItem('userShopForMeCart'));

		this.couponcode = "";
		this.couponInValidMsg = "";
		this.couponApplied = false;

		if(this.usercart.paymentMethod.paymentMethodKey == 'paypalstandard'){
					this.paypalConfig();
				}	
	}

	validateEwallet() {
		if(this.ewallet !== ''){
			this.ewalletInvalid = false;

			if(this.usercart['isCurrencyChanged'] == 'Y')
				var totalCost = this.usercart['data'].defaultTotalCost;
			else
				var totalCost = this.usercart['data'].totalCost;

			const formData = {"ewalletId" : this.ewallet, "amountToBePaid" : totalCost, "userId" : this.userId};
			 this.shopformeService.validateEwallet(formData).subscribe((response:any) => {
		        if(response.status == 1){
		        	this.ewalletReadonly = true;
		        	this.ewalletId = response.results;
		        	this.ewalletMsg = '';
		        	this.toastr.success("E-Wallet ID veirfied successfully.");
		        } else {
		        	this.ewalletMsg = response.results;
		        }
	         });
		} else {
			this.ewalletInvalid = true;
		}
	}

	ewalletCheck($event) {
		if($event != '')
			this.ewalletInvalid = false;
		else
			this.ewalletInvalid = true;
	}

	checkEmptyCouponInput(){
		if(this.couponcode == ''){
			this.blankCouponCode = true;
		} else {
			this.blankCouponCode = false;
		}
	}

	createRangeYear(number){
		var d = new Date();
	    var n = d.getFullYear();

		var expiryYear: number[] = [];
		for(var i = n; i <= n+20; i++){
		 	expiryYear.push(i);
		}
		return expiryYear;
	}

	onSearchChange(searchValue : string ) {  
	
		let regexMap = [
	      {regEx: /^4[0-9]{5}/ig,cardType: "VISA"},
	      {regEx: /^5[1-5][0-9]{4}/ig,cardType: "MASTERCARD"},
	      {regEx: /^3[47][0-9]{3}/ig,cardType: "AMEX"},
	      {regEx: /^(5[06-8]\d{4}|6\d{5})/ig,cardType: "MAESTRO"},
	      {regEx: /^(6(011|5[0-9][0-9])[0-9]{12})/ig,cardType: "DISCOVER"}
	    ];

	    this.payCardType = '';
		for (let j = 0; j < regexMap.length; j++) {
		  if (searchValue.match(regexMap[j].regEx)) {
		    this.payCardType = regexMap[j].cardType;
		    break;
		  }
		}
	}

	paypalConfig() {
		this.spinner.show();
		this.usercart = JSON.parse(localStorage.getItem('userShopForMeCart'));

		
		let totalCost = parseFloat(this.usercart['data']['totalCost'])+parseFloat(this.usercart['data']['totalTax']);
		let total = parseFloat(totalCost.toFixed(2));
		let paidCurrency = this.usercart['defaultCurrencyCode'];
		this.paypalPayment = true;
		
		this.regService.storeTempData(JSON.parse(localStorage.getItem('userShopForMeCart')),'shopforme','paypalstandard').subscribe((res:any) => {
			this.tempDataId = res.tempDataId;
			this.content.getpaypalipnsettings(this.usercart.currencyCode, total).subscribe((response:any) => {
				this.paypalipnConfig = response.results;


				if(this.usercart['isCurrencyChanged'] == 'Y')
				{
					var totalCost = parseFloat(this.usercart['data']['defaultTotalCost'])+parseFloat(this.usercart['data']['defaultTotalTax']);

					var paidCurrency = this.usercart['defaultCurrencyCode'];

					var total = parseFloat(totalCost.toFixed(2));
				}
				else{
					var totalCost = parseFloat(this.usercart['data']['totalCost'])+parseFloat(this.usercart['data']['totalTax']);

					var paidCurrency = this.usercart['currencyCode'];


					var total = parseFloat(totalCost.toFixed(2));
				}
				
				this.paypalPayment = true;

				if(this.paypalipnConfig.mode == 'Sandbox')
				{
			      	this.payPalConfig = new PayPalConfig(PayPalIntegrationType.ClientSideREST, PayPalEnvironment.Sandbox, {
			        commit: true,
			        client: {
			          sandbox: this.paypalipnConfig.key,
			        },
			        button: {
			          label: 'paypal',
			        },
			        onPaymentComplete: (data, actions) => {
			        	this.spinner.show();

			        	const formData = JSON.parse(localStorage.getItem('userShopForMeCart'));
						formData['paypalData'] = data;
						formData['tempDataId'] = this.tempDataId;
			            this.shopformeService.submitOrder(formData).subscribe((response:any) => {
				             if(response.status == 1){
				             	localStorage.removeItem('userShopForMeCart');  
				             	setTimeout(() => {
							        /** spinner ends after 3 seconds */
							        this.spinner.hide();
							    }, 3000);
							    this.toastr.success('Your order has been placed successfully!!');
				              	this.router.navigate(['shop-for-me','order-history']);   
				             } else {
				             	localStorage.removeItem('userShopForMeCart');  
				             	this.toastr.error('There seems to be some while processing the order. Please contact site admin.');	

				             	setTimeout(() => {
							        /** spinner ends after 3 seconds */
							        this.spinner.hide();
							    }, 3000);
							    this.router.navigate(['shop-for-me','order-form']);   
				             }
			            });
			        },
			        onCancel: (data, actions) => {
			          this.toastr.error('The Payment has not been proccessed. Please try again');
			        },
			        onError: (err) => {
			          this.toastr.error('The Payment has not been proccessed. Please try again');
			        },
			        onClick: () => {
			        	this.acceptTermsConditions = 'Y';
			           	this.toastr.success('On clicking this you agree to Shoptomydoor Terms and Conditions');
			         },
			        transactions: [{
			          amount: {
			            currency: this.paypalipnConfig.currencyCode,
			            total: this.paypalipnConfig.total
			          }
			        }]
			      });
			    }
			    else if(this.paypalipnConfig.mode == "Production")
			    {
			    	this.payPalConfig = new PayPalConfig(PayPalIntegrationType.ClientSideREST, PayPalEnvironment.Production, {
			        commit: true,
			        client: {
			          production: this.paypalipnConfig.key,
			        },
			        button: {
			          label: 'paypal',
			        },
			        onPaymentComplete: (data, actions) => {
			        	this.spinner.show();

			        	const formData = JSON.parse(localStorage.getItem('userShopForMeCart'));
						formData['paypalData'] = data;
						formData['tempDataId'] = this.tempDataId;
			            this.shopformeService.submitOrder(formData).subscribe((response:any) => {
				             if(response.status == 1){
				             	localStorage.removeItem('userShopForMeCart');  
				             	setTimeout(() => {
							        /** spinner ends after 3 seconds */
							        this.spinner.hide();
							    }, 3000);
							    this.toastr.success('Your order has been placed successfully!!');
				              	this.router.navigate(['shop-for-me','order-history']);   
				             } else {
				             	localStorage.removeItem('userShopForMeCart');  
				             	this.toastr.error('There seems to be some while processing the order. Please contact site admin.');	

				             	setTimeout(() => {
							        /** spinner ends after 3 seconds */
							        this.spinner.hide();
							    }, 3000);
							    this.router.navigate(['shop-for-me','order-form']);   
				             }
			            });
			        },
			        onCancel: (data, actions) => {
			          this.toastr.error('The Payment has not been proccessed. Please try again');
			        },
			        onError: (err) => {
			          this.toastr.error('The Payment has not been proccessed. Please try again');
			        },
			        onClick: () => {
			        	this.acceptTermsConditions = 'Y';
			           	this.toastr.success('On clicking this you agree to Shoptomydoor Terms and Conditions');
			         },
			        transactions: [{
			          amount: {
			            currency: this.paypalipnConfig.currencyCode,
			            total: this.paypalipnConfig.total
			          }
			        }]
			      });
			    }

			    setTimeout(() => {
			    	/** spinner ends after 5 seconds */
			   		this.spinner.hide();
				}, 3000);

		    }); // paypal ipn config bracket end

		}); // store temp data end
    }


	hack(value) {
	   return Object.values(value)
	}

     /* To copy any Text */
    copyText(val: string){
        let selBox = document.createElement('textarea');
        selBox.style.position = 'fixed';
        selBox.style.left = '0';
        selBox.style.top = '0';
        selBox.style.opacity = '0';
        selBox.value = val;
        document.body.appendChild(selBox);
        selBox.focus();
        selBox.select();
        document.execCommand('copy');
        document.body.removeChild(selBox);

        alert("Coupon copied to Clipboard");
      }    

}
