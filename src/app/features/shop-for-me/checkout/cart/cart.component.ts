import { Component, OnInit } from '@angular/core';
import { ShopformeService } from '../../../../services/shopforme.service';
import {Router} from "@angular/router";
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { CurrencyModalComponent } from '../../../../shared/components/currency-modal/currency-modal.component';
import { RegistrationService } from '../../../../services/registration.service';

@Component({
	selector: 'app-cart',
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
	dtOptions: DataTables.Settings = {};
	bagList : Array<any> = [];
	cartDetails : any;
	showDiscount = false;
	bsModalRef: BsModalRef;
    exchangeRate: any = 1;
    currencySymbol: any = '';
    defaultCurrencySymbol: any = '';
    isCurrencyChanged: any = 'N';
    couponcode: string;
    couponInValidMsg: string;
    couponInValid:boolean = false;
    blankCouponCode:boolean = true;
	personaldetails : Array<any> = [];
	user : Array<any>;

	constructor(
		private shopformeService :  ShopformeService,
		private auth: RegistrationService,
		private router: Router,
		private modalService: BsModalService,
	) { 
	   	this.user = this.auth.item.user; 
	}

	ngOnInit() {
		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

		this.getUserCartList();
	}

	//Fetch Cart List from Session
  	getUserCartList() {
  		const usercart = JSON.parse(localStorage.getItem('userShopForMeCart'));
  		this.bagList = usercart.items;
  		this.cartDetails = usercart.data;

  		this.currencySymbol = usercart.currencySymbol;
  		this.defaultCurrencySymbol = usercart.defaultCurrencySymbol;
  		this.isCurrencyChanged = usercart.isCurrencyChanged;
	}

	clearcart() {
		this.shopformeService.clearUserCart('shopforme').subscribe((response:any) => {
		if(response.results == 'success')
			{
				localStorage.removeItem('userShopForMeCart');
				this.router.navigate(['/shop-for-me','order-form']);
			}
			
		});
	}

	showCurrency() {
		
		const usercart = JSON.parse(localStorage.getItem('userShopForMeCart'));
		const currencyCode = usercart.currencyCode;

		this.shopformeService.getCurrencyList(currencyCode).subscribe((response:any) => {
			this.bsModalRef = this.modalService.show(CurrencyModalComponent, {});
			this.bsModalRef.content.currencyList = response;
		});

	}

	addInsurance($event) {
		const usercart = JSON.parse(localStorage.getItem('userShopForMeCart'));

		if($event.target.checked == true){
			this.cartDetails['isInsuranceCharged']  = 'Y';
			this.cartDetails['totalCost'] = this.cartDetails['totalInsurance']+this.cartDetails['totalCost'];

	
		} else {
			this.cartDetails['isInsuranceCharged'] = 'N';	
			this.cartDetails['totalCost'] = this.cartDetails['totalCost']-this.cartDetails['totalInsurance'];
		}

		//Reassign Storage Data
	    usercart['data'] = this.cartDetails;
	    localStorage.setItem('userShopForMeCart', JSON.stringify(usercart));
	}

	checkout() {
		this.router.navigate(['/shop-for-me','checkout','personal-detail']);
	}

	hack(value) {
	   return Object.values(value)
	}

	validateCoupon(){
		this.shopformeService.validatecouponcode(this.couponcode).subscribe((response:any) => {
			this.couponInValid = false;
			this.couponInValidMsg = '';
			if(response.results == 'invalid'){
				this.couponInValidMsg = response.message;
				this.couponInValid = true;
			}
		});
	}

	checkEmptyCouponInput(){
		if(this.couponcode == ''){
			this.blankCouponCode = true;
		} else {
			this.blankCouponCode = false;
		}
	}
}
