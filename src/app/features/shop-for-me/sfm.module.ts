import { NgModule } from '@angular/core';
import { SFMRoutingModule } from './sfm-routing.module';
import { SFMComponent } from './sfm.component';
import { SharedModule } from '@app/shared';
import { HowItWorkComponent } from './how-it-work/how-it-work.component';
import { OrderFormComponent } from './order-form/order-form.component';
import { MyWishlistComponent } from './my-wishlist/my-wishlist.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { CartComponent } from './checkout/cart/cart.component';
import { PersonalDetailComponent } from './checkout/personal-detail/personal-detail.component';
import { ShippingAndPaymentComponent } from './checkout/shipping-and-payment/shipping-and-payment.component';
import { PlaceOrderComponent } from './checkout/place-order/place-order.component';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { ShippingcostModalComponent } from './order-history/shippingcost-modal.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { NumberOnlyDirective } from '../../services/number.directive';
import { ShowItemDetailModalComponent } from './order-history/showitem-modal.component';

@NgModule({
	imports: [
		NgxPaginationModule,
		SFMRoutingModule,
		SharedModule
	],
	exports: [
		ShippingcostModalComponent,
		ShowItemDetailModalComponent,
	],
	declarations: [
		SFMComponent,
		HowItWorkComponent,
		OrderFormComponent,
		MyWishlistComponent,
		CheckoutComponent,
		CartComponent,
		PersonalDetailComponent,
		ShippingAndPaymentComponent,
		PlaceOrderComponent,
		OrderHistoryComponent,
		ShippingcostModalComponent,
		NumberOnlyDirective,
		ShowItemDetailModalComponent,
	],
	entryComponents: [
		ShippingcostModalComponent,
		ShowItemDetailModalComponent,
	]
})
export class SFMModule {

}
