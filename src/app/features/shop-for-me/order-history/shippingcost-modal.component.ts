import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm }   from '@angular/forms';
import { RegistrationService } from '../../../services/registration.service';
import { ShopformeService } from '../../../services/shopforme.service';
import {Router} from "@angular/router";
import { AppGlobals } from '../../../app.globals';

@Component({
	selector: 'app-shippingcost-modal',
	templateUrl: './shippingcost-modal.component.html',
	styleUrls: ['./shippingcost-modal.component.scss'],
	providers: [AppGlobals]
})
export class ShippingcostModalComponent implements OnInit {

	title: string;
	closeBtnName: string;
	shippingTypes: Array<any>;
	paymentDetails: any;
	defaultCurrencySymbol: string;
	procurementId: any;
	dtOptions: DataTables.Settings = {};
	bagList : any;
	totalItemCost : any;
	totalProcessingFee : any;
	urgentPurchaseCost : any;
	totalProcurementCost : any;
	showShipping : any;

	constructor(
		public bsModalRef: BsModalRef,
		public userInfo : RegistrationService,
		private shopformeService :  ShopformeService,
		private router: Router,
		private _global: AppGlobals,
	) {
			this.defaultCurrencySymbol = this._global.defaultCurrencySymbol;
	}

	ngOnInit() {

		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

	}

	//Save Cart Items For Procurement
	submitProcurement($event, shippingId) {
		const formData =  {
			procurementId : this.procurementId,
			shippingId : shippingId,
			paymentDetails :  this.paymentDetails
		};
		this.shopformeService.submitrequestforcostdata(formData).subscribe((data:any) => {
			if(data.results == 'success'){
				localStorage.setItem('userShopForMeCart', '');
				localStorage.setItem('userShopForMeCart', JSON.stringify(data.procurement));
				this.bsModalRef.hide();
				this.router.navigate(['shop-for-me','checkout']);
			}
		});
	}


	hack(value) {
	   return Object.values(value)
	}


  
}
