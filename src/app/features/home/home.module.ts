import { NgModule } from '@angular/core';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { SharedModule } from '@app/shared';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
	imports: [
		HomeRoutingModule,
		SharedModule,
		LazyLoadImageModule,
	],
	declarations: [
		HomeComponent
	]
})
export class HomeModule {

}
