import { Component, OnInit, Renderer2 } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ServiceModalComponent } from '../../shared/components/service-modal/service-modal.component';
import { ContentService } from '../../services/content.service';
import { HomeBlocks } from '../../shared/models/homeblocks.model';
import { DomSanitizer } from '@angular/platform-browser';
import { AppGlobals } from '../../app.globals';
import { Meta, Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgForm }   from '@angular/forms';
import { Router, ActivatedRoute, Params, ParamMap } from '@angular/router';


@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
	providers: [AppGlobals]
})
export class HomeComponent implements OnInit {
	bannerImages: Array<object>;
	valueAdded: Array<object>;	
	services: Array<object>;
	sporsors: Array<object>;
	stores: Array<object>;
	company: Array<object>;
	showChat: boolean;
	showShipment: boolean;
	showpopover: boolean;
	isFirstOpen: boolean;
	oneAtATime: boolean;
	bsModalRef: BsModalRef;
	homeBlocks= new HomeBlocks();
	blockWhyShoptomydoor : Array<object>;
	blockWorks: Array<object>;
	blockService: Array<object>;
	blockValueAddedService: Array<object>;
	blockShopForMe: Array<object>;
	blockStoreIcon: Array<object>;
	blockWorksImg: any;
	valueAddedImg: any;
	blockServiceImg: any;
	partnerBanking; any;
	partnerLogistic: any;
	partnerContent : any = "";
	specialOfferBanner : Array<object>;
	specialOfferContent : any = "";
	defaultImage1 :any;
	defaultImage2 :any;
	order : any;
	tracking : any;

	constructor(
		private renderer: Renderer2,
		private modalService: BsModalService,
		private contentService : ContentService,
		private sanitizer:DomSanitizer,
		private _global: AppGlobals,
		private meta: Meta, 
		private title: Title,
		public spinner: NgxSpinnerService,
		public router: Router,
		public route: ActivatedRoute,
	) { 
		title.setTitle(this._global.siteTitle);
	    meta.addTags([ 
	      {
	        name: 'keywords', content: this._global.siteMetaKeywords
	      },
	      {
	        name: 'description', content: this._global.siteMetaDescription
	      },
	    ]);
	}

	ngOnInit() {
		//this.spinner.show();
		this.getBannerImages();
		this.getHomepageBlocks();
		this.getStoreIcons();
		this.getSpecialBanners();
		this.bannerImages= [];
		this.blockWhyShoptomydoor=[];
		this.blockWorks=[];
		this.blockService=[];
		this.blockValueAddedService=[];
		this.blockShopForMe=[];
		this.blockStoreIcon=[];
		this.specialOfferBanner = [];
		this.blockWorksImg=['signup.svg','shop-online.svg','we-deliver.svg'];
		this.blockServiceImg=['amazon-and-ebay.svg','buying-cars.svg','car-purchase.svg','shop-via-phone.svg'];
		this.valueAddedImg = [	'free-warehouse.svg',
								'free-storage.svg',
								'free-repackaging.svg',
								'customer-care.svg',
								'online-tracking.svg',
								'african-countries.svg',
								'free-export.svg',
								'discount-on-shipping.svg',
							];
		/*this.valueAdded = [
			{
				img: 'free-warehouse.svg',
				title: 'Free Warehouse Address (USA, UK and China)'
			},
			{
				img: 'free-storage.svg',
				title: '21 Days Free of Charge  Storage Period'
			},
			{
				img: 'free-repackaging.svg',
				title: 'Free Repackaging and Consolidation'
			},
			{
				img: 'customer-care.svg',
				title: 'Responsive 24/7 Customer Care'
			},
			{
				img: 'online-tracking.svg',
				title: 'Online 24/7 Tracking With Regular Notification'
			},
			{
				img: 'free-insurance.svg',
				title: 'Free Insurance Coverage Money'
			},
			{
				img: 'personal-shopper.svg',
				title: 'Personal Shopper Service'
			},
			{
				img: 'discount-on-shipping.svg',
				title: 'Up to 70% discount on Shipping'
			},
		];*/

		// services
		this.services = [
			{
				img: 'air-shipping.svg',
				title: 'Air Shipping',
				placement: 'right'
			},
			{
				img: 'ocean-shipping.svg',
				title: 'Ocean Shipping',
				placement: 'right'
			},
			{
				img: 'car-purchase.svg',
				title: 'Car Purchase',
				placement: 'left'
			},
			{
				img: 'procurement.svg',
				title: 'Procurement',
				placement: 'left'
			}
		];

		// sporsors
		this.sporsors = [
			{ img: 'mc.png' },
			{ img: 'visa.png' },
			{ img: 'access.png' },
			{ img: 'zenith.png' },
			{ img: 'first-bank.png' },
			{ img: 'fcmb.png' },
			{ img: 'stanbic.png' },
			{ img: 'keystone-bank.png' },
			{ img: 'sc.png' },
			{ img: 'uba.png' },
			{ img: 'unity.png' },
			{ img: 'fidelity.png' },
			{ img: 'hb.png' },
			{ img: 'sterling-bank.png' }
		];

		// stors
		this.stores = [
			{ img: 'amazone.png' },
			{ img: 'ebay.png' },
			{ img: 'next.png' },
			{ img: 'woot.png' },
			{ img: 'myhabit.png' },
			{ img: 'amazone.png' },
			{ img: 'ebay.png' },
			{ img: 'next.png' },
			{ img: 'woot.png' },
			{ img: 'myhabit.png' }
		];

		// accordion
		this.isFirstOpen = true;
		this.oneAtATime = true;


		// Partner
		this.getPartnerContent();
		this.getPartnersBanking();
		this.getPartnersLogistic();

		this.showShipment = true;
		this.defaultImage1 = "assets/imgs/loader.svg";
		
		this.defaultImage2 = "assets/imgs/images.png";
	}

	ngAfterViewInit() {
		this.route.fragment.subscribe((fragment: string) => {
			if(fragment){
	       	  this.scroll(fragment);
			}
	    });
	}

	scroll(id) {
	  let el = document.getElementById(id);
	  el.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});
	}

	openServiceModal(content) {
		this.bsModalRef = this.modalService.show(ServiceModalComponent, {});
		this.bsModalRef.content.serviceContent = content;
		this.bsModalRef.content.closeBtnName = 'Close';
	}

	toggleChat() {
		this.showChat = this.showChat ? false : true;
		this.showShipment = false;
	}
	
	toggleShipment() {
		this.showShipment = this.showShipment ? false : true;
		this.showChat = false;
	}

	isLargeScreen() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		if (width > 992) {
			return true;
		} else {
			return false;
		}
	}

	// openPopover() {
	// 	this.showpopover = this.showpopover ? false : true;
	// 	// this.showChat = false;
	// }

	onShown() {
		//console.log('Popover Shown');
		this.renderer.addClass(document.body, 'popover-open');
	}

	onHidden() {
		//console.log('Popover Hidden');
		this.renderer.removeClass(document.body, 'popover-open');
	}

	getBannerImages() {

		this.contentService.bannerContent().subscribe((data:any) => {
			for (let eachBanner of data.results) {
				//var bannerContent = new Object();
				//bannerContent = {'imagePath':'http://10.0.14.167/somnathp/shoptomydoor_git/stmd/public/uploads/banner/'+eachBanner.imagePath}; 
				eachBanner.bannerContent = this.sanitizer.bypassSecurityTrustHtml(eachBanner.bannerContent);
				this.bannerImages.push(eachBanner);
			}
		});
	}

	getSpecialBanners() {

		this.contentService.specialOfferBannerContent().subscribe((data:any) => {
			for (let eachBanner of data.results) {
				eachBanner.imagePath = eachBanner.imagePath;
				eachBanner.pageLink= eachBanner.pageLink;
				this.specialOfferBanner.push(eachBanner);
			}
		});
	}

	getHomepageBlocks() {

		this.contentService.homepageBlockContent().subscribe((data:any) => {
			var workBlockIndex = 0;
			var serviceBlockIndex = 0;
			var valueAddedBlockIndex = 0;
			//console.log(data.results);
			for(let eachBlockContent of data.results) {

				eachBlockContent.content = this.sanitizer.bypassSecurityTrustHtml(eachBlockContent.content);
				if(eachBlockContent.slug =='why_shoptomydoor')
				{
					this.blockWhyShoptomydoor.push(eachBlockContent);
				}
				else if(eachBlockContent.slug =='how_it_works')
				{
					eachBlockContent.image = this.blockWorksImg[workBlockIndex];
					if(workBlockIndex<2)
						eachBlockContent.placement = "right";
					else
						eachBlockContent.placement = "left";
					workBlockIndex++;

					this.blockWorks.push(eachBlockContent);
				}
				else if(eachBlockContent.slug == 'service')
				{
					eachBlockContent.image = this.blockServiceImg[serviceBlockIndex];
					if(serviceBlockIndex<2)
						eachBlockContent.placement = "right";
					else
						eachBlockContent.placement = "left";
					serviceBlockIndex++;

					this.blockService.push(eachBlockContent);
				}
				else if(eachBlockContent.slug == 'value_added_service')
				{
					eachBlockContent.image = this.valueAddedImg[valueAddedBlockIndex];
					this.blockValueAddedService.push(eachBlockContent);
					valueAddedBlockIndex++;
				}
				else if(eachBlockContent.slug == 'shop_for_me')
				{
					this.blockShopForMe.push(eachBlockContent);
				}
				else if(eachBlockContent.slug == 'special_offers')
				{
					this.specialOfferContent = eachBlockContent;
				}
			}

		});
	}

	getStoreIcons() {

		this.contentService.storeIcons().subscribe((data:any) => {
			this.blockStoreIcon = data.results;
			//this.spinner.hide();
			//console.log(this.blockStoreIcon);
		});
	}

	getPartnerContent() {
		this.contentService.getPartnerContent().subscribe((data:any) => {
			this.partnerContent = data.results;
		})
	}

	getPartnersBanking(){
		this.contentService.getPartnersBanking().subscribe((data:any) => {
			this.partnerBanking = data.results.PartnerBanking.Bimages;
		});
	}

	getPartnersLogistic(){
		this.contentService.getPartnersLogistic().subscribe((data:any) => {
			this.partnerLogistic = data.results.PartnerLogistic.Limages;
		});
	}

	checkItemArrived(form : NgForm) {
		if(form.value.trackingNumber!='')
		{
			this.router.navigate(['item-arrived',form.value.trackingNumber]);
		}
	}

	checkTrackShipment(form : NgForm) {
		if(form.value.orderNumber!='')
		{
			this.router.navigate(['track-shipment',form.value.orderNumber]);
		}
	}


}
