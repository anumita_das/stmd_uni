import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, ParamMap } from '@angular/router';
import { ContentService } from '../../services/content.service';
import { AppGlobals } from '../../app.globals';
import { NgForm } from '@angular/forms'; 
import { ToastrService } from 'ngx-toastr'; 
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-rating',
	templateUrl: './rating.component.html',
	styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {
	shipmentId : any;
	ratingvalue : any;
	formSubmit : boolean;
	deliveryId : any;
	feedback : any = "";
	phonenumber: number;

	constructor(
	private content: ContentService,
	private router: Router,
	private route: ActivatedRoute,
	private _global: AppGlobals,
	public toastr : ToastrService,
	public spinner : NgxSpinnerService,
	) { }
	ratingMoods: Array<object>;

	ngOnInit() {

	if(this.route.snapshot.paramMap.get('shipmentId') && this.route.snapshot.paramMap.get('deliveryId') && this.route.snapshot.paramMap.get('ratingval'))
		{
			this.shipmentId = this.route.snapshot.paramMap.get('shipmentId');
			this.ratingvalue = this.route.snapshot.paramMap.get('ratingval');
			this.deliveryId = this.route.snapshot.paramMap.get('deliveryId');
		}


		this.ratingMoods = [
			{
				name: 'Terrible',
				bg: '#EF0000',
				imgPath: 'assets/imgs/icons/terrible.svg'
			},
			{
				name: 'Bad',
				bg: '#FB9600',
				imgPath: 'assets/imgs/icons/bad.svg'
			},
			{
				name: 'Okay',
				bg: '#F6FB00',
				imgPath: 'assets/imgs/icons/okay.svg'
			},
			{
				name: 'Good',
				bg: '#BEFB00',
				imgPath: 'assets/imgs/icons/good.svg'
			},			
			{
				name: 'Excellent',
				bg: '#4AF800',
				imgPath: 'assets/imgs/icons/excellent.svg'
			},
			
		];


	}
	saverating(val)
	{
		this.ratingvalue = val;
	}

	onSubmit(form : NgForm) {		
			console.log(form.value);
			this.spinner.show();
			this.formSubmit = true; 
			this.content.saverating(form.value, this.shipmentId, this.deliveryId, this.ratingvalue).subscribe((data:any) => {
				this.formSubmit = false;
				this.spinner.hide();
				if(data.status == 1)
				{
					form.resetForm();
					if(data.rating == "Excellent" || data.rating == "Good")
					{
						this.router.navigate(['/thank-you', "1"]);
					}else{
						this.router.navigate(['/thank-you', "0"]);
					}
					
				}
				else
				{
					this.toastr.error("Something went wrong")
				}
			});
		
	}

}
