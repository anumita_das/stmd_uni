import { NgModule } from '@angular/core';
import { RatingRoutingModule } from './rating-routing.module';
import { RatingComponent } from './rating.component';
import { SharedModule } from '@app/shared';


@NgModule({
	imports: [
		RatingRoutingModule,
		SharedModule
	],
	declarations: [
		RatingComponent
	]
})
export class RatingModule {

}
