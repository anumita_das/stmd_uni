import { NgModule } from '@angular/core';
import { HelpRoutingModule } from './help-routing.module';
import { HelpComponent } from './help.component';
import { SharedModule } from '@app/shared';
//import { GetStartComponent } from './get-start/get-start.component';

@NgModule({
	imports: [
		HelpRoutingModule,
		SharedModule
	],
	exports: [],
	declarations: [
		HelpComponent,
		//GetStartComponent,
	],
	entryComponents: []
})
export class HelpModule {

}
