import { Component, OnInit, NgZone, HostListener } from '@angular/core';
import { ContentService } from '../../services/content.service';
import { Router, ActivatedRoute, Params, ParamMap } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
	selector: 'app-help',
	templateUrl: './help.component.html',
	styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {
	isMediumScreen: boolean;
	sidebar: boolean;
	first: any;
	test: any;
	sub: any;
	href: any;
	cat: any;
	constructor(
		private ngZone: NgZone,
		private contentService:ContentService,
		private route: ActivatedRoute,
		private sanitizer: DomSanitizer,
	) { }

	ngOnInit() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		//console.log('Width: ' + width);
		this.checkWindowSize(width);

		this.sub = this.route.params.subscribe(params => {
       		this.href = params['slug']; 
       		this.cat = params['category'];
       		//console.log("category = "+params['category']);
       		this.getPageContent();
   		});

	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		//console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	getPageContent(){
		this.contentService.getfirsthelpsection(this.href, this.cat).subscribe((data:any) => { 
			this.first = this.sanitizer.bypassSecurityTrustHtml(data.data.first.content);

			//console.log(data.data.first.content);
			//this.test = this.domSanitizer.bypassSecurityTrustHtml('<iframe frameborder="0" height="460" scrolling="no" src="https://www.youtube.com/embed/uDGwCO1HDg0" width="300"></iframe>');
		});
	}
}
