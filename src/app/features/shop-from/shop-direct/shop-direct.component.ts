import { Component, OnInit } from '@angular/core';
import { RegistrationService } from '@app/services/registration.service';



@Component({
	selector: 'app-shop-direct',
	templateUrl: './shop-direct.component.html',
	styleUrls: ['./shop-direct.component.scss']
})
export class ShopDirectComponent implements OnInit {
	userId: number;
	isLoggedIn: boolean;

	constructor(
		private regService: RegistrationService
	) {
		if (this.regService.item != null) {
			this.userId = this.regService.item.user.id;
			this.isLoggedIn = true;
		} else {
			this.isLoggedIn = false;
		}

	}

	ngOnInit() {
	}

	isLargeScreen() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		if (width > 992) {
			return true;
		} else {
			return false;
		}
	}



}
