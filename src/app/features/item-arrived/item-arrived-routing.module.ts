import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItemArrivedComponent } from './item-arrived.component';

const routes: Routes = [
	{ path: '', component: ItemArrivedComponent }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ItemArrivedRoutingModule {

}
