import { NgModule } from '@angular/core';
import { TrackShipmentRoutingModule } from './track-shipment-routing.module';
import { TrackShipmentComponent } from './track-shipment.component';
import { SharedModule } from '@app/shared';


@NgModule({
	imports: [
		TrackShipmentRoutingModule,
		SharedModule
	],
	declarations: [
		TrackShipmentComponent
	]
})
export class TrackShipmentModule {

}
