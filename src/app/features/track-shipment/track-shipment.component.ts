import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, ParamMap } from '@angular/router';
import { ContentService } from '../../services/content.service';
import { NgForm }   from '@angular/forms';

@Component({
	selector: 'app-track-shipment',
	templateUrl: './track-shipment.component.html',
	styleUrls: ['./track-shipment.component.scss']
})
export class TrackShipmentComponent implements OnInit {
	tracks: any = '';
	orderNumber : any = '';
	noData : boolean = false;
	constructor(
		public route: ActivatedRoute,
		public content : ContentService,
		public router: Router,
		) { }

	ngOnInit() {
		// tracks
		/*this.tracks = [
			{
				title: 'Booked',
				date: '07-02-2018',
				completed: true,
				visited: false,
			},
			{
				title: 'Picked Up',
				date: '07-02-2018',
				completed: false,
				visited: true,
			},
			{
				title: 'Item Received',
				date: '07-02-2018',
				completed: false,
				visited: false,
			}
		];*/

		if(this.route.snapshot.paramMap.get('ordernumber'))
		{
			this.orderNumber = this.route.snapshot.paramMap.get('ordernumber');
			this.getTrackingDetails();
		}
	}

	getTrackingDetails() {
		this.noData = false;
		this.tracks = '';
		this.content.shipmentTrackingDetails(this.orderNumber).subscribe((data:any) => {
			if(data.status == 1)
			{
				this.tracks = data.results;
			}
			else if(data.status == '-1')
			{
				this.noData = true;
			}

			console.log(this.tracks);
		});
	}

	checkTrackShipment(form : NgForm) {
		if(form.value.orderNumber!='')
		{
			this.router.navigate(['track-shipment',form.value.orderNumber]);
			this.orderNumber = form.value.orderNumber;
			this.getTrackingDetails();
		}
	}

	showComment(arrIndex, statusIndex) {
		console.log(this.tracks[arrIndex].statusDetails[statusIndex].comment);
	}
}
