import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageNotFoundRoutingModule } from './page-not-found-routing.module';
import {PageNotFoundComponent} from './page-not-found.component';
import { SharedModule } from '@app/shared';


@NgModule({
	imports: [
		PageNotFoundRoutingModule,
		SharedModule,
	],
	declarations: [
		PageNotFoundComponent
	],
	exports: [
		PageNotFoundComponent,
	]
})
export class PageNotFoundModule {

}
