
import { Component, OnInit, HostListener } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { ToastrService } from 'ngx-toastr';
import { NgForm }   from '@angular/forms';
import { AppGlobals } from '../../../app.globals';
import { Meta, Title } from '@angular/platform-browser';
import { AutoService } from '../../../shared/services/auto.service';
import { ContentService } from '../../../services/content.service';
import { RegistrationService } from '../../../services/registration.service';
import { GetquoteService } from '../../../shared/services/getquote.service';
import { ShopformeService } from '../../../services/shopforme.service';
import { Router } from "@angular/router";
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AutoConfirmModalComponent } from '../../../shared/components/auto-confirm-modal/auto-confirm-modal.component';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-ship-my-car',
	templateUrl: './ship-my-car.component.html',
	styleUrls: ['./ship-my-car.component.scss']
})
export class ShipMyCarComponent implements OnInit {
	isMediumScreen: boolean;
	colorTheme = 'theme-default';
	bsConfig: Partial<BsDatepickerConfig>;
	makeList : any;
	modelList : any;
	yearList : any = [];
	countryList : any;
	destinationCountryList : any;
	stateList : any;
	cityList : any;
	destStateList : any;
	destCityList : any;
	warehouseId : any;
	itemPrice : any = 0;
	pickupCost : number = 0;
	shippingCost : number = 0;
	insuranceCost : number = 0.00;
	isInsuranceCharged : any = '';
	totalCost : number = 0;
	currencyCode : any;
	currencySymbol : any = '';
	exchangeRate : number = 0.00;
	defaultCurrency : any;
	carPictureImage : any;
	carTitleImage : any;
	formSubmit : boolean = false;
	carConditionVal : any = "drivable";
	bsModalRef: BsModalRef;
	locationTypeArr : any;
	phcodeList : any;
	selectedItem : any;
	altphcodeVal : any;
	pickupPhoneCode : any = '1';
	receiverPhoneCode : any = '1';
	constructor(
		private _global: AppGlobals,
		private meta: Meta, 
		private title: Title,
		public auto: AutoService,
		public content : ContentService,
		private toastr: ToastrService,
		public regService : RegistrationService,
		public getquoteService : GetquoteService,
		public sfmService : ShopformeService,
		public router : Router,
		public modalService: BsModalService,
		public spinner : NgxSpinnerService,
		) { 

		title.setTitle(this._global.siteTitle);
	    meta.addTags([ 
	      {
	        name: 'keywords', content: this._global.siteMetaKeywords
	      },
	      {
	        name: 'description', content: this._global.siteMetaDescription
	      },
	    ]);
	}

	ngOnInit() {
		if(this.currencySymbol == '')
			this.currencySymbol = this._global.defaultCurrencySymbol;

		let warehouse = JSON.parse(localStorage.getItem('selectedWarehouse'));
		if(warehouse == null)
			localStorage.setItem('selectedWarehouse',JSON.stringify(0));

		this.getMakeList();
		this.getAllCode();
		let currentdate = new Date();
		this.generateYearList(currentdate.getFullYear(),'1950');
		this.getAllCountry();
		this.getAllWarehouse();
		this.getLocationType();
		this.auto.checkCartData(this.regService.userId,'ship_my_car').subscribe((data:any) => {
			if(data.status=='1')
			{
				this.bsModalRef = this.modalService.show(AutoConfirmModalComponent, {
						backdrop  : 'static',
   						keyboard  : false
   					});
				this.bsModalRef.content.type = "ship_my_car";
			}
		});
	}
	showSuccess() {
		this.toastr.success('Hello world!', 'Toastr fun!');
	}
	generateYearList(max,min){
		for (var i=max; i>=min; i--) {
	      this.yearList.push(i);
	    }
	}
	getLocationType() {
		this.auto.getLocationType().subscribe((data:any) => {
			if(data.status == '1')
			{
				this.locationTypeArr = data.results;
			}
		});
	}
	getMakeList(){
		this.getquoteService.makelist().subscribe((data:any) => {
			//console.log(data);
			this.makeList = data;
		});
	}
	getModel(form : NgForm){
		let selectedMake = form.value.makeId;
		console.log(selectedMake);
		if(selectedMake !='')
		{
			this.getquoteService.modellist(selectedMake).subscribe((data:any) => {
						//console.log(data);
				this.modelList = data;
				this.getAllCharges(form);

			});
		}
		else
		{
			this.modelList = [];
		}
	}
	getAllCode()
	{
		this.content.allCode().subscribe((data:any)=>{
			this.phcodeList = data;
			this.selectedItem = data[0]['image']+data[0]['name']+data[0]['isdCode'];
		});
	}
	getAllCountry(){
		this.auto.allCountry().subscribe((data:any)=>{
			this.countryList = data;
		});

		this.auto.allDestinationCountry().subscribe((data:any) => {
			this.destinationCountryList = data;
		});
	}

	getStateCity(form : NgForm){
		let selectedCountry = form.value.pickupCountry;
		this.content.stateCityOfCountry(selectedCountry).subscribe((data:any) => {
			//console.log(data);
			if(data.stateList)
			{
				this.stateList = data.stateList;
				this.cityList = [];
			}
			if(data.cityList)
			{
				this.cityList = data.cityList;
			}
		});
	}

	getCity(form : NgForm) {
		let selectedState = form.value.pickupState;
		this.content.cityOfStates(selectedState).subscribe((data:any) => {
			if(data.cityList)
			{
				this.cityList = data.cityList;
			}
		});
	}

	getdestinationStateCity(form : NgForm){
		let selectedCountry = form.value.destinationCountry;
		this.content.stateCityOfCountry(selectedCountry).subscribe((data:any) => {
			//console.log(data);
			if(data.stateList)
			{
				this.destStateList = data.stateList;
				this.destCityList = [];
			}
			if(data.cityList)
			{
				this.destCityList = data.cityList;
			}
		});
	}

	getdestinationCity(form : NgForm) {
		let selectedState = form.value.destinationState;
		this.content.cityOfStates(selectedState).subscribe((data:any) => {
			if(data.cityList)
			{
				this.destCityList = data.cityList;
			}
		});
	}

	getAllWarehouse(){
		this.content.allWarehouse().subscribe((data:any) => {
			let index = 0;
			if(data.status == 1)
			{
				for(let eachWarehouse of data.results) {
					if(index == JSON.parse(localStorage.getItem('selectedWarehouse')))
					{
						this.warehouseId = eachWarehouse.id;
					}
					index++;
				}
				//console.log(this.warehouseId);
			}
		});
	}

	getAllCharges(form : NgForm) {

		let vehiclePrice = Math.abs(parseFloat(form.value.itemPrice)).toFixed(2);
		this.auto.getAllChargesShipmycar(form.value,this.warehouseId,this.regService.userId).subscribe((data:any) => {
			this.itemPrice = vehiclePrice;
			let charges = JSON.parse(data.results);
			this.pickupCost = charges.pickupCost;
			this.shippingCost = charges.shippingCost;
			this.totalCost = charges.totalCost;
			this.currencyCode = charges.currencyCode;
			this.currencySymbol = charges.currencySymbol;
			this.exchangeRate = charges.exchangeRate;
			this.defaultCurrency = charges.defaultCurrency;
			this.defaultCurrency = charges.defaultCurrency;
			this.isInsuranceCharged = charges.isInsuranceCharged;
			this.insuranceCost = charges.insuranceCost;
		})
	}

	carPicture(event) {

		let fileItem = event.target.files[0];
  		const file: File = fileItem;
  		this.carPictureImage = file;
  		//console.log(this.carPictureImage);
	}
	carTitle(event) {

		let fileItem = event.target.files[0];
  		const file: File = fileItem;
  		this.carTitleImage = file;
  		//console.log(this.carTitleImage);

	}
	onSubmit(form : NgForm) {
		if(form.value.shippingCost!='0.00')
		{
			this.spinner.show();
			this.formSubmit = true;	
			const formData = new FormData();
      		if(this.carPictureImage != null)
		        formData.append('itemImage', this.carPictureImage, this.carPictureImage.name);
		     if(this.carTitleImage != null)
		        formData.append('carTitle', this.carTitleImage, this.carTitleImage.name);
	        for(let eachField in form.value)
	        {
	          //let name = 'postData['+eachField+']';
	          formData.append(eachField,form.value[eachField]);
	          //console.log(formPost[eachField]);
	        }

	        this.sfmService.updateUserCart(formData,'ship_my_car').subscribe((data:any) => {
				this.spinner.hide();
				if(data.status == '1')
				{
					localStorage.setItem('userAutoCart', '');
	 				localStorage.setItem('userAutoCart', JSON.stringify(data.results));
	 				this.router.navigate(['auto','checkout']);

				}
			});
			/*this.auto.saveShipMyCar(form.value,this.carPictureImage,this.carTitleImage,this.warehouseId,this.regService.userId).subscribe((data : any) => {
				this.formSubmit = false;
				if(data.status == 1)
				{
					this.toastr.success('Request for pickup is saved');
					console.log('Request for pickup is saved');
					form.resetForm();
				}
				else
				{
					this.toastr.error('Something went wrong');
				}
			});*/
		}
		else
		{
			this.toastr.error('No shipping cost or other costs available for given data');
		}
	}

	setTwoNumberDecimal($event) {
		if(!isNaN(parseFloat($event.target.value))){
		 	$event.target.value = Math.abs(parseFloat($event.target.value)).toFixed(2);
		}
	 	else {
	 		$event.target.value = parseFloat("0").toFixed(2);
	 	}
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		this.checkWindowSize(event.target.innerWidth);
	}
}
