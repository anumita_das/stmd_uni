import { Component, OnInit, HostListener } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgForm }   from '@angular/forms';
import { AppGlobals } from '../../../app.globals';
import { Meta, Title } from '@angular/platform-browser';
import { AutoService } from '../../../shared/services/auto.service';
import { ContentService } from '../../../services/content.service';
import { RegistrationService } from '../../../services/registration.service';
import { ShopformeService } from '../../../services/shopforme.service';
import { Router } from "@angular/router";
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AutoConfirmModalComponent } from '../../../shared/components/auto-confirm-modal/auto-confirm-modal.component';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-buy-a-car-for-me',
	templateUrl: './buy-a-car-for-me.component.html',
	styleUrls: ['./buy-a-car-for-me.component.scss']
})
export class BuyACarForMeComponent implements OnInit {
	isMediumScreen: boolean;
	websiteList : any;
	websiteLogo : string;
	selectedWebsite : number;
	makeList : any;
	modelList : any;
	yearList : any;
	countryList : any;
	stateList : any;
	cityList : any;
	warehouseId : number;
	processingFee : number = 0.00;
	itemPrice : number = 0.00;
	pickupCost : number = 0.00;
	shippingCost : number = 0.00;
	totalCost : number = 0.00;
	insuranceCost : number = 0.00;
	currencyCode : any;
	currencySymbol : any = '';
	exchangeRate : number = 0.00;
	isInsuranceCharged : any = '';
	defaultCurrency : any;
	selectedFile : any;
	formSubmit : boolean = false;
	website : any = "";
	make : any = "";
	bsModalRef: BsModalRef;
	websiteDetails : any = "";
	constructor(
		private _global: AppGlobals,
		private meta: Meta, 
		private title: Title,
		public auto: AutoService,
		public content : ContentService,
		private toastr: ToastrService,
		public regService : RegistrationService,
		public sfmService : ShopformeService,
		public router : Router,
		public modalService: BsModalService,
		public spinner : NgxSpinnerService,
		) { 

		title.setTitle(this._global.siteTitle);
	    meta.addTags([ 
	      {
	        name: 'keywords', content: this._global.siteMetaKeywords
	      },
	      {
	        name: 'description', content: this._global.siteMetaDescription
	      },
	    ]);
	}

	ngOnInit() {
		if(this.currencySymbol == '')
			this.currencySymbol = this._global.defaultCurrencySymbol;

		let warehouse = JSON.parse(localStorage.getItem('selectedWarehouse'));
		if(warehouse == null)
			localStorage.setItem('selectedWarehouse',JSON.stringify(0));
		
		this.yearList = [];
		let currentdate = new Date();
		this.auto.checkCartData(this.regService.userId,'buy_a_car').subscribe((data:any) => {
			if(data.status=='1')
			{
				this.bsModalRef = this.modalService.show(AutoConfirmModalComponent, {
						backdrop  : 'static',
   						keyboard  : false
   					});
				this.bsModalRef.content.type = "buy_a_car";
				/*let conf = window.confirm("There are previous data in you cart. Want to continue with that?");
				if(conf) {
				    localStorage.setItem('userAutoCart', '');
	 				localStorage.setItem('userAutoCart', JSON.stringify(data.results));
	 				this.router.navigate(['auto','checkout']);
				}
				else
				{
					this.auto.deleteCartData(this.regService.userId,'buy_a_car').subscribe((data:any) => {
						if(data.status == '1')
							this.toastr.success('Previous cart data removed');
						else
							this.toastr.error('Something went wrong');
					});
				}*/

			}
		});
		this.generateYearList(currentdate.getFullYear(),'1950');
		this.getAllWebsite();
		this.getAllCountry();
		this.getAllWarehouse();
		//console.log(JSON.parse(localStorage.getItem('selectedWarehouse')));
	}
	showSuccess() {
		this.toastr.success('Hello world!', 'Toastr fun!');
	}
	generateYearList(max,min){
		for (var i=max; i>=min; i--) {
	      this.yearList.push(i);
	    }
	}
	getAllWebsite() {

		this.auto.getAllWebsite().subscribe((data:any) => {
			if(data.status == 1)
			{
				this.websiteList = data.results;
			}

			//console.log(this.websiteList);
		})
	}

	getMake(event) {

		const selectBox = event.target;
		const dropdownIndex = selectBox.options[selectBox.selectedIndex].dataset.sectionvalue;
		this.makeList = [];
		this.modelList = [];
		this.selectedWebsite = event.target.value;
		this.websiteDetails = this.websiteList[dropdownIndex];
		this.auto.getWebsiteMake(this.selectedWebsite).subscribe((data:any) => {
			if(data.status == '1')
			{
				this.makeList = JSON.parse(data.results);
				//console.log(this.makeList);
			}
		//console.log(this.websiteDetails);
		});
	}

	getModel(event) {
		let selectedMake = event.target.value;
		if(selectedMake != '')
		{
			this.auto.getWebsiteModel(this.selectedWebsite,selectedMake).subscribe((data:any) => {
				if(data.status == '1')
				{
						this.modelList = JSON.parse(data.results);
				}
			});
		}
		else
		{
			this.modelList = [];
		}
	}

	getAllCountry(){
		this.auto.allCountry().subscribe((data:any)=>{
			this.countryList = data;
		});
	}

	getStateCity(event){
		let selectedCountry = event.target.value;
		this.content.stateCityOfCountry(selectedCountry).subscribe((data:any) => {
			this.cityList = [];
			//console.log(data);
			if(data.stateList)
			{
				this.stateList = data.stateList;
			}
			if(data.cityList)
			{
				this.cityList = data.cityList;
			}
		});
	}

	getCity(event) {
		let selectedState = event.target.value;
		this.content.cityOfStates(selectedState).subscribe((data:any) => {
			if(data.cityList)
			{
				this.cityList = data.cityList;
			}
		});
	}

	getAllWarehouse(){
		this.content.allWarehouse().subscribe((data:any) => {
			let index = 0;
			if(data.status == 1)
			{
				for(let eachWarehouse of data.results) {
					if(index == JSON.parse(localStorage.getItem('selectedWarehouse')))
					{
						this.warehouseId = eachWarehouse.id;
					}
					index++;
				}
				//console.log(this.warehouseId);
			}
		});
	}

	getAllCharges(form : NgForm) {
		//console.log(form.value);

		let vehiclePrice : any = Math.abs(parseFloat(form.value.price));
		this.auto.getAllChargesBuyacar(form.value,this.warehouseId,this.regService.userId).subscribe((data:any) => {
			this.itemPrice = vehiclePrice;
			let charges = JSON.parse(data.results);
			this.processingFee = charges.processingFee;
			this.pickupCost = charges.pickupCost;
			this.shippingCost = charges.shippingCost;
			this.totalCost = charges.totalCost;
			this.insuranceCost = charges.insuranceCost;
			this.currencyCode = charges.currencyCode;
			this.currencySymbol = charges.currencySymbol;
			this.exchangeRate = charges.exchangeRate;
			this.defaultCurrency = charges.defaultCurrency;
			this.isInsuranceCharged = charges.isInsuranceCharged;
		});
	}

	fileUpload(event) {
  		let fileItem = event.target.files[0];
  		const file: File = fileItem;
  		this.selectedFile = file;
  		//console.log(this.selectedFile);
  	}
	onSubmit(form : NgForm){
		console.log(form.value);
		if(form.value.processingFee!='0.00' && form.value.pickupCost!='0.00' && form.value.shippingCost!='0.00')
		{
			this.spinner.show();
			this.formSubmit = true;
			const formData = new FormData();
	  		//console.log(formPost);
	  		if(this.selectedFile != null)
	      		formData.append('itemImage', this.selectedFile, this.selectedFile.name);
	      	for(let eachField in form.value)
	      	{
	      		formData.append(eachField,form.value[eachField]);
	      		//console.log(formPost[eachField]);
	      	}
			this.sfmService.updateUserCart(formData,'buy_a_car').subscribe((data:any) => {
				this.spinner.hide();
				if(data.status == '1')
				{
					localStorage.setItem('userAutoCart', '');
	 				localStorage.setItem('userAutoCart', JSON.stringify(data.results));
	 				this.router.navigate(['auto','checkout']);

				}
			});

			/*this.auto.saveBuycarData(form.value, this.selectedFile,this.warehouseId,this.regService.userId).subscribe((data:any) => {
			this.formSubmit = false;
				if(data.status=='1')
				{
					this.toastr.success('Data saved with payment status unpaid');
					console.log('Data saved with payment status unpaid');
					form.resetForm();
				}
				else
				{
					this.toastr.success('Something went wrong');
					console.log('Something Went wrong');
				}
			});*/
		}
		else
		{
			console.log('No shipping cost or other costs available for given data');
			this.toastr.error('No shipping cost or other costs available for given data');
		}
	}

	setTwoNumberDecimal($event) {
		if(!isNaN(parseFloat($event.target.value))){
		 	$event.target.value = Math.abs(parseFloat($event.target.value)).toFixed(2);
		}
	 	else {
	 		$event.target.value = Math.abs(parseFloat("0")).toFixed(2);
	 	}
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		this.checkWindowSize(event.target.innerWidth);
	}
}
