import { Component, OnInit, Renderer2, HostListener } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgForm }   from '@angular/forms';
import { AppGlobals } from '../../../app.globals';
import { Meta, Title } from '@angular/platform-browser';
import { AutoService } from '../../../shared/services/auto.service';
import { ShopformeService } from '../../../services/shopforme.service';
import {Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { AutoImagesModalComponent } from '../../../shared/components/auto-images-modal/auto-images-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


@Component({
	selector: 'app-my-warehouse',
	templateUrl: './my-warehouse.component.html',
	styleUrls: ['./my-warehouse.component.scss']
})
export class CarWarehouseComponent implements OnInit {
	tracks: Array<any>;
	links: Array<any>;
	isMediumScreen: boolean;
	userId:number;
	cars:any=[];
	perPage:any = 10;
	currentPage: number = 1;
	pageSize: number;
	totalItems: number;
	imageData: any;
	bsModalRef: BsModalRef;

	constructor(
		private _global: AppGlobals,
		private meta: Meta, 
		private title: Title,
		private toastr: ToastrService,
		private renderer: Renderer2,
		public auto: AutoService,
		public sfmService : ShopformeService,
		public spinner : NgxSpinnerService,
		public modalService: BsModalService,
		public router : Router,
	) { }

	ngOnInit() {
		this.spinner.show();
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		console.log('Width: ' + width);
		this.checkWindowSize(width);

		// links
		this.links = [
			{
				title: 'Cars I Have Bought',
				url: 'auto/car-i-have-bought'
			},
			{
				title: 'My Cars in Transit',
				url: 'auto/my-warehouse'
			}
		];

		var parsed =  JSON.parse(localStorage.getItem('tokens'));
		this.userId = parsed.user.id;

		this.auto.getCarWarehouseDetails(this.userId, this.currentPage, this.perPage).subscribe((data:any) => {
			if(data.results == 'success'){
				this.cars = data.cars;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;
			}
			this.spinner.hide();
		});


		
	}

	pageChanged($event)  {
    	this.spinner.show();
		this.auto.getCarWarehouseDetails(this.userId, $event, this.perPage).subscribe((data:any) => {
			if(data.results == 'success'){
				this.currentPage = $event;
				this.cars = data.cars;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;	
				
			}
			this.spinner.hide();
		});
	}

	perPageChanged($event)  {
		this.spinner.show();
		this.auto.getCarWarehouseDetails(this.userId, 1, this.perPage).subscribe((data:any) => {
			if(data.results == 'success'){
				this.currentPage = 1;
				this.cars = data.cars;
				this.totalItems = data.totalItems;
				this.pageSize= data.itemsPerPage;	
					
			}
			this.spinner.hide();
		});
	}


	// check window size
	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	showSuccess() {
		this.toastr.success('Hello world!', 'Toastr fun!');
	}

	downloadImage(param){ 
	//window.location = param;

    }

    print(procurementId,invoiceType) {
    	this.spinner.show();
		let printContents, popupWin;

		this.auto.getInvoice(procurementId,'autoshipment',invoiceType,this.userId).subscribe((response:any) => {
		    printContents = response.data;
		    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
		    popupWin.document.open();
		    popupWin.document.write(`
		      <html>
		        <head>
		          <title>Print tab</title>
		          <style>
		          //........Customized style.......
		          </style>
		        </head>
		    <body onload="window.print();window.close()">${printContents}</body>
		      </html>`
		    );
		    popupWin.document.close();
		    this.spinner.hide();
		});

	    
	}

	hack(value) {
		
		if(value!=undefined && value!=null)
		{
			return Object.values(value);
		}
	   
	}

	showFullImage(imageId) {
		console.log(imageId);
		localStorage.removeItem('autowarehouseimageid');
		localStorage.setItem('autowarehouseimageid', JSON.stringify(imageId));
		this.bsModalRef = this.modalService.show(AutoImagesModalComponent, {
			class: 'modal-sm modal-dialog-centered'
		});
		
		this.bsModalRef.content.imageId = imageId;
	}

	processShipmentpayment(shipmentId) {

		this.spinner.show();
		const formData = new FormData();
		formData.append('autoShipmentId',shipmentId);
		this.sfmService.updateUserCart(formData,'process_autoshipment').subscribe((data:any) => {

			this.spinner.hide();

			if(data.status == '1')
			{
				localStorage.setItem('userAutoCart', '');
 				localStorage.setItem('userAutoCart', JSON.stringify(data.results));
 				this.router.navigate(['auto','checkout']);

			}

		});
	}
}
