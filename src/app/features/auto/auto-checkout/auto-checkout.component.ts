import { Component, OnInit } from '@angular/core';
import { RegistrationService } from '../../../services/registration.service';
import { ContentService } from '../../../services/content.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm }   from '@angular/forms';
import { AutoService } from '../../../shared/services/auto.service';
import { CurrencyModalComponent } from '../../../shared/components/currency-modal/currency-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ShopformeService } from '../../../services/shopforme.service';
import { Router } from "@angular/router";
import { AppGlobals } from '../../../app.globals';
import { NgxSpinnerService } from 'ngx-spinner';
import { PaystackModalComponent } from '../../../shared/components/paystack-modal/paystack-modal.component';
import { PayeezyModalComponent } from '../../../shared/components/payeezy-modal/payeezy-modal.component';
import { PayPalConfig, PayPalEnvironment, PayPalIntegrationType } from 'ngx-paypal';


@Component({
	selector: 'app-auto-checkout',
	templateUrl: './auto-checkout.component.html',
	styleUrls: ['./auto-checkout.component.scss']
})
export class AutoCheckoutComponent implements OnInit {
	dtOptions: DataTables.Settings = {};
	constructor(
		public regService : RegistrationService,
		public auto : AutoService,
		public content : ContentService,
		public modalService: BsModalService,
		public shopformeService : ShopformeService,
		public toastr : ToastrService,
		public router : Router,
		private _global: AppGlobals,
		public spinner : NgxSpinnerService,
	) { }
	autoCart : any;
	userData : any;
	shippingAddress : any;
	billingAddress : any;
	formSubmit : boolean = false;
	warehouseId : any;
	bsModalRef: BsModalRef;
	paymentMethodList : any;
	billingCountry : any;
	paymentMethodId : any ='' ;
	termsCondition : any = false;
	poNumber : any = '';
	companyName : any = '';
	buyerName : any = '';
	position : any = '';
	payCardType : any = '';
	cardNumber : any = '';
	cardCode : any = '';
	expMonth : any = '';
	expYear : any = '';
	currencySymbol:any = '';
	paypalPayment : any = false;
	payPalConfig: any;
	ewallet : any = '';
    ewalletInvalid:boolean = false;
    ewalletMsg: string = '';
    ewalletReadonly:boolean = false; 
    ewalletId: any;
    subscriptionCouponCode: string;
    couponcode: string;
    couponInValidMsg: string;
    couponInValid:boolean = false;
    blankCouponCode:boolean = true;
    pointEarned:string = '';
    amountEarned:string = '';
    couponReadonly : boolean =false;
    couponApplied : boolean =false;
    paystackRef : any = '';
    paystackData : any = '';
    paypalipnConfig:any;
    showPaystackOverlay : boolean = false;
    totalCost: number;
    tempDataId : any = '';

	ngOnInit() {
		this.spinner.show();
		this.autoCart = JSON.parse(localStorage.getItem('userAutoCart'));
		//console.log('this'+this.autoCart);
		if(this.autoCart == null)
		{
                    this.router.navigate(['auto','buy-a-car-for-me']);
		}
		else
		{
			//console.log(this.autoCart);
			if(this.currencySymbol == '')
			this.currencySymbol = this._global.defaultCurrencySymbol;
			if (this.autoCart != null) {
				if(this.autoCart['tax']==null)
					this.autoCart['tax'] = '0.00';
				if(this.autoCart['discountAmount'] == null)
					this.autoCart['discountAmount'] = '0.00';
				if(this.autoCart['totalCost']!= null)
					this.autoCart['totalcostWithoutDiscount'] = this.autoCart['totalCost']
   
				if(this.autoCart['couponCode'] != null)
				{
					this.couponcode = this.autoCart['couponCode'];
					this.couponReadonly = true;
				} else {
                                    if(this.regService.item.user.subscription.couponCode){
                                        this.couponcode = this.regService.item.user.subscription.couponCode;
                                        this.couponReadonly = true;
                                        this.validateCoupon();
                                    } 
                                }
				if(this.autoCart['paymentMethodId']!=null)
				{
					this.paymentMethodId = this.autoCart['paymentMethodId'];
					if(this.autoCart['paymentMethodId']==10)
					{
						this.paypalConfig('payment');
						this.paypalPayment=true;
					}
				}


				//console.log(this.autoCart);
			}

			this.totalCost = parseFloat(this.autoCart['totalCost']);

			if(this.regService.item!=null)
				this.userData = this.regService.item.user;
			//console.log(this.userData);

			this.getShippingAddress();
			this.getBillingAddress();
			this.getAllWarehouse();
		}
		// datatable
		/*this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};*/
	}

	getPaymentMethodList(billingCountry) {
		const data = {billingCountryId : billingCountry};
		this.shopformeService.getPaymentMethodList(data).subscribe((data:any) => {
			this.paymentMethodList = data;
			//console.log(this.paymentMethodList);
		});
	}

	getShippingAddress() {
		if(this.autoCart.type == 'ship_my_car')
		{
			this.shippingAddress = {
				name : this.autoCart.recceiverName,
				address : this.autoCart.receiverAddress,
				city : {name : this.autoCart.shippingCityName},
				state : {name : this.autoCart.shippingStateName},
				country : {name : this.autoCart.shippingCountryName},
				zipcode : 'N/A',
			}
		}
		else
		{
			this.regService.userShippingAddress('shipping').subscribe((data:any)=> {
				if(data.status == '1') {
					this.shippingAddress = data.results[0];
					//console.log(this.shippingAddress);
				}
			});
		}
	}

	getBillingAddress() {
		this.regService.userShippingAddress('billing').subscribe((data:any)=> {
			if(data.status == '1') {
				this.billingAddress = data.results[0];
				if(this.billingAddress.countryId!=null)
				{
					this.getPaymentMethodList(this.billingAddress.countryId);
					this.billingCountry = this.billingAddress.countryId;
				}
				//console.log(this.billingAddress);
			}
		});
	}
	getAllWarehouse(){
		this.content.allWarehouse().subscribe((data:any) => {
			let index = 0;
			if(data.status == 1)
			{
				for(let eachWarehouse of data.results) {
					if(index == JSON.parse(localStorage.getItem('selectedWarehouse')))
					{
						this.warehouseId = eachWarehouse.id;
					}
					index++;
				}
				//console.log(this.warehouseId);
			}
			this.spinner.hide();
		});
	}
	savedata(form : NgForm) {
		//this.formSubmit = true;
		//if(this.auto)
		//console.log(form.value);

		const autoCart = JSON.parse(localStorage.getItem('userAutoCart'));
		this.autoCart['warehouseId'] = this.warehouseId;
		console.log(this.autoCart);
		if(autoCart.paymentMethodKey == 'wire_transfer')
		{
			this.autoCart['poNumber'] = form.value.poNumber;
			this.autoCart['companyName'] = form.value.companyName;
			this.autoCart['buyerName'] = form.value.buyerName;
			this.autoCart['position'] = form.value.position;
			//usercart['data'] = this.autoCart;
			localStorage.setItem('userAutoCart', JSON.stringify(this.autoCart));
		}
		else if(autoCart.paymentMethodKey == 'credit_debit_card')
		{
			this.autoCart['cardNumber'] = form.value.cardNumber;
			this.autoCart['expMonth'] = form.value.expMonth;
			this.autoCart['expYear'] = form.value.expYear;
			this.autoCart['cardCode'] = form.value.cardCode;
			this.autoCart['cardType'] = form.value.payCardType;
			localStorage.setItem('userAutoCart', JSON.stringify(this.autoCart));
		}
		else if(autoCart.paymentMethodKey == 'ewallet')
		{
			this.autoCart['ewalletId'] = this.ewalletId;
			localStorage.setItem('userAutoCart', JSON.stringify(this.autoCart));
		}

		//console.log(this.autoCart);
		if(this.paymentMethodId!='')
		{
			if(this.termsCondition)
			{
				
				
				if(this.autoCart.paymentMethodKey == 'paystack_checkout')
				{
					localStorage.setItem('userAutoCart', JSON.stringify(this.autoCart));
					this.showPaystackOverlay = true;
					this.content.getAmountForPaystack(this.autoCart['totalCost'],this.autoCart['defaultCurrency']).subscribe((data:any) => {
						//console.log(data.amountForPaystack);
						this.bsModalRef = this.modalService.show(PaystackModalComponent, {});
						this.bsModalRef.content.warehouseId = this.warehouseId;
						this.bsModalRef.content.payAmount = data.amountForPaystack;
					});

					this.modalService.onHide.subscribe(() => {
						setTimeout(() => {
							this.showPaystackOverlay = false;
					    }, 3000);
						
					});
					
				}
				else if(this.autoCart.paymentMethodKey == 'payeezy'){
					this.showPaystackOverlay = true;

					//var storedData =  JSON.parse(localStorage.getItem('cartData'));
					//console.log(storedData);
					//Clear Storage Data
		    		//delete storedData.paymentdetails ;

		    		var totalCostPayable = parseFloat(this.autoCart['totalCost']);
		    		this.regService.storeTempData(JSON.parse(localStorage.getItem('userAutoCart')),this.autoCart.type,'payeezy').subscribe((res:any) => {

						this.content.getAmountForPayeezy(totalCostPayable,this.autoCart['defaultCurrency']).subscribe((data:any) => {
							this.bsModalRef = this.modalService.show(PayeezyModalComponent, {backdrop: 'static', keyboard: false});
							this.bsModalRef.content.warehouseId = this.warehouseId;
							this.bsModalRef.content.payAmount = data.amountForPayeezy;
							this.bsModalRef.content.amountDisplay = data.amountDisplay;
							this.bsModalRef.content.paySymbol = this.currencySymbol;
							this.bsModalRef.content.tempDataId = res.tempDataId;
						});

						this.modalService.onHide.subscribe(() => {
							setTimeout(() => {
								this.showPaystackOverlay = false;
						    }, 3000);
							
						});
					});
					
				}
				else
				{
					this.spinner.show();
					if(this.autoCart.type == 'ship_my_car')
					{
						this.formSubmit = true;
						this.paypalPayment = false;
						this.autoCart['userId'] = this.regService.userId;
						this.autoCart['warehouseId'] = this.warehouseId;
						this.auto.saveShipMyCar(this.autoCart).subscribe((data:any) => {
							this.formSubmit = false;
							this.spinner.hide();
							if(data.status == '1')
							{
								this.toastr.success("Data successfully submited.");
								localStorage.removeItem('userAutoCart');
								this.router.navigate(['auto','my-warehouse']);

							}else if(data.status == '2') {
									this.toastr.success('', data.msg,{
										disableTimeOut : true,
										positionClass: 'toast-center-center',
										closeButton : true
									});
								localStorage.removeItem('userAutoCart');
								this.router.navigate(['auto','my-warehouse']);
							}
							else if(data.status == '-1'){
								this.toastr.error("Something went wrong");
							}
							else if(data.status == '-2'){
								this.toastr.error(data.results);
							}

						});
					}
					else
					{
						this.auto.saveBuycarData(this.autoCart, this.warehouseId,this.regService.userId).subscribe((data:any) => {
							this.formSubmit = false;
							this.spinner.hide();
							if(data.status == '1')
							{
								this.toastr.success("Data successfully submited.");
								localStorage.removeItem('userAutoCart');
								this.router.navigate(['auto','car-i-have-bought']);

							}else if(data.status == '2') {
									this.toastr.success('', data.msg,{
										disableTimeOut : true,
										positionClass: 'toast-center-center',
										closeButton : true
									});
								localStorage.removeItem('userAutoCart');

								this.router.navigate(['auto','my-warehouse']);

							}
							else if(data.status == '-1'){
								this.toastr.error("Something went wrong");
							}
							else if(data.status == '-2'){
								this.toastr.error(data.results);
							}
						});
					}
				}
			}
			else
			{
				this.toastr.error("Please agree to our terms and condition");
			}
		}
		else
		{
			this.toastr.error("Please select any payment method");
		}
	}

	onSubmit(form : NgForm) {
		//console.log(form);
	}

	onSearchChange(searchValue : string ) {  
		
		let regexMap = [
	      {regEx: /^4[0-9]{5}/ig,cardType: "VISA"},
	      {regEx: /^5[1-5][0-9]{4}/ig,cardType: "MASTERCARD"},
	      {regEx: /^3[47][0-9]{3}/ig,cardType: "AMEX"},
	      {regEx: /^(5[06-8]\d{4}|6\d{5})/ig,cardType: "MAESTRO"},
	      {regEx: /^(6(011|5[0-9][0-9])[0-9]{12})/ig,cardType: "DISCOVER"}
	    ];

	    this.payCardType = '';
	    //console.log(searchValue.trim());
		for (let j = 0; j < regexMap.length; j++) {
		  if (searchValue.match(regexMap[j].regEx)) {
		    this.payCardType = regexMap[j].cardType;
		    break;
		  }
		}
	}

	createRangeYear(number){
		var d = new Date();
	    var n = d.getFullYear();

		var expiryYear: number[] = [];
		for(var i = n; i <= n+20; i++){
		 	expiryYear.push(i);
		}
		return expiryYear;
	}

	changePaymentMethod(event, paymentMethodName, paymentMethodKey, paymentTax, paymentTaxType) {
		//console.log(paymentTax);
		const usercart = JSON.parse(localStorage.getItem('userAutoCart'));
		this.autoCart['paymentMethodId'] = event.target.value;
		this.autoCart['paymentMethodName'] = paymentMethodName;
		this.autoCart['paymentMethodKey'] = paymentMethodKey;
		this.paymentMethodId = event.target.value;
		//console.log(this.autoCart['paymentMethodId']);
		this.autoCart['totalCost'] = parseFloat(this.autoCart['totalCost'])-parseFloat(this.autoCart['tax']);
		if(paymentTaxType == '0') // flat charge
		{
			this.autoCart['tax']  = paymentTax;
		}
		else // percent charge
		{
			if(this.autoCart.type == 'ship_my_car')
				this.autoCart['tax'] = parseFloat(this.autoCart['shippingCost'])*(paymentTax/100);
			else
				this.autoCart['tax'] = (parseFloat(this.autoCart['processingFee'])+parseFloat(this.autoCart['shippingCost']))*(paymentTax/100);
		}

		//console.log(this.autoCart['tax']);
		this.autoCart['totalCost'] = parseFloat(this.autoCart['tax'])+parseFloat(this.autoCart['totalCost']);
		this.autoCart['totalcostWithoutDiscount'] = this.autoCart['totalCost'];
		usercart['data'] = this.autoCart;
		const formData = {
	  	  usercart : usercart['data'],
	  	  userId : this.regService.userId,
	  	};
	  	this.auto.updateUserCartContent(formData).subscribe((data : any) => {
	  		localStorage.setItem('userAutoCart', JSON.stringify(data.results));
	  		if(paymentMethodKey=='paypalstandard')
		  	{
		  		this.paypalConfig('payment');
		  	}
		  	else
		  	{
		  		this.paypalPayment = false;
		  	}
	  	});
	  	
	}

	showCurrency() {
		const usercart = JSON.parse(localStorage.getItem('userAutoCart'));
		const currencyCode = usercart.currencyCode;

		this.shopformeService.getCurrencyList(currencyCode).subscribe((response:any) => {
			this.bsModalRef = this.modalService.show(CurrencyModalComponent, {});
			this.bsModalRef.content.currencyList = response;
		});
	}

	addInsurance($event) {
		const usercart = JSON.parse(localStorage.getItem('userAutoCart'));
		
		

		if($event.target.checked == true){
			this.autoCart['isInsuranceCharged']  = 'Y';
			this.autoCart['totalCost'] = parseFloat(this.autoCart['insuranceCost'])+parseFloat(this.autoCart['totalCost']);

	
		} else {
			this.autoCart['isInsuranceCharged'] = 'N';	
			this.autoCart['totalCost'] = parseFloat(this.autoCart['totalCost'])-parseFloat(this.autoCart['insuranceCost']);
		}
		this.autoCart['totalcostWithoutDiscount'] = this.autoCart['totalCost'];
		//Reassign Storage Data
	    usercart['data'] = this.autoCart;
	    const formData = {
	  	  usercart : usercart['data'],
	  	  userId : this.regService.userId,
	  	};
	  	this.auto.updateUserCartContent(formData).subscribe((data : any) => {
	  		localStorage.setItem('userAutoCart', JSON.stringify(data.results));
	  	});
	    
	}

	setTermsCond(event){
		this.termsCondition = event.target.checked;
	}

	validateEwallet() {
		if(this.ewallet !== ''){
			this.ewalletInvalid = false;
			const formData = {"ewalletId" : this.ewallet, "amountToBePaid" : this.autoCart.totalCost, "userId" : this.regService.userId};
			 this.shopformeService.validateEwallet(formData).subscribe((response:any) => {
		        if(response.status == 1){
		        	this.ewalletReadonly = true;
		        	this.ewalletId = response.results;
		        	this.ewalletMsg = '';
		        } else {
		        	this.ewalletMsg = response.results;
		        }
	         });
		} else {
			this.ewalletInvalid = true;
		}
	}

	ewalletCheck($event) {
		if($event != '')
			this.ewalletInvalid = false;
		else
			this.ewalletInvalid = true;
	}

	checkEmptyCouponInput(){
		if(this.couponcode == ''){
			this.blankCouponCode = true;
		} else {
			this.blankCouponCode = false;
		}
	}

	validateCoupon(){
		this.autoCart['couponCode'] = this.couponcode;
		localStorage.setItem('userAutoCart', JSON.stringify(this.autoCart));
		this.autoCart['userId'] = this.regService.userId;
		this.autoCart['warehouseId'] = this.warehouseId;
		this.auto.validatecouponcode(this.autoCart).subscribe((response:any) => {

			var storedData =  JSON.parse(localStorage.getItem('userAutoCart'));

			this.couponInValid = false;
			this.couponInValidMsg = '';
			if(response.results == 'invalid'){
				this.couponReadonly = false;
				this.couponInValidMsg = response.message;
				this.couponInValid = true;
				this.amountEarned = '';
				this.pointEarned = '';
				this.autoCart['totalCost'] = parseFloat(this.autoCart['totalCost']) + parseFloat(this.autoCart['discountAmount']);
				this.autoCart['discountAmount'] = '0.00';
				this.autoCart['couponCode'] = '';
				this.autoCart['point'] = '';

				//localStorage.setItem('userAutoCart', JSON.stringify(this.autoCart));
			}
			if(response.results == 'valid'){
				this.couponInValidMsg = response.message;
				this.couponInValid = true;
				this.couponReadonly = true;
				if(response.amount_or_point == 'Point'){
					this.pointEarned = "You can earn " + response.point_to_be_discounted + " points";
					this.autoCart['discountAmount'] = '0.00';
					this.autoCart['couponCode'] = this.couponcode;
					this.autoCart['point'] = response.point_to_be_discounted;
					this.autoCart['couponId'] = response.couponId;
					//localStorage.setItem('userAutoCart', JSON.stringify(this.autoCart));
					/*var storedData =  JSON.parse(localStorage.getItem('userAutoCart'));
					delete storedData.couponStore;
					const couponStore = Object.assign({"couponStore":{"couponCode": this.couponcode, "amount":'', "point":response.point_to_be_discounted}},storedData);
			    	localStorage.setItem('userAutoCart', JSON.stringify(couponStore));*/
				} else {
					this.amountEarned = response.amount_to_be_discounted;
					this.autoCart['discountAmount'] = this.amountEarned;
					this.autoCart['couponCode'] = this.couponcode;
					this.autoCart['point'] = '';
					this.autoCart['totalCost'] = this.autoCart['totalCost'] - this.autoCart['discountAmount'];
					this.autoCart['couponId'] = response.couponId;
					//this.paypalConfig();
					localStorage.setItem('userAutoCart', JSON.stringify(this.autoCart));
					/*var storedData =  JSON.parse(localStorage.getItem('userAutoCart'));
					delete storedData.couponStore;
					const couponStore = Object.assign({"couponStore":{"couponCode": this.couponcode, "amount":this.amountEarned, "point":''}},storedData);
			    	localStorage.setItem('userAutoCart', JSON.stringify(couponStore));*/
				}
                                    
                                this.couponApplied = true;
			}

			    const formData = {
			  	  usercart : this.autoCart,
			  	  userId : this.regService.userId,
			  	};
			  	this.auto.updateUserCartContent(formData).subscribe((data : any) => {
			  		localStorage.setItem('userAutoCart', JSON.stringify(data.results));
			  		this.paypalConfig('coupon');
			  	});
				
			});
	}

	clearCoupon() {
		//console.log('here');
		this.couponReadonly = false;
		this.blankCouponCode = true;
		this.couponInValid = false;
		this.couponcode = '';
		this.autoCart['totalCost'] = parseFloat(this.autoCart['totalCost']) + parseFloat(this.autoCart['discountAmount']);
		this.autoCart['discountAmount'] = '0.00';
		this.autoCart['couponCode'] = '';
		this.autoCart['point'] = '';
		const formData = {
	  	  usercart : this.autoCart,
	  	  userId : this.regService.userId,
	  	};
	  	this.auto.updateUserCartContent(formData).subscribe((data : any) => {
	  		localStorage.setItem('userAutoCart', JSON.stringify(data.results));
	  	});
	  	//this.paypalConfig();
	}

	paypalConfig(fromaction) {
		this.spinner.show();
		this.termsCondition = true;
		this.toastr.success('By clicking this you agree to Shoptomydoor Terms and Conditions');
		this.autoCart = JSON.parse(localStorage.getItem('userAutoCart'));
		let total :any ="";
		if(this.autoCart)
		{
			total = parseFloat(this.autoCart['totalCost']).toFixed(2);
			//total = Number.parseFloat(this.autoCart['totalCost'].toFixed(2));
		}
		 
		let paidCurrency = this.autoCart['defaultCurrency'];
		if(fromaction == 'payment')
			this.paypalPayment = true;

		this.regService.storeTempData(JSON.parse(localStorage.getItem('userAutoCart')),this.autoCart.type,'paypalstandard').subscribe((res:any) => {
			this.tempDataId = res.tempDataId;

			this.content.getpaypalipnsettings(this.autoCart.defaultCurrency, total).subscribe((response:any) => {
				this.paypalipnConfig = response.results;

				if(this.paypalipnConfig.mode == "Sandbox")
				{

			      	this.payPalConfig = new PayPalConfig(PayPalIntegrationType.ClientSideREST, PayPalEnvironment.Sandbox, {
			        commit: true,
			        client: {
			          //sandbox: 'AR3rs-NnrWJC6iL3ja_g7FHO0m3P3n3RM0x6mZHfuXKY1tjt9XHGfG23gYolJ3Kr_e0KXUMlE-y7Ce7c',
			          sandbox: this.paypalipnConfig.key,
			        },
			        button: {
			          label: 'paypal',
			        },
			        onPaymentComplete: (data, actions) => {
			        	//console.log(data);
			        	//console.log(actions);
			        	this.spinner.show();
			          	this.autoCart = JSON.parse(localStorage.getItem('userAutoCart'));
			          	this.autoCart['paypalData'] = data;
			          	this.autoCart['tempDataId'] = this.tempDataId;
						if(this.autoCart.type == 'ship_my_car')
						{
							this.autoCart['userId'] = this.regService.userId;
							this.autoCart['warehouseId'] = this.warehouseId;
							this.auto.saveShipMyCar(this.autoCart).subscribe((data:any) => {
								this.formSubmit = false;
								this.spinner.hide();
								if(data.status == '1')
								{
									this.toastr.success("Data successfully submited.");
									localStorage.removeItem('userAutoCart');
									this.router.navigate(['auto','my-warehouse']);
								}else if(data.status == '2') {
									this.toastr.success('', data.msg,{
										disableTimeOut : true,
										positionClass: 'toast-center-center',
										closeButton : true
									});
								localStorage.removeItem('userAutoCart');
								this.router.navigate(['auto','my-warehouse']);
								}
								else if(data.status == '-1'){
									this.toastr.error("Something went wrong");
								}
								else if(data.status == '-2'){
									this.toastr.error(data.results);
								}

							});
						}
						else
						{
							this.auto.saveBuycarData(this.autoCart, this.warehouseId,this.regService.userId).subscribe((data:any) => {
								this.formSubmit = false;
										this.spinner.hide();
										if(data.status == '1')
										{
											this.toastr.success("Data successfully submited.");
											localStorage.removeItem('userAutoCart');
											this.router.navigate(['auto','car-i-have-bought']);
										}else if(data.status == '2') {
												this.toastr.success('', data.msg,{
													disableTimeOut : true,
													positionClass: 'toast-center-center',
													closeButton : true
												});
											localStorage.removeItem('userAutoCart');
											this.router.navigate(['auto','car-i-have-bought']);
										}
										else if(data.status == '-1'){
											this.toastr.error("Something went wrong");
										}
										else if(data.status == '-2')
										{
											this.toastr.error(data.results);
											localStorage.removeItem('userAutoCart');
											this.router.navigate(['auto','car-i-have-bought']);
										}
							});
						}
			        },
			        onCancel: (data, actions) => {
			          this.toastr.error('The Payment has not been proccessed. Please try again');
			        },
			        onError: (err) => {
			          this.toastr.error('The Payment has not been proccessed. Please try again');
			        },
			        transactions: [{
			          amount: {
			            currency: this.paypalipnConfig.currencyCode,
			            total: this.paypalipnConfig.total
			          }
			        }]
			      });

		      	}
		      else if(this.paypalipnConfig.mode == 'Production')
		      {
		      	this.payPalConfig = new PayPalConfig(PayPalIntegrationType.ClientSideREST, PayPalEnvironment.Production, {
			        commit: true,
			        client: {
			          //sandbox: 'AR3rs-NnrWJC6iL3ja_g7FHO0m3P3n3RM0x6mZHfuXKY1tjt9XHGfG23gYolJ3Kr_e0KXUMlE-y7Ce7c',
			          production: this.paypalipnConfig.key,
			        },
			        button: {
			          label: 'paypal',
			        },
			        onPaymentComplete: (data, actions) => {
			        	//console.log(data);
			        	//console.log(actions);
			        	this.spinner.show();
			          	this.autoCart = JSON.parse(localStorage.getItem('userAutoCart'));
			          	this.autoCart['paypalData'] = data;
			          	this.autoCart['tempDataId'] = this.tempDataId;
						if(this.autoCart.type == 'ship_my_car')
						{
							this.autoCart['userId'] = this.regService.userId;
							this.autoCart['warehouseId'] = this.warehouseId;
							this.auto.saveShipMyCar(this.autoCart).subscribe((data:any) => {
								this.formSubmit = false;
								this.spinner.hide();
								if(data.status == '1')
								{
									this.toastr.success("Data successfully submited.");
									localStorage.removeItem('userAutoCart');
									this.router.navigate(['auto','my-warehouse']);
								}else if(data.status == '2') {
										this.toastr.success('', data.msg,{
											disableTimeOut : true,
											positionClass: 'toast-center-center',
											closeButton : true
										});
									localStorage.removeItem('userAutoCart');
									this.router.navigate(['auto','my-warehouse']);
								}
								else if(data.status == '-1'){
									this.toastr.error("Something went wrong");
								}
								else if(data.status == '-2'){
									this.toastr.error(data.results);
								}

							});
						}
						else
						{
							this.auto.saveBuycarData(this.autoCart, this.warehouseId,this.regService.userId).subscribe((data:any) => {
								this.formSubmit = false;
										this.spinner.hide();
										if(data.status == '1')
										{
											this.toastr.success("Data successfully submited.");
											localStorage.removeItem('userAutoCart');
											this.router.navigate(['auto','car-i-have-bought']);
										}else if(data.status == '2') {
												this.toastr.success('', data.msg,{
													disableTimeOut : true,
													positionClass: 'toast-center-center',
													closeButton : true
												});
											localStorage.removeItem('userAutoCart');
											this.router.navigate(['auto','car-i-have-bought']);
										}
										else if(data.status == '-1'){
											this.toastr.error("Something went wrong");
										}
										else if(data.status == '-2')
										{
											this.toastr.error(data.results);
											localStorage.removeItem('userAutoCart');
											this.router.navigate(['auto','car-i-have-bought']);
										}
							});
						}
			        },
			        onCancel: (data, actions) => {
			          this.toastr.error('The Payment has not been proccessed. Please try again');
			        },
			        onError: (err) => {
			          this.toastr.error('The Payment has not been proccessed. Please try again');
			        },
			        transactions: [{
			          amount: {
			            currency: this.paypalipnConfig.currencyCode,
			            total: this.paypalipnConfig.total
			          }
			        }]
			      });
		      }

		      setTimeout(() => {
			    	/** spinner ends after 5 seconds */
			   		this.spinner.hide();
				}, 2000);
		      
		    }); // paypal ipn config bracket end
		});
    }

     /* To copy any Text */
    copyText(val: string){
        let selBox = document.createElement('textarea');
        selBox.style.position = 'fixed';
        selBox.style.left = '0';
        selBox.style.top = '0';
        selBox.style.opacity = '0';
        selBox.value = val;
        document.body.appendChild(selBox);
        selBox.focus();
        selBox.select();
        document.execCommand('copy');
        document.body.removeChild(selBox);

        alert("Coupon copied to Clipboard");
      }    
}
