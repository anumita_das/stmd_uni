import { Component, OnInit } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { NgForm }   from '@angular/forms';
import { AppGlobals } from '../../../app.globals';
import { Meta, Title } from '@angular/platform-browser';
import { AutoService } from '../../../shared/services/auto.service';
import { ContentService } from '../../../services/content.service';

@Component({
	selector: 'app-other-vehicle',
	templateUrl: './other-vehicle.component.html',
	styleUrls: ['./other-vehicle.component.scss']
})
export class OtherVehicleComponent implements OnInit {
	colorTheme = 'theme-default';
	bsConfig: Partial<BsDatepickerConfig>;
	websiteList : any;
	websiteLogo : string;
	selectedWebsite : number;
	makeList : any;
	modelList : any;
	yearList : any;
	countryList : any;
	stateList : any;
	cityList : any;
	destStateList : any;
	destCityList : any;
	warehouseId : number;
	processingFee : number;
	itemPrice : number;
	selectedFile : any;
	constructor(
		private _global: AppGlobals,
		private meta: Meta, 
		private title: Title,
		public auto: AutoService,
		public content : ContentService,
		) { 

		title.setTitle(this._global.siteTitle);
	    meta.addTags([ 
	      {
	        name: 'keywords', content: this._global.siteMetaKeywords
	      },
	      {
	        name: 'description', content: this._global.siteMetaDescription
	      },
	    ]);
	}

	ngOnInit() {
		this.yearList = [];
		let currentdate = new Date();
		this.generateYearList(currentdate.getFullYear(),'1950');
		this.getAllWebsite();
		this.getAllCountry();
		this.getAllWarehouse();
		this.processingFee = 0;
		this.itemPrice = 0;
		//console.log(JSON.parse(localStorage.getItem('selectedWarehouse')));
	}

	generateYearList(max,min){
		for (var i=max; i>=min; i--) {
	      this.yearList.push(i);
	    }
	}
	getAllWebsite() {
		this.auto.getAllWebsite().subscribe((data:any) => {
			
			if(data.status == 1)
			{
				this.websiteList = data.results;
			}

			//console.log(this.websiteList);
		})
	}

	getMake(event) {
		this.selectedWebsite = event.target.value;
		this.auto.getWebsiteMake(this.selectedWebsite).subscribe((data:any) => {
			if(data.status == '1')
			{
				this.makeList = JSON.parse(data.results);
				//console.log(this.makeList);
			}
		});
	}

	getModel(event) {
		let selectedMake = event.target.value;
		this.auto.getWebsiteModel(this.selectedWebsite,selectedMake).subscribe((data:any) => {
			if(data.status == '1')
			{
				this.modelList = JSON.parse(data.results);
			}
		})
	}

	getAllCountry(){
		this.content.allCountry().subscribe((data:any)=>{
			this.countryList = data;
		});
	}

	getStateCity(event){
		let selectedCountry = event.target.value;
		this.content.stateCityOfCountry(selectedCountry).subscribe((data:any) => {
			//console.log(data);
			if(data.stateList)
			{
				this.stateList = data.stateList;
			}
		});
	}

	getCity(event) {
		let selectedState = event.target.value;
		this.content.cityOfStates(selectedState).subscribe((data:any) => {
			if(data.cityList)
			{
				this.cityList = data.cityList;
			}
		});
	}

	getdestinationStateCity(event){
		let selectedCountry = event.target.value;
		this.content.stateCityOfCountry(selectedCountry).subscribe((data:any) => {
			//console.log(data);
			if(data.stateList)
			{
				this.destStateList = data.stateList;
			}
		});
	}

	getdestinationCity(event) {
		let selectedState = event.target.value;
		this.content.cityOfStates(selectedState).subscribe((data:any) => {
			if(data.cityList)
			{
				this.destCityList = data.cityList;
			}
		});
	}

	getAllWarehouse(){
		this.content.allWarehouse().subscribe((data:any) => {
			let index = 0;
			if(data.status == 1)
			{
				for(let eachWarehouse of data.results) {
					if(index == JSON.parse(localStorage.getItem('selectedWarehouse')))
					{
						this.warehouseId = eachWarehouse.id;
					}
					index++;
				}
				//console.log(this.warehouseId);
			}
		});
	}

	getProcessingFee(event) {
		let vehiclePrice = event.target.value;
		this.auto.getProcessingFee(vehiclePrice,this.warehouseId).subscribe((data:any) => {
			this.itemPrice = vehiclePrice;
			this.processingFee = data.results;
		})
	}

	fileUpload(event) {
  		let fileItem = event.target.files[0];
  		const file: File = fileItem;
  		this.selectedFile = file;
  		console.log(this.selectedFile);
  	}
	onSubmit(form : NgForm){
		this.auto.saveOthervehicleData(form.value, this.selectedFile).subscribe((data:any) => {

		});
	}
}
