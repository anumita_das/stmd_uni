import { NgModule } from '@angular/core';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { SharedModule } from '@app/shared';

@NgModule({
	imports: [
		LoginRoutingModule,
		SharedModule
	],
	declarations: [LoginComponent]
})
export class LoginModule {

}
