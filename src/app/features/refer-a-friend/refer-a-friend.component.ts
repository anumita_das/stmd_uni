import { Component, OnInit, NgZone, HostListener } from '@angular/core';
import { RegistrationService } from '../../services/registration.service';
import { NgForm,FormControl }   from '@angular/forms';
import { config } from '../../app.config';
import { ToastrService } from 'ngx-toastr'; 
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-refer-a-friend',
	templateUrl: './refer-a-friend.component.html',
	styleUrls: ['./refer-a-friend.component.scss']
})
export class ReferAFriendComponent implements OnInit {
	isMediumScreen: boolean;
	sidebar: boolean;
	chips: Array<any>;
	formSubmit : boolean;
	formError : boolean;
	submitSuccess : boolean;
	serverUrl : any = '';
	referralCode : string = '';
	users : any;
	submitError : boolean;
	message: any = '';

	constructor(
		private ngZone: NgZone,
		private regService : RegistrationService,
		public toastr : ToastrService,
		public spinner : NgxSpinnerService,
	) { }

	ngOnInit() {
		this.formSubmit = false; 
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		console.log('Width: ' + width);
		this.checkWindowSize(width);
		this.serverUrl = config.serverUrl;
		if(this.regService.item.user.referralCode)
		{
			this.referralCode = this.regService.item.user.referralCode;
		}

		this.chips = ['Javascript', 'Typescript'];

		this.message = this.serverUrl+'join/refer/'+ this.referralCode;
	}

	checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		// console.log('Width: ' + event.target.innerWidth);
		this.checkWindowSize(event.target.innerWidth);
	}

	// public onSelect(item) {
	// 	console.log('tag selected: value is ' + item);
	// }

	onSubmit(form : NgForm) {
		
		//console.log(form.value);
		let error = 0;
		let submittedUser = form.value.users;
		for(let eachuser of submittedUser)
		{
			if(!this.checkEmailValid(eachuser.value))
				error = 1;
		}
		if(error == 0)
		{
			this.spinner.show()
			this.formSubmit = true; 
			this.regService.sendInivitation(form.value).subscribe((data:any) => {
				this.formSubmit = false;
				this.spinner.hide();
				if(data.status == 1)
				{
					//form.message = '';
					//this.submitSuccess = true;
					//this.formError = false;
					form.resetForm();
					this.toastr.success("Invitations sent successfully");
				}
				else
				{
					this.toastr.error("Something went wrong")
					//this.submitSuccess = false;
					//this.formError = true;
				}
			});
		}
		else
		{
			this.toastr.error('Please enter valid email')
		}
	}
	checkEmailValid(email) {
		let reg = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
		return reg.test(email);
	}

	private validateAsync(control: FormControl): Promise<any> {
        return new Promise(resolve => {
            const value = control.value;
            let reg = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
            const result: any = !reg.test(value) ? {
                email: true
            } : null;

            setTimeout(() => {
                resolve(result);
            }, 400);
        });
    }

    public asyncErrorMessages = {
        email: 'Please add valid email id'
    };

    public asyncValidators = [this.validateAsync];

}
