import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, ParamMap } from '@angular/router';
import { RegistrationService } from '../../services/registration.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-success',
	templateUrl: './success.component.html',
	styleUrls: ['./success.component.scss']
})
export class SuccessComponent implements OnInit {

	showDiv : boolean;
	showMsg : boolean;
	flag : any;
	activationResendUser: boolean = false;

	constructor(
	private route: ActivatedRoute,
	private regService : RegistrationService,
	private toastr: ToastrService,
	public spinner : NgxSpinnerService,
	) { }
	socialIcons: Array<object>;

	

	ngOnInit() {
		}

	resendActivationMail(userid) {
		this.spinner.show();
		this.regService.resendActivation(userid).subscribe((data:any)=>{
			this.spinner.hide();
			if(data.status=='1')
			{
				this.activationResendUser = false;
			}
			else
			{
				this.toastr.error('Something went wrong! Please click resend link once again')
			}
		});
	}


}
