import { NgModule } from '@angular/core';
import { SuccessRoutingModule } from './success-routing.module';
import { SuccessComponent } from './success.component';
import { SharedModule } from '@app/shared';


@NgModule({
	imports: [
		SuccessRoutingModule,
		SharedModule
	],
	declarations: [
		SuccessComponent
	]
})
export class SuccessModule {

}
