 import { Injectable } from '@angular/core';

@Injectable()
export class AppGlobals {
    readonly siteTitle: string = 'Shop and Ship from USA & UK Stores to Africa | Save Up to 80% | Shoptomydoor';
    readonly siteMetaKeywords: string = 'shipping from USA and UK to Nigeria, Shop &amp; ship from USA to Nigeria, shipping companies in Nigeria, shipping to Nigeria from US, cargo to Nigeria, shipping from Nigeria to us, procurement from US, Shipping cost from Nigeria to USA,  online stores that ship to Nigeria, door to door shipping from USA to Nigeria, US stores that ship to Nigeria, shipping companies in Nigeria, Air shipping to Nigeria, ship to Nigeria';
    readonly siteMetaDescription: string = 'Shop and ship with Shoptomydoor from US & UK to Nigeria at reduce rate. We offer import-export shipping solution for business/personal needs. Shop from amazon, eBay, etc.';
    defaultCurrencySymbol: string = '$';
    virtualTourEntered : string;
}