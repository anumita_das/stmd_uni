import { Injectable, Inject, ElementRef } from '@angular/core';
import { DOCUMENT } from '@angular/common';
 
@Injectable({
   providedIn: 'root'
})
 
export class MetaService { 
   constructor(
   	@Inject(DOCUMENT) private dom
   	) { }
    
   createCanonicalURL() {   	
		  let link: HTMLLinkElement = this.dom.createElement('link');
        
        let url = this.dom.URL
        if(url.match('^http://')){
              url = url.replace("http://","https://")
         }
		  link.setAttribute('rel', 'canonical');
		  this.dom.head.appendChild(link);
		  link.setAttribute('href', url);		
   }
}