import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';  // <-- #1 import module

// Custom Components
import { HeaderComponent } from './components/header/header.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { SidebarFlyoutComponent } from './components/sidebar-flyout/sidebar-flyout.component';
import { SidebarAccordionComponent } from './components/sidebar-accordion/sidebar-accordion.component';
import { SidebarHelpComponent } from './components/sidebar-help/sidebar-help.component';
import { FooterComponent } from './components/footer/footer.component';
import { GetAQuoteComponent } from './components/get-a-quote/get-a-quote.component';
import { GetAQuoteItemComponent } from './components/get-a-quote/get-a-quote-item.component';
import { GetAQuoteAutoComponent } from './components/get-a-quote/get-a-quote-auto.component';
import { ShopCountryTabComponent } from './components/shop-country-tab/shop-country-tab.component';
import { ServiceModalComponent } from './components/service-modal/service-modal.component';
import { AddressbookModalComponent } from './components/addressbook-modal/addressbook-modal.component';
import { QuotePopupModalComponent } from './components/quote-popup-modal/quote-popup-modal.component';
import { StaticinfoModalComponent } from './components/staticinfo-modal/staticinfo-modal.component';
import { WarehouseModalComponent } from './components/warehouse-modal/warehouse-modal.component';
import { RecipientModalComponent } from './components/recipient-modal/recipient-modal.component';
import { PhotoShotModalComponent } from './components/photo-shot-modal/photo-shot-modal.component';
import { RecountModalComponent } from './components/recount-modal/recount-modal.component';
import { ReweighModalComponent } from './components/reweigh-modal/reweigh-modal.component';
import { ItempriceModalComponent } from './components/itemprice-modal/itemprice-modal.component';
import { WronginventoryModalComponent } from './components/wronginventory-modal/wronginventory-modal.component';
import { CurrencyModalComponent } from './components/currency-modal/currency-modal.component';
import { AutoConfirmModalComponent } from './components/auto-confirm-modal/auto-confirm-modal.component';
import { AutoImagesModalComponent } from './components/auto-images-modal/auto-images-modal.component';
import { PaystackModalComponent } from './components/paystack-modal/paystack-modal.component';
import { PayeezyModalComponent } from './components/payeezy-modal/payeezy-modal.component';
import { WalletModalComponent } from './components/wallet-modal/wallet-modal.component';
import { PointtowalletModalComponent } from './components/pointtowallet-modal/pointtowallet-modal.component';
import { DeliveryaddressotpModalComponent } from './components/deliveryaddressotp-modal/deliveryaddressotp-modal.component';
import { MsgModalComponent } from './components/msg-modal/msg-modal.component';
import { ForgotPasswordModalComponent } from '../shared/components/forgot-password-modal/forgot-password-modal.component';
import { GalleryModalComponent } from '../shared/components/gallery-modal/gallery-modal.component';
import { RequestModalComponent } from './components/request-modal/request-modal.component';
import { StorefromModalComponent } from './components/storefrom-modal/storefrom-modal.component';
import { ShipmentSettingsModalComponent } from './components/shipment-settings/shipmentsettings-modal.component';
import { TermsModalComponent } from './components/terms-modal/terms-modal.component';
import { ContactFormModalComponent } from './components/contactform-modal/contactform-modal.component';
import { ChangeCoordinatorComponent } from './components/change-coordinator/change-coordinator.component';
import { ShipmenttypeModalComponent } from './components/shipmenttype-modal/shipmenttype-modal.component';
import { SignUpModalComponent } from './components/signup-modal/signup-modal.component';

import { NumberOnlyDirective } from '../number.directive';

import { BsDropdownModule, TabsModule, PopoverModule, ProgressbarModule, AlertModule, RatingModule } from 'ngx-bootstrap';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { HttpClientModule } from '@angular/common/http';
import { OwlModule } from 'ngx-owl-carousel';
import { NgSelectModule } from '@ng-select/ng-select';
import { SidebarModule } from 'ng-sidebar';
import { AccordionModule } from 'ngx-bootstrap';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap';


import { WindowWidthService } from './services/windowWidth.service';
import { ClickOutsideModule } from 'ng4-click-outside';
import { SideMenuService } from './services/side-menu.service';
import { HelpMenuService } from './services/help-menu.service';
import { NgxPageScrollModule } from 'ngx-page-scroll';
import { TooltipModule } from 'ngx-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { TagInputModule } from 'ngx-chips';
import { DataTablesModule } from 'angular-datatables';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { TimepickerModule } from 'ngx-bootstrap';

import { AgmCoreModule } from '@agm/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxPayPalModule } from 'ngx-paypal';
import { Angular4PaystackModule } from 'angular4-paystack';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { WalkthroughModule } from 'angular-walkthrough';
import { ReturnModalComponent } from './components/return-modal/return-modal.component';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { SubscribeService } from './services/subscribe.service';
import { SubscriptionPlanService } from './services/subscription-plan.service';
import { CartSubscriptionModalComponent } from './components/cart-subscription-modal/cart-subscription-modal.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ShareButtonsModule } from '@ngx-share/buttons';


// import { FlyoutModule } from 'ngx-flyout';

@NgModule({
	imports: [
		CommonModule,
		// FlyoutModule,
		RouterModule,
		BsDropdownModule,
		TabsModule.forRoot(),
		PopoverModule.forRoot(),
		ProgressbarModule.forRoot(),
		HttpClientModule,
		AngularSvgIconModule,
		OwlModule,
		NgSelectModule,
		ReactiveFormsModule,
		FormsModule,
		SidebarModule.forRoot(),
		AccordionModule.forRoot(),
		CollapseModule.forRoot(),
		ModalModule.forRoot(),
		ClickOutsideModule,
		TooltipModule.forRoot(),
		NgxPageScrollModule,
		NgxDatatableModule,
		TagInputModule,
		DataTablesModule,
		BsDatepickerModule.forRoot(),
		TimepickerModule.forRoot(),
		AgmCoreModule.forRoot(),
		NgxSpinnerModule,
		NgxPayPalModule,
		Angular4PaystackModule,
		LoadingBarRouterModule,
		WalkthroughModule,
		LazyLoadImageModule,
		AlertModule.forRoot(),
		FontAwesomeModule,
		ShareButtonsModule,
		RatingModule.forRoot(),
	],
	exports: [
		CommonModule,
		ReactiveFormsModule,
		FormsModule,
		HeaderComponent,
		GetAQuoteComponent,
		GetAQuoteItemComponent,
		GetAQuoteAutoComponent,
		ShopCountryTabComponent,
		NavigationComponent,
		SidebarFlyoutComponent,
		SidebarAccordionComponent,
		SidebarHelpComponent,
		FooterComponent,
		ServiceModalComponent,
		SignUpModalComponent,
		RecipientModalComponent,
		AddressbookModalComponent,
		QuotePopupModalComponent,
		StaticinfoModalComponent,
		WalletModalComponent,
		PointtowalletModalComponent,
		DeliveryaddressotpModalComponent,
		MsgModalComponent,
		ForgotPasswordModalComponent,
		WarehouseModalComponent,
		CurrencyModalComponent,
		ShipmenttypeModalComponent,
		AutoConfirmModalComponent,
		AutoImagesModalComponent,
		PaystackModalComponent,
		PayeezyModalComponent,
		HttpClientModule,
		AngularSvgIconModule,
		OwlModule,
		TabsModule,
		PopoverModule,
		ProgressbarModule,
		NgSelectModule,
		SidebarModule,
		AccordionModule,
		CollapseModule,
		ModalModule,
		ClickOutsideModule,
		TooltipModule,
		TagInputModule,
		DataTablesModule,
		BsDropdownModule,
		BsDatepickerModule,
		TimepickerModule,
		AgmCoreModule,
		NgxSpinnerModule,
		NgxPayPalModule,
		LoadingBarRouterModule,
		WalkthroughModule,
		StorefromModalComponent,
		ChangeCoordinatorComponent,
		NumberOnlyDirective,
		AlertModule,
		FontAwesomeModule,
		ShareButtonsModule,
		RatingModule,
	],
	declarations: [
		HeaderComponent,
		NavigationComponent,
		SidebarFlyoutComponent,
		SidebarAccordionComponent,
		SidebarHelpComponent,
		FooterComponent,
		GetAQuoteComponent,
		GetAQuoteItemComponent,
		GetAQuoteAutoComponent,
		ShopCountryTabComponent,
		ServiceModalComponent,
		SignUpModalComponent,
		RecipientModalComponent,
		AddressbookModalComponent,
		QuotePopupModalComponent,
		StaticinfoModalComponent,
		WalletModalComponent,
		PointtowalletModalComponent,
		DeliveryaddressotpModalComponent,
		MsgModalComponent,
		ForgotPasswordModalComponent,
		WarehouseModalComponent,
		PhotoShotModalComponent,
		RecountModalComponent,
		ReweighModalComponent,
		ReturnModalComponent,
		ItempriceModalComponent,
		WronginventoryModalComponent,
		CurrencyModalComponent,
		AutoConfirmModalComponent,
		AutoImagesModalComponent,
		PaystackModalComponent,
		PayeezyModalComponent,
		GalleryModalComponent,
		RequestModalComponent,
		StorefromModalComponent,
		NumberOnlyDirective,
		ShipmentSettingsModalComponent,
		TermsModalComponent,
		ContactFormModalComponent,
		ChangeCoordinatorComponent,
        ShipmenttypeModalComponent,
        CartSubscriptionModalComponent
	],
	entryComponents: [
		ServiceModalComponent,
		SignUpModalComponent,
		AddressbookModalComponent,
		QuotePopupModalComponent,
		CartSubscriptionModalComponent,
		StaticinfoModalComponent,
		WalletModalComponent,
		PointtowalletModalComponent,
		DeliveryaddressotpModalComponent,
		MsgModalComponent,
		ForgotPasswordModalComponent,
		WarehouseModalComponent,
		RecipientModalComponent,
		PhotoShotModalComponent,
		RecountModalComponent,
		ReweighModalComponent,
		ReturnModalComponent,
		ItempriceModalComponent,
		WronginventoryModalComponent,
		CurrencyModalComponent,
		AutoConfirmModalComponent,
		AutoImagesModalComponent,
		PaystackModalComponent,
		PayeezyModalComponent,
		GalleryModalComponent,
		StorefromModalComponent,
		ShipmentSettingsModalComponent,
		TermsModalComponent,
		ContactFormModalComponent,
		ChangeCoordinatorComponent,
                ShipmenttypeModalComponent
	],
	providers: [
		WindowWidthService,
		SideMenuService,
		HelpMenuService,
		SubscribeService,
                SubscriptionPlanService
	]
})
export class SharedModule { }
