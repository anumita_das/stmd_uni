export class UserAddress {
	userid : string;
	title : string;
	unit : any;
	firstName : string;
	lastName : string;
	email : string;
	address : string;
	alternateAddress : string;
	country : any;
	state : any;
	city : any;
	zipcode : string;
	phone : string
	alternatePhone : string;
	addressType: string;
}