export class UserShipmentSettings {
	remove_shoe_box : string;
	original_box : string;
	quick_shipout : string;
	special_instruction:string;
}