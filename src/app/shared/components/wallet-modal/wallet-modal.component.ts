import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm }   from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { RegistrationService } from '../../../services/registration.service';
import { ContentService } from '../../../services/content.service';

@Component({
	selector: 'app-wallet-modal',
	templateUrl: './wallet-modal.component.html',
	styleUrls: ['./wallet-modal.component.scss']
})
export class WalletModalComponent implements OnInit {

	userId : number;
	inputPassword:any;
	code:any;
	wcodePattern = "^.{8,}$";
	constructor(
		public regService : RegistrationService,
		public bsModalRef: BsModalRef,
		public toastr : ToastrService,
		private content: ContentService,
		) {
			this.userId = this.regService.item.user.id;
		}

	ngOnInit() {
	}

	onSubmit(){
	}

	selectWalletCode(){
		this.content.getWalletCode(this.inputPassword, this.userId).subscribe((data:any) => { 

			if(data.status == 1){
				//this.bsModalRef.hide();
				if(data.countData == 1)
				this.code = "Your e-Wallet code is: "+data.results.walletCode;
				else
				this.code = data.results.walletCode;
			} else {
				this.code = 'Wrong password entered';
			}

		});
	}

}
