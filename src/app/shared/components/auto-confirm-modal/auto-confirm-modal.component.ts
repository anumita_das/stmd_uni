import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AutoService } from '../../services/auto.service';
import { RegistrationService } from '../../../services/registration.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
	selector: 'app-auto-confirm-modal',
	templateUrl: './auto-confirm-modal.component.html',
	styleUrls: ['./auto-confirm-modal.component.scss']
})
export class AutoConfirmModalComponent implements OnInit {

	title: string;
	closeBtnName: string;
	list: any[] = [];
	type : string = 'buy_a_car';

	constructor(
		public bsModalRef: BsModalRef,
		public auto : AutoService,
		public regService : RegistrationService,
		public router : Router,
		public toastr : ToastrService,
	) { }

	ngOnInit() {
		this.list.push('PROFIT!!!');
	}

	previosData() {
		this.auto.checkCartData(this.regService.userId,this.type).subscribe((data:any) => {
			if(data.status=='1')
			{
				this.bsModalRef.hide();
				localStorage.setItem('userAutoCart', '');
 				localStorage.setItem('userAutoCart', JSON.stringify(data.results));
 				this.router.navigate(['auto','checkout']);
			}
		})
	}

	newData() {
		this.auto.deleteCartData(this.regService.userId,this.type).subscribe((data:any) => {
			this.bsModalRef.hide();
			if(data.status == '1')
			{
				localStorage.removeItem('userAutoCart');
				this.toastr.success('Previous cart data removed');
			}
			else
				this.toastr.error('Something went wrong');
		});
	}


}
