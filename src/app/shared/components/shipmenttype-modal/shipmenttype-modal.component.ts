import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { RegistrationService } from '../../../services/registration.service';

@Component({
	selector: 'app-shipmenttype-modal',
	templateUrl: './shipmenttype-modal.component.html',
	styleUrls: ['./shipmenttype-modal.component.scss']
})
export class ShipmenttypeModalComponent implements OnInit {

	userId : number;
	shipmentTypeId : any = '';
	description : any = '';
	constructor(
		public regService : RegistrationService,
		public bsModalRef: BsModalRef,
		) {
			this.userId = this.regService.item.user.id;
		}

	ngOnInit() {
	}



}
