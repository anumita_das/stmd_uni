import { Component, OnInit, NgZone } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm } from '@angular/forms';
import { ContentService } from '../../../services/content.service';
import { RegistrationService } from '../../../services/registration.service';
import { ToastrService } from 'ngx-toastr';
import { DeliveryaddressotpModalComponent } from '../deliveryaddressotp-modal/deliveryaddressotp-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute,Router } from '@angular/router';


@Component({
	selector: 'app-terms-modal',
	templateUrl: './terms-modal.component.html',
	styleUrls: ['./terms-modal.component.scss']
})

export class TermsModalComponent implements OnInit {
	isMediumScreen: boolean;
	sidebar: boolean;
	pageContent: any;
	others: Array<any>;
	formSubmit : boolean;
	submitError : boolean;
	acceptTermsConditions: any;
	constructor(
		private ngZone: NgZone,
		public regService : RegistrationService,
		public toastr : ToastrService,
		public spinner : NgxSpinnerService,
		public bsModalRef: BsModalRef,
		private modalService: BsModalService,
		private contentService: ContentService,
		private sanitizer: DomSanitizer,
		private router: Router,
		) { }

	ngOnInit() {
		this.spinner.show();
		this.formSubmit = false;
		this.getPageContent();


		setTimeout(() => {
	        	/** spinner ends after 1 seconds */
	        	this.spinner.hide();
	    	}, 2000);
		
	}

	onSubmit(form: NgForm){

		this.spinner.show();
		this.formSubmit = true;
		console.log(form.value);
		this.regService.saveSignature(form.value).subscribe((data:any) => {
			this.formSubmit = false;
			if(data.status == 1)
			{
				
				this.toastr.success("Please check your mail to get a copy of terms and condition");
				this.bsModalRef.hide();
				window.location.reload();

			}
			else
			{
				this.toastr.error("Something went wrong");				

			}
			this.spinner.hide();
		});
		

	}

	
	getPageContent() {
		this.contentService.pageContent("terms-conditions-join").subscribe((data: any) => {
			if (data.status == 1) {
				let content = data.pageContent[0];
				this.pageContent = this.sanitizer.bypassSecurityTrustHtml(content.pageContent);
			} else {
				this.pageContent = "Pagecontent Not found!!";
				this.router.navigate(['/']); 
			}
		});
	}

	checkTermsConditions($event)
	{
		if($event.target.checked == true)
			this.acceptTermsConditions = 'Y';
		else 
			this.acceptTermsConditions = 'N';

	}


}
