import { Component, OnInit } from '@angular/core';
import { SideMenuService } from '../../services/side-menu.service';

@Component({
	selector: 'app-sidebar-flyout',
	templateUrl: './sidebar-flyout.component.html',
	styleUrls: ['./sidebar-flyout.component.scss']
})
export class SidebarFlyoutComponent implements OnInit {
	sideNavList: any;
	showSubMenu: boolean;
	submenu: any;
	subSubmenu: any;
	outSideClick: boolean;
	selectedItem: any;
	subSelectedItem: any;
	oneAtATime = true;
	isOpen: boolean;

	constructor(
		private sideNavListData: SideMenuService
	) {
		this.outSideClick = false;
	}
	ngOnInit() {

		this.sideNavList = this.sideNavListData.getData();


		// this.sideNavList = [
		// 	{
		// 		name: 'My Warehouse', icon: 'shop-online.svg', link: '', children: [
		// 			{
		// 				name: 'Modify Profile', link: '', subChildren: [
		// 					{ name: 'Shipping Preference', link: '' },
		// 					{ name: 'Other Options', link: '' }
		// 				]
		// 			},
		// 			{ name: 'Redeem Reward Points', link: 'https://google.com', subChildren: [] },
		// 			{ name: 'View Messages', link: 'https://google.com', subChildren: [] },
		// 			{ name: 'View Order History', link: 'https://google.com', subChildren: [] },
		// 			{ name: 'Refer A Friend', link: 'https://google.com', subChildren: [] },
		// 			{
		// 				name: 'Manage Setting', link: '', subChildren: [
		// 					{ name: 'Shipping Preference', link: '' },
		// 					{ name: 'Other Options', link: '' }
		// 				]
		// 			}
		// 		]
		// 	},
		// 	{ name: 'Shop For Me', icon: 'shop-for-me.svg', link: 'https://google.com' },
		// 	{ name: 'Fill & Ship', icon: 'fill-ship.svg', link: 'https://google.com' },
		// 	{ name: 'Auto', icon: 'auto.svg', link: 'https://google.com' },
		// 	{ name: 'Payment', icon: 'payment.svg', link: 'https://google.com' },
		// 	{
		// 		name: 'My Account', icon: 'user.svg', link: '', children: [
		// 			{ name: 'Modify Profile', link: 'https://google.com', subChildren: [] },
		// 			{ name: 'Redeem Reward Points', link: 'https://google.com', subChildren: [] },
		// 			{ name: 'View Messages', link: 'https://google.com', subChildren: [] },
		// 			{ name: 'View Order History', link: 'https://google.com', subChildren: [] },
		// 			{ name: 'Refer A Friend', link: 'https://google.com', subChildren: [] },
		// 			{
		// 				name: 'Manage Setting', link: '', subChildren: [
		// 					{ name: 'Shipping Preference', link: '' },
		// 					{ name: 'Other Options', link: '' }
		// 				]
		// 			}
		// 		]
		// 	},
		// 	{ name: 'Rewards', icon: 'rewards.svg', link: 'https://google.com' },
		// 	{ name: 'Help', icon: 'help.svg', link: 'https://google.com' },
		// ];
	}

	// toggleSubMenu() {
	// 	this.showSubMenu = this.showSubMenu ? false : true;
	// 	// this.showSubMenu === i ? this.showSubMenu = - 1 : show = i
	// }
	showsubmenu(index) {
		this.submenu = this.sideNavList[index];
		// console.log(this.submenu);
		// this.submenu = this.sideNavList[index];
		// console.log(this.submenu.length);

	}
	showsubsubmenu(index, indexSub) {
		this.subSubmenu = this.sideNavList[index].children[indexSub];
		console.log(this.subSubmenu);
		// console.log(this.submenu.length);

	}

	showSubSubmenu(index, indexSub) {
		if (this.sideNavList[index].children[indexSub].subChildren.length = 0) {
			this.isOpen = false;
		} else {
			this.isOpen = true;
		}
	}

	onClickedOutside(e: Event) {
		console.log('Clicked outside:', e);
		console.log('close', this.outSideClick);

		this.outSideClick = false;
	}

	menuClick(event, newValue) {
		console.log(newValue);
		this.selectedItem = newValue;  // don't forget to update the model here
		// ... do other stuff here ...
	}
	subMenuClick(event, newValue) {
		console.log(newValue);
		this.subSelectedItem = newValue;  // don't forget to update the model here
		// ... do other stuff here ...
	}


}
