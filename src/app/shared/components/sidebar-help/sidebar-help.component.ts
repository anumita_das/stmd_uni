import { Component, OnInit } from '@angular/core';
import { HelpMenuService } from '../../services/help-menu.service';
import { config } from '../../../app.config';
@Component({
	selector: 'app-sidebar-help',
	templateUrl: './sidebar-help.component.html',
	styleUrls: ['./sidebar-help.component.scss']
})
export class SidebarHelpComponent implements OnInit {
	oneAtATime = true;
	helpNavList: any;
	//newcats: any;
	rootUrl:any = config.api;
	selected:any;

	constructor(
		private helpNavListData: HelpMenuService
	) {

	}
	ngOnInit() { 
		//this.helpNavList = this.helpNavListData.getData();
		//this.newcats = this.helpNavListData.getAllCustomerHelpCategory();
		this.helpNavListData.getAllCustomerHelpCategory().subscribe((data:any) => { 
			//this.getFaqs = data.results.Faq;
			this.helpNavList = data.results;
		});
		
	}

	activateClass(index){
  		this.selected = index;		   
	}
}
