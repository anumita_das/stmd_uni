import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { MywarehouseService } from '../../../services/mywarehouse.service';
import { RegistrationService } from '../../../services/registration.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
@Component({
	selector: 'app-itemprice-modal',
	templateUrl: './itemprice-modal.component.html',
	styleUrls: ['./itemprice-modal.component.scss']
})
export class ItempriceModalComponent implements OnInit {

	title: string;
	closeBtnName: string;
	minPrice : any;
	maxPrice : any;
	packageId : any;
	email : any;
	itemPrice : any;
	totalPrice : any;
	valueEntered : any;
	lessValueEntered : boolean = false;
	uploadEnabled : boolean = false;
	deliveryId : any;
	invoiceList : any = "";

	constructor(
		public bsModalRef: BsModalRef,
		public mywarehouseService : MywarehouseService,
		public regService : RegistrationService,
		public toastr: ToastrService,
		public spinner : NgxSpinnerService,
	) {
		this.email = this.regService.item.user.email;
	}


	ngOnInit() {
		
	}

	getDiscountedUploadedInvoice() {
		this.mywarehouseService.getDiscountedUploadedInvoice(this.deliveryId).subscribe((data:any) =>{
			if(data.status == '1')
			{
				this.invoiceList = data.results;
				this.uploadEnabled = true;
			}
		});				
	}

	setTwoNumberDecimal($event) {
		if(!isNaN(parseFloat($event.target.value))){
		 	this.maxPrice = Math.abs(parseFloat($event.target.value)).toFixed(2);
		}
	 	else {
	 		this.maxPrice = Math.abs(parseFloat("0")).toFixed(2);
	 	}
	}

	saveItemPrice() {

		if(parseFloat(this.maxPrice)<parseFloat(this.minPrice))
		{

			this.lessValueEntered = true;
		}
		else
		{

			this.valueEntered = 'max';
			this.mywarehouseService.updateItemprice(this.packageId,this.maxPrice).subscribe((data:any) => {
				if(data.status == '1')
				{
					this.itemPrice = data.results.itemPrice;
					this.totalPrice = data.results.totalPrice;
					this.toastr.success('Value Updated Successfully');
					this.bsModalRef.hide();
				}
			});
		}
	}

	enableUpload() {
		this.getDiscountedUploadedInvoice();
		//this.uploadEnabled = true;
	}

	fileUpload(event) {
  		this.spinner.show();
  		let fileItem = event.target.files[0];

        const formData = new FormData();
      	formData.append('fileItem', fileItem, fileItem.name);
      	formData.append('packageId', this.packageId);
      	formData.append('requestedItemprice',this.maxPrice);

        this.mywarehouseService.uploadDicountedInvoice(formData).subscribe((data:any) => {
        	this.spinner.hide();
        	if(data.status == 1)
        	{
        		this.toastr.success('Request successfully send to Admin. Admin will update the price after verification');
        		this.bsModalRef.hide();
        	}
        });
    }

    previousFileUpload(event,selectedItem) {

    	this.spinner.show();
    	const formData = new FormData();

      	formData.append('fileItem', selectedItem);
      	formData.append('packageId', this.packageId);
      	formData.append('requestedItemprice',this.maxPrice);

      	this.mywarehouseService.uploadDicountedInvoice(formData).subscribe((data:any) => {
        	this.spinner.hide();
        	if(data.status == 1)
        	{
        		this.toastr.success('Request successfully send to Admin. Admin will update the price after verification');
        		this.bsModalRef.hide();
        	}
        });
    }


}
