import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItempriceModalComponent } from './itemprice-modal.component';

describe('ItempriceModalComponent', () => {
	let component: ItempriceModalComponent;
	let fixture: ComponentFixture<ItempriceModalComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ItempriceModalComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ItempriceModalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
