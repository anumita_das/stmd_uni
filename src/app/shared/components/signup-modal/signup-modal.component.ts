import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
// import { Router } from '@angular/router';


@Component({
	selector: 'app-signup-modal',
	templateUrl: './signup-modal.component.html',
	styleUrls: ['./signup-modal.component.scss']
})
export class SignUpModalComponent implements OnInit {

	title: string;
	closeBtnName: string;
	list: any[] = [];
	serviceContent : any = "";

	constructor(public bsModalRef: BsModalRef) { }

	ngOnInit() {
		
	}


}
