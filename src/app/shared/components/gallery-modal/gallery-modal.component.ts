import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';


@Component({
	selector: 'app-gallery-modal',
	templateUrl: './gallery-modal.component.html',
	styleUrls: ['./gallery-modal.component.scss']
})
export class GalleryModalComponent implements OnInit {
	galleryImages: any;
	type: any;
	imagepath: any;
	

	constructor(public bsModalRef: BsModalRef) { }

	ngOnInit() {
		this.galleryImages = [
			{
				img: 'assets/imgs/gallery/full-size/img-1.jpeg',
				description: 'Some caption'
			},
			{
				img: 'assets/imgs/gallery/full-size/img-2.jpeg',
				description: 'Some caption'
			},
			{
				img: 'assets/imgs/gallery/full-size/img-3.jpeg',
				description: 'Some caption'
			},
			{
				img: 'assets/imgs/gallery/full-size/img-4.jpg',
				description: 'Some caption'
			},
			{
				img: 'assets/imgs/gallery/full-size/img-1.jpeg',
				description: 'Some caption'
			},
			{
				img: 'assets/imgs/gallery/full-size/img-2.jpeg',
				description: 'Some caption'
			},
			{
				img: 'assets/imgs/gallery/full-size/img-3.jpeg',
				description: 'Some caption'
			},
			{
				img: 'assets/imgs/gallery/full-size/img-4.jpg',
				description: 'Some caption'
			}
		];
	}




}
