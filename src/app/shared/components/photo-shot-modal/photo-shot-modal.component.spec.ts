import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotoShotModalComponent } from './photo-shot-modal.component';

describe('PhotoShotModalComponent', () => {
	let component: PhotoShotModalComponent;
	let fixture: ComponentFixture<PhotoShotModalComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [PhotoShotModalComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PhotoShotModalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
