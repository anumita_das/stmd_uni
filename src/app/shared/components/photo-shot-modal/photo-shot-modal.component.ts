import { Component, OnInit} from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { MywarehouseService } from '../../../services/mywarehouse.service';
import { RegistrationService } from '../../../services/registration.service';

@Component({
	selector: 'app-photo-shot-modal',
	templateUrl: './photo-shot-modal.component.html',
	styleUrls: ['./photo-shot-modal.component.scss'],
})
export class PhotoShotModalComponent implements OnInit {
	title: string;
	closeBtnName: string;
	deliveryId: any;
	shipmentId: any;
	packageId: any;
	email: any;
	isPhotoShotDisabled: any = 'N';
	shipment: any;
	packagetype: any = '';
	photoshotCost: any = '';

	constructor(
		public bsModalRef: BsModalRef,
		public mywarehouseService : MywarehouseService,
		public regService : RegistrationService,
	) {
		this.email = this.regService.item.user.email;
	 }

	ngOnInit() {
		
	}

	addDeliveryPhotoShot(deliveryId, shipmentId){
		if(this.packagetype == 'package'){
			const formData = {deliveryId : deliveryId, shipmentId : shipmentId, packageId: this.packageId, email : this.email};
			this.mywarehouseService.addpackagesnapshot(formData).subscribe((data:any) => {
				this.isPhotoShotDisabled = 'Y';
				this.shipment = {
					otherChargeAmount : data.results.otherChargeAmount,
					totalCost : data.results.totalCost,
				};
				this.bsModalRef.hide();
			});
		} else {
			const formData = {deliveryId : deliveryId, shipmentId : shipmentId, email : this.email, type : 'snapshot'};
			this.mywarehouseService.adddeliveryothercharges(formData).subscribe((data:any) => {
				this.isPhotoShotDisabled = 'Y';
				this.shipment = {
					otherChargeAmount : data.results.otherChargeAmount,
					totalCost : data.results.totalCost,
				};
				this.bsModalRef.hide();
			});
		}
		
	}

}
