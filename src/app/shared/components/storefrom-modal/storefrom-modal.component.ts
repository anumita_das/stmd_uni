import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm }   from '@angular/forms';
import { RegistrationService } from '../../../services/registration.service';
import { ShopformeService } from '../../../services/shopforme.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-storefrom-modal',
	templateUrl: './storefrom-modal.component.html',
	styleUrls: ['./storefrom-modal.component.scss']
})
export class StorefromModalComponent implements OnInit {

	title: string;
	closeBtnName: string;
	list: any[] = [];
	warehouseCode : any;
	bagList : any;

	constructor(
		public bsModalRef: BsModalRef,
		public userInfo : RegistrationService,
		public shopformeService : ShopformeService,
		private toastr: ToastrService,
	) { }

	ngOnInit() {

	}

	clearcart() {
		this.shopformeService.clearUserCart('shopforme').subscribe((response:any) => {
		if(response.results == 'success')
			{
				this.toastr.success('Items have been removed from cart!');
				window.location.reload();
			}
			
		});
	}


	//Save Cart Items For Later 
	saveCartItems() {
		const formData =  {items : this.bagList };
		this.shopformeService.savecartlist(formData).subscribe((data:any) => {
			if(data.results == 'success'){
				this.toastr.success('Items have been saved for later!');
	 			window.location.reload();
	 		}
			
		});
	}

	continue(){
		localStorage.removeItem('storeShopForMe');
		window.location.reload();
	}



}
