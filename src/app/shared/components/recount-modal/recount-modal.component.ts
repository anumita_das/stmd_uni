import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { MywarehouseService } from '../../../services/mywarehouse.service';
import { RegistrationService } from '../../../services/registration.service';

@Component({
	selector: 'app-recount-modal',
	templateUrl: './recount-modal.component.html',
	styleUrls: ['./recount-modal.component.scss']
})
export class RecountModalComponent implements OnInit {

	title: string;
	closeBtnName: string;
	deliveryId: any;
	shipmentId: any;
	email: any;
	isRecountDisabled: any = 'N';
	shipment: any;
	recountCost : any = "";

	constructor(
		public bsModalRef: BsModalRef,
		public mywarehouseService : MywarehouseService,
		public regService : RegistrationService,
	) {
		this.email = this.regService.item.user.email;
	}


	ngOnInit() {
		
	}

	addDeliveryRecount(deliveryId, shipmentId){
		const formData = {deliveryId : deliveryId, shipmentId : shipmentId, email : this.email, type : 'recount'};
		this.mywarehouseService.adddeliveryothercharges(formData).subscribe((data:any) => {
			this.isRecountDisabled = 'Y';
			this.shipment = {
				otherChargeAmount : data.results.otherChargeAmount,
				totalCost : data.results.totalCost,
			};
			this.bsModalRef.hide();
		});
	}
}
