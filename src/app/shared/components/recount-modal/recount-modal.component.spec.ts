import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecountModalComponent } from './recount-modal.component';

describe('RecountModalComponent', () => {
	let component: RecountModalComponent;
	let fixture: ComponentFixture<RecountModalComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [RecountModalComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(RecountModalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
