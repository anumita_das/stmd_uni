import { Component, OnInit } from '@angular/core';
import { SideMenuService } from '../../services/side-menu.service';
@Component({
	selector: 'app-sidebar-accordion',
	templateUrl: './sidebar-accordion.component.html',
	styleUrls: ['./sidebar-accordion.component.scss']
})
export class SidebarAccordionComponent implements OnInit {
	oneAtATime = true;
	sideNavList: any;

	constructor(
		private sideNavListData: SideMenuService
	) {

	}
	ngOnInit() {
		this.sideNavList = this.sideNavListData.getData();
	}
}
