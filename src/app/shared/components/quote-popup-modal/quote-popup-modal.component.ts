import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AutoService } from '../../services/auto.service';
import { RegistrationService } from '../../../services/registration.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
	selector: 'app-quote-popup-modal',
	templateUrl: './quote-popup-modal.component.html',
	styleUrls: ['./quote-popup-modal.component.scss']
})
export class QuotePopupModalComponent implements OnInit {

	title: string;
	closeBtnName: string;
	list: any[] = [];
	dataFound : boolean = false;

	constructor(
		public bsModalRef: BsModalRef,
		public auto : AutoService,
		public regService : RegistrationService,
		public router : Router,
		public toastr : ToastrService,
	) { }

	ngOnInit() {
		this.list.push('PROFIT!!!');
	}


}
