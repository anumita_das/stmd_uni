import { Component, OnInit } from '@angular/core';
import { GetAQuoteComponent } from './get-a-quote.component';
import { GetquoteService } from '../../services/getquote.service';
import { NgForm }   from '@angular/forms';
import { AppGlobals } from '../../../app.globals';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { QuotePopupModalComponent } from '../../../shared/components/quote-popup-modal/quote-popup-modal.component';

@Component({
	selector: 'app-get-a-quote-item',
	templateUrl: './get-a-quote-item.component.html',
	styleUrls: ['./get-a-quote.component.scss']
})

export class GetAQuoteItemComponent extends GetAQuoteComponent  {
   
   	bsModalRef: BsModalRef;
  	itemCount = 1;
  	productArray = [];
  	currencySymbol : any = '';
  	quoteMsg  : any = '';
  	scrollDone : any;
  	isQuoteRequested : boolean = false;
	getquoteitem =  {
		from : {
			countryId : "",
			stateId : "",
			cityId : "",
			stateName : "",
			cityName : "",
		}, 
		to : {
			countryId : "",
			stateId : "",
			cityId : "",
			stateName : "",
			cityName : "",
		}, 
		viewCurrency : "USD",
		itemContactName : "",
		itemContactEmail : "",
		itemContactPhone : "",
		items : [{ 
			subcategoryList : [],
			productList : [],
			categoryId : "",
			subCategoryId : "0",
			productId : "0",
			quantity : "",
			declaredValue : "",
			itemDescription : "",
			itemUrl : "",
			productImgSrc : "assets/imgs/box.png",
			quoteRequest : false,
		}]
	};

	public categoryList : any;
	public sendQuoteRequest : boolean = false;
	

	constructor(
		protected getquoteService : GetquoteService,
		protected _global: AppGlobals,
		protected spinner : NgxSpinnerService,
		protected toastr : ToastrService,
		protected modalService: BsModalService,
	) { 
		super(getquoteService,_global,spinner,toastr,modalService);
	  	super.getCurrencyList();
	  	super.getWarehouseList();
	 }


	ngOnInit() {
		this.currencySymbol = this._global.defaultCurrencySymbol;
	  	this.getCategoryList();
	  	super.getCountryList();
	}

	countryChanged(countryId){
		super.countryChanged(countryId);
	}

	stateChanged(stateId){
		super.stateChanged(stateId);
	}

	tocountryChanged(countryId){
		super.tocountryChanged(countryId);
	}

	tostateChanged(stateId){
		super.tostateChanged(stateId);
	}

	getCategoryList(){
		this.getquoteService.categorylist().subscribe((data:any) => {
			this.categoryList = data;
		});
	}

	categoryChanged(categoryId, index){

		this.getquoteitem.items[index].productList = [];
		this.getquoteitem.items[index].subcategoryList = [];

		this.updateQuoteRequest(categoryId,index);
		this.getquoteService.subcategorylist(categoryId).subscribe((data:any) => {
			if(data.length > 0){
				this.getquoteitem.items[index].subcategoryList = data;
				this.getquoteitem.items[index].subCategoryId  = "0";
				this.getquoteitem.items[index].productId = "0";
			    this.getquoteitem.items[index].productImgSrc  = "assets/imgs/box.png";
			} else {
				this.getquoteitem.items[index].subCategoryId  = "0";
				this.getquoteitem.items[index].productId = "0";
				this.getquoteitem.items[index].productImgSrc  = "assets/imgs/box.png";
			}
		});
	}

	subCategoryChanged(subCategoryId, index){
		this.getquoteitem.items[index].productList = [];

		this.updateQuoteRequest(subCategoryId,index);

		this.getquoteService.productlist(subCategoryId).subscribe((data:any) => {
			if(data.length > 0){
				this.getquoteitem.items[index].productList = data;
				this.getquoteitem.items[index].productId  = "0";
				this.getquoteitem.items[index].productImgSrc  = "assets/imgs/box.png";
			} else {
				this.getquoteitem.items[index].productId  = "0";
				this.getquoteitem.items[index].productImgSrc  = "assets/imgs/box.png";
			}
		});
	}

	productChanged(productId, index) {
		
		this.updateQuoteRequest(productId,index);

		for(let result of this.getquoteitem.items[index].productList){
   			this.productArray[result.id] = result.image;
		}
		if(productId!=0)
			this.getquoteitem.items[index].productImgSrc  = this.productArray[productId];
		else
			this.getquoteitem.items[index].productImgSrc  = "assets/imgs/box.png";
	}

	updateQuoteRequest(valueToCheck,mainIndex) {
		if(valueToCheck == 0)
		{
			this.sendQuoteRequest = true;
			this.getquoteitem.items[mainIndex].quoteRequest = true;
		}
		else
		{
			var array = this.hack(this.getquoteitem.items);
			let notListed = 0;
			array.forEach(function (item, index) {

				if(item['categoryId'] == 0 || item['subCategoryId'] == 0 || item['productId'] == 0)
				{
					notListed = 1;
					item['quoteRequest'] = true;
				}
				else
				{
					item['quoteRequest'] = false;
				}
			});

			if(notListed == 1)
				this.sendQuoteRequest = true;
			else
				this.sendQuoteRequest = false;
		}
	}

	addItem() {
		this.getquoteitem.items.push({
			subcategoryList : [],
			productList : [],
    		categoryId : "",
			subCategoryId : "0",
			productId : "0",
			quantity : "",
			declaredValue : "",
			itemDescription : "",
			itemUrl : "",
			productImgSrc : "assets/imgs/box.png",
			quoteRequest : false,

      });
      this.itemCount = this.itemCount+1;
	}

	// deletePackageRow
	deletePackageRow(index) {
		this.getquoteitem.items.splice(index, 1);
		this.itemCount = this.itemCount -1;
		this.updateQuoteRequest('-1','-1')
	}


	dataSubmit(form: any, formData: any){
		
		this.spinner.show();
		this.showShipping = false;
		this.scrollDone = 0;
		if(!this.sendQuoteRequest)
		{
			this.isQuoteRequested = true;
			this.getquoteService.getItemQuote(form).subscribe((data:any) => {
				this.bsModalRef = this.modalService.show(QuotePopupModalComponent,
					{
						backdrop  : 'static',
   						keyboard  : false
   					}
				);


				this.spinner.hide();
				this.sendQuoteRequest = true; 
				if(data.status == 1)
				{
					this.bsModalRef.content.dataFound = true;
					formData.submitted = false;
					this.showShipping = true;
					this.shippingTypes = data.results;
					this.quoteMsg = data.quoteMsg;
					setInterval(()=>{
						this.scrollDone++;
						this.scroll('shippingType');
					},1000);
				}
				else
				{
					formData.submitted = false;
					this.toastr.error(data.results);
				}
			});
		}
		else {
			this.getquoteService.saveItemQuoteContact(form).subscribe((data:any) => {
				this.spinner.hide();
				if(!this.isQuoteRequested)
				{
					this.bsModalRef = this.modalService.show(QuotePopupModalComponent,
					{
						backdrop  : 'static',
   						keyboard  : false
   					});
				}
				if(data.status)
				{
					this.bsModalRef.content.dataFound = true;
					this.toastr.success('Request submitted successfully');
					setTimeout(() => {
						window.location.reload();
	          		}, 5000);
					//formRef.resetForm();
					//this.resetFormValues();
					//window.location.reload();
				}
				else
				{
					this.toastr.error('Something went wrong. Please try again later');
				}
			});
		}
	}

	onSubmitItemcontact(form) {
		this.spinner.show();
		this.getquoteService.saveItemQuoteContact(form.value).subscribe((data:any) => {
			this.spinner.hide();
			if(data.status)
			{
				this.toastr.success('Request submitted successfully');
				this.resetFormValues();
			}
			else
			{
				this.toastr.error('Something went wrong. Please try again later');
			}
		});
	}

	setInteger($event)
	{
		$event.target.value = Math.abs(Math.floor($event.target.value));
    }

    hack(value) {
		if(value!=undefined && value!=null)
		{
			return Object.values(value);
		}
	   
	}

	resetFormValues() {

		this.getquoteitem =  {
			from : {
				countryId : "",
				stateId : "",
				cityId : "",
				stateName : "",
				cityName : "",
			}, 
			to : {
				countryId : "",
				stateId : "",
				cityId : "",
				stateName : "",
				cityName : "",
			}, 
			viewCurrency : "",
			itemContactName : "",
			itemContactEmail : "",
			itemContactPhone : "",
			items : [{ 
				subcategoryList : [],
				productList : [],
				categoryId : "",
				subCategoryId : "",
				productId : "",
				quantity : "",
				declaredValue : "",
				itemDescription : "",
				itemUrl : "",
				productImgSrc : "assets/imgs/box.png",
				quoteRequest : false,
			}]
		};
	}

	scroll(id) {
		if(this.scrollDone <= 5)
		{
		  let el = document.getElementById(id);
		  el.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});
		}
	}

}
