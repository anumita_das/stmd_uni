import { Component, OnInit, NgZone, HostListener } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm }   from '@angular/forms';
import { AppGlobals } from '../../../app.globals';
import { MywarehouseService } from '../../../services/mywarehouse.service';
import { RegistrationService } from '../../../services/registration.service';
import {Router} from "@angular/router";

@Component({
	selector: 'app-return-modal',
	templateUrl: './return-modal.component.html',
	styleUrls: ['./return-modal.component.scss']
})
export class ReturnModalComponent implements OnInit {
	
	dtOptions: DataTables.Settings = {};
	title: string;
	closeBtnName: string;
	deliveryId: any;
	shipmentId: any;
	costOfReturn: any;
	email: any;
	isReturnDisabled: any = 'N';
	shipment: any;
	returnCost : any = '';
	showReturnDetails : boolean = true;
	showDeliveryItems : boolean = false;
	selectedFile :any = [];
	showUpload: any;
	checkItemValue: any;
	showUploadLabel : boolean = false;
	package = {};
	returnlist = {};
	costReturn:any;
	showMsg : boolean = false;

	constructor(
		public bsModalRef: BsModalRef,
		public mywarehouseService : MywarehouseService,
		public regService : RegistrationService,
		private router: Router,
	) {
		this.email = this.regService.item.user.email;
	}


	ngOnInit() {

		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};
		this.showUpload = [];
		this.checkItemValue =[];
	}

	selectItems(deliveryId, shipmentId, returnCost){
		const formData = {deliveryId : deliveryId, shipmentId : shipmentId, returnCost: returnCost};
		this.showReturnDetails = false;
		this.showDeliveryItems = true;

		this.mywarehouseService.selectDelieveryItems(formData).subscribe((data:any) => {
		 if(data.results){
		 	this.shipment = data.results;
		 	this.costReturn = data.returnCost;
		 }else{
		 	this.shipment = [];
		 }			
			
		});
	}

	onSubmit(form: NgForm){	
		let formData = new FormData();
		if(this.selectedFile.length == 0)
		{
			this.showMsg = true;
		}else{

			this.showMsg = false;

			/*for(let i=0; i<this.selectedFile.length; i++)
		  	{
		  		
				formData.append('post['+i+'][image]', this.selectedFile[i], this.selectedFile[i].name);
			
		  		
		  	}*/
		  	console.log(this.checkItemValue);
			for(let i=0; i<this.checkItemValue.length; i++)
			{
				formData.append('post['+i+'][image]', this.selectedFile[i], this.selectedFile[i].name);
				formData.append('post['+i+'][id]', this.checkItemValue[i]);
				formData.append('post['+i+'][shipmentId]', this.shipmentId);
				formData.append('post['+i+'][deliveryId]', this.deliveryId);
				formData.append('post['+i+'][costOfReturn]', this.costReturn);
			}
		  	this.mywarehouseService.updateReturnData(formData).subscribe((data:any) => {
		  		if(data.status == 1){
		  			localStorage.removeItem('returnItemDetails');
		 			localStorage.setItem('returnItemDetails', JSON.stringify(data.results));
		 			this.bsModalRef.hide();
		 			this.router.navigate(['my-warehouse','payment']);
		  		}
		  		

		  	});	

		}
	  	
	}

	fileUpload(event, i) {
		if(event.target.files[0]=="")
		{
			this.showMsg = true;
		}
		else{
			this.showMsg = false;
			let fileItem = event.target.files[0];
			const file: File = fileItem;
			this.selectedFile.push(file);

		}
		
	}

	checkItem(event, i)
	{
		if (event.target.checked) {
			this.showUpload[i] = 1;
			this.checkItemValue.push(event.target.value);
			this.showUploadLabel = true;
		}else{
			this.showUpload[i] = 0;
			this.showUploadLabel = false;
		}
	}

	hack(value) {
		if(value!=undefined && value!=null)
		{
			return Object.values(value);
		}
	   
	}
}
