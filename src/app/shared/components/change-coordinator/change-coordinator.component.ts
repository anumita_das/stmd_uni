import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm }   from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { RegistrationService } from '../../../services/registration.service';
import { ContentService } from '../../../services/content.service';

@Component({
	selector: 'app-change-coordinator',
	templateUrl: './change-coordinator.component.html',
	styleUrls: ['./change-coordinator.component.scss']
})
export class ChangeCoordinatorComponent implements OnInit {

	userId : number;
	inputPassword:any;
	code:any;
	countryList : any;
	stateList:any;
	cityList:any;
	coordinatorList:any;
	groupId:any;
	constructor(
		public regService : RegistrationService,
		public bsModalRef: BsModalRef,
		public toastr : ToastrService,
		private contentService: ContentService,
		public spinner : NgxSpinnerService,
		) {
			this.userId = this.regService.item.user.id;
		}

	ngOnInit() {
		this.getAllCountry();
	}

	getAllCountry(){
		this.contentService.allCountry().subscribe((data:any)=>{
			this.countryList = data;
		});
	}

	getStateCity(event){
		let selectedCountry = event.target.value;
		this.contentService.stateCityOfCountry(selectedCountry).subscribe((data:any) => {
			if(data.stateList)
			{
				this.stateList = data.stateList;
				this.cityList = [];
				this.cityList = data.cityList;
			}
		});
	}

	getCity(event) {
		let selectedState = event.target.value;
		this.contentService.cityOfStates(selectedState).subscribe((data:any) => {
			if(data.cityList)
			{
				this.cityList = data.cityList;
			}
		});
	}
 
	dataSubmit(form : NgForm) {

		this.spinner.show();
		this.regService.saveCoordinatorChange(form.value).subscribe((data:any) => {
			this.spinner.hide();
			if(data.status == 1) {
				this.toastr.success(data.results);
				this.bsModalRef.hide();
			} else {
				this.toastr.error(data.results);
			}
		});
	}

}
