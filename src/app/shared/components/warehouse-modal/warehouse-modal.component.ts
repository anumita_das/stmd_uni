import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm }   from '@angular/forms';
import { RegistrationService } from '../../../services/registration.service';
// import { Router } from '@angular/router';


@Component({
	selector: 'app-warehouse-modal',
	templateUrl: './warehouse-modal.component.html',
	styleUrls: ['./warehouse-modal.component.scss']
})
export class WarehouseModalComponent implements OnInit {

	title: string;
	closeBtnName: string;
	list: any[] = [];
	warehouseAddress : any;

	constructor(
		public bsModalRef: BsModalRef,
		public userInfo : RegistrationService,
		) { }

	ngOnInit() {
		//console.log(this.userInfo.item);
		//console.log(this.closeBtnName);
	}


}
