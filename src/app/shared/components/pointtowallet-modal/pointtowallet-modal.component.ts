import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm }   from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { RegistrationService } from '../../../services/registration.service';
import { ContentService } from '../../../services/content.service';

@Component({
	selector: 'app-pointtowallet-modal',
	templateUrl: './pointtowallet-modal.component.html',
	styleUrls: ['./pointtowallet-modal.component.scss']
})
export class PointtowalletModalComponent implements OnInit {

	userId : number;
	userPoints : number = 0;
	userTotalPoints : number = 0;
	constructor(
		public regService : RegistrationService,
		public bsModalRef: BsModalRef,
		public toastr : ToastrService,
		private content: ContentService,
		) {
			this.userId = this.regService.item.user.id;
		}

		ngOnInit() {
		}

		onSubmit(form : NgForm) {
			if(form.value.userPoints == 0)
				this.toastr.error("Entered point must be greater than 0");
			else if(parseInt(form.value.userPoints) > this.userTotalPoints)
				this.toastr.error("Entered point is greater than point in your account");
			else {
				this.regService.convertPoints(form.value.userPoints).subscribe((res:any) => {
					if(res.status == '1') {
						this.toastr.success(res.results);
						window.location.reload();
					} else if(res.status == '0') {
						this.toastr.error(res.results);
					}
				});
			}
		}

}
