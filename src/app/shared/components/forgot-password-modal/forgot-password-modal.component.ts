import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { RegistrationService } from '../../../services/registration.service';
// import { Router } from '@angular/router';
import { ContentService } from '../../../services/content.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm }   from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-forgot-password-modal',
	templateUrl: './forgot-password-modal.component.html',
	styleUrls: ['./forgot-password-modal.component.scss']
})
export class ForgotPasswordModalComponent implements OnInit {

	title: string;
	closeBtnName: string;
	list: any[] = [];
	userId : number;
	inputPassword:any;
	inputConfirmPassword:any;
	inputEmail:any;
	code:any;
	color:any;

	constructor(
	public bsModalRef: BsModalRef,
	private content: ContentService,
	public toastr : ToastrService,
	public spinner : NgxSpinnerService,
	) {
	}

	ngOnInit() {
		
	}


	onSubmit(form: NgForm){
		this.spinner.show();
		this.content.submitPassword(this.inputEmail).subscribe((data:any) => {
			this.code = data.results;
			this.color = data.color;

			setTimeout(() => {
			        /** spinner ends after 3 seconds */
			        this.spinner.hide();
			    }, 2000);
		});
	}

}


