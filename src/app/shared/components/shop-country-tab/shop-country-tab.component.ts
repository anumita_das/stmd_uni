import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { ContentService } from '../../../services/content.service';
import { RegistrationService } from '../../../services/registration.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-shop-country-tab',
	templateUrl: './shop-country-tab.component.html',
	styleUrls: ['./shop-country-tab.component.scss']
})
export class ShopCountryTabComponent implements OnInit {
	shopCategories: Array<any>;
	shopItems: Array<any>;
	shopItems2: Array<any>;
	showShop: boolean;
	showPopUp: boolean;
	userId : number;
	isLoggedIn : boolean;
	resultMsg : any;
	categoryName : any;

	public storeList : any;


	constructor(
		private router: Router,
		private renderer: Renderer2,
		private content: ContentService,
		private regService : RegistrationService,
		public spinner: NgxSpinnerService,

	) { 
		if(this.regService.item != null)
			{
				this.userId = this.regService.item.user.id;
				this.isLoggedIn = true;
			}else{
				this.isLoggedIn = false;
			}
	}

	ngOnInit() {

		this.spinner.show();

		setTimeout(() => {
			this.getCategoryList();
			this.spinner.hide();
		}, 6000);

		

		this.showShop = false;

		

		if(this.userId)
		{
			this.showPopUp = true;
		}else{
			this.showPopUp = false;
		}

		

		
	}

	//Fetch Category List
	getCategoryList(){
		this.content.categorylist('shopforme').subscribe((data:any) => {
			this.shopCategories = data;
		});
	}

	getStoresByCategory(catId)
	{
		//this.spinner.show();

		const formdata = {type : 'shopforme', categoryId : catId};
		this.content.storeByCategory(formdata).subscribe((data:any) => {
			//this.spinner.hide();
			
			if(data.status == 1)
			{
				this.showShop = true;
				this.shopItems = data.result;
				this.resultMsg = "";
				this.categoryName = "";
			}
			else if(data.status == 2)
			{				
				this.showShop = true;
				this.resultMsg = "No shop found in this category";
				this.categoryName = data.result;
			}
			else{
				
				this.showShop = false;

			}
			
		});
	
	}

	gotoOrderForm(storeId, categoryId, storeUrl='', shopDirect)
	{
		
		const warehoseId = $('#warehouseId').val();

		 let myObj = {storeId : storeId, categoryId : categoryId, warehouseId : warehoseId, warehouseCode : $('#warehouseCode').val()};

		 localStorage.setItem('storeShopForMe', JSON.stringify(myObj));

		 if(this.userId && shopDirect == "N")
		 {
		 	this.router.navigate(['shop-for-me','order-form']);
		 }
		 else{
		 	window.open(storeUrl, "_blank");
		 }

	}

	onShown() {
		console.log('Popover Shown');
		this.renderer.addClass(document.body, 'popover-open');
	}

	onHidden() {
		console.log('Popover Hidden');
		this.renderer.removeClass(document.body, 'popover-open');
	}

	changeShopCategorie() {

	}
}
