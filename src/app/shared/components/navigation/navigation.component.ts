import { Component, OnInit } from '@angular/core';
import { ContentService } from '../../../services/content.service';
import { Router, ActivatedRoute, Params, ParamMap } from '@angular/router';

@Component({
	selector: 'app-navigation',
	templateUrl: './navigation.component.html',
	styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
	// private _opened = false;
	isCollapsed = true;
	headerNav : any;
	activeClass:any;
	baseurl:any;
	constructor(
	public content : ContentService,
	private route: ActivatedRoute,
	) { }

	ngOnInit() {

	this.route.params.subscribe(params => {
		this.activeClass = params.page;
    });



	this.content.getHeaderNavigation().subscribe((data:any) => {
			
			this.headerNav = data.headermNav;
			this.activeClass = this.activeClass;
			this.baseurl = data.baseurl;
		});

	}

	ngAfterViewInit() {
		this.route.fragment.subscribe((fragment: string) => {
			if(fragment){
	       	  this.scroll(fragment);
			}
	    });
	}

	// private _toggleSidebar() {
	// 	this._opened = !this._opened;
	// }

	scroll(id) {
	  let el = document.getElementById(id);
	  el.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});
	}

	isMediumScreen() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		if (width >= 992) {
			return true;
		} else {
			return false;
		}
	}
}
