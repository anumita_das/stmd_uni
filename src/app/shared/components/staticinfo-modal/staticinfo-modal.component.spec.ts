import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaticinfoModalComponent } from './staticinfo-modal.component';

describe('StaticinfoModalComponent', () => {
	let component: StaticinfoModalComponent;
	let fixture: ComponentFixture<StaticinfoModalComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [StaticinfoModalComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(StaticinfoModalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
