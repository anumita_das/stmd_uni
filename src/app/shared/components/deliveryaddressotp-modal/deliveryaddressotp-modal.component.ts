import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm }   from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { RegistrationService } from '../../../services/registration.service';
import { ContentService } from '../../../services/content.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-deliveryaddressotp-modal',
	templateUrl: './deliveryaddressotp-modal.component.html',
	styleUrls: ['./deliveryaddressotp-modal.component.scss']
})
export class DeliveryaddressotpModalComponent implements OnInit {

	userId : number;
	otp:any;
	otpcode:any;
	adressbookId:any;
	formValue:any;
	addressType:any;
	postAction:any;
	isdCodes:any;
	setisdefault:any;
	userShippingAddress : Array<object>;
	countdown:any;
	resendLinkEnabled:any;

	constructor(
		public regService : RegistrationService,
		public bsModalRef: BsModalRef,
		public toastr : ToastrService,
		private content: ContentService,
		public spinner: NgxSpinnerService,
		) {
			this.userId = this.regService.item.user.id;
		}

	ngOnInit() {
		
	}

	onSubmit(){
	}

	getUserAddressInfo() {
		this.regService.userShippingAddress('All').subscribe((data:any) =>{
			if(data.status == 1)
			{
				this.userShippingAddress = data.results;
			}
			//this.spinner.hide();
		});
	}

	submitForm(){
		/*this.content.getWalletCode(this.inputPassword, this.userId).subscribe((data:any) => { 

			if(data.countData == 1){
				//this.bsModalRef.hide();
				this.code = "Your e-Wallet code is: "+data.results.walletCode;
			} else {
				this.code = 'Wrong password entered';
			}

		});*/
		this.spinner.show();
		this.regService.checkotpforshippingaddress(this.userId, this.otp).subscribe((data:any) => {
			if(data.status == '1'){
				//this.bsModalRef.hide();
				if(this.setisdefault == ''){
					this.regService.saveUserShippingAddress(this.formValue,this.adressbookId,this.addressType,this.postAction,this.isdCodes).subscribe((data:any) => {
					this.spinner.hide();
					if(data.status == '1')
					{
						//this.formSubmitted = false;

						let userData = JSON.parse(localStorage.getItem('tokens'));
						userData.user.unit = data.results;
						localStorage.setItem('tokens', JSON.stringify(userData));

						this.bsModalRef.hide();
						this.toastr.success('Address updated successfully');
						setInterval(()=>{
						      location.reload();
						},3000);
						//location.reload();
						
					} 
					/*else if(data.status == '-1')
					{
						this.formError = true;
					}*/

				});

				}

				if(this.setisdefault == 'default'){
					this.regService.setDefaultShippingAddress(this.adressbookId, this.addressType).subscribe((data:any) => {
						this.spinner.hide();
						this.adressbookId = '';
						if(data.status==1)
						{
							this.getUserAddressInfo();
							this.toastr.success('Selected address modified to default '+this.addressType+' address');
							setInterval(()=>{
						      location.reload();
							},3000);
						}
						else if(data.status == '-1')
						{
							this.toastr.error(data.results);
						}
						else
						{
							this.toastr.error('Something went wrong');
						}
					});
				}


			} else {
						this.spinner.hide();
						this.toastr.error('Wrong OTP entered. Please submit correct OTP!');
					}
		});

	}

	resendOtp(){
		this.regService.reSendOtpForModifyShippingAddress(this.userId, this.adressbookId).subscribe((data:any) => {
			console.log("mail fired");
		});

		this.resendLinkEnabled = false;
		var counter = 10;
		var newYearCountdown = setInterval(() => {
		  this.countdown = counter;
		  counter--;
		  if (counter === 0) {
		    clearInterval(newYearCountdown);
		    this.resendLinkEnabled = true;
		  }
		}, 1000);
	}
}
