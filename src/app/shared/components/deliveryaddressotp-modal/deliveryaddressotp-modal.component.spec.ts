import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryaddressotpModalComponent } from './deliveryaddressotp-modal.component';

describe('DeliveryaddressotpModalComponent', () => {
	let component: DeliveryaddressotpModalComponent;
	let fixture: ComponentFixture<DeliveryaddressotpModalComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [DeliveryaddressotpModalComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DeliveryaddressotpModalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
