import { Component, Renderer2, OnInit, Inject } from '@angular/core';
import { ContentService } from '../../../services/content.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RegistrationService } from '../../../services/registration.service';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import { DOCUMENT } from '@angular/common';

@Component({
	selector: 'app-footer',
	templateUrl: './footer.component.html',
	styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
	socialIcons: Array<object> = [];
	cards: Array<object>;
	footerNav : any;
	pageType : any = '';
	date : any;
	now : any;
	diffMs : any;
	diffMins :any;
	diff : any;
	fraudImage: any;
	fraudLink: any;
	fullYear: any;
	htmlScript: any;
	showhideMenu: boolean= true;

	constructor(
		private router: Router,
		public content : ContentService,
		private route: ActivatedRoute,
		private regService : RegistrationService,
		private sanitizer: DomSanitizer,
		private _renderer2: Renderer2, 
        @Inject(DOCUMENT) private _document: Document
	) { }

	ngOnInit() {
		this.content.getFooterNavigation().subscribe((data:any) => {
			//console.log(data.footermNav);
			this.footerNav = data.footermNav;
		});

		this.fraudImage="assets/imgs/zero1.png";
		this.getSocialLinks();
		this.cards = [
			{ img: 'assets/imgs/icons/visa.png', },
			{ img: 'assets/imgs/icons/mastercard.png', },
			{ img: 'assets/imgs/icons/paypal.png', },
			{ img: 'assets/imgs/icons/paystack.png', }
		];

		//fetch page type
		this.route.params.subscribe((params)=> {
		  //check lead Id here
		  if(params['page']){
		   this.content.getpagetype(params['page']).subscribe((data:any) => {
                        if(data != null)
                            this.pageType = data.pageType;
                    });
		  }
		});
		if(this.regService.userId != ''){
			this.initListenerFooter();
			this.reset();
		}

		if (this.router.url === '/join2' || this.router.url === '/success')
		{
			
			this.pageType = 2;
			this.showhideMenu = false;
			
		}

		this.fullYear = (new Date()).getFullYear();
		this.htmlScript = this.sanitizer.bypassSecurityTrustHtml("<script type='text/javascript' src='https://seal.geotrust.com/getgeotrustsslseal?host_name=www.shoptomydoor.com&size=M&lang=en'></script><br /><a href='http://www.geotrust.com/ssl/' target='_blank'  style='color:#000000; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;'></a>");

		//this.getfraudbanner();
	}

	getfraudbanner()
	{
		this.content.fraudBanner().subscribe((data:any) => {
			if(data.status == 1)
			{
				this.fraudImage = data.results.bannerImage;
				this.fraudLink = data.results.pageLink;
			}
			
		})

	}




	initListenerFooter() {
	    document.body.addEventListener('click', () => this.reset());
	    //document.body.addEventListener('mouseover',()=> this.reset());
	    //document.body.addEventListener('mouseout',() => this.reset());
	    //document.body.addEventListener('keydown',() => this.reset());
	    //document.body.addEventListener('keyup',() => this.reset());
	    document.body.addEventListener('keypress',() => this.reset());
  	}

  	reset() {
	    console.log("firing reset function from footer component");

	    

	    	/* SET TIME AT LOCAL STORAGE TO DIFFERNCIATE 2 MINUTES TO CALL THE API AGAIN */
	    	if (localStorage.getItem("checkloginstatustime") === null) {
		    	this.date = new Date();
		    	localStorage.setItem("checkloginstatustime", this.date);
		    }

		    /* SEE THE TIME DIFFERENCE */
		    this.now = new Date();
		    //this.diffMs = (this.now - this.date); // milliseconds 
		    //this.diffMins = Math.round(((this.diffMs % 86400000) % 3600000) / 60000); // minutes

		    this.diffMs = new Date(this.now).getTime() - new Date(localStorage.getItem("checkloginstatustime")).getTime();
		    this.diffMins = Math.round(((this.diffMs % 86400000) % 3600000) / 60000); // minutes
		    if(this.diffMins >= 2){

		    	this.regService.loginstatuscheck(this.regService.userId).subscribe((data:any) => {

			    	if(data.msg == 'null'){
			    		/* Purge */
			    		localStorage.clear();
						this.regService.isLoggedIn = false;
						location.reload();
			    	}
			    	this.date = new Date();
			    	localStorage.setItem("checkloginstatustime", this.date);
		    	});
		    }
	    

	    //this.payPalIPN('', '');
	}

 



	getSocialLinks() {

		this.content.getFooterSocialLinks().subscribe((data:any) => {
			if(data.status == '1')
			{
				for(let eachlink of data.results)
				{
					let socialArr = [];
					socialArr['link'] = eachlink.mediaLink;
					socialArr['icon'] = eachlink.faclass;
					if(eachlink.media == 'fb')
					{
						socialArr['bg'] = '#3b5998'; 
					}
					if(eachlink.media == 'youtube')
					{
						socialArr['bg'] = '#e32607';
					}
					if(eachlink.media == 'twit')
					{
						socialArr['bg'] = '#23bbf3';
					}
					if(eachlink.media == 'linked')
					{
						socialArr['bg'] = '#0274b3'; 
					}
					if(eachlink.media == 'instagram')
					{
						socialArr['bg'] = '#ff6b00';
					}

					this.socialIcons.push(socialArr);
				}
			}


		});
	}
}
