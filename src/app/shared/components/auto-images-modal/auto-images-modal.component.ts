import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AutoService } from '../../services/auto.service';
import { RegistrationService } from '../../../services/registration.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
	selector: 'app-auto-images-modal',
	templateUrl: './auto-images-modal.component.html',
	styleUrls: ['./auto-images-modal.component.scss']
})
export class AutoImagesModalComponent implements OnInit {

	title: string;
	closeBtnName: string;
	list: any[] = [];
	type : string = 'buy_a_car';
	imageId : any;
	imageDetails : any;

	constructor(
		public bsModalRef: BsModalRef,
		public auto : AutoService,
		public regService : RegistrationService,
		public router : Router,
		public toastr : ToastrService,
	) { }

	ngOnInit() {
		this.getImageDetails();
	}

	getImageDetails() {
		
		var imageId = JSON.parse(localStorage.getItem('autowarehouseimageid'));
		console.log(imageId);
		this.auto.getWarehouseImage(imageId).subscribe((data:any) => {
			this.imageDetails = data.results;
		});
	}
}
