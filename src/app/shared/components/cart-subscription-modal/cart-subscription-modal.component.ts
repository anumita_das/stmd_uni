import { Component, OnInit, HostListener } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { SubscribeService } from '@app/shared/services/subscribe.service';
import { ServiceModalComponent } from '@app/shared/components/service-modal/service-modal.component';
import { AppGlobals } from '@app/app.globals';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cart-subscription-modal',
  templateUrl: './cart-subscription-modal.component.html',
  styleUrls: ['./cart-subscription-modal.component.scss'],
  providers: [AppGlobals]
})
export class CartSubscriptionModalComponent implements OnInit {
	isFirstOpen: boolean;
	oneAtATime: boolean;
	dtOptions: DataTables.Settings = {};
	show: boolean;
	isMediumScreen: boolean;
	featureList: any;
    accountFeatureList : any;
    featuresdata : any;
    baseAmount: any;
    settings: any;

  constructor(
		private modalService: BsModalService,
		public spinner: NgxSpinnerService,
		public router: Router,
		public route: ActivatedRoute,
		private subscribeService: SubscribeService,
		public bsModalRef: BsModalRef,
  ) { }

  ngOnInit() {
  	this.spinner.hide();

  	// accordion
		this.isFirstOpen = true;
		this.oneAtATime = true;

		// datatable
		this.dtOptions = {
			paging: false,
			// bInfo: false,
			ordering: false,
			searching: false,
			// bSort: false,
			autoWidth: false,
			dom: '<"table-responsive"t>'
		};

		this.getDetails();

  }

  getDetails() {
		this.subscribeService.getsubscriptiondetails().subscribe((response:any) => {
            if(response.features){
                this.featuresdata = response.features.account;				
                this.featureList = response.features.general;		
            }
            if(response.settings) {
                this.settings = response.settings;
            }  
            this.baseAmount = response.baseSubscriptionAmount;
		});
	}

  checkWindowSize(width) {
		if (width >= 992) {
			this.isMediumScreen = false;
		} else {
			this.isMediumScreen = true;
		}
	}

	@HostListener('window: resize', ['$event'])
	onResize(event) {
		this.checkWindowSize(event.target.innerWidth);
	}

  	isLargeScreen() {
		const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		if (width > 992) {
			return true;
		} else {
			return false;
		}
	}
	showHideFeature() {
		this.accountFeatureList = this.featuresdata;
           
	}
	paytosubscribe(selectedPlan,baseAmount){
			this.subscribeService.sendsubscriptionplan(selectedPlan, baseAmount);
            this.bsModalRef.hide();
        }
    showSubscribtion()
    {
    	this.bsModalRef.hide();
    }

}
