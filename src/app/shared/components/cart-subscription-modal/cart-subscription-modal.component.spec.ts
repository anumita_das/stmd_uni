import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartSubscriptionModalComponent } from './cart-subscription-modal.component';

describe('CartSubscriptionModalComponent', () => {
  let component: CartSubscriptionModalComponent;
  let fixture: ComponentFixture<CartSubscriptionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartSubscriptionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartSubscriptionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
