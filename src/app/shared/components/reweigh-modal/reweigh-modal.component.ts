import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { MywarehouseService } from '../../../services/mywarehouse.service';
import { RegistrationService } from '../../../services/registration.service';

@Component({
	selector: 'app-reweigh-modal',
	templateUrl: './reweigh-modal.component.html',
	styleUrls: ['./reweigh-modal.component.scss']
})
export class ReweighModalComponent implements OnInit {

	title: string;
	closeBtnName: string;
	deliveryId: any;
	shipmentId: any;
	email: any;
	isReweighDisabled: any = 'N';
	shipment: any;
	reweighCost : any = '';

	constructor(
		public bsModalRef: BsModalRef,
		public mywarehouseService : MywarehouseService,
		public regService : RegistrationService,
	) {
		this.email = this.regService.item.user.email;
	}


	ngOnInit() {
		
	}

	addDeliveryReweigh(deliveryId, shipmentId){
		const formData = {deliveryId : deliveryId, shipmentId : shipmentId, email : this.email, type : 'reweigh'};
		this.mywarehouseService.adddeliveryothercharges(formData).subscribe((data:any) => {
			this.isReweighDisabled = 'Y';
			this.shipment = {
				otherChargeAmount : data.results.otherChargeAmount,
				totalCost : data.results.totalCost,
			};
			this.bsModalRef.hide();
		});
	}


}
