import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm } from '@angular/forms';
import { ContentService } from '../../../services/content.service';
import { RegistrationService } from '../../../services/registration.service';
import { UserAddress } from '../../../shared/models/useraddress.model';
import { ToastrService } from 'ngx-toastr';
import { DeliveryaddressotpModalComponent } from '../deliveryaddressotp-modal/deliveryaddressotp-modal.component';
import { ShipmentSettingsModalComponent } from '../shipment-settings/shipmentsettings-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
	selector: 'app-addressbook-modal',
	templateUrl: './addressbook-modal.component.html',
	styleUrls: ['./addressbook-modal.component.scss']
})
export class AddressbookModalComponent implements OnInit {
	title: string;
	closeBtnName: string;
	list: any[] = [];
	countryList : any;
	stateList:any;
	cityList:any;
	UserAddress = new UserAddress();
	formError : boolean;
	formSubmitted : boolean;
	adressbookId = 0;
	addressType: string;
	stmdAddress : boolean = false;
	userAddressSelection : boolean = true;
	locationDetails : any = '';
	locationArr : any = [];
	locationIdMapping : any = [];
	locationValue = "";
	postAction : string = "";
	phcodeList : any =[];
	selectedItem : string = "";
	phcodeVal : any;
	altphcodeVal : any;
	isReadOnly : boolean = true;
	countryIsdCodes : any;
	userId : number;
	resendLinkEnabled:any = false;
	constructor(
		public bsModalRef: BsModalRef,
		private content:ContentService,
		private user:RegistrationService,
		public toastr : ToastrService,
		private modalService: BsModalService,
		public spinner : NgxSpinnerService,
		) { this.userId = this.user.item.user.id; }

	ngOnInit() {
		this.getAllCountry();
		this.getAllCode();
		this.formError = false;
		this.formSubmitted = false;
		this.UserAddress.title = this.user.item.user.title;
		this.UserAddress.firstName = this.user.item.user.firstName;
		this.UserAddress.lastName = this.user.item.user.lastName;
		this.UserAddress.email = this.user.item.user.email;
		this.UserAddress.country = '';
		this.UserAddress.state = '';
		this.UserAddress.city = '';
		//this.phcodeVal = "234";
		//this.altphcodeVal = "234";
		this.phcodeVal = '';
		this.altphcodeVal = '';
		this.UserAddress.unit = this.user.item.user.unit;
	}

	onSubmit(form: NgForm){

		this.formSubmitted = true;
		if(form.value.locationId!='')
		{
			form.value.locationId = this.locationIdMapping[form.value.locationId];
		}
		const isdCodes = {
			phCode : form.value.phcode,
			altphCode : form.value.altphcode
		}

			this.spinner.show();
			this.user.sendOtpForModifyShippingAddress(this.userId, this.adressbookId, 'yes').subscribe((data:any) => {
				this.spinner.show();
				if(data.status == '1'){
					this.spinner.hide();
					this.bsModalRef = this.modalService.show(DeliveryaddressotpModalComponent, {});
					this.bsModalRef.content.closeBtnName = 'Close';
					this.bsModalRef.content.formValue = form.value;
					this.bsModalRef.content.adressbookId = this.adressbookId;
					this.bsModalRef.content.addressType = this.addressType;
					this.bsModalRef.content.postAction = this.postAction;
					this.bsModalRef.content.isdCodes = isdCodes;
					this.bsModalRef.content.setisdefault = '';
					setTimeout(() => {
				      // simulating API call, sets to true after 5 seconds
				      this.resendLinkEnabled = true;
				      this.bsModalRef.content.resendLinkEnabled = this.resendLinkEnabled;
				   }, 11000);

					var counter = 10;
					var newYearCountdown = setInterval(() => {
					  //console.log(counter);
					  this.bsModalRef.content.countdown = counter;
					  counter--;
					  if (counter === 0) {
					    clearInterval(newYearCountdown);
					  }
					}, 1000);

					} else if(data.status == '-1'){
						this.user.saveUserShippingAddress(form.value,this.adressbookId,this.addressType,this.postAction,isdCodes).subscribe((data:any) => {
			
							if(data.status == '1')
							{
								this.formSubmitted = false;

								let userData = JSON.parse(localStorage.getItem('tokens'));
								userData.user.unit = data.results;
								localStorage.setItem('tokens', JSON.stringify(userData));
								this.toastr.success('Address updated successfully');
								this.bsModalRef.hide();
								if(data.olduser == 0)
								{
									this.bsModalRef = this.modalService.show(ShipmentSettingsModalComponent, {
										class: 'modal-lg',
										backdrop  : 'static',
		   								keyboard  : false
									});
									this.bsModalRef.content.closeBtnName = 'Close';
									this.bsModalRef.content.formValue = form.value;
								}else{
									setTimeout(() => {
										  window.location.reload();
										}, 2000);
								}
							}
							else if(data.status == '-1')
							{
								this.formError = true;
							}

						});
					}
					else {
						console.log("error generating otp code!");
						this.toastr.error('error generating otp code!');
					}
			});

		/*this.user.saveUserShippingAddress(form.value,this.adressbookId,this.addressType,this.postAction,isdCodes).subscribe((data:any) => {
			if(data.status == '1')
			{
				this.formSubmitted = false;

				let userData = JSON.parse(localStorage.getItem('tokens'));
				userData.user.unit = data.results;
				localStorage.setItem('tokens', JSON.stringify(userData));

				//this.bsModalRef.hide();

				//location.reload();

				this.toastr.success('Address updated successfully');

				this.bsModalRef = this.modalService.show(ShipmentSettingsModalComponent, {});
				this.bsModalRef.content.closeBtnName = 'Close';
				this.bsModalRef.content.formValue = form.value;
				
			}
			else if(data.status == '-1')
			{
				this.formError = true;
			}

		});*/
	}

	getAllCode()
	{
		this.content.allCode().subscribe((data:any)=>{
			this.phcodeList = data;
			this.selectedItem = data[0]['image']+data[0]['name']+data[0]['isdCode'];
		});
	}

	getAllCountry(){
	 let backindex = localStorage.getItem('backIndex');
		if(backindex == "modify")
		{
			this.content.allCountry().subscribe((data:any)=>{
			let arr = [];
			this.countryList = data;
			for(let eachCountry of data)
			{
				arr[eachCountry.id] = eachCountry.isdCode;
			}
			this.countryIsdCodes = arr;
			});
		}else{
			this.content.allOthersCountry().subscribe((data:any)=>{
			let arr = [];
			this.countryList = data;
			for(let eachCountry of data)
			{
				arr[eachCountry.id] = eachCountry.isdCode;
			}
			this.countryIsdCodes = arr;
			});
		}
		
	}

	

	getStateCity(event){

		let selectedCountry = event.target.value;
		this.phcodeVal = this.countryIsdCodes[selectedCountry];
		this.stateList = [];
		this.cityList = [];
		this.content.stateCityOfCountry(selectedCountry).subscribe((data:any) => {
			//console.log(data);
			if(data.stateList)
			{
				this.stateList = data.stateList;
				
			}
			if(data.cityList)
			{
				this.cityList = data.cityList;
			}

			this.UserAddress.state = "";
			this.UserAddress.city = "";
		});
	}

	getLocationStateCity(event){
		let selectedCountry = event.target.value;
		this.locationValue = "";
		this.locationDetails = "";
		this.locationArr =[];
		this.locationIdMapping =[];
		this.stateList = [];
		this.cityList = [];
		this.resetLocationAutofill();
		this.content.getLocationState(selectedCountry).subscribe((data:any) => {
			if(data.stateList)
			{
				this.stateList = data.stateList;
			}
			if(data.cityList)
			{
				this.cityList = data.cityList;
			}
			this.UserAddress.state = "";
			this.UserAddress.city = ""; 
		});
	}

	getCity(event) {
		let selectedState = event.target.value;
		this.cityList = [];
		this.UserAddress.city = "";

		if(selectedState != ''){
			this.content.cityOfStates(selectedState).subscribe((data:any) => {
				if(data.cityList)
				{
					this.cityList = data.cityList;
				}
				});
		}
		
	}

	getLocationCity(event) {
		let selectedState = event.target.value;
		this.cityList = [];
		this.UserAddress.city = "";

		if(selectedState != ''){
			this.content.getLocationCity(selectedState).subscribe((data:any) => {
				if(data.cityList)
				{
					this.cityList = data.cityList;
				}
				});
		} else {
			this.content.getLocationState(this.UserAddress.country).subscribe((data:any) => {
				if(data.cityList)
				{
					this.cityList = data.cityList;
				}
			});
		}
		
	}

	resetLocationAutofill() {
		this.UserAddress.email = '';
		this.UserAddress.address = '';
		this.UserAddress.alternateAddress = '';
		this.UserAddress.zipcode = '';
		this.UserAddress.phone = '';
		this.UserAddress.alternatePhone = '';
	}

	setDeliveryAddress(addressType) {

		this.UserAddress = {
			userid : '',
			title : this.user.item.user.title,
			firstName : this.user.item.user.firstName,
			lastName : this.user.item.user.lastName,
			email : this.user.item.user.email,
			address : '',
			alternateAddress : '',
			country : '',
			state : '',
			city : '',
			zipcode : '',
			phone : '',
			alternatePhone : '',
			addressType : '',
			unit:'',
		};
		this.phcodeVal = "234";
		this.altphcodeVal = "234";
		if(addressType == 'userAddress')
		{
			this.userAddressSelection = true;
			this.stmdAddress = false;
			this.formSubmitted = false;
			this.getAllCountry();
		}
		else
		{
			this.UserAddress.email = '';
			this.userAddressSelection = false;
			this.stmdAddress = true;
			this.formSubmitted = true;
			this.getLocationCountry();
		}
	}

	getLocationCountry() {
		this.content.getLocationCountry().subscribe((data:any) => {
			this.countryList = data;
		});
	}

	getLocations(form : NgForm) {
		this.resetLocationAutofill();
		this.content.getLocationDetails(form.value).subscribe((data:any) => {
			if(data.status == '1')
			{
				this.locationDetails = data.results;
				this.locationArr =[];
				this.locationIdMapping =[];
				for(let eachLocation of data.results)
				{
					//console.log(eachLocation);
					this.locationArr.push(eachLocation.businessName+' - '+eachLocation.address);
					this.locationIdMapping.push(eachLocation.id);
				}
				
			}
			else if(data.status == '-1')
			{
				this.toastr.error(data.results);
			}

		});
	}

	populateLocationValues(event) {
		this.resetLocationAutofill();
		if(event.target.value !='')
		{
			let selectedLocationIndex = event.target.value;
			let selectedLocationDetails =  this.locationDetails[selectedLocationIndex];
			//console.log(selectedLocationDetails);
			this.formSubmitted = false;
			this.UserAddress = {
				userid : this.user.userId,
				unit: this.user.item.user.unit,
				title : this.user.item.user.title,
				firstName : this.user.item.user.firstName,
				lastName : this.user.item.user.lastName,
				email : selectedLocationDetails.email,
				address : selectedLocationDetails.address,
				alternateAddress : selectedLocationDetails.alternateAddress,
				country : selectedLocationDetails.countryId,
				state : selectedLocationDetails.stateId,
				city : selectedLocationDetails.cityId,
				zipcode : selectedLocationDetails.zipcode,
				phone : selectedLocationDetails.phone,
				alternatePhone : selectedLocationDetails.alternatePhone,
				addressType : '',
			};
			this.phcodeVal = selectedLocationDetails.isdCode;
			this.altphcodeVal = selectedLocationDetails.altIsdCode;
		}
	}

	setaction(action) {
		this.postAction = action;
	}


}
