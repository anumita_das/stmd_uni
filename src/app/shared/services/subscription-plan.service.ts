import { Injectable } from '@angular/core';

@Injectable()
export class SubscriptionPlanService {
	featureList = [
		{
			feature: 'Free US A and UK address to shop from thousands of stores',
			des: 'Free US A and UK address to shop from thousands of storesFree US A and UK address to shop from thousands of stores',
			one_time_shop: true,
			subscription_and_save: true,
		},
		{
			feature: 'Exclusive discount on all shipping',
			des: 'Exclusive discount on all shippingExclusive discount on all shipping',
			one_time_shop: false,
			subscription_and_save: true,
		},
		{
			feature: 'Dedicated Account Via',
			des: 'Dedicated Account ViaDedicated Account Via',
			one_time_shop: false,
			subscription_and_save: true,
		},
		{
			feature: 'Free receipt o f your first package shipment',
			des: 'Free receipt o f your first package shipmentFree receipt o f your first package shipment',
			one_time_shop: false,
			subscription_and_save: true,
		},
		{
			feature: 'Free US A and UK address to shop from thousands of stores',
			des: 'Free US A and UK address to shop from thousands of storesFree US A and UK address to shop from thousands of stores',
			one_time_shop: true,
			subscription_and_save: true,
		},
		{
			feature: 'Exclusive discount on all shipping',
			des: 'Exclusive discount on all shippingExclusive discount on all shipping',
			one_time_shop: true,
			subscription_and_save: true,
		},
		{
			feature: 'Dedicated Account Via',
			des: 'Dedicated Account ViaDedicated Account Via',
			one_time_shop: true,
			subscription_and_save: true,
		},
		{
			feature: 'Free receipt o f your first package shipment',
			des: 'Free receipt o f your first package shipmentFree receipt o f your first package shipment',
			one_time_shop: false,
			subscription_and_save: true,
		}

	];

	getData(num?: number) {
		console.log('bbb', this.featureList);
		if (num) {
			const arr = JSON.parse(JSON.stringify(this.featureList));
			arr.length = num;
			return arr;
		}
		return this.featureList;
	}
}
