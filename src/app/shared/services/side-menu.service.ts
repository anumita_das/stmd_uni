import { Injectable } from '@angular/core';

@Injectable()
export class SideMenuService {
	sideNavList = [
		{ name: 'My Shipments', icon: 'shop-online.svg', link: '/my-warehouse/my-shipments' },
		/*{ name: 'My Group', icon: 'group_icon.svg', link: 'my-group/group-info' , children:[				
				{ name: 'Group Info', link: 'my-group/group-info', subChildren: [] },
				{ name: 'Group Shipments', link: 'my-group/my-shipments', subChildren: [] },
				{ name: 'Group Shipment Order History', link: 'my-group/order-history', subChildren: [] },
				
			] 
		},
		{ name: 'Store to Shop From', icon: 'shop-for-me.svg', link: '/store-shop-from'},
		{
			name: 'Shop For Me', icon: 'shop-for-me.svg', link: 'shop-for-me', children: [
				{ name: 'How It Works', link: 'shop-for-me/how-it-work', subChildren: [] },
				{ name: 'Order Form', link: 'shop-for-me/order-form', subChildren: [] },
				{ name: 'My Saved Orders', link: 'shop-for-me/my-wishlist', subChildren: [] },
				{ name: 'Order History', link: 'shop-for-me/order-history', subChildren: [] }
			]
		},
		{
			name: 'Fill & Ship', icon: 'fill-ship.svg', link: 'fill-and-ship', children: [
				{ name: 'My Fill and Ship', link: 'fill-and-ship/overview', subChildren: [] },
				{ name: 'New Fill and Ship', link: 'fill-and-ship/order-form', subChildren: [] }
			]
		},*/
		{
			name: 'Auto', icon: 'auto.svg', link: 'auto', children: [
				{ name: 'Buy A Car For Me', link: 'auto/buy-a-car-for-me', subChildren: [] },
				{ name: 'Ship My Car', link: 'auto/ship-my-car', subChildren: [] },
				{ name: 'Car I Have Bought', link: 'auto/car-i-have-bought', subChildren: [] },

			]
		},
		/*{
			name: 'Auto Parts', icon: 'auto.svg', link: '/auto-parts', children: [
				{ name: 'How It Works', link: 'auto-parts/how-it-work', subChildren: [] },
				{ name: 'Order Form', link: 'auto-parts/order-form', subChildren: [] },
				{ name: 'My Saved Orders', link: 'auto-parts/my-wishlist', subChildren: [] },
				{ name: 'Order History', link: 'auto-parts/order-history', subChildren: [] }
			]
		},*/
		{ name: 'Payment', icon: 'payment.svg', link: '/payment' },
		{
			name: 'My Account', icon: 'user.svg', link: '', children: [
				{ name: 'Modify Profile', link: 'modify-account', subChildren: [] },
				{ name: 'Address Book', link: 'addressbook', subChildren: [] },
				{ name: 'Order History', link: 'my-account/order-history', subChildren: [] },
				{ name: 'My Documents', link: 'my-account/my-documents', subChildren: [] },
				{ name: 'My Messages', link: 'my-account/message', subChildren: [] },
				{ name: 'My Images', link: 'my-account/gallery', subChildren: [] },
				{ name: 'My Reward Details', link: 'my-account/reward', subChildren: [] },
				{ name: 'File a claim', link: 'my-account/file-claim', subChildren: [] },
				{ name: 'Manage Setting', link: 'shipment-setting', subChildren: [] },

			]
		},
		{ name: 'Refer A Friend', icon: 'rewards.svg', link: '/refer-a-friend' },
		{ name: 'Help', icon: 'help.svg', link: '/help/get-started/content' },
	];

	getData() {
		return this.sideNavList;
	}
}
