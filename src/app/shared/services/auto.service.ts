import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/toPromise';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { config } from '../../app.config';

@Injectable({
  providedIn: 'root'
})
export class AutoService {
imageData: any;
	private rootUrl = config.api;
	private httpOptions: any;
  	constructor(private http: HttpClient, private sanitizer: DomSanitizer) {

  		this.httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json'})
        };
  	}

  	getAllWebsite() {
  		return this.http.get(this.rootUrl+'getwebsiteist',this.httpOptions);
  	}

  	getWebsiteMake( websiteid : number) {
  		return this.http.get(this.rootUrl+'getwebsitemake/'+websiteid,this.httpOptions);
  	}

  	getWebsiteModel(websiteid : number,makeid : number) {
  		return this.http.get(this.rootUrl+'getwebsitemodel/'+websiteid+'/'+makeid,this.httpOptions);
  	}

  	getProcessingFee(carprice : number, warehouseid:number){
  		return this.http.get(this.rootUrl+'getautoprocessingfee/'+carprice+'/'+warehouseid,this.httpOptions);
  	}

  	getAllChargesBuyacar(formData,warehouseid:number,userId : string) {
  		return this.http.post(this.rootUrl+'getautoallcharges/'+userId+'/'+warehouseid,formData,this.httpOptions);
  	}

    getAllChargesShipmycar(formData,warehouseid:number,userId:string) {
      const data = {
          formData : formData,
          warehouseId : warehouseid,
          userId : userId
      };
      return this.http.post(this.rootUrl+'getshipmycarallcharges',data,this.httpOptions);
    }

    getUserCart(userId) {
      const data = {
          userId : userId
      }
      return this.http.post(this.rootUrl+'getuserautocart',data,this.httpOptions);
    }

  	saveOthervehicleData(formPost,fileToUpload: File) {
  		
  		const formData = new FormData();

      	formData.append('fileItem', fileToUpload, fileToUpload.name);
      	formData.append('postData',formPost);

  		return this.http.post(this.rootUrl+'saveothervehicledata',formData);
  	}

  	saveBuycarData(formData,warehouseId,userId) {
  		
  		/*const formData = new FormData();
  		console.log(formPost);
  		if(fileToUpload != null)
      		formData.append('fileItem', fileToUpload, fileToUpload.name);
      	for(let eachField in formPost)
      	{
      		formData.append(eachField,formPost[eachField]);
      		//console.log(formPost[eachField]);
      	}*/

  		return this.http.post(this.rootUrl+'savebuycardata/'+userId+'/'+warehouseId,formData);
  	}

    saveShipMyCar(formData) {

      return this.http.post(this.rootUrl+'saveshipmycar',formData);
    }

    updateUserCartData(formData) {
      return this.http.post(this.rootUrl+'updatecartcurrency',formData);
    }

    updateUserCartContent(formData) {
      return this.http.post(this.rootUrl+'updatecartcontent',formData);
    }

    checkCartData(userId,type) {

      const data = {
        userId : userId,
        type : type
      }
      return this.http.post(this.rootUrl+'checkautocartdata',data,this.httpOptions);
    }

    deleteCartData(userId,type) {

      const data = {
        userId : userId,
        type : type
      }
      return this.http.post(this.rootUrl+'deleteautocartdata',data,this.httpOptions);
    }

    getCarIHaveBoughtDetails(userId, currentPage, perPage) {
      const data = {userId : userId, perPage:perPage, currentPage:currentPage};
      return this.http.post(this.rootUrl+'getcarihaveboughtdetails',data);
    }

    getCarWarehouseDetails(userId, currentPage, perPage) {
      const data = {userId : userId, perPage:perPage, currentPage:currentPage};
      return this.http.post(this.rootUrl+'getcarwarehousedetails',data,this.httpOptions);
    }

    validatecouponcode(formData) {
      return this.http.post(this.rootUrl+'autovalidatecoupon',formData);
    }

    getInvoice(dataId,type,invoiceType,userId) {
      const data = {
        dataId : dataId,
        type : type,
        invoiceType : invoiceType,
        userId : userId
      };
      return this.http.post(this.rootUrl+'autoinvoice',data);
    }

    getLocationType() {
      return this.http.get(this.rootUrl+'getlocationtype',this.httpOptions);
    }

    allCountry() {

      return this.http.get(this.rootUrl+'getcountry/auto/pickup',this.httpOptions);
    }

    allDestinationCountry() {

      return this.http.get(this.rootUrl+'getcountry/auto/destination',this.httpOptions);
    }

    getWarehouseImage(imageId) {
      return this.http.get(this.rootUrl+'getautowarehouseimage/'+imageId,this.httpOptions);
    }

}
