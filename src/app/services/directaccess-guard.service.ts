import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot} from '@angular/router';


@Injectable()
export class DirectAccessGuardService implements CanActivate {
  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.router.url === '/') {
      this.router.navigate(['join']); // Navigate away to some other page
      return false;
    }
    return true;
  }

 
}
