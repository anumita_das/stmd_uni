import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../shared/models/user.model'; 
import { config } from '../app.config';


@Injectable({
  providedIn: 'root'
})
export class ShopformeService {

	private rootUrl = config.api;
	private httpOptions: any;
  isLoggedIn = false;
  redirectUrl = 'my-account';
  item : any;
  
  userId = '';

	constructor(private http: HttpClient) {
    //localStorage.clear();
    this.item = JSON.parse(localStorage.getItem('tokens'));
    //console.log(this.item.user.id);
    if (this.item != null) {
      if (this.item.token) {
        this.isLoggedIn = true;
      }
      this.userId = this.item.user.id;
    }

  	this.httpOptions = {
          headers: new HttpHeaders({ 'Content-Type': 'application/json'})
      };
	}

  getUserCartList(data) {
    return this.http.post(this.rootUrl+'getusercartlist/',data,this.httpOptions)
  }

  updateUserCart(formData,type){
    return this.http.post(this.rootUrl+'updateusercart/'+ this.userId + '/' +type, formData)
  }

  savecartlist(data){
    return this.http.post(this.rootUrl+'savecartlist/'+ this.userId,data,this.httpOptions)
  }

  submitprocurementdata(formData) {
    return this.http.post(this.rootUrl+'submitprocurementdata/'+ this.userId,formData,this.httpOptions)
  }

  //For Auto Parts
  
  submitautopartsdata(formData) {
    return this.http.post(this.rootUrl+'submitautopartsdata/'+ this.userId,formData,this.httpOptions)
  }

  calculateShippingCost(formData) {
     return this.http.post(this.rootUrl+'calculateshippingcost/',formData,this.httpOptions)
  }

  reCalculateCart(formData) {
    return this.http.post(this.rootUrl+'recalculateshipmentcost/',formData,this.httpOptions)
  }

  getSaveForLaterList(data) {
    const formData = {currentPage : data, userId : this.userId};

    return this.http.post(this.rootUrl+'getsaveforlaterlist/',formData,this.httpOptions)
  }

  //To fetch auto parts data of Save for later
  getAutoPartsList(data)
  {
    const formData = {currentPage : data, userId : this.userId};

    return this.http.post(this.rootUrl+'getautopartslist/',formData,this.httpOptions)

  }


  removeCartItem(id) {
    const data = {id:id};

    return this.http.post(this.rootUrl+'removecartitem/',data,this.httpOptions)
  }

  moveToUserCart(data) {
   const formData = {itemIds : data, userId : this.userId};

    return this.http.post(this.rootUrl+'movetousercart/',formData,this.httpOptions)
  }

  setProcurementPaid(cartProcurementId) {
    return this.http.post(this.rootUrl+'processProcurementPaid/'+cartProcurementId,this.httpOptions)
  }

  processProcurementDetails(cartProcurementId) {
    return this.http.post(this.rootUrl+'processProcurementDetails/'+cartProcurementId,this.httpOptions)
  }


    getprocurementshipmentdetails(params, perPage, currentPage) {
       const formData = {orderDate:params.orderDate, rangeDate:params.rangeDate, orderId:params.orderId, status:params.status, currentPage:currentPage, perPage:perPage, userId : this.userId};
       return this.http.post(this.rootUrl+'getProcurementShipmentDetails/',formData,this.httpOptions)
    }

    getprocurementshipmentdetailsPaging(params) {
      const formData = params;
      formData['userId'] = this.userId;
      return this.http.post(this.rootUrl+'getProcurementShipmentDetails/',formData,this.httpOptions)
    }

    getprocurementshipmentforautopartsdetails(params, perPage, currentPage) {
      const formData = {orderDate:params.orderDate, rangeDate:params.rangeDate, orderId:params.orderId, status:params.status, currentPage:currentPage, perPage:perPage, userId : this.userId};
      return this.http.post(this.rootUrl+'getProcurementShipmentForAutoPartsDetails/',formData,this.httpOptions)
    }

    getprocurementshipmentforautopartsdetailsPaging(params) {
    const formData = params;
      formData['userId'] = this.userId;
      return this.http.post(this.rootUrl+'getProcurementShipmentForAutoPartsDetails/',formData,this.httpOptions)
    }



    getURLpagengx(no, params){
    //console.log(url);
    const formData = params;
    //const url = `${this.url}/api/product?page=`+no;
    return this.http.post(this.rootUrl+'getProcurementShipmentDetails?page='+no,formData,this.httpOptions)
    }


  clearUserCart(type) {
    const formData = {userId : this.userId, type: type};
    return this.http.post(this.rootUrl+'clearusercart/',formData,this.httpOptions)
  }

  updateUserCartData(data) {
    const formData = {userId : this.userId, fromCurrency: data.fromCurrency, toCurrency: data.toCurrency, usercart: data.usercart};
    return this.http.post(this.rootUrl+'updateusercartdata/',formData,this.httpOptions)
  }


  checkUserDetail()
  {
    const formData = {userId : this.userId};
    return this.http.post(this.rootUrl+'checkuserdetail/',formData,this.httpOptions);
  }

  getCurrencyList(data) {
     return this.http.post(this.rootUrl+'getcurrencylist/',{currencyCode:data},this.httpOptions);
  }

  getPaymentMethodList(data) {
     return this.http.post(this.rootUrl+'getpaymentmethodlist/',data,this.httpOptions);
  }


  validatecouponcode(params) {
    var usercart = JSON.parse(localStorage.getItem('userShopForMeCart'));
    if(JSON.stringify(usercart) == 'null'){
      //console.log("inner");
      var usercart = JSON.parse(localStorage.getItem('cartData'));
    }
    //console.log(JSON.stringify(usercart));
    const userdetails = JSON.parse(localStorage.getItem('tokens'));
    const couponcode = {coupon:params, usercart:usercart, userId: this.userId};
      return this.http.post(this.rootUrl+'validateCouponCode/',couponcode,this.httpOptions)
    }

  submitOrder(formData) {
     return this.http.post(this.rootUrl+'submitorder/'+this.userId,formData,this.httpOptions);
  }


  placeOrder(cartData)
  {
    return this.http.post(this.rootUrl+'placeorder/'+this.userId,cartData,this.httpOptions);
  }

  validateEwallet(formData) {
    return this.http.post(this.rootUrl+'validateewallet/',formData,this.httpOptions);
  }

  getprocurementshippingcost(data) {
     const formData = {procurementId : data, userId : this.userId};
     return this.http.post(this.rootUrl+'getprocurementshippingcost/',formData,this.httpOptions);
  }

  submitrequestforcostdata(formData) {
    return this.http.post(this.rootUrl+'submitrequestforcostdata/'+ this.userId,formData,this.httpOptions);
  }

  getInvoice(data){
     const formData = {procurementId : data, userId : this.userId};
     return this.http.post(this.rootUrl+'getinvoice/',formData,this.httpOptions);
  }

  getPaymentInvoice(paidForId, paidFor,invoiceId){
     const formData = {paidForId : paidForId, paidFor : paidFor, invoiceId : invoiceId, userId : this.userId};
     return this.http.post(this.rootUrl+'getpaymentinvoice/',formData,this.httpOptions);
  }


  getAutopartsInvoice(data)
  {
     const formData = {procurementId : data, userId : this.userId};
     return this.http.post(this.rootUrl+'getautopartsinvoice/',formData,this.httpOptions);
  }

  updateUserCartAutoPartsData(data) {
    const formData = {userId : this.userId, fromCurrency: data.fromCurrency, toCurrency: data.toCurrency, usercart: data.usercart};
    return this.http.post(this.rootUrl+'updateusercartautopartsdata/',formData,this.httpOptions)
  }


  submitrequestforcostautopartsdata(formData){
    return this.http.post(this.rootUrl+'submitrequestforcostautopartsdata/'+ this.userId,formData,this.httpOptions);
  }

  getprocurementdetail(data) {
     const formData = {procurementId : data, userId : this.userId};
     return this.http.post(this.rootUrl+'getprocurementItems/',formData,this.httpOptions);
  }
   getautopartsdetail(data) {
     const formData = {procurementId : data, userId : this.userId};
     return this.http.post(this.rootUrl+'getautopartsItems/',formData,this.httpOptions);
  }

  getShipmentImagesList()
  {
     const formData = {userId : this.userId};
     return this.http.post(this.rootUrl+'getgalleryImages/',formData,this.httpOptions);
  }

  getShipmentImages(itemId, shipmentId)
  {
    const formData = {item:itemId, shipment: shipmentId, userId : this.userId};
     return this.http.post(this.rootUrl+'getgalleryImages/',formData,this.httpOptions);
  }
}
