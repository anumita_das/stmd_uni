import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpErrorResponse } from '@angular/common/http';
import { Response } from '@angular/http';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { config } from '../app.config';
import { retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ContentService {

	private rootUrl = config.api;
	private httpOptions: any;
  userId: any = '';
  item:any = "";
  isLoggedIn: boolean = false;

  	constructor(private http: HttpClient) { 
  		this.httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json'})
        };
        this.item = JSON.parse(localStorage.getItem('tokens'));
        if (this.item != null) {
          if (this.item.token) {
            this.isLoggedIn = true;
          }
          this.userId = this.item.user.id;
        }
  	}

  	bannerContent() {

    	return this.http.get(this.rootUrl+'banner/1',this.httpOptions);

    }

    homepageBlockContent() {

      return this.http.get(this.rootUrl+'homepageblock',this.httpOptions);

    }

    specialOfferBannerContent() {

     return this.http.get(this.rootUrl+'banner/8',this.httpOptions);

    }

    storeIcons() {

      return this.http.get(this.rootUrl+'storedata',this.httpOptions);
    }

    registrationBanner() {

      return this.http.get(this.rootUrl+'registrationbanner',this.httpOptions);
    }

    getquoteBanner() {

      return this.http.get(this.rootUrl+'getquotebanner',this.httpOptions);
    }

    fraudBanner(){
      return this.http.get(this.rootUrl+'fraudbanner',this.httpOptions);
    }

    registrationBannerById(id) {

      return this.http.get(this.rootUrl+'registrationbannerById/'+id,this.httpOptions);
    }

    allWarehouse() {

      return this.http.get(this.rootUrl+'getallwarehouse',this.httpOptions);
    }

    getUserWarehose(userId) {
      const postData = {userId:userId};
      return this.http.post(this.rootUrl+'getUserWarehose',postData,this.httpOptions);
    }

    allwarehouseshopfrom()
    {
      return this.http.get(this.rootUrl+'getallwarehouseshopfrom',this.httpOptions);
    }

    allCountry() {

      return this.http.get(this.rootUrl+'getcountry',this.httpOptions);
    }

    allOthersCountry()
    {
      return this.http.get(this.rootUrl+'getothercountry',this.httpOptions);
    }

    allCode()
    {
      return this.http.get(this.rootUrl+'getisdcode',this.httpOptions);
    }


    stateCityOfCountry(country:number) {

      return this.http.get(this.rootUrl+'getstatecity/'+country,this.httpOptions);
    }

    allstateCityOfCountry(country:number) {

      return this.http.get(this.rootUrl+'getallstatecity/'+country,this.httpOptions);
    }

    cityOfStates(state:number) {

      return this.http.get(this.rootUrl+'getcity/'+state,this.httpOptions);
    }

   pageContent(data:any) {
      const request = {urlSlug:data};
      const url = this.rootUrl+'content';
      return this.http.post(url, request, this.httpOptions).pipe(catchError(this.handleError));
    }

    handleError(error) {
     let errorMessage = '';
     if (error instanceof HttpErrorResponse && error.status == 404) {
        errorMessage = `${error.status}`;

     } else {
      errorMessage = `Error: ${error.error.message}`;

     }

     //window.alert(errorMessage);

     return throwError(errorMessage);

   }

    

    saveContactUs(formData){
      return this.http.post(this.rootUrl+'savecontactus',formData,this.httpOptions); 
    }
    saveSendMessage(formData){
      return this.http.post(this.rootUrl+'savesendmessage',formData,this.httpOptions);
    }

    warehouselist(data:any) {
      return this.http.get(this.rootUrl+'getuserwarehouselist/'+data,this.httpOptions); 
    }
    
    warehouselistshopfrom(data:any){
      return this.http.get(this.rootUrl+'getshopfromwarehouselist/'+data,this.httpOptions); 
    }

    storelist(formData) {
      return this.http.post(this.rootUrl+'getstore/',formData,this.httpOptions);
    }

    storeByCategory(formData)
    {
      return this.http.post(this.rootUrl+'getstoreByCatergory', formData,this.httpOptions);
    }

    categorylist(formData) {
      return this.http.post(this.rootUrl+'getcategory/',formData,this.httpOptions);
    }
    categorylistwithoutstore(type)
    {
      const data = {
          type : type,
          storeId : ''
      };
       return this.http.post(this.rootUrl+'getcategory/',data,this.httpOptions);

    }


    subcategorylist(data:any) {
     return this.http.get(this.rootUrl+'getsubcategory/'+data,this.httpOptions);
    }

    productlist(data:any) {
       return this.http.get(this.rootUrl+'getproduct/'+data,this.httpOptions);
    }

    makelist() {
       return this.http.get(this.rootUrl+'getmakelist',this.httpOptions);
    }

    modellist(data:any) {
       return this.http.get(this.rootUrl+'getmodellist/'+data,this.httpOptions);
    }

    getFaq(){
      return this.http.get(this.rootUrl+'faq',this.httpOptions); 
    }

    contactInfo(){
      return this.http.get(this.rootUrl+'getcontactinfo');
    }

    getCurrencyList() {
      return this.http.get(this.rootUrl+'getcurrencylist');
    }

    getDefaultCurrency() {
      return this.http.get(this.rootUrl+'getdefaultcurrency');
    }

    payAuthorizeDotNet(data:any): Observable<any> {
      const request = {formvalues:data};
      const url = this.rootUrl+'payauthorizedotnet';
      return this.http.post(url, request, this.httpOptions);
    }

    payPayPalDoDirect(data:any): Observable<any> {
      const request = {formvalues:data};
      const url = this.rootUrl+'paypaypalpro';
      return this.http.post(url, request);
    }

    getPaypalOrAuthorizeDotNet(): Observable<any> {
      const url = this.rootUrl+'getPayMethod';
      return this.http.post(url, this.httpOptions);
    }

    getAmountForPaystack(payAmount,defaultCurrency) {
      const data = {
          payAmount : payAmount,
          defaultCurrency : defaultCurrency
      };

      return this.http.post(this.rootUrl+'getamountforpaystack',data);
    }

    getAmountForPayeezy(payAmount,defaultCurrency) {
      const data = {
          payAmount : payAmount,
          defaultCurrency : defaultCurrency
      };

      return this.http.post(this.rootUrl+'getamountforpayeezy',data);
    }

    getAmountForPaypalStandard(payAmount,defaultCurrency) {
      const data = {
          payAmount : payAmount,
          defaultCurrency : defaultCurrency
      };

      return this.http.post(this.rootUrl+'getamountforpaypalstandard',data);
    }



    getPartnerContent() {
      return this.http.get(this.rootUrl+'getpartnercontent',this.httpOptions);
    }
    
    getPartnersBanking() {
       return this.http.get(this.rootUrl+'getpartnerbanking',this.httpOptions);
    }

    getPartnersLogistic() {
       return this.http.get(this.rootUrl+'getpartnerlogistic',this.httpOptions);
    }

    getRewardDetails(id) {
       const data = {userId : id};
       return this.http.post(this.rootUrl+'getrewarddetails', data, this.httpOptions);
    }

    getRewardTransaction(activity, id, currentPage, perPage) {
       const data = {activity: activity, userId: id, perPage:perPage, currentPage:currentPage};
       return this.http.post(this.rootUrl+'getpointtransction', data, this.httpOptions);
    }

    getWalletCode(inputPassword, id) {
       const data = {inputPassword: inputPassword, userId: id};
       return this.http.post(this.rootUrl+'getwalletcode', data, this.httpOptions);
    }

    itemArrivedDetails(trackingNumber, itemarrivalDate, rangeDate) {
        const data = {
          trackingNumber : trackingNumber,
          itemarrivalDate: itemarrivalDate,
          rangeDate : rangeDate
        }

        return this.http.post(this.rootUrl+'itemarriveddetails',data);
    }

    shipmentTrackingDetails(orderNumber) {
      const data = {
          orderNumber :orderNumber
      }

      return this.http.post(this.rootUrl+'shipmenttrackingdetails',data);
    }

    getPaystackSettings() {
       return this.http.post(this.rootUrl+'getpaystacksettings', this.httpOptions);

    }

    getDocuments(id) {
      const data = {userId: id};
      return this.http.post(this.rootUrl+'getdocuments', data, this.httpOptions);
    }

    deleteDocument(userId, docId) {
      const data = {userId: userId, docId: docId};
      return this.http.post(this.rootUrl+'deletedocument', data, this.httpOptions);
    }

    uploadDocument(formData, userId) {
      //console.log(formData);
      //const data = {formData: formData, userId: userId};
      //return this.http.post(this.rootUrl+'uploaddocument', data, this.httpOptions);
      return this.http.post(this.rootUrl+'uploaddocument/'+ userId + '/', formData)
    }

    postclaim(formData, userId) {
      return this.http.post(this.rootUrl+'postclaim/'+ userId + '/', formData);
    }

    getMessages(activity, msgtype, id, currentPage, perPage) {
       const data = {activity: activity, msgtype: msgtype, userId: id, perPage:perPage, currentPage:currentPage};
       return this.http.post(this.rootUrl+'getmessages', data, this.httpOptions);
    }

    getClaims(useEmail, currentPage, perPage){
      const data = {useEmail: useEmail, perPage:perPage, currentPage:currentPage};
       return this.http.post(this.rootUrl+'getclaims', data, this.httpOptions);
    }

    getMessageContent(id) {
       var msgId = localStorage.getItem('msgId');
       const data = {msgId:msgId, userId: id};
       return this.http.post(this.rootUrl+'getmessagecontent', data, this.httpOptions);
    }

    submitPassword(inputEmail) {
       const data = {inputEmail:inputEmail, serverUrl:""};
       return this.http.post(this.rootUrl+'userforgotPassword', data, this.httpOptions);
    }

    submitTokenPassword(password, confirmPassword, token) {
       const data = {password:password, confirmPassword:confirmPassword, token:token};
       return this.http.post(this.rootUrl+'userforgotPasswordToken', data, this.httpOptions);
    }

    getLocationCountry() {
      return this.http.get(this.rootUrl+'getlocationcountry',this.httpOptions);
    }

    getLocationState(country:number) {
      return this.http.get(this.rootUrl+'getlocationstate/'+country,this.httpOptions);
    }

    getLocationCity(state:number) {
      return this.http.get(this.rootUrl+'getlocationcity/'+state,this.httpOptions);
    }

    getHeaderNavigation() {
       return this.http.get(this.rootUrl+'getheadernavigation', this.httpOptions);
    }

    getFooterNavigation() {
       return this.http.get(this.rootUrl+'getfooternavigation', this.httpOptions);
    }

    getLocationDetails(formValue) {
      const data = {
          countryId : formValue.country,
          stateId : formValue.state,
          cityId : formValue.city
      };

      return this.http.post(this.rootUrl+'getlocationdetails',data,this.httpOptions);

    }

    getpagetype(data:any) {
      const request = {urlSlug:data};
      const url = this.rootUrl+'getpagetype';
      return this.http.post(url, request, this.httpOptions);
    }

    getFooterSocialLinks() {
      return this.http.get(this.rootUrl+'getsociallinks');
    }

    getfirsthelpsection(href, cat){
      const data = {href:href, cat:cat};
      return this.http.post(this.rootUrl+'firsthelpsection',data,this.httpOptions);
    }

    getAdBanner() {
        return this.http.get(this.rootUrl+'gethomepageadbanner',this.httpOptions);
    }

    getAllContactUsReasons()
    {
      return this.http.post(this.rootUrl+'getcontactusreasons',this.httpOptions);
    }

    // Get the config of paypal ipn
    getpaypalipnsettings(currencyCode, total)
    {
      const data = {currencyCode:currencyCode, total:total};
      return this.http.post(this.rootUrl+'getpaypalipnsettings',data,this.httpOptions);
    }

    savePaystackReference(paystackReference,paymentData,paymentUrl) {
      const data = {
        paystackReference : paystackReference,
        paymentData : paymentData,
        paymentUrl : paymentUrl,
        userId : this.userId,
      };

      return this.http.post(this.rootUrl+'savepaystackreference',data);
    }

    verifyitemarrivedtime(formData) {
      return this.http.post(this.rootUrl+'verifyitemarrivedtime',formData);
    } 

    verifyTrackingDetails(formData) {
      return this.http.post(this.rootUrl+'verifytrackingdetails',formData);
    }

    saveContact(formData)
    {
      return this.http.post(this.rootUrl+'saveContact', formData);
    }


    getPaymentMethodList(formData) {
        return this.http.post(this.rootUrl+'getpaymentmethodlist', formData);
    }

   saverating(formData, shipmentId, deliveryId, ratingval)
    {
       const data = {
          formData : formData,
          shipmentId : shipmentId,
          ratingval : ratingval,
          deliveryId: deliveryId
      };
      return this.http.post(this.rootUrl+'saverating',data, this.httpOptions);
    }

    rateourservice(shipmentId, deliveryId)
    {
      const data = {
          shipmentId : shipmentId,
          deliveryId: deliveryId
      };

      return this.http.post(this.rootUrl+'rateourservice',data, this.httpOptions);
    }

    delaymailtoadmin(shipmentId, deliveryId)
    {
      const data = {
          shipmentId : shipmentId,
          deliveryId: deliveryId
      };

      return this.http.post(this.rootUrl+'delaymailtoadmin',data, this.httpOptions);
    }

}
