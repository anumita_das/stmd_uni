import { TestBed, inject } from '@angular/core/testing';

import { DirectAccessGuardService } from './directaccess-guard.service';

describe('DirectAccessGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DirectAccessGuardService]
    });
  });

  it('should be created', inject([DirectAccessGuardService], (service: DirectAccessGuardService) => {
    expect(service).toBeTruthy();
  }));
});
