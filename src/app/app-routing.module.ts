import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { DirectAccessGuardService } from './services/directaccess-guard.service';

const routes: Routes = [
	{ path: 'login', loadChildren: './features/login/login.module#LoginModule' },
	{ path: 'forgot-password/:token', loadChildren: './features/forgot-password/forgot-password.module#ForgotPasswordModule' },
	{ path: 'join', loadChildren: './features/join/join.module#JoinModule' },
	{ path: 'join/:id', loadChildren: './features/join/join.module#JoinModule' },
	{ path: 'join/:id/:token', loadChildren: './features/join/join.module#JoinModule' },
	{ path: 'join2', loadChildren: './features/join2/join2.module#Join2Module' },
	{ path: 'contact-us', loadChildren: './features/contact-us/contact-us.module#ContactUsModule' },
	{ path: 'send-us-message', loadChildren: './features/send-us-message/send-us-message.module#SendUsMessageModule' },
	// { path: 'help', loadChildren: './features/help/help.module#HelpModule' },
	{ path: 'help', loadChildren: './features/help/help.module#HelpModule' },
	{ path: 'help/:category/:slug', loadChildren: './features/help/help.module#HelpModule' },
	{ path: 'my-account', canActivateChild: [AuthGuardService], loadChildren: './features/my-account/my-account.module#MyAccountModule' },
	{ path: 'addressbook', canActivateChild: [AuthGuardService], loadChildren: './features/addressbook/addressbook.module#AddressbookModule' },
	{ path: 'modify-account', canActivateChild: [AuthGuardService], loadChildren: './features/modify-account/modify-account.module#ModifyAccountModule' },
	{ path: 'shipment-setting', canActivateChild: [AuthGuardService], loadChildren: './features/shipment-setting/shipment-setting.module#ShipmentSettingModule' },
	{ path: 'my-warehouse', canActivateChild: [AuthGuardService], loadChildren: './features/my-warehouse/my-warehouse.module#MyWarehouseModule' },
	{ path: 'my-group', canActivateChild: [AuthGuardService], loadChildren: './features/my-group/my-group.module#MyGroupModule' },
	{ path: 'store-shop-from', canActivateChild: [AuthGuardService], loadChildren: './features/store-shop-from/store-shop-from.module#StoreShopFromModule' },
	{ path: 'fill-and-ship', loadChildren: './features/fill-ship/fs.module#FSModule' },
	{ path: 'refer-a-friend', canActivateChild: [AuthGuardService], loadChildren: './features/refer-a-friend/refer-a-friend.module#ReferAFriendModule' },
	{ path: 'auto', canActivateChild: [AuthGuardService], loadChildren: './features/auto/auto.module#AutoModule' },
	{ path: 'auto-parts', canActivateChild: [AuthGuardService], loadChildren: './features/auto-parts/autoparts.module#AutoPartsModule' },
	{ path: 'shop-for-me', canActivateChild: [AuthGuardService], loadChildren: './features/shop-for-me/sfm.module#SFMModule' },
	{ path: 'auto', canActivateChild: [AuthGuardService], loadChildren: './features/auto/auto.module#AutoModule' },
	{ path: 'payment', canActivateChild: [AuthGuardService], loadChildren: './features/payment/payment.module#PaymentModule' },
	{ path: 'faq', loadChildren: './features/faq/faq.module#FAQModule' },
	{ path: 'track-shipment/:ordernumber', loadChildren: './features/track-shipment/track-shipment.module#TrackShipmentModule' },
	{ path: 'shipment', loadChildren: './features/shipment/shipment.module#ShipmentModule' },
	{ path: 'rating/:shipmentId/:deliveryId/:ratingval', loadChildren: './features/rating/rating.module#RatingModule' },
	{ path: 'thank-you/:flag', loadChildren: './features/thank-you/thank-you.module#ThankYouModule' },
	{ path: 'success', canActivate: [DirectAccessGuardService], loadChildren: './features/success/success.module#SuccessModule' },
	{ path: 'confirm/:shipmentId/:deliveryId/:flag', loadChildren: './features/confirm/confirm.module#ConfirmModule' },
	{ path: 'item-arrived/:tracking', loadChildren: './features/item-arrived/item-arrived.module#ItemArrivedModule' },
	{ path: 'shop-from', loadChildren: './features/shop-from/shop-from.module#ShopFromModule' },
	{ path: 'shop-from-web', loadChildren: './features/shop-from-web/shop-from-web.module#ShopFromWebModule' },
	{ path: 'subscribe', loadChildren: './features/subscribe/subscribe.module#SubscribeModule' },
	{ path: 'getquote', loadChildren: './features/getquote/getquote.module#GetquoteModule'},
	{ path: ':page', loadChildren: './features/content/content.module#ContentModule' },
	{ path: '', loadChildren: './features/home/home.module#HomeModule', pathMatch: 'full' },
	{ path: '**', loadChildren: './features/page-not-found/page-not-found.module#PageNotFoundModule', pathMatch: 'full' },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
	providers: [
		{ provide: LocationStrategy, useClass: PathLocationStrategy }
	]

})
export class AppRoutingModule { }
