import { Component, HostListener, OnInit } from '@angular/core';
import { WindowWidthService } from '@app/shared/services/windowWidth.service';
import { ContentService } from '@app/services/content.service';
import { AppGlobals } from './app.globals';
import { Meta, Title, BrowserModule } from '@angular/platform-browser';
import { NgxJsonLdModule } from '@ngx-lite/json-ld';
import { MetaService } from './meta.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	providers: [AppGlobals]
})
export class AppComponent {
	title = 'app';

	constructor(
		private windowWidthService: WindowWidthService,
		private content: ContentService,
		private _global: AppGlobals,
		private meta: Meta, 
		private pagetitle: Title,
		private metaService: MetaService
	) {
		pagetitle.setTitle(this._global.siteTitle);
	    meta.addTags([ 
	      {
	        name: 'keywords', content: this._global.siteMetaKeywords
	      },
	      {
	        name: 'description', content: this._global.siteMetaDescription
	      },
	    ]);
	}

	


	ngOnInit() {
		this.metaService.createCanonicalURL();
		this.content.getDefaultCurrency().subscribe((response:any) => {
			if(response.defaultcurrency != '')
				this._global.defaultCurrencySymbol = response.defaultcurrency;
		});
		this._global.virtualTourEntered = '0';
	}


}
