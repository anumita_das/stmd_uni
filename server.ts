import 'zone.js/dist/zone-node';
import 'reflect-metadata';
//import {enableProdMode} from '@angular/core';
// Express Engine
//import {ngExpressEngine} from '@nguniversal/express-engine';
// Import module map for lazy loading
//import {provideModuleMap} from '@nguniversal/module-map-ngfactory-loader';
import { renderModuleFactory } from '@angular/platform-server';

import 'localstorage-polyfill';

import * as express from 'express';
import {join} from 'path';
import { readFileSync } from 'fs';

const domino = require('domino');
const fs = require('fs');
const path = require('path');

// Faster server renders w/ Prod mode (dev mode never needed)
//enableProdMode();

// Express server
const app = express();

const PORT = process.env.PORT || 5000;
const DIST_FOLDER = join(process.cwd(), 'dist/browser');

// Our index.html we'll use as our template
const template = readFileSync(join(DIST_FOLDER, 'index.html')).toString();
const win = domino.createWindow(template);

global['window'] = win;
global['Node'] = win.Node;
global['navigator'] = win.navigator;
global['Event'] = win.Event;
global['Event']['prototype'] = win.Event.prototype;
global['document'] = win.document;
global['localStorage'] = localStorage;

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const {AppServerModuleNgFactory, LAZY_MODULE_MAP, ngExpressEngine, provideModuleMap} = require('./dist/server/main');

// Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
app.engine('html', ngExpressEngine({
  bootstrap: AppServerModuleNgFactory,
  providers: [
    provideModuleMap(LAZY_MODULE_MAP)
  ]
}));


app.set('view engine', 'html');
app.set('views', DIST_FOLDER);

// Example Express Rest API endpoints
// app.get('/api/**', (req, res) => { });
// Server static files from /browser
app.get('*.*', express.static(DIST_FOLDER, {
  maxAge: '1y'
}));

// All regular routes use the Universal engine
/*app.get('*', (req, res) => {
  res.render('index', { req });
});*/

app.get('*', (req, res) => {
  res.render('index', {	
    req,
    res,
  }, (err: Error, html: string) => {
    res.status(html && html.includes('404_error') ? 404 : html ? 200 : 500).send(html || err.message);
  });
});

// Start up the Node server
app.listen(PORT, () => {
  console.log(`Node Express server listening on http://localhost:${PORT}`);
});
